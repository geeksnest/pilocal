<?php

error_reporting(E_ALL);

try {

	/**
	 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
	 */
	$di = new \Phalcon\DI\FactoryDefault();

	/**
	 * Registering a router
	 */
	$di['router'] = function() {

		$router = new \Phalcon\Mvc\Router(false);

//		$router->add('/:controller/:action/:params', array(
//			'module' => 'frontend',
//			'controller' => 1,
//			'action' => 2,
//			'params' => 3,
//		));

//		$router->add('/template/page/:action', array(
//			'module' => 'frontend',
//			'controller' => ,
//			'action' => 2,
//			'params' => 3,
//		));
//		$router->add('/:controller', array(
//			'module' => 'frontend',
//			'controller' => 1
//		));
//		$router->add('/', array(
//			'module' => 'frontend',
//			'controller' => 'index',
//			'action' => 'index'
//		));

		$router->add('/page/:action/:params', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 1,
			'params' => 2
		));

		$router->add('/action-map/{slugs}', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'sharepage'
		));

		$router->setDefaults(
			array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'index'
			)
		);


		$router->add('/superagent', array(
			'module' => 'backend',
			'controller' => 'index',
			'action' => 'index'
		));

		$router->add('/superagent/:controller', array(
			'module'=> 'backend',
			'controller' => 1
		));

		$router->add('/superagent/:controller/:action/:params', array(
			'module'=> 'backend',
			'controller' => 1,
			'action' => 2,
			'params' => 3,
		));

		$router->removeExtraSlashes(true);

		return $router;
	};

	/**
	 * The URL component is used to generate all kind of urls in the application
	 */
	$di->set('url', function() {
		$url = new \Phalcon\Mvc\Url();
		$url->setBaseUri('/');
		return $url;
	});

	/**
	 * Start the session the first time some component request the session service
	 */
	$di->set('session', function() {
		$session = new \Phalcon\Session\Adapter\Files();
		$session->start();
		return $session;
	});

	/**
	 * If the configuration specify the use of metadata adapter use it or use memory otherwise
	 */
	$di->set('modelsMetadata', function () {
	    return new MetaDataAdapter();
	});

    /*
    ModelsManager
    */
	$di->set('modelsManager', function() {
	      return new Phalcon\Mvc\Model\Manager();
	});
    $config = include "../app/config/config.php";
    // Store it in the Di container
    $di->set('config', function () use ($config) {
        return $config;
    });
	/**
	 * Handle the request
	 */
	$application = new \Phalcon\Mvc\Application();

	$application->setDI($di);

	/**
	 * Register application modules
	 */
	$application->registerModules(array(
		'frontend' => array(
			'className' => 'Modules\Frontend\Module',
			'path' => '../app/frontend/Module.php'
		),
		'backend' => array(
			'className' => 'Modules\Backend\Module',
			'path' => '../app/backend/Module.php'
		)
	));

	echo $application->handle()->getContent();

} catch (Phalcon\Exception $e) {
	echo $e->getMessage();
} catch (PDOException $e){
	echo $e->getMessage();
}
