/* JS */


$(document).ready(function(){
  $(".totop").hide();
    if($('#country').length){
        print_country("country");
    }
  $(function(){
    $(window).scroll(function(){
      if ($(this).scrollTop()>400)
      {
        $('.totop').fadeIn();
      } 
      else
      {
        $('.totop').fadeOut();
      }
    });

    $('.totop a').click(function (e) {
      e.preventDefault();
      $('body,html').animate({scrollTop: 0}, 500);
    });

  });
});

