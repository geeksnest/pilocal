'use strict';

/* Controllers */

app.controller('searchCtrl', function($scope, appConfig, $state, $http, mapQuery){

    $scope.showtags = true;
    $scope.tags = []
    $scope.amazonurl = appConfig.bucket;
    var offset = 1,
        key = null,
        cat = null,
        tag = null;

    mapQuery.getTags(function(data) {
        angular.forEach(data, function(value, key) {
            if(key <= 50){
                $scope.tags.push(data[key]);
            }
        });
    });

    var getmaps = function(off, keyword, category, tag) {
        mapQuery.searchMap(off, keyword, category, tag, function(data) {
            $scope.maps = data.maps;
            $scope.maxSize = 9;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = off;
        });
    }

    getmaps(offset, key, cat, tag);

    $scope.search = function(keyword) {
        $scope.keyword = null;
        if(keyword!="" && keyword!=null){
            $scope.showtags = false;
            $scope.msg = "Search result/s for: " + keyword;
            getmaps(1, keyword, cat, tag);
        }else {
            $scope.showtags = true;
            getmaps(1, null, null, null);
        }
    }

    $scope.selecttags = function(tags) {
        $scope.showtags = false;
        $scope.msg = "Result/s for tag: " + tags;
        getmaps(1, null, cat, tags);
    }

    $scope.setPage = function(off) {
        getmaps(off, key, cat, tag);
    }
});