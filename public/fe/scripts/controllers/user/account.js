'use strict';

/* Controllers */

app.controller('accountCtrl', function($scope, appConfig, $state, $http, User, Login, toaster, $modal){

    $scope.showeditusrname = false;
    var uname, aemail;
    User.getInfo(Login.getCurrentUser().id, function(data) {
        $scope.agent = data;
        uname = data.username;
        aemail = data.email;
    });

    var editAcctCtrl = function($scope, $modalInstance, input, type) {
        if(type=='username'){
            $scope.msg = "Are you sure you want to change your username?";
        }else {
            $scope.msg = "Are you sure you want to change your email address?";
        }
        $scope.disable = true;
        $scope.close = function() {
            $modalInstance.dismiss();
        }

        $scope.auth = function(pass) {
            User.chckpassword(Login.getCurrentUser().id, {password:pass}, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    $scope.disable = false;
                    console.log(data.success);
                }else {
                    $scope.disable = true;
                }
            });
        }

        $scope.update = function() {
            User.changeusername(Login.getCurrentUser().id, {input : input, type:type }, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    if(type == 'username'){
                        uname = input;
                    }else {
                        aemail = input;
                    }
                    toaster.pop('success', "", data.success);
                }else {
                    toaster.pop('danger', "", data.error);
                }
                $modalInstance.dismiss();
            });
        }
    }

    $scope.update = function(data, info) {
        if((data != uname && info=='username') || (data != aemail && info=='email')){
            var modalInstance = $modal.open({
                templateUrl: 'editAcct.html',
                controller: editAcctCtrl,
                resolve : {
                    input: function() {
                        return data;
                    },
                    type: function() {
                        return info;
                    }
                }
            }).result.finally(function() {
                $scope.agent.username = uname;
                $scope.agent.email = aemail;
            });
        }
    }

    var changePassCtrl = function($scope, $modalInstance) {
        $scope.disable = true;
        $scope.disableinput= true;

        $scope.pstrength = function(){
            var passstrength = $('.strength_meter').find('div').attr("class");
            console.log(passstrength);
            if(passstrength == "weak" || passstrength == "veryweak"){
                $scope.disable = true;
            }else {
                $scope.disable = false;
            }
        }

        $scope.close = function() {
            $modalInstance.dismiss();
        }

        $scope.auth = function(pass) {
            User.chckpassword(Login.getCurrentUser().id, {password:pass}, function(data) {
                toaster.clear()
                if(data.hasOwnProperty('success')){
                    $scope.disableinput= false;
                    console.log(data.success);
                }else {
                    toaster.pop('error', "", data.error );
                    $scope.disableinput= true;
                    $scope.disable = true;
                }
            });
        }

        $scope.changePass = function(pass) {
            User.changePass(Login.getCurrentUser().id, {password:pass}, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    $modalInstance.dismiss();
                    toaster.pop('success', "", data.success );
                }else {
                    toaster.pop('error', "", data.error );
                }
            });
        }
    }

    $scope.changepass = function() {
        var modalInstance = $modal.open({
            templateUrl: 'changePass.html',
            controller: changePassCtrl
        });
    }

});
