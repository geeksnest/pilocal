app.controller('MissionMgrCtrl', function($scope, toaster, Login, mapQuery, Maps, appConfig, Comments, Upload, $modal, $window, $sce){
	var title = null;
    $scope.comments = [];
    $scope.reply = [];
    $scope.commentload = 4;
    $scope.comment = '';
    var comments = [];
    var addpin = false;
    $scope.options = {
        styles: appConfig.googleMapStyle
    };

	if(Login.getCurrentUser() != null){
		$scope.agentid = Login.getCurrentUser().id;
        $scope.agentprofpic = Login.getCurrentUser().profile_pic_name != null ? appConfig.bucket + "uploads/agentpic/" + Login.getCurrentUser().id + "/" + Login.getCurrentUser().profile_pic_name : appConfig.BaseUrl + "/img/agentdefault.png";
    }else {
        $window.location = '/';
	}
    $scope.approvingpins = function(val) {
        var approve = (val == true ? 1 : 0);
        console.log(approve);
        mapQuery.approvepins($scope.mapdetails.id, approve, function(data) {
            getmap($scope.mapdetails.mapslugs, null);
        });
    }
    var viewPinCtrl = function($scope, $modalInstance, id, mapQuery, appConfig, $sce, $timeout, $animate, Login, $state, toaster, Notification) {
        $scope.slides = [];
        $scope.agentpic = (Login.getCurrentUser() != null && Login.getCurrentUser().profile_pic_name != null ? appConfig.bucket + "uploads/agentpic/" + Login.getCurrentUser().id + "/" + Login.getCurrentUser().profile_pic_name : appConfig.BaseUrl + "/img/agentdefault.png");
        $scope.showdelete = false;
        $scope.showsetkingpin = false;
        $scope.showeditpin = false;
        $scope.agentid = (Login.getCurrentUser() == null ? null : Login.getCurrentUser().id);
        $scope.showmedia = false;
        $scope.spinner = false;
        $scope.currentPinImages = [];
        $scope.showedit = false;
        $scope.commentload = 0;
        var temp = '';
        $scope.showdeleteComment = false;
        $scope.showeditComment = false;
        $scope.deleting = false;
        var loadcount = 0;
        $scope.comments = [];
        $scope.hidedelete = false;

        var viddisp = function(video) {
            if (video != undefined && video != '' && video != null) {
                var queryString = video.substring(video.indexOf('?') + 1);
                var param = parseQueryString(queryString);
                if (param.hasOwnProperty('v')) {
                    $scope.slides.push({ videodisplay: $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" width="100%" height="400px" allowfullscreen></iframe>'), type: "video", image: "http://img.youtube.com/vi/" + param.v + "/hqdefault.jpg", video: video});
                } else {
                    $scope.videodisplay = '';
                    toaster.clear();
                    toaster.pop('error', "", 'Invalid youtube link.');
                }
            } else {
                $scope.videodisplay = '';
            }
        }

        var filterslides = function(markerimages, id, map_id){
            mapQuery.slides(markerimages, id, map_id, function(data) {
                viddisp($scope.pin.video);
                angular.forEach(data, function(value, key) {
                    $scope.slides[$scope.slides.length] = data[key];
                })
            });

            $timeout(function () {
                $('#imageGallery').lightSlider({
                    gallery:true,
                    item:1,
                    loop:true,
                    thumbItem:9,
                    slideMargin:0,
                    enableDrag: false,
                    currentPagerPosition:'left'
                });
            }, 500);
        }

        $scope.loadcomment = function(){
            $scope.commentload += 2;
            angular.forEach(pincomments, function(value, key) {
                if(key <= $scope.commentload && key >= loadcount){
                    loadcount++;
                    $scope.comments.push(value);
                    $scope.comments[key].profile_pic_name = (value['profile_pic_name'] != null ? appConfig.bucket + "uploads/agentpic/" + value['agent'] + "/" + value['profile_pic_name'] : appConfig.BaseUrl + "/img/agentdefault.png");
                    $scope.comments[key].first_name = $scope.comments[key].first_name != null ? $scope.comments[key].first_name :  "Superagent";
                    $scope.comments[key].last_name = $scope.comments[key].last_name != null ? $scope.comments[key].last_name :  "";
                    console.log("COMMENTS");
                    console.log($scope.comments[key]);
                    $scope.comments[key].reply = $sce.trustAsHtml($scope.comments[key].reply);
                }
            });
        }

        var getpin = function() {
            mapQuery.getpin(id, function(data) {
                $scope.commentcount = data.comments.length;
                pincomments = data.comments;
                $scope.pendingaction = data.pin.status == 1 || data.map.agent != Login.getCurrentUser().id ? false : true;
                $scope.pin = data.pin;
                $scope.pin.hide_agent = (data.pin.hide_agent == 1 ? "true" : "false");
                $scope.markerviews = data.pin.views;
                $scope.loadcomment();
                $scope.profpic = (data.pin.profile_pic_name != null && data.pin.hide_agent == 1? appConfig.bucket + "uploads/agentpic/" + data.pin.agent + "/" + data.pin.profile_pic_name : appConfig.BaseUrl + "/img/agentdefault.png");
                filterslides(data.images, id, data.map.id);
                $scope.mapdetails = data.map;
                $scope.mapagent = data.map.agent;
                $scope.agent = data.pin.hide_agent == 1 ? "Agent " + data.agent.first_name + " " + data.agent.last_name : "Anonymous Agent";
            });
        }

        getpin();

        $scope.cancel = function() {
            $modalInstance.dismiss();
        }

        $scope.deletemission = function() {
            $scope.showdelete = true;
        }
        $scope.setkingpin = function() {
            $scope.showsetkingpin = true;
        }
        $scope.editpin = function() {
            $scope.showeditpin = true;
        }
        $scope.deletePin = function(id) {
           mapQuery.deletePin(id, function(data) {
                getmap($scope.mapdetails.mapslugs, null);
                $modalInstance.dismiss();
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                }else {
                    toaster.pop('error', "", data.error);
                }
            });
        }
        $scope.setPin = function(pinid) {
            mapQuery.setKingPin(pinid, $scope.pin.map_id, function(data) {
                getmap($scope.mapdetails.mapslugs, null);
                $modalInstance.dismiss();
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                }else {
                    toaster.pop('error', "", data.error);
                }
            });
        }

        $scope.showalbum = function() {
            $scope.showmedia = true;
        }

        $scope.validatevid = function(video) {
            if (video !== undefined) {
                var queryString = video.substring(video.indexOf('?') + 1);
                var param = parseQueryString(queryString);
                if (param.hasOwnProperty('v')) {
                    $scope.videodisplay = $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" width="100%" height="300px" allowfullscreen></iframe>');
                    $scope.yt = true;
                } else {
                    $scope.videodisplay = '';
                    toaster.clear();
                    toaster.pop('error', "", 'Invalid youtube link.');
                    $scope.yt = false;
                }
            } else {
                $scope.videodisplay = '';
            }
        }

        $scope.updateyt = function(video) {
            mapQuery.updatevid(id, video, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                    var queryString = video.substring(video.indexOf('?') + 1);
                    var param = parseQueryString(queryString);
                    if (param.hasOwnProperty('v')) {
                        $scope.slides[0] = {
                            videodisplay: $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" width="100%" height="400px" allowfullscreen></iframe>'),
                            type: "video",
                            image: "http://img.youtube.com/vi/" + param.v + "/hqdefault.jpg",
                            video: video
                        }
                        getmap($scope.mapdetails.mapslugs, null);
                    }
                }else {
                    toaster.pop('error', "", data.error);
                }
            });
        }

        $scope.prepare = function(files) {
            if (files && files.length) {
                files.map(function(file) {
                    if (file.size >= 2000000) {
                        toaster.clear();
                        toaster.pop('error', "", "File is too big");
                    } else {
                        $scope.spinner = true;
                        Maps.saveMarkerPics(files, $scope.pin.id, $scope.pin.map_id, function(data) {
                            toaster.clear();
                            toaster.pop('success', "", 'Image/s has been successfully uploaded.');
                            mapQuery.getSlides(id, function(data) {
                                $scope.slides= [];
                                filterslides(data, id, $scope.pin.map_id);
                            });
                            $scope.spinner = $scope.showupload = false;
                        });
                    }
                });
                $timeout(function() {
                    $scope.flashError = [];
                }, 10000);
            }
        };
        $scope.removepinpic = function(index) {
            $scope.currentPinImages.splice(index, 1);
        }

        $scope.edit = function() {
            $scope.showedit = true;
            temp = $scope.pin.description;
        }

        $scope.updatedesc = function() {
            mapQuery.updatePinDesc($scope.pin.id, $scope.pin.description, function(data) {
                if(data.hasOwnProperty('success')){
                    toaster.clear();
                    toaster.pop('success', "", data.success);
                }else {
                    toaster.clear();
                    toaster.pop('error', "", data.error);
                }
                $scope.showedit = false;
            });
        }

        $scope.canceledit = function() {
            $scope.showedit = false;
            $scope.pin.description = temp;
        }

        //comment
        $scope.addresponse = function(reply) {
            if(reply!=null && reply!=''){
                Comments.addpinreply(id, Login.getCurrentUser().id, $scope.mapdetails.id, {reply:reply}, function(data) {
                    $scope.commentload +=1;
                    angular.forEach(data.reply, function(value, key) {
                        if(key <= $scope.commentload){
                            $scope.comments[key] = value;
                            $scope.comments[key].profile_pic_name = ($scope.comments[key].profile_pic_name != null ? appConfig.bucket + "uploads/agentpic/" + $scope.comments[key].agent + "/" + $scope.comments[key].profile_pic_name : appConfig.BaseUrl + "/img/agentdefault.png");
                            $scope.comments[key].first_name = $scope.comments[key].first_name != null ? $scope.comments[key].first_name :  "Superagent";
                            $scope.comments[key].last_name = $scope.comments[key].last_name != null ? $scope.comments[key].last_name :  "";
                            $scope.comments[key].reply = $sce.trustAsHtml($scope.comments[key].reply);
                        }
                    });
                    $scope.commentcount = data.reply.length;
                    toaster.clear();
                    if(data.hasOwnProperty('success')){
                        toaster.pop('success', "", data.success);
                        $scope.comment = undefined;
                        $(".input.comment").val(undefined);
                        $scope.comment = '';
                        CKEDITOR.instances['editor1'].setData('');
                        getmap($scope.mapdetails.mapslugs, null);
                    }else {
                        toaster.pop('error', "", data.error);
                    }
                });
            }
        }

        $scope.deleteComment = function(id) {
            $scope.replyid = id;
            $scope.showdeleteComment = true;
        }

        $scope.deletec = function() {
            Comments.deleteres($scope.replyid, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                }else {
                    toaster.pop('error', "", data.error);
                }
                getmap($scope.mapdetails.mapslugs, null);
                $modalInstance.dismiss();
            });
        }

        $scope.editResponse = function(id, reply) {
            $scope.id = id;
            $scope.reply = reply;
            $scope.showeditComment = true;
        }

        $scope.editcomment = function() {
            Comments.editres($scope.id, {reply:$scope.reply}, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                }else {
                    toaster.pop('error', "", data.error);
                }
                getmap($scope.mapdetails.mapslugs, null);
                $modalInstance.dismiss();
            });
        }

        $scope.deleteimg = function(id) {
            $scope.deleting = true;
            mapQuery.deletepinimg(id, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    angular.forEach($scope.slides, function(value, key) {
                        if(value['id'] == id){
                            $scope.slides.splice(key, 1);
                        }
                    });
                    toaster.pop('success', "", data.success);
                    $scope.deleting = false;
                }else {
                    toaster.pop('error', "", data.error);
                }
            });
        }

        $scope.hideagent = function(hide_agent) {
            hide_agent = (hide_agent == "true" ? 1 : 0);
            mapQuery.hideagent(id, hide_agent, function(data) {
                getmap($scope.mapdetails.mapslugs, null);
            });
        }

        $scope.pendingpinAction = function(id, action){
            mapQuery.pendingpinAction(id, action, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                    getmap($scope.mapdetails.mapslugs, null);
                    $modalInstance.dismiss();
                    Notification.count.total = Notification.count.total - 1;
                    Notification.count.pendingpins = Notification.count.pendingpins - 1;
                }else {
                    toaster.pop('error', "", data.error);
                }
            });
        }
    }

    var getcomments = function() {
        Comments.getComments($scope.mapdetails.id, $scope.commentload, function(data){
            $scope.spinner = false;
            $scope.commentcount = data.commentcount;
            $scope.replies = data.reply;
            $scope.replycount = data.replycount;
            comments = data.comments;
            loadcomment();

            angular.forEach($scope.comments, function(value, key) {
                if(value['showreply']){
                    showreplyid = value['id'];
                }
            });

            angular.forEach(comments, function(value, key) {
                $scope.comments[key] = value;
                if(value['id'] == showreplyid){
                    $scope.comments[key]['showreply'] = true;
                }else {
                    $scope.comments[key]['showreply'] = false;
                }
            });
        });
    }

    var commentthumb = function(pinimages, mapId) {
        angular.forEach(pinimages, function(value, key) {
            $scope.PinImages.push({ image: appConfig.bucket + 'uploads/maps/' + mapId + '/' + value['marker_id'] + '/' + value['filename'], marker_id : value['marker_id']});
        });
    }

    var commentvideo = function(markers, mapid, markerimages) {
        angular.forEach(markers, function(key, value) {
            if(value['video'] != '' && value['video'] != null){
                if (video != undefined && video != '' && video != null) {
                    var queryString = video.substring(video.indexOf('?') + 1);
                    var param = parseQueryString(queryString);
                    if (param.hasOwnProperty('v')) {
                        $scope.PinImages.push({ image: "http://img.youtube.com/vi/" + param.v + "/hqdefault.jpg", marker_id: value['id']});
                    }
                }
            }
        });
        commentthumb(markerimages, mapid);
    }

    var loadcomment = function() {
        angular.forEach(comments, function(value, key) {
            if(key <= $scope.commentload){
                $scope.comments[key] = comments[key];
                $scope.comments[key]['profile_pic_name'] = (comments[key].profile_pic_name != null && (comments[key].hide_agent == null ||  comments[key].hide_agent == 1) ? appConfig.bucket + "uploads/agentpic/" + comments[key].agent + "/" + comments[key].profile_pic_name : appConfig.BaseUrl + "/img/agentdefault.png");
                $scope.comments[key].first_name = $scope.comments[key].first_name != null ? "Agent " + $scope.comments[key].first_name :  "Superagent";
                $scope.comments[key].last_name = $scope.comments[key].last_name != null ? $scope.comments[key].last_name :  "";
                $scope.comments[key].comment = $sce.trustAsHtml($scope.comments[key].comment);
                if(key > 0){
                    $scope.comments[key]['showreply'] = false;
                }else {
                    $scope.comments[key]['showreply'] = true;
                }
            }
        });
        angular.forEach($scope.replies, function(value, key) {
            if(key <= $scope.replies.length){
                $scope.replies[key].profile_pic_name = ($scope.replies[key].profile_pic_name != null ? appConfig.bucket + "uploads/agentpic/" + $scope.replies[key].agent + "/" + $scope.replies[key].profile_pic_name : appConfig.BaseUrl + "/img/agentdefault.png");
                $scope.replies[key].reply = $sce.trustAsHtml($scope.replies[key].reply);
            }
        });
    }

    var parseQueryString = function(queryString) {
        var params = {},
            queries, temp, i, l;

        // Split into key/value pairs
        queries = queryString.split("&");

        // Convert the array of strings into an object
        for (i = 0, l = queries.length; i < l; i++) {
            temp = queries[i].split('=');
            params[temp[0]] = temp[1];
        }

        return params;
    };

    var videocover = function(video) {
        if (video != undefined && video != '' && video != null) {
            var queryString = video.substring(video.indexOf('?') + 1);
            var param = parseQueryString(queryString);
            if (param.hasOwnProperty('v')) {
                $scope.mapdetails.cover =  $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" width="100%" height="148px" allowfullscreen></iframe>');
            }
        }
    }

	var getmap = function(title, id) {
        $scope.missionmarkers = [];
        $scope.PinImages = [];
        $scope.mapdetails = [];
		mapQuery.getPins(title, id, function(data) {
            if(data.hasOwnProperty('map')){
                console.log("LOADING MAP INFORMATION!!!");
                console.log(data);
                $scope.default = false;
                $scope.mapnews = data.mapnews;
                Maps.currentMap = data.map.id;
                $scope.replycount = data.replycount;
                commentvideo(data.markers, data.map.id, data.pinimages);
                $scope.agent = data.agent;
                comments = data.comments;
                $scope.replies = data.reply;
                loadcomment();
                $scope.commentcount = data.commentcount;
                $scope.mapdetails = data.map;
                $scope.mapdetails.coverType=='video' ? videocover($scope.mapdetails.cover) : $scope.mapdetails.cover = appConfig.bucket + 'uploads/maps/' + $scope.mapdetails.id + '/' + $scope.mapdetails.cover;
                $scope.mapviews = data.map.views;
                $scope.actioncount = data.actioncount;
                $scope.mapcount = data.mapcount;
                $scope.mapactions = data.markers.length;
                Maps.mapDetails.title = data.map.title;
                Maps.mapDetails.description = data.map.description;
                Maps.mapSlugs = data.map.mapslugs;
                $scope.categories = data.categories;
                $scope.tags = data.tags;
                $scope.hide_agent = ($scope.mapdetails.hide_agent == 1 ? "true" : "false");
                $scope.approvepins = ($scope.mapdetails.approvepins == 1 ? true : false);
    			$scope.map = {
    				center: {
    					latitude: data.markers[0].lat,
    					longitude: data.markers[0].long
    				},
    				zoom: 10,
    				events: {
    					click: function(event, a, b) {
    						if (addpin) {
    							createMarker(b[0].latLng.lat(), b[0].latLng.lng());
    						}
    					}
    				},
                    refresh: false
    			};

                angular.forEach(data.markers, function(value, key){
                    var pin = '';
            		if (value['pin'] == 0 && value['status'] == 1) {
                        pin = Maps.pin;
                    } else if(value['pin'] == 1 && value['status'] == 1){
                        pin = Maps.kingpin;
                    } else if(value['pin'] == 0 && value['status'] == 0){
                        pin = Maps.pendingpin;
                    }
                    console.log('======================= GET MAPS');
                    console.log();
                    $scope.missionmarkers.push({
    	                id: data.markers[key].id,
    	                idKey: data.markers[key].id,
    	                agentid: data.markers[key].agent,
    	                agentname: (data.markers[key].hide_agent==1 ? (data.markers[key].username.length > 9 ? data.markers[key].username.substr(0, 9) + "..." : data.markers[key].username) : "Anonymous"),
    	                agentpic: (data.markers[key].profile_pic_name != null && data.markers[key].hide_agent==1 ? appConfig.bucket + "uploads/agentpic/" + data.markers[key].agent + "/" + data.markers[key].profile_pic_name : appConfig.BaseUrl + "/img/agentdefault.png"),
    	                coords: {
    	                    latitude: data.markers[key].lat,
    	                    longitude: data.markers[key].long
    	                },
    	                options: {
    	                    draggable: false,
    	                    animation: google.maps.Animation.DROP,
    	                    icon: pin
    	                },
                        WindowWithCustomClass: {
                            options: {
                                boxClass: 'custom-info-window view',
                                disableAutoPan: true
                            }
                        },
    	                events: {
                            click: function(marker, eventName, args) {
                                var modalInstance = $modal.open({
                                  templateUrl: 'viewPin.html',
                                  controller: viewPinCtrl,
                                  size: 'lg',
                                  resolve: {
                                    id: function() {
                                      return args.idKey;
                                    }
                                  }
                                });
                            },
                            mouseover: function(marker, eventName, args){
                                angular.forEach($scope.missionmarkers, function(value, key) {
                                    if(value['id'] == args.idKey){
                                        $scope.missionmarkers[key].show = true;
                                    }
                                });
                            },
                           mouseout: function(marker, eventName, args) {
                                angular.forEach($scope.missionmarkers, function(value, key) {
                                    if(value['id'] == args.idKey){
                                        $scope.missionmarkers[key].show = false;
                                    }
                                });
                            }
                        },
    	                show: false,
    	                markerInfo: {},
    	                cancel: true
    	            });
                });
            } else {
                $scope.default = true;
                $scope.mapcount = data.mapcount;
                $scope.agent = data.agent;
                $scope.map = {
                    center: {
                        latitude: 1,
                        longitude: 1
                    },
                    zoom: 1,
                    events: {},
                    refresh: false
                };

                angular.forEach(data.markers, function(value, key){
                    var pin = '';
                    if (value['pin'] == 0 && value['status'] == 1) {
                        pin = Maps.pin;
                    } else if(value['pin'] == 1 && value['status'] == 1){
                        pin = Maps.kingpin;
                    } else if(value['pin'] == 0 && value['status'] == 0){
                        pin = Maps.pendingpin;
                    }

                    $scope.missionmarkers.push({
                        id: data.markers[key].id,
                        idKey: data.markers[key].id,
                        agentid: data.markers[key].agent,
                        agentname: (data.markers[key].hide_agent==1 ? (data.markers[key].username.length > 9 ? data.markers[key].username.substr(0, 9) + "..." : data.markers[key].username) : "Anonymous"),
                        agentpic: (data.markers[key].profile_pic_name != null ? appConfig.bucket + "uploads/agentpic/" + data.markers[key].agent + "/" + data.markers[key].profile_pic_name : appConfig.BaseUrl + "/img/agentdefault.png"),
                        coords: {
                            latitude: data.markers[key].lat,
                            longitude: data.markers[key].long
                        },
                        options: {
                            draggable: false,
                            animation: google.maps.Animation.DROP,
                            icon: pin
                        },
                        WindowWithCustomClass: {
                            options: {
                                boxClass: 'custom-info-window view',
                                disableAutoPan: true
                            }
                        },
                        events: {
                            click: function(marker, eventName, args) {
                                var modalInstance = $modal.open({
                                  templateUrl: 'viewPin.html',
                                  controller: viewPinCtrl,
                                  size: 'lg',
                                  resolve: {
                                    id: function() {
                                      return args.idKey;
                                    }
                                  }
                                });
                            },
                            mouseover: function(marker, eventName, args){
                                angular.forEach($scope.missionmarkers, function(value, key) {
                                    if(value['id'] == args.idKey){
                                        $scope.missionmarkers[key].show = true;
                                    }
                                });
                            },
                           mouseout: function(marker, eventName, args) {
                                angular.forEach($scope.missionmarkers, function(value, key) {
                                    if(value['id'] == args.idKey){
                                        $scope.missionmarkers[key].show = false;
                                    }
                                });
                            }
                        },
                        show: false,
                        markerInfo: {},
                        cancel: true
                    });
                });
            }
		});
	}

	getmap(title, Login.getCurrentUser().id);

    $scope.viewglobe = function() {
        getmap(title, Login.getCurrentUser().id);
    }

    //comment
    $scope.commentlistener = function(comment) {
        comments = [];
        if(comment!=null && comment!=''){
            Comments.add($scope.mapdetails.id, Login.getCurrentUser().id, {comment:comment}, "map", null, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                    $scope.comment = undefined;
                    $(".input.comment").val(undefined);
                    CKEDITOR.instances['editor'].setData('');
                    getcomments();
                }else {
                    toaster.pop('error', "", data.error);
                }
            });
        }
    }

    //reply to comment
    $scope.replytocomment = function(reply, commentid, type, marker_id) {
        var i = $scope.replies.length;
        while (i--){
            if (commentid == $scope.replies[i].mapcomment_id){
                $scope.replies.splice(i, 1);
            }
        }
        angular.forEach($scope.replycount, function(value, key) {
            if(value['id'] == commentid){
                $scope.replycount[key].loaded = value['loaded'] + 1;
                $scope.replycount[key].count = value['count'] + 2;
                Comments.replytocomment(commentid, $scope.mapdetails.id, Login.getCurrentUser().id, type, marker_id, {reply: reply}, $scope.replycount[key].loaded, $scope.replycount[key].count, $scope.commentload, function(data) {
                    toaster.clear();
                    if(data.hasOwnProperty('success')){
                        toaster.pop('success', "", data.success);
                        $scope.comment = undefined;
                        $(".input.reply").val(undefined);
                        angular.forEach($scope.comments, function(value, key) {
                            if(value['showreply']){
                                showreplyid = value['id'];
                            }
                        });

                        angular.forEach(data.comments, function(value, key) {
                            $scope.comments[key] = data.comments[key];
                            $scope.comments[key]['profile_pic_name'] = (value['profile_pic_name'] != null && comments[key].hide_agent == null || comments[key].hide_agent == 1 ? appConfig.bucket + "uploads/agentpic/" + value['agent'] + "/" + value['profile_pic_name'] : appConfig.BaseUrl + "/img/agentdefault.png");
                            $scope.comments[key].comment = $sce.trustAsHtml($scope.comments[key].comment);
                            if(value['id'] == showreplyid){
                                $scope.comments[key]['showreply'] = true;
                            }else {
                                $scope.comments[key]['showreply'] = false;
                            }
                        });
                        angular.forEach(data.reply, function(value, key) {
                            value['profile_pic_name'] = (value['profile_pic_name'] != null ? appConfig.bucket + "uploads/agentpic/" + value['agent'] + "/" + value['profile_pic_name'] : appConfig.BaseUrl + "/img/agentdefault.png");
                            value['reply'] = $sce.trustAsHtml(value['reply']);
                            $scope.replies.push(value);
                        });
                    }else {
                        toaster.pop('error', "", data.error);
                    }
                });
            }
        });
    }

    $scope.loadcomment = function() {
        $scope.spinner = true;
        $scope.commentload += 3;
        getcomments();
    };

    $scope.showreplybox = function(id) {
        angular.forEach($scope.comments, function(value, key) {
            if(value['id']==id){
                $scope.comments[key]['showreply'] = true;
            }else {
                $scope.comments[key]['showreply'] = false;
            }
        });
    }

    $scope.loadmorereply = function(id) {
        var i = $scope.replies.length;
        while (i--){
            if (id == $scope.replies[i].mapcomment_id){
                $scope.replies.splice(i, 1);
            }
        }

        angular.forEach($scope.replycount, function(value, key) {
            if(id == value['id']){
                $scope.replycount[key].loaded = value['loaded'] +3;
                Comments.loadreply(id, $scope.replycount[key].loaded, value['count'], function(data) {
                    angular.forEach(data, function(value, key) {
                        value['profile_pic_name'] = (value['profile_pic_name'] != null ? appConfig.bucket + "uploads/agentpic/" + value['agent'] + "/" + value['profile_pic_name'] : appConfig.BaseUrl + "/img/agentdefault.png");
                        $scope.replies.push(value);
                    });
                });
            }
        });
    }

    var deleteCommentCtrl = function($scope, $modalInstance, Comments, toaster, id, type) {
        $scope.msg = "Are you sure you want to delete this " + type + "?";

        $scope.delete = function() {
            Comments.deleteComments(id, type, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                    getcomments();
                }else {
                    toaster.pop('danger', "", data.error);
                }
                $modalInstance.dismiss();
            });
        }

        $scope.close = function() {
            $modalInstance.dismiss();
        }
    }

    $scope.changestat = function(stat) {
        mapQuery.changeStatus($scope.mapdetails.id, stat, function(data) {
            toaster.clear();
            if(data.hasOwnProperty('success')){
                toaster.pop('success', "", data.success);
                $scope.mapdetails.status = stat;
            }else {
                toaster.pop('danger', "", data.error);
            }
        });
    }

    $scope.deleteComment = function(id, type) {
        var modalInstance = $modal.open({
          templateUrl: 'deleleComment.html',
          controller: deleteCommentCtrl,
          resolve: {
            id: function() {
              return id;
            },
            type: function() {
                return type;
            }
          }
        });
    }

    var editCommentCtrl = function($scope, $modalInstance, Comments, toaster, id, msg, type) {
        $scope.msg = "Are you sure you want to edit this " + type + "?";

        $scope.edit = function() {
            Comments.editcomment(id, type, {msg:msg}, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                    getcomments();
                }else {
                    toaster.pop('danger', "", data.error);
                }
                $modalInstance.dismiss();
            });
        }

        $scope.close = function() {
            $modalInstance.dismiss();
        }
    }

    $scope.editComment = function(id, msg, type) {
        var modalInstance = $modal.open({
            templateUrl: 'editComment.html',
            controller: editCommentCtrl,
            resolve: {
                id: function() {
                    return id;
                },
                msg: function() {
                    return msg;
                },
                type: function() {
                    return type;
                }
            }
        });
    }

    Maps.categories(function(data){
        Maps.mapCategory = data;
    });

    Maps.tags(function(data){
        var tt = [];
        for(var x in data){
            tt.push(data[x].tag);
        }
        Maps.mapTags = tt;
    });

    var editmapCtrl = function($scope, $modalInstance, Maps, mapQuery, toaster, mapslug) {
        mapQuery.getmapCatTags(Maps.currentMap, function(data) {
            var catlist = [];
            data.category.map(function(val){
                catlist.push(val);
            });
            var taglist = [];
            data.tags.map(function(val){
                taglist.push(val.tag);
            });

            $scope.gmap = {
                'title': Maps.mapDetails.title,
                'description': Maps.mapDetails.description,
                'category' : catlist,
                'tags' : taglist
            }
        });

        $scope.data = Maps.mapCategory;
        $scope.tag = Maps.mapTags;

        $scope.closeMapInfo = function(){
            console.log('Closing Modal Map Info');
            Maps.showMapInfo = false;
            $modalInstance.dismiss();
        }

        $scope.saveMapInfo = function(data) {
            mapQuery.editmapinfo(Maps.currentMap, $scope.gmap, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                    Maps.mapDetails.title = $scope.gmap.title;
                    Maps.mapDetails.description = $scope.gmap.description;
                    getmap(mapslug);
                }else {
                    toaster.pop('danger', "", data.error);
                }
            });

            $modalInstance.close(data);
        }
    }

    $scope.editMap = function() {
        var modalInstance = $modal.open({
          templateUrl: 'editMap.html',
          controller: editmapCtrl,
          resolve: {
            mapslug: function() {
                return $scope.mapdetails.mapslugs;
            }
          }
        });
    }

    var viewMaplistCtrl = function($scope, $modalInstance, agent, mapQuery, appConfig, $sce, $timeout, $animate, Login, $state, toaster, appConfig) {
        $scope.agent = {
            name : agent.first_name + " " + agent.last_name,
            pic : agent.profile_pic_name != null ? appConfig.bucket + "uploads/agentpic/" + agent.id + "/" + agent.profile_pic_name : appConfig.BaseUrl + "/img/agentdefault.png"
        }
        $scope.options = {
            styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
        };

        mapQuery.getagentMaps(agent.id, function(data) {
            angular.forEach(data.data, function(value, key){
                data.data[key]['map'] = {
                    center: {
                        latitude: value['lat'],
                        longitude: value['long']
                    },
                    zoom: 10
                };
                mapQuery.getFblikes(value['mapslugs'], function(fb) {
                    data.data[key]['likes'] = fb[0].like_count;
                });

                angular.forEach(data.data[key]['markers'], function(value, key2) {
                    if(value['pin'] == 1){
                        data.data[key]['markers'][key2]['icon'] = Maps.kingpin;
                    }else {
                        data.data[key]['markers'][key2]['icon'] = Maps.pin;
                    }
                })
            });
            $scope.maps = data.data;
        });

        $scope.selectMap = function(mapslug) {
            getmap(mapslug, null);
            $modalInstance.dismiss();
        }
    }

    $scope.viewlist = function() {
        var modalInstance = $modal.open({
            templateUrl: 'viewMapList.html',
            controller: viewMaplistCtrl,
            size: 'lg',
            resolve: {
                agent: function() {
                    return $scope.agent;
                }
            }
        });
    }

    var loadannouncements = function() {
        mapQuery.getnewslist($scope.mapdetails.id, function(data) {
            $scope.mapnews = data;
        });
    }

    var addNewsCtrl = function($scope, mapid, $modalInstance, mapQuery, toaster, Maps, $timeout) {
        $scope.news = {
            coverType: 'image'
        };
        $scope.spinner = false;
        $scope.disable = true;
        $scope.onTabSelect = function(val){
            $scope.disable = true;
            $scope.news.coverType = val;
            if(val=='image'){
                $scope.news.videolink = undefined;
            }else {
                $scope.news.image = undefined;
            }
            console.log($scope.news);
        }

        $scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
        $scope.generateThumb = function(file) {
            $scope.disable = false;
            if (file != null) {
                $scope.file=file;
                if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
                    $timeout(function() {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function(e) {
                            $timeout(function() {
                                file.dataUrl = e.target.result;
                            });
                        }
                    });
                }
            }
        };

        $scope.viddisp = function(video) {
            if (video !== undefined) {
                var queryString = video.substring(video.indexOf('?') + 1);
                var param = parseQueryString(queryString);
                if (param.hasOwnProperty('v')) {
                    $scope.disable = false;
                    $scope.video=video;
                    $scope.videodisplay = $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" style="width:500px; height:300px;" allowfullscreen></iframe>');
                } else {
                    $scope.video='';
                    $scope.videodisplay = '';
                    toaster.clear();
                    toaster.pop('error', "", 'Invalid youtube link.');
                }
            } else {
                $scope.videodisplay = '';
            }
        }

        $scope.close = function() {
            $modalInstance.dismiss();
        }

        $scope.saveNews = function(news) {
            $scope.spinner = true;
            news.coverType == 'image' ? console.log("video") : news.image = [], news.image.push({name:"sample"});
            Maps.saveNewsPic(news.coverType, news.image[0], mapid, function(id) {
                news.image = news.image[0].name;
                mapQuery.saveNews(id, news, function(data) {
                    toaster.clear();
                    if(data.hasOwnProperty('success')){
                        loadannouncements();
                        toaster.pop('success', "", data.success);
                    }else {
                        toaster.pop('error', "", data.error);
                    }
                    $scope.close();
                    $scope.spinner = false;
                });
            });
        }
    }

    $scope.addNews = function() {
        var modalInstance = $modal.open({
            templateUrl: 'mapNews.html',
            controller: addNewsCtrl,
            resolve: {
                mapid: function() {
                    return $scope.mapdetails.id;
                }
            }
        });
    }

    var viewMapNewsCtrl = function($scope, $modalInstance, news, appConfig, agentid, agent, mapQuery, $timeout) {
        $scope.news = news;
        if($scope.news.coverType == 'image'){
           $scope.news.cover = appConfig.bucket + "uploads/mapnews/" + news.mapid + "/" + news.cover;
        }
        $scope.agentid = agentid;
        $scope.agent = agent;
        $scope.deleteshow = false;
        $scope.editshow = false;
        $scope.close = function() {
            $modalInstance.close();
        }
        if(news.coverType=='video'){
            var video = news.cover;
            if (video !== undefined) {
                var queryString = video.substring(video.indexOf('?') + 1);
                var param = parseQueryString(queryString);
                if (param.hasOwnProperty('v')) {
                    $scope.video=video;
                    $scope.videodisplay = $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" style="width:500px; height:300px;" allowfullscreen></iframe>');
                } else {
                    $scope.video='';
                    $scope.videodisplay = '';
                    toaster.clear();
                    toaster.pop('error', "", 'Invalid youtube link.');
                }
            } else {
                $scope.videodisplay = '';
            }
        }

        $scope.close = function() {
            $modalInstance.dismiss();
        }

        $scope.deleteNews = function() {
            $scope.deleteshow = true;
        }

        $scope.delete = function() {
            mapQuery.deletenews(news.id, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    loadannouncements();
                    $modalInstance.dismiss();
                    toaster.pop('success', "", data.success);
                }else {
                    toaster.pop('error', "", data.error);
                }
            });
        }

        $scope.editNews = function() {
            $scope.editshow = true;
            $scope.news.cover = news.cover;
            if($scope.news.coverType == 'image'){
                $scope.isImage = true;
                $scope.isVideo = false;
            }else {
                $scope.isImage = false;
                $scope.isVideo = true;
            }
        }

        $scope.onTabSelect = function(val){
            $scope.disable = true;
            $scope.news.coverType = val;
            if(val=='image'){
                $scope.news.cover = undefined;
            }else {
                $scope.news.image = undefined;
            }
            console.log($scope.news);
        }

        $scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
        $scope.generateThumb = function(file) {
            if (file != null) {
                $scope.file=file;
                if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
                    $timeout(function() {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function(e) {
                            $timeout(function() {
                                file.dataUrl = e.target.result;
                                $scope.disable = false;
                            });
                        }
                    });
                }
            }
        };

        $scope.viddisp = function(video) {
            if (video !== undefined) {
                var queryString = video.substring(video.indexOf('?') + 1);
                var param = parseQueryString(queryString);
                if (param.hasOwnProperty('v')) {
                    $scope.video=video;
                    $scope.videodisplay = $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" width="100%" height="90%" allowfullscreen></iframe>');
                    $scope.disable = false;
                } else {
                    $scope.video='';
                    $scope.videodisplay = '';
                    toaster.clear();
                    toaster.pop('error', "", 'Invalid youtube link.');
                }
            } else {
                $scope.videodisplay = '';
            }
        }

        $scope.updateNews = function(news) {
            $scope.spinner = true;
            news.coverType == 'image' ? console.log("video") : news.image = [], news.image.push({name:"sample"});
            Maps.saveNewsPic(news.coverType, news.image[0], news.mapid, function(id) {
                news.image = news.image[0].name;
                mapQuery.updateNews(news.id, news, function(data) {
                    toaster.clear();
                    if(data.hasOwnProperty('success')){
                        loadannouncements();
                        toaster.pop('success', "", data.success);
                    }else {
                        toaster.pop('error', "", data.error);
                    }
                    $scope.close();
                    $scope.spinner = false;
                });
            });
        }

    }

    $scope.viewmapnews = function(news) {
        var modalInstance = $modal.open({
            templateUrl: 'viewNews.html',
            controller: viewMapNewsCtrl,
            resolve: {
                news: function() {
                    return news;
                },
                agentid: function() {
                    return $scope.mapdetails.agent;
                },
                agent: function() {
                    return $scope.agentid;
                }
            }
        }).result.finally(function(){
            loadannouncements();
        });
    }

    var loadednews = 3;

    var viewmapnews = function(news) {
        $scope.viewmapnews(news);
    }

    $scope.viewmorenews = function() {
        var modalInstance = $modal.open({
            templateUrl: 'viewNewsList.html',
            controller: function($scope, $modalInstance, appConfig, mapQuery, mapid, appConfig) {
                var loadnews = function(){
                    loadednews +=2;
                    mapQuery.getnews(mapid, loadednews, function(data) {
                        $scope.news = data.news;
                        console.log($scope.news);
                        $scope.count = data.count;
                        angular.forEach($scope.news, function(value, key) {
                            if(value['coverType'] == 'image'){
                                $scope.news[key]['cover'] = appConfig.bucket + "uploads/mapnews/" + value['mapid'] + "/" + value['cover'];
                                console.log($scope.news[key])
                            }
                        });
                    });
                }

                loadnews();

                $scope.loadmore = function() {
                    loadnews();
                }

                $scope.viewnews = function(news) {
                    viewmapnews(news);
                }
                $scope.close = function() {
                    $modalInstance.dismiss();
                }
            },
            resolve: {
                mapid: function() {
                    return $scope.mapdetails.id;
                }
            }
        });
    }

    $scope.searchbox = {
        template:'searchbox.tpl.html',
        position:'TOP_LEFT',
        //position: 'BOTTOM',
        options: { bounds: {} } ,
        //parentdiv:'searchBoxParent',
        events: {
            places_changed: function (searchBox) {
                var place = searchBox.getPlaces();
                if (!place || place == 'undefined' || place.length == 0) {
                    console.log('no place data :(');
                    return;
                }

                $scope.map.center.latitude = place[0].geometry.location.lat();
                $scope.map.center.longitude = place[0].geometry.location.lng();
            }
        }
    };

    var changecoverCtrl = function($scope, $modalInstance, mapQuery, toaster, mapid, $timeout, type) {
        $scope.imageloader=false;
        $scope.imagecontent=true;
        $scope.directory = 'maps/' + mapid;
        $scope.videoenabled=true;
        $scope.mediaGallery = 'all';
        $scope.invalidvideo = false;
        $scope.currentSelected = '';
        $scope.currentDeleting = '';
        $scope.type = type;
        $scope.s3link = appConfig.bucket;

        $scope.set = function(id){
            console.log(id);
            $scope.currentSelected = id;
            $scope.contentvideo = id;
            if(type=='content'){
                $scope.copy();
            }
        }

        var returnYoutubeThumb = function(item){
            var x= '';
            var thumb = {};
            if(item){
                var newdata = item;
                var x;
                x = newdata.video.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
                thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
                thumb['yid'] = x[1];
                return thumb;
            }else{
                return x;
            }
        }



        $scope.returnImageThumb = function(){
            return function (item) {
                if(item){
                    return appConfig.bucket + "/uploads/maps/" + mapid + "/" + item;
                }else{
                    return item;
                }
            };
        }

        var loadgallery = function() {
            $scope.imagelist = [];
            $scope.imagelength = 0;
            $scope.videolist = [];
            $scope.videoslength = 0;
            mapQuery.loadgallery(mapid, function(data){
                console.log(mapid)
                if(data.hasOwnProperty('image')){
                    $scope.imagelist = data.image;
                    $scope.imagelength = data.image.length;
                }
                        console.log(data);
                if(data.hasOwnProperty('video')){
                    for (var x in data.video){
                        var newd = returnYoutubeThumb(data.video[x]);
                        data.video[x].videourl = newd.url;
                        data.video[x].youtubeid = newd.yid;
                    }

                    $scope.videolist = data.video;
                    $scope.videoslength = data.video.length;
                }
            });
        }

        loadgallery();

        $scope.$watch('files', function () {
            $scope.upload($scope.files);

        });

        $scope.upload = function (files)
        {

            var filename
            var filecount = 0;
            if (files && files.length)
            {
                $scope.imageloader=true;
                $scope.imagecontent=false;

                for (var i = 0; i < files.length; i++)
                {
                    var file = files[i];

                    if (file.size >= 2000000)
                    {
                        toaster.pop('error', '', 'File ' + file.name + ' is too big');
                        filecount = filecount + 1;

                        if(filecount == files.length)
                        {
                            $scope.imageloader=false;
                            $scope.imagecontent=true;
                        }


                    }
                    else

                    {

                        var promises;

                        promises = Upload.upload({

                            url: appConfig.bucket, //S3 upload url including bucket name
                            method: 'POST',
                            transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                key: 'uploads/maps/' + mapid + '/' + file.name, // the key to store the file on S3, could be file name or customized
                                AWSAccessKeyId: appConfig.AWSAccessKeyId,
                                acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                policy: appConfig.policy, // base64-encoded json policy (see article below)
                                signature: appConfig.signature, // base64-encoded signature based on policy string (see article below)
                                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                            },
                            file: file
                        })
                        promises.then(function(data){

                            filecount = filecount + 1;
                            filename = data.config.file.name;
                            var fileout = {
                                'imgfilename' : filename
                            };
                            mapQuery.saveCover(fileout, mapid, 'image', function(data){
                                loadgallery();
                                if(filecount == files.length)
                                {
                                    $scope.imageloader=false;
                                    $scope.imagecontent=true;
                                }
                            });
                        });
                    }

                }
            }
        };

        $scope.savevid = function(newsvid) {
            if(newsvid){
                var x = newsvid.match(/<iframe width="(.*?)" height="(.*?)" src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
                console.log(x);
                if(x==null){
                    $scope.invalidvideo = true;
                    $scope.video='';
                }else{
                    var newslink = { 'newsvid': newsvid }
                    mapQuery.saveCover(newslink, mapid, 'video', function(data){
                        loadgallery();
                        $scope.video='';
                        $scope.invalidvideo = false;
                        console.log(data);
                    });
                }
            }
        };



        $scope.deletevideo = function (videoid, $event)
        {
            var datavideo = {
                'videoid' : videoid
            };
            mapQuery.deleteMedia(datavideo, mapid, 'video', function(data){
                loadgallery();
            });
            $event.stopPropagation();
        }

        $scope.deletenewsimg = function (dataimg, $event)
        {
            var fileout = {
                'imgfilename' : dataimg
            };
            mapQuery.deleteMedia(fileout, mapid, 'image', function(data) {
                loadgallery();

            });
            $event.stopPropagation();
        }

        $scope.copy = function() {
            console.log($scope.contentvideo);
            var text = '';
            if($scope.contentvideo.filename){
                text = appConfig.bucket + '/uploads/newsimage/' + $scope.contentvideo.filename;
            }else if($scope.contentvideo.videoid){
                text = $scope.contentvideo.video;
            }
            var pp = window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
            if(pp != "" && pp !== null) {
                $modalInstance.dismiss('cancel');
            }
        }

        $scope.ok = function(category) {
            $modalInstance.close($scope.contentvideo);
        }

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        }
    }

    $scope.changecover = function() {
        var modalInstance = $modal.open({
            templateUrl: 'mediagallery.html',
            controller: changecoverCtrl,
            resolve: {
                mapid: function() {
                    return $scope.mapdetails.id;
                },
                type: function(){
                    return 'featured';
                } 
            },
            windowClass: 'xxx-modal-window'
        }).result.then(function(res) {
            if(res){
                var cover = '';
                if(res.filename){
                    $scope.mapdetails.coverType = 'image';
                    $scope.mapdetails.cover = appConfig.bucket + "uploads/maps/" + $scope.mapdetails.id + "/" + res.filename;
                    cover = res.filename; 
                }else if(res.videoid){
                    $scope.mapdetails.coverType = 'video';
                    cover = "https://www.youtube.com/watch?v=" + res.youtubeid;
                    videocover("https://www.youtube.com/watch?v=" + res.youtubeid);
                }

                mapQuery.updateCover($scope.mapdetails.id, { data : cover }, $scope.mapdetails.coverType, function(data) {
                    if(data.hasOwnProperty('error')){
                        toaster.pop('error', '', data.error);
                    }
                });
            }
        });
    }
})
