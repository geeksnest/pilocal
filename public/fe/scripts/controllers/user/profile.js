'use strict';

/* Controllers */

app.controller('ProfileCtrl', function($scope, $stateParams, $anchorScroll, $location, User, store, $window, Upload, Login, BirthDayMonthYear, $timeout, appConfig, toaster, mapQuery, $modal){
    if($stateParams.username == undefined){
        $window.location = '/registration';
    }
    $scope.months = BirthDayMonthYear.month();
    $scope.day = BirthDayMonthYear.day();
    $scope.year = BirthDayMonthYear.year();
    $scope.amazon = appConfig.bucket;
    $scope.spinner = false;
    var agentpic = '';

    var getUserinfo = function(id){
    	User.getInfo(id, function(data){
            $scope.agent = data;
            $scope.profpic = (data.profile_pic_name != null ? appConfig.bucket + "uploads/agentpic/" + data.id + "/" + data.profile_pic_name : appConfig.BaseUrl + "/img/agentdefault.png");
            $scope.agent.month = parseInt(Date.parse(data.birthday).toString("M"));
            $scope.agent.day = parseInt(Date.parse(data.birthday).toString("dd"));
            $scope.agent.year = parseInt(Date.parse(data.birthday).toString("yyyy"));
            agentpic = data.profile_pic_name;
            $timeout(function() {
                var cindex = $("#country")[0].selectedIndex;
                print_state('state', cindex);
                $("#state").val($scope.agent.state);
            }, 500);
    	});
    }

    // $scope.prepare = function(file) {
    //     if (file && file.length) {
    //       if (file[0].size >= 200000) {
    //         toaster.clear();
    //         toaster.pop('danger', "", 'File is too big' );
    //         $scope.file[0] = '';
    //       } else {
    //         $scope.file = file;
    //         $scope.agent.profile_pic_name = file[0].name;
    //       }
    //     }
    // }

    $scope.updateagent = function(agent) {
        User.updateprofile(agent, function(data) {
            toaster.clear();
            if(data.hasOwnProperty('success')){
                toaster.pop('success', "", data.success);
            }else {
                toaster.pop('danger', "", data.error);
            }
            $scope.spinner = false;
        });
       
        Login.loginmenu = agent.first_name + " " + agent.last_name;
    }

    getUserinfo(Login.getCurrentUser().id);

    var changeprofpicCtrl = function($scope, $modalInstance, toaster, $timeout, type) {
        $scope.imageloader=false;
        $scope.imagecontent=true;
        $scope.directory = 'agentpic/' + Login.getCurrentUser().id;
        $scope.videoenabled=false;
        $scope.mediaGallery = 'images';
        $scope.invalidvideo = false;
        $scope.currentSelected = '';
        $scope.currentDeleting = '';
        $scope.type = type;
        $scope.s3link = appConfig.bucket;

        $scope.set = function(id){
            $scope.currentSelected = id;
            $scope.contentvideo = id;
            if(type=='content'){
                $scope.copy();
            }
        }

        $scope.returnImageThumb = function(){
            return function (item) {
                if(item){
                    return appConfig.bucket + "/uploads/agentpic/" + Login.getCurrentUser().id + "/" + item;
                }else{
                    return item;
                }
            };
        }

        var loadgallery = function() {
            User.loadgallery(Login.getCurrentUser().id, function(data){
                if(data.hasOwnProperty('error')){
                    toaster.pop('error', '', data.error);
                    $modalInstance.dismiss();
                }else {
                    $scope.imagelist = data;
                    $scope.imagelength = data.length;
                }
                console.log($scope.imagelist);
            });
        }

        loadgallery();

        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        });

        $scope.upload = function (files)
        {

            var filename
            var filecount = 0;
            if (files && files.length)
            {
                $scope.imageloader=true;
                $scope.imagecontent=false;

                for (var i = 0; i < files.length; i++)
                {
                    var file = files[i];

                    if (file.size >= 2000000)
                    {
                        toaster.pop('error', '', 'File ' + file.name + ' is too big');
                        filecount = filecount + 1;

                        if(filecount == files.length)
                        {
                            $scope.imageloader=false;
                            $scope.imagecontent=true;
                        }


                    }
                    else

                    {
                        User.uploadagentpic(file, Login.getCurrentUser().id, function(data) {
                            if(data.error){
                                toaster.pop('error','',data.error);
                            }
                            loadgallery();
                            $scope.imageloader=false;
                            $scope.imagecontent=true;
                        });
                    }

                }
            }
        };

        $scope.deletenewsimg = function (dataimg, $event)
        {
            User.deleteMedia(Login.getCurrentUser().id, {filename:dataimg}, function(data) {
                if(data.hasOwnProperty('error')){
                    toaster.pop('error','',data.error);
                }
                loadgallery();
            });
            $event.stopPropagation();
        }

        $scope.ok = function(category) {
            $modalInstance.close($scope.contentvideo);
        }

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        }
    }

    $scope.chprofpic = function() {
        var modalInstance = $modal.open({
            templateUrl: 'mediagallery.html',
            controller: changeprofpicCtrl,
            resolve: {
                type: function(){
                    return 'featured';
                } 
            },
            windowClass: 'xxx-modal-window'
        }).result.then(function(res) {
            if(res){
                $scope.profpic = appConfig.bucket + "uploads/agentpic/" + $scope.agent.id + "/" + res.filename;
                $scope.agent.profile_pic_name = res.filename;
            }
        });
    }
})
