'use strict';

/* Controllers */

app.controller('ContactusCtrl', function($scope, $state, $anchorScroll, $location, store, vcRecaptchaService, $window, Contactus, toaster, appConfig) {
    $scope.spinner = false;
    $scope.response = null;
    $scope.widgetId = null;
    $scope.captcharesponse = false;
    $scope.model = {
        key: appConfig.recaptcha
    };
    $scope.setResponse = function (response) {
        console.info('Response available');
        $scope.response = response;
    };
    $scope.setWidgetId = function (widgetId) {
        console.info('Created widget ID: %s', widgetId);
        $scope.widgetId = widgetId;
    };

    $scope.submit = function(feedback) {
        $scope.disable = true;
        Contactus.save(feedback, function(data) {
            toaster.clear();
            if(data.hasOwnProperty('success')){
                toaster.pop('success', "", data.success);
                $scope.feedback = {};
                vcRecaptchaService.reload($scope.widgetId);
                $scope.$broadcast('show-errors-reset');
                $scope.disable = false;
            }else {
                toaster.pop('error', "", data.error);
            }
        });
    }
})
