'use strict';

/* Controllers */
app.controller('categoryCtrl', function($timeout,$http, $scope, mapQuery, $rootScope, $interval, $sce, appConfig, Login, $state, Maps, store, $stateParams, $anchorScroll, ServiceMain, $modal) {
    $anchorScroll();
    console.log($stateParams.catslug);

    var num = 10;
    var off = 1;
    $scope.sortby = "ALL";
    store.set('SeeMore', "ALL");
    $scope.sortoption = [
        { name: 'ALL', value: 'ALL' },
        { name: 'FEATURED', value: 'FEATURED' },
        { name: 'POPULAR', value: 'POPULAR' },
        { name: 'GREATEST', value: 'GREATEST' }
    ];
    $scope.options = {
        styles: appConfig.googleMapStyle
    };
    var getmaps = function(num, off, status){
        mapQuery.getMaps(num, off, status, $stateParams.catslug,function(data) {
            console.log(data);
            angular.forEach(data.data, function(value, key){
                data.data[key]['map'] = {
                    center: {
                        latitude: value['lat'],
                        longitude: value['long']
                    },
                    zoom: 10
                };
                mapQuery.getFblikes(value['mapslugs'], function(fb) {
                    data.data[key]['likes'] = fb[0].like_count;
                });
                value['created_at'] = new Date(value['created_at'].replace(/-/g, '/'));

                angular.forEach(data.data[key]['markers'], function(value, key2) {
                    if(value['pin'] == 1){
                        data.data[key]['markers'][key2]['icon'] = Maps.kingpin;
                    }else {
                        data.data[key]['markers'][key2]['icon'] = Maps.pin;
                    }
                })
            });
            $scope.maps = data.data;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
            $scope.category  = data.category;
            $scope.loading = false;
            console.log($scope.maps);

            $timeout(function(){
                FB.XFBML.parse();
            }, 100);

        });
    }

    var seemore = 'ALL'
    if(store.get('SeeMore')){
        seemore=store.get('SeeMore');
        $scope.sortby = seemore;
    }

    getmaps(num,off,seemore);

    $scope.setPage = function (pageNo ,status) {
        getmaps(num, pageNo, status);
        store.set('SeeMore', status);
    };

    $interval(function() {
        getmaps();
    }, 600000);

    ServiceMain.getCategories(function(data){
        $scope.categories = data;
    });

    ServiceMain.getNews(function(data){
        $scope.news = data;
    });

    $scope.seeMoreCategories = function(){
        var modalInstance = $modal.open({
            templateUrl: 'otherCategories.html',
            controller: function($scope, $modalInstance){

                ServiceMain.getCategories(function(data){
                    var startnextindex = Math.ceil(data.length / 2);
                    var col1 = [];
                    console.log(startnextindex);
                    for(var x=0; x< startnextindex; x++){
                        col1.push(data[x]);
                    }
                    var col2 = [];
                    for(var y=startnextindex; y < data.length; y++){
                        col2.push(data[y]);
                    }
                    console.log(col1);
                    console.log(col2);
                    $scope.col1 = col1;
                    $scope.col2 = col2;
                });

                $scope.closeMapInfo = function(){
                    $modalInstance.dismiss();
                }

                $scope.clickCatInfo = function(data)	{

                    $modalInstance.close(data);
                }
            }
        }).result.then(function (data) {
                $anchorScroll()
            }, function () {

            });
    }

});
