'use strict';

/* Controllers */
app.controller('InfoWindowCtrl', function($scope, Maps, $anchorScroll, $location, User, store, $window, Login, $modal) {

  $scope.mapcover = '';

  $scope.$watch(function() {
    return Maps.getToggleMarkerInfo()
  }, function() {
    var el;
    if (Maps.getToggleMarkerInfo()) {

      el = '<a class="markercontrol" ng-click="controlClick()"><i class="fa fa-ban"></i> Close</a> ';
      console.log('Show Cancel');
    } else {
      el = '<a class="markercontrol" ng-click="clickInfo(map)"><i class="fa fa-plus-circle"></i> Info</a> ';
      console.log('Show Info');
    }

    $scope.buttons = el;
  });
  $scope.clickInfo = function(map) {
    $('.gmap').animate({
      width: '60%'
    }, {
      complete: function() {
        $('.gmap-marker-info').show();
        google.maps.event.trigger($scope.map, 'resize');
        var pin = Maps.currentPin;
        $scope.map.setCenter(new google.maps.LatLng(pin.coords.latitude, pin.coords.longitude));
        $scope.rightPanelInfo = Maps.setToggleMarkerInfo(true);
      }
    })
    $scope.mk = {
      'description': '',
      'videolink': ''
    }
  }

  $scope.controlClick = function() {
    console.log('CONTROL HAS BEEN CLICKED');
    if (Maps.currentPin === null) {
      alert('You do not have any pins in your map yet, please input one.');
    } else {
      if (Maps.getToggleMarkerInfo()) {
        $scope.rightPanelInfo = Maps.setToggleMarkerInfo(false);
        $('.gmap').animate({
          width: '100%'
        }, {
          complete: function() {
            $('.gmap-marker-info').hide();
            google.maps.event.trigger($scope.map, 'resize');
            var pin = Maps.currentPin;
            $scope.map.setCenter(new google.maps.LatLng(pin.coords.latitude, pin.coords.longitude));
            $scope.rightPanelInfo = Maps.setToggleMarkerInfo(false);
          }
        })
      } else {
        $('.gmap').animate({
          width: '60%'
        }, {
          complete: function() {
            $('.gmap-marker-info').show();
            google.maps.event.trigger($scope.map, 'resize')
            var pin = Maps.currentPin;
            if (pin != null) {
              $scope.map.setCenter(new google.maps.LatLng(pin.coords.latitude, pin.coords.longitude));
            }

          }
        })
        $scope.rightPanelInfo = Maps.setToggleMarkerInfo(true);
      }
    }
  };

  $scope.setKingPin = function(val1, val2){
    console.log(Maps.currentPin);
    console.log('SETTING KING PIN');
    console.log(Maps.currentPin.kingpin)
    if(!Maps.currentPin.kingpin){
      Maps.showMakeKingpin = true;
    }
  }

  $scope.deleteMarker = function() {
    Maps.showDelete = true;
  }

  // Map Info Button
  $scope.clickMapInfo = function() {
    Maps.showMapInfo = true;
  }

  // Map Info Button
  $scope.clickMapCover = function() {
    Maps.showMapCover = true;
  }

  $scope.$watch(function() {
    return Maps.coverVal;
  }, function(){
    if(Maps.coverType=='image'){
      if(Maps.coverType == ''){
        $scope.mapcover = '/img/mapcontrols/coverphoto.png';
      }else{
        $scope.mapcover = Maps.coverVal.dataUrl;
      }
    }else{
      if(Maps.coverType == ''){
        $scope.mapcover = '/img/mapcontrols/coverphoto.png';
      }else{
        $scope.mapcover = '/img/mapcontrols/video.png';
      }
    }
  });

})
