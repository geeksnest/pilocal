'use strict';

/* Controllers */
app.controller('MapsCreateCtrl', function($http, $log, $scope, Maps, $cookieStore, rfc4122, $compile, $rootScope, $timeout, $sce, $location, appConfig, Login, $state, $modal, mapQuery, currentLocation, toaster, User) {

	//check if user is logged in
	if(Login.getCurrentUser() == null){
		$state.go('login');
		$scope.agentName = "User";
	}else{
		$scope.agentName = Login.getCurrentUser().firstname + " " + Login.getCurrentUser().lastname;
	}

	User.getInfo(Login.getCurrentUser().username, function(data) {
		$scope.agentpic = (data.profile_pic_name != null ? appConfig.bucket + "uploads/agentpic/" + data.id + "/" + data.profile_pic_name : appConfig.BaseUrl + "/img/agentdefault.png");
	});

	Maps.mapSlugs = '';
	Maps.mapDetails.title = 'Mission Title';
	Maps.mapDetails.description = 'Mission Description';
	Maps.mapDetails.category = '';
	Maps.showDelete= false;
	Maps.showMapInfo= false;
	Maps.showMapCover= false;
	Maps.coverType= '';
	Maps.coverVal= '';
	Maps.mapSlugs='';


	console.log(currentLocation);
	$log.info('==== Maps Create Controller ====');
	$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
	$cookieStore.remove('markersInfo');
	$cookieStore.put('markersInfo', []);
	$scope.notiCheck = false;
	$scope.markers = [];
	$scope.markerImages = [];
	$scope.PinImages = [];
	$scope.currentPinImages = [];
	$scope.flashError = [];
	$scope.loader = false;
	$scope.loaderText = '';
	$scope.rightPanelInfo = Maps.getToggleMarkerInfo();
	$scope.showDelete = Maps.showDelete;
	$scope.mapcover = '';
	$scope.mk = {
		'description': '',
		'videolink': ''
	};
	$scope.coverPic = '';
	$scope.mapTitle = 'Mission Title';
	$scope.mapDescription = 'Mission Description';
	$scope.mapCategory = '';
	$scope.mapTags = '';
	$scope.mapcover = '/img/mapcontrols/coverphoto.png';


	//map
	$scope.map = {
		center: {
			latitude: currentLocation.coords.latitude,
			longitude: currentLocation.coords.longitude
		},
		zoom: 10,
		events: {
			click: function(event, a, b) {
				createMarker(b[0].latLng.lat(), b[0].latLng.lng())
					//console.log(b[0].latLng.lat());
					//console.log(b[0].latLng.lng());
			}
		},
		infoWindowWithCustomClass: {
			options: {
				boxClass: 'custom-info-window',
				disableAutoPan: true
			}
		}
	};

	$scope.searchbox = {
		template:'searchbox.tpl.html',
		position:'TOP_LEFT',
		//position: 'BOTTOM',
		options: { bounds: {} } ,
		//parentdiv:'searchBoxParent',
		events: {
			places_changed: function (searchBox) {
				var place = searchBox.getPlaces();
				if (!place || place == 'undefined' || place.length == 0) {
					console.log('no place data :(');
					return;
				}

				$scope.map.center.latitude = place[0].geometry.location.lat();
				$scope.map.center.longitude = place[0].geometry.location.lng();
			}
		}
	};

	$scope.options = {
		styles: appConfig.googleMapStyle
	};

	$scope.$watch(function() {
		return Maps.getToggleMarkerInfo();
	}, function() {
		var el;
		if (Maps.getToggleMarkerInfo()) {
			el = '<a ng-click="controlClick()"><i class="fa fa-ban"></i> Cancel</a> ';
			console.log('Show Cancel');
		} else {
			el = '<a ng-click="clickInfo(map)"><i class="fa fa-plus-circle"></i> Info</a> ';
			console.log('Show Info');
		}
		$scope.buttons = el;
	});

	var resetPinDetails = function(mkr, currentPin){
		$scope.mk.description = undefined;
		$scope.mk.videolink = undefined;
		$scope.currentPinImages = viewPinPic(currentPin)
		$scope.viddisp($scope.mk.videolink);
		$scope.notiCheck = false;
	}
	var deletePin = function($scope, $modalInstance, Maps) {
		$scope.showBtn = true;
		$scope.msg = "Are you sure you want to delete this pin?";
		$scope.deletePin = function() {
			var array = Maps.markers;
			var currentPin = Maps.currentPin.idKey;
			for (var x in array) {
				if (array[x].idKey == currentPin) {
					array.splice(x, 1);
					Maps.setCurrentPin(Maps.markers[0]);
					Maps.showTrueMarker(currentPin);
					resetPinDetails(currentPin);

					Maps.markers.length > 0 ? Maps.markers[0].options.icon = Maps.kingpin : console.log("No options");
					//Display the index 0 info's
					Maps.markers = array;
					$scope.showDelete = Maps.showDelete = false;
					console.log(Maps.markers);
				}
			}

			var mi = $cookieStore.get('markersInfo');
			for (var x in mi) {
				if (mi[x].idKey == currentPin) {
					mi.splice(x, 1);
					$cookieStore.put('markersInfo', mi);
				}
			}
			var pm = Maps.pinImages;
			for (var x in pm) {
				if (pm[x].idKey == currentPin) {
					pm.splice(x, 1);
					Maps.pinImages = pm;
				}
			}
			$scope.showBtn = false;
			$scope.msg ="Pin has been successfully deleted"
			$timeout(function() {
		   		$scope.close()
			});
			 console.log(Maps);
		}

		$scope.close = function(){
			$modalInstance.dismiss();
			Maps.showDelete = false;
		}
	}

	$scope.$watch(function() {
		return Maps.showDelete;
		}, function() {
		if (Maps.showDelete) {
			var modalInstance = $modal.open({
				templateUrl: 'deletePin.html',
				controller: deletePin
			});
		}
	});


	$scope.showMapInfo = function(){
		Maps.showMapInfo = true;
	}

	$scope.clickMapCover = function() {
		Maps.showMapCover = true;
	}

	$scope.$watch(function() {
		return Maps.showMakeKingpin;
	}, function(){
		if(Maps.showMakeKingpin){

			var modalInstance = $modal.open({
				templateUrl: 'setKingPin.html',
				controller: function($scope, $modalInstance, Maps){
					console.log('CONTROLLER FOR Set KingPin');

					$scope.close = function(){
						Maps.showMakeKingpin = false;
						$modalInstance.close();
					}

					$scope.setPin = function(data)	{

						Maps.currentPin.kingpin = true;

						Maps.markers.map(function(data){
								if(data.idKey == Maps.currentPin.idKey){
									data.kingpin = true;
									data.options.icon = Maps.kingpin
								}else{
									data.kingpin = false;
									data.options.icon = Maps.pin;
								}
						});

						Maps.showMakeKingpin = false;
						$modalInstance.close(data);
					}
				}
			}).result.then(function (data) {
				//$scope.mapTitle = data.title;
				//$scope.mapDescription = data.description;
			}, function () {
				Maps.showMakeKingpin = false;
			});

		}
	});

	$scope.$watch(function() {
		return Maps.coverVal;
	}, function(){
		if(Maps.coverType=='image'){
			if(Maps.coverType == ''){
				$scope.mapcover = '/img/mapcontrols/coverphoto.png';
			}else{
				$scope.mapcover = Maps.coverVal.dataUrl;
			}
		}else{
			if(Maps.coverType == ''){
				$scope.mapcover = '/img/mapcontrols/coverphoto.png';
			}else{
				$scope.mapcover = '/img/mapcontrols/video.png';
			}
		}
	});

	Maps.categories(function(data){
		Maps.mapCategory = data;
	});

	Maps.tags(function(data){

		console.log(data);
		var tt = [];
		for(var x in data){
			tt.push(data[x].tag);
		}
		Maps.mapTags = tt;
	});

	$scope.$watch(function() {
		return Maps.showMapInfo;
	}, function() {
		if (Maps.showMapInfo) {
			var modalInstance = $modal.open({
				templateUrl: 'mapInfo.html',
				controller: function($scope, $modalInstance, Maps){
					console.log('CONTROLLER FOR SHOW MAP');

						$scope.data = Maps.mapCategory;
						$scope.gmap = {
							'title': Maps.mapDetails.title,
							'description': Maps.mapDetails.description,
							'category': Maps.mapDetails.category,
							'tags': Maps.mapDetails.tags
						}

					$scope.select2Options = {
						'multiple': true,
						'simple_tags': true,
						'tags': []  // Can be empty list.
					};

					$scope.tag = Maps.mapTags;


					$scope.closeMapInfo = function(){
						console.log('Closing Modal Map Info');
						Maps.showMapInfo = false;
						$modalInstance.dismiss();
					}

					$scope.saveMapInfo = function(data)	{
						Maps.mapDetails.title = data.title;
						Maps.mapDetails.description = data.description;
						Maps.mapDetails.category = data.category;
						Maps.mapDetails.tags = data.tags;
						console.log(Maps.mapDetails);
						Maps.showMapInfo = false;
						$modalInstance.close(data);
					}
				}
			}).result.then(function (data) {
					console.log('Hitting THEN');
					$scope.mapTitle = data.title;
					$scope.mapDescription = data.description;

					var cats = [];

					for(var x in data.category){
						cats.push(data.category[x].cattitle);
					}
					console.log(cats.join());
					if(cats.length > 0){
						$scope.mapCategory = cats.join();
					}else{
						$scope.mapCategory = '';
					}

					if(data.tags.length){
						$scope.mapTags = data.tags.join();
					}else{
						$scope.mapTags = '';
					}

				}, function () {
					console.log('Hitting OTHERS');
					Maps.showMapInfo = false;
				});
		}
	});


	$scope.$watch(function() {
		return Maps.showMapCover;
	}, function() {
		if (Maps.showMapCover) {
			var modalInstance = $modal.open({
				templateUrl: 'mapCover.html',
				controller: function($scope, $modalInstance, Maps, toaster){
                    $scope.spinner = false;
					$scope.isImage = false;
					$scope.isVideo = false;
					$scope.file='';
					$scope.video='';
					$scope.cover = {
						coverPic: [],
						videolink: ''
					}

					$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
					$scope.generateThumb = function(file) {
						console.log(file);
						if (file != null) {
							$scope.file=file;
							if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
								$timeout(function() {
									var fileReader = new FileReader();
									fileReader.readAsDataURL(file);
									fileReader.onload = function(e) {
										$timeout(function() {
											file.dataUrl = e.target.result;
										});
									}
								});
							}
						}
					};

					$scope.viddisp = function(video) {
						if (video !== undefined) {
							var queryString = video.substring(video.indexOf('?') + 1);
							var param = parseQueryString(queryString);
							if (param.hasOwnProperty('v')) {
								$scope.video=video;
								$scope.videodisplay = $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" style="width:500px; height:300px;" allowfullscreen></iframe>');
							} else {
								$scope.video='';
								$scope.videodisplay = '';
								toaster.clear();
								toaster.pop('error', "", 'Invalid youtube link.');
							}
						} else {
							$scope.videodisplay = '';
						}
					}

					$scope.closeMapCover = function(){
						console.log('Closing Modal Map Cover');
						Maps.showMapCover = false;
						$modalInstance.dismiss();
					}

					$scope.saveMapCover = function(data, coverType)	{
						console.log('SAVING MAP COVER');
						if($scope.coverType=='image'){
							Maps.coverVal =$scope.file;
						}else{
							Maps.coverVal =$scope.video;
						}
						Maps.coverType=$scope.coverType;
						console.log(coverType);
						Maps.showMapCover = false;
						$modalInstance.close(data);
					}

					$scope.onTabSelect = function(val){
						$scope.coverType = val;
					}

					if(Maps.coverType==''){
						$scope.coverType = 'image';
						Maps.coverType = 'image';
						$scope.isImage = true;
						$scope.isVideo = false;

						if(Maps.coverVal){
							$scope.generateThumb(Maps.coverVal)
						}

					}else{
						$scope.coverType = Maps.coverType;

						if($scope.coverType=='image'){
							$scope.isImage = false;
						}else{
							$scope.isVideo = true;
						}

						console.log("OPENING VALUES");
						console.log($scope.isVideo);
						console.log(Maps.coverVal);

						if(Maps.coverVal && Maps.coverType=='image'){
							$scope.cover.coverPic.push(Maps.coverVal);
						}else if(Maps.coverVal && Maps.coverType=='video'){
							$scope.cover.videolink = Maps.coverVal;
							$scope.viddisp(Maps.coverVal);
						}
					}

				}
			}).result.then(function (data) {

				}, function(){
					Maps.showMapCover = false;
				});
		}
	});

	/* Saving Map Informations to Cookies1 */
	var saveCookieMap = function(data) {
		$log.log('Saving User Information');
		var array = $cookieStore.get('markersInfo');
		for (var i in array) {
			if (array[i].idKey == data.idKey) {
				array[i]['description'] = data.description;
				array[i]['videolink'] = data.videolink;
				array[i]['images'] = [];
				$cookieStore.put('markersInfo', array);
				return true;
			}
		}
		array.push(data);
		$cookieStore.put('markersInfo', array);
	}

	var viewCookieMap = function(id) {
		$log.log('Viewing Marker Information');
		var array = $cookieStore.get('markersInfo');
		for (var x in array) {
			if (array[x].idKey == id) {
				return array[x];
			}
		}
	}

	var savePinPic = function(pics, idkey) {
		var array = Maps.pinImages;
		for (var y in array) {
			if (array[y].idKey == idkey) {
				$log.log(array[y]['images']);
				$log.log(typeof array[y]['images'] == 'undefined');
				var arrayPic = array[y]['images'];
				pics.map(function(pic) {
					arrayPic.push(pic);
				});
				array[y]['images'] = arrayPic;
				$log.log('TEST==================================');
				$log.log(Maps.pinImages);
				Maps.pinImages = array;
				$scope.currentPinImages = arrayPic;
				return true;
			}
		}
	}

	var viewPinPic = function(id) {
		var array = Maps.pinImages;
		for (var x in array) {
			if (array[x].idKey == id) {
				return array[x].images;
			}
		}
	}

	// var saveMarkerPicInfo = function(pics, idkey){
	//     $log.log('Saving Marker Pitures1');
	//     $log.log(pics);
	//     var array = $cookieStore.get('markersInfo');
	//     for(var y in array){
	//         if(array[y].idKey == idkey){
	//             $log.log(array[y]['images']);
	//             $log.log(typeof array[y]['images'] == 'undefined');

	//                 var arrayPic = array[y]['images'];
	//                 pics.map(function(pic){
	//                     arrayPic.push(pic);
	//                 });
	//                 array[y]['images'] = arrayPic;
	//                 $log.log('==================================');
	//                 $log.log(arrayPic)
	//                 $scope.currentPinImages = arrayPic;
	//                 $cookieStore.put('markersInfo', array);
	//             return true;
	//         }
	//     }
	// }
	/* End Map Informations to Cookies*/

	// var showTrueMarker = function(id){
	//     for(var x in $scope.markers){
	//         if($scope.markers[x].idKey == id){
	//             $scope.markers[ x ].show=true;
	//             return x;
	//         }
	//     }
	// }

	//Login.redirectToMainifLogin();

	var createMarker = function(lat, long) {

		var pin = '';
		var kingpin = false;
		if ($scope.markers.length > 0) {
			pin = Maps.pin;
		} else {
			pin = Maps.kingpin;
			kingpin = true;
		}

		$scope.notiCheck = false;
		$scope.$apply(function() {
			var idmark = rfc4122.v4();
			var closeWindows = function() {
				for (var m in $scope.markers) {
					$scope.markers[m].show = false;
				}
			}
			$scope.markers.push({
				id: idmark,
				idKey: idmark,
				coords: {
					latitude: lat,
					longitude: long
				},
				options: {
					draggable: true,
					animation: google.maps.Animation.DROP,
					icon: pin
				},
				events: {
					dragend: function(marker, eventName, args) {
						//$log.log('marker dragend');
						var lat = marker.getPosition().lat();
						var lon = marker.getPosition().lng();
						Maps.setCurrentPin(args);
						$scope.notiCheck = false;
						//$log.log(lat);
						//$log.log(lon);
					},
					click: function(marker, eventName, args) {
						$scope.notiCheck = false;
						closeWindows();
						console.log('Click Marker');
						Maps.setCurrentPin(args);
						Maps.showTrueMarker(args.idKey);
						//$scope.markers[ args.idKey ].show=true;
						var array = viewCookieMap(args.idKey);
						if (array) {
							$scope.mk.description = array.description;
							$scope.mk.videolink = array.videolink;
							$scope.currentPinImages = viewPinPic(args.idKey)
							$scope.viddisp($scope.mk.videolink);
						} else {
							$scope.mk = {
								'description': '',
								'videolink': ''
							}
							$scope.videodisplay = '';
						}
						$scope.rightPanelInfo = Maps.getToggleMarkerInfo();
					}
				},
				show: true,
				markerInfo: {},
				cancel: true,
				kingpin: kingpin
			});
			Maps.markers = $scope.markers;
			$scope.mk = {
				'description': '',
				'videolink': ''
			}
			closeWindows();
			var mapid = Maps.showTrueMarker(idmark);
			Maps.currentPin = $scope.markers[mapid];
			$scope.rightPanelInfo = Maps.getToggleMarkerInfo();

			var mi = $cookieStore.get('markersInfo');
			mi.push({
				'idKey': idmark,
				'images': []
			});
			$cookieStore.put('markersInfo', mi);
			$scope.videodisplay = '';
			Maps.pinImages.push({
				'idKey': idmark,
				'images': []
			});
			$scope.currentPinImages = [];
		});
	    $scope.mk = {
	      'description': '',
	      'videolink': ''
	    }
		console.log(Maps.markers);
	};

	$scope.saveMarkerInfo = function(data) {
		console.log('Saving Marker Info');
		var pin = Maps.currentPin;
		data['idKey'] = pin.idKey;
		saveCookieMap(data);
		$scope.notiCheck = true;
	}

	$scope.prepare = function(files) {
		if (files && files.length) {
			//var markerpics = Maps.saveMarkerPics(files);
			var newData = [];

			//markerpics.then(function(values){
			files.map(function(file) {
				if (file.size >= 2000000) {
					console.log('File is too big!');
					$scope.flashError.push({
						'name': file.name
					});
				} else {
					newData.push(file);
				}
			});
			savePinPic(newData, Maps.currentPin.idKey)
			$timeout(function() {
				$scope.flashError = [];
			}, 10000);
			//});
		}
	};

	$scope.removepinpic = function(index) {
		console.log(index);
		var array = Maps.pinImages;
		for (var x in array) {
			if (array[x].idKey == Maps.currentPin.idKey) {
				array[x].images.splice(index, 1);
				$scope.currentPinImages = array[x].images;
				console.log(array);
				return Maps.pinImages = array;
			}
		}
	}

	$scope.removeerror = function(index) {
		$scope.flashError.splice(index, 1);
	}

	var parseQueryString = function(queryString) {
		var params = {},
			queries, temp, i, l;

		// Split into key/value pairs
		queries = queryString.split("&");

		// Convert the array of strings into an object
		for (i = 0, l = queries.length; i < l; i++) {
			temp = queries[i].split('=');
			params[temp[0]] = temp[1];
		}

		return params;
	};

	$scope.viddisp = function(video) {
		if (video !== undefined) {
			var queryString = video.substring(video.indexOf('?') + 1);
			var param = parseQueryString(queryString);
			if (param.hasOwnProperty('v')) {
				$scope.videodisplay = $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" width="100%" height="100%" allowfullscreen></iframe>');
			} else {
				$scope.videodisplay = '';
				toaster.clear();
				toaster.pop('error', "", 'Invalid youtube link.');
			}
		} else {
			$scope.videodisplay = '';
		}
	}

	$scope.hideDeleteButton = function() {
		$scope.showDelete = Maps.showDelete = false;
	}

	$scope.gotoview = function(){
		$scope.loaderText = 'Your map has been successfully generated.';
		$timeout(function () {
			$state.go('mapview', { map: Maps.mapSlugs });
		}, 2000);
	}
	var validations = function(){
		var errs = [];
		if(Maps.mapDetails.title=='Mission Title' || Maps.mapDetails.title =='' ) {
			errs.push('You might want to change your mission title.');
		}
		if(Maps.mapDetails.description=='Mission Description' || Maps.mapDetails.description == ''){
			errs.push('You might want to change your mission description.');
		}
		if(Maps.mapDetails.category==''){
			errs.push('You might want to select your category.');
		}
		if(!Maps.coverVal){
			errs.push('We require you to add a cover for your mission.');
		}else{
			if(Maps.coverType=='video'){
				try{
					var video = Maps.coverVal;
					var queryString = video.substring(video.indexOf('?') + 1);
					var param = parseQueryString(queryString);
					if (!param.hasOwnProperty('v')) {
						errs.push('Invalid Cover Youtube Link.');
					}
				}catch(e){
					console.log(e);
					errs.push('Invalid Cover Youtube Link.');
				}
			}
		}

		var mkinfo = $cookieStore.get('markersInfo');
		console.log(mkinfo);
		for (var m in mkinfo) {
			var dd = mkinfo[m];
				if(!dd.hasOwnProperty('description')){
					if(dd.hasOwnProperty('description') == ''){
						errs.push('You are required to add description to your markers.');
						break;
					}else{
						errs.push('You are required to add description to your markers.');
						break;
					}
				}
				if(dd.hasOwnProperty('videolink')){
					if(dd['videolink'] != ''){
						try{
							var video = dd['videolink'];
							var queryString = video.substring(video.indexOf('?') + 1);
							var param = parseQueryString(queryString);
							if (!param.hasOwnProperty('v')) {
								errs.push('Invalid Marker Youtube Link.');
								break;
							}
						}catch(e){
							console.log(e);
							errs.push('Invalid Marker Youtube Link.');
							break;
						}
					}
				}
		}

		if(errs.length == 0){
			return false;
		}else{
			return errs;
		}


	}
	$scope.saveMap = function(show) {

		if(validations()){
			var errs = validations();
			console.log(errs);
			var list = '';
			for(var x in errs){
				list = list + "<li>" + errs[x] + "</li>";
			}
			var errmess = '<ul>' + list + '</ul>';
			toaster.clear();
			toaster.pop('error', "You are required to add and change the following: ", errmess, null, 'trustedHtml');
		}else{

			//Display loader for saving

			var gmap = Maps.mapDetails;
			gmap['showauthor'] = show;
			$scope.loader = true;
			$scope.loaderText = 'Saving your map and markers information...';
			gmap['userid'] = Login.getCurrentUser()['id'];
			var mkinfo = $cookieStore.get('markersInfo');
			var mapmarker = Maps.markers;
			for (var m in mkinfo) {
				mapmarker.map(function(mk) {
					if (mkinfo[m]['idKey'] === mk['idKey']) {
						mkinfo[m]['latitude'] = mk['coords']['latitude'];
						mkinfo[m]['longitude'] = mk['coords']['longitude'];
					}
				});
			}
			gmap['markersInfo'] = mkinfo;
			gmap['cover'] = Maps.coverType == 'image' ? Maps.coverVal.name : Maps.coverVal;
			gmap['coverType'] = Maps.coverType;

			$http({
				url: appConfig.ResourceUrl + "/map/create/info",
				method: "POST",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data: $.param(gmap)
			}).success(function(data, status, headers, config) {
				$scope.loaderText = 'Creating your Mission Map...';
				console.log('CREATING MISSION MAP');
				console.log(data);
				Maps.mapSlugs= data.data.slugs;
				//Maps.mapSlugs =
				if (data.hasOwnProperty('200')) {
					//console.log(data);
					Maps.saveCoverPic(Maps.coverType, Maps.coverVal, data.data.id, function(mapdataid) {
						if (Maps.pinImages && Maps.pinImages.length) {
							$scope.loaderText = 'Uploading pin images...';
							var y = 0;
							Maps.pinImages.map(function(image) {
								if (image['images'] && image['images'].length) {
									Maps.saveMarkerPics(image['images'], image['idKey'], mapdataid, function(data) {
										if (y === (image['images'].length - 1)) {

										}else {
											$scope.loaderText = 'Your map has been successfully generated.';
											$scope.gotoview();
										}
										$scope.gotoview();
										y++;
									});
								}else {
									$scope.gotoview();
								}
							});
						}else {
							$scope.gotoview();
						}
					})
				} else {
					$scope.loaderText = 'An error occurred please try again later.';
					$timeout(function () {
						location.reload();
					}, 2000);
				}
				console.log(data);
			})
		}

	}

	$scope.checkfilechange = function(param1) {
		console.log(param1);
	}



});
