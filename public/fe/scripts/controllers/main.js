'use strict';

/* Controllers */

app.controller('MainCtrl', function($scope, Login, store, $window, ServiceMain, appConfig, $state, $http, $interval, $timeout, Notification, $modal, $anchorScroll, $sce, toaster) {

    /*
     * @type {Object}
     */
    $scope.sidebar = {
        'status': false
    }

    /*
     * Toggles the sidebar's status
     */
    $scope.sidebar.toggle = function() {
        $scope.sidebar.status = !$scope.sidebar.status;
    };

    /**
     * Search for a query
     * @param {Object} ev
     * @param {String} q
     */
    $scope.search = function(ev, q) {
        if (ev.which == 13) { // if ENTER key has been pressed, search for query
            if (q) { // if QUERY has text, start search
                $scope.query = null;
                return alert("You have searched for \"" + q + "\"."); // temporary return
            }
        }
    }

    /**
     * Send a request for subscription of newsletters
     * @param {Object} ev
     * @param {String} email
     */
    $scope.subscribe = function(ev, email) {
        if (ev.which == 13) { // if ENTER key has been pressed, send request
            if (email) { // if EMAIL has text, start request
                $scope.recipient = null;
                return alert("You have subscribed on our newsletters."); // temporary return
            }
        }
    }

    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        autoplay: 5000,
        // effect: 'cube',
        // cube: {
        //   slideShadows: false,
        //   shadow: false
        // }
    })

    console.log('==== Main Page ====');

    Login.getUserFromToken(function(data) {
        if (data.hasOwnProperty('username')) {
            $scope.loginmenu = data.firstname + " " + data.lastname;
            Login.loginmenu = $scope.loginmenu;
            $scope.username = data.username;

            $interval(function() {
                $http({
                    url: appConfig.ResourceUrl + '/agent/notification/' + data.id,
                    method: 'GET',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                }).success(function(data, status, headers, config) {
                    Notification.count.total = data.total;
                    Notification.count.pendingpins = data.pendingpin;
                }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
            }, 20000);

        } else {
            $scope.loginmenu = '';
        }
    })

    $scope.$watch(function() {
        return Login.loginmenu;
    }, function() {
        $scope.loginmenu = Login.loginmenu;
    });

    $scope.$watch(function() {
        return Notification.count.total;
    }, function() {
        $scope.notification = {
            total: Notification.count.total,
            pendingpin: Notification.count.pendingpins
        }
    });

    $scope.logout = function() {
        Login.logout(function(data) {
            if (data.hasOwnProperty('success')) {
                store.remove('AccessToken');
                $window.location = '/';
            }
        });
    }

    // $scope.loadindex = function(){

    // }

    $scope.seeMore = function(type) {
        store.set('SeeMore', type);
        $state.go('missionmaps', {});
    };

    $scope.addComments = function(com1, com2) {
        return com1 | 0 + com2 | 0;
    }

    // ServiceMain.getCategories(function(data) {
    //     $scope.categories = data;
    // });

    $scope.seeMoreCategories = function() {
        var modalInstance = $modal.open({
            templateUrl: 'otherCategories.html',
            controller: function($scope, $modalInstance) {

                ServiceMain.getCategories(function(data) {
                    var startnextindex = Math.ceil(data.length / 2);
                    var col1 = [];
                    console.log(startnextindex);
                    for (var x = 0; x < startnextindex; x++) {
                        col1.push(data[x]);
                    }
                    var col2 = [];
                    for (var y = startnextindex; y < data.length; y++) {
                        col2.push(data[y]);
                    }
                    console.log(col1);
                    console.log(col2);
                    $scope.col1 = col1;
                    $scope.col2 = col2;
                });

                $scope.closeMapInfo = function() {
                    $modalInstance.dismiss();
                }

                $scope.clickCatInfo = function(data) {

                    $modalInstance.close(data);
                }
            }
        }).result.then(function(data) {
            $anchorScroll()
        }, function() {

        });
    }

    var parseQueryString = function(queryString) {
        var params = {},
            queries, temp, i, l;

        // Split into key/value pairs
        queries = queryString.split("&");

        // Convert the array of strings into an object
        for (i = 0, l = queries.length; i < l; i++) {
            temp = queries[i].split('=');
            params[temp[0]] = temp[1];
        }

        return params;
    };

    $scope.viewNews = function(news) {
        var modalInstance = $modal.open({
            templateUrl: 'viewNews.html',
            controller: function($scope, $modalInstance, news, appConfig) {
                $scope.title = "News";
                $scope.news = news;
                $scope.deleteshow = false;
                $scope.editshow = false;
                $scope.agentid = "jeevon";
                $scope.amazonlink = appConfig.bucket;
                $http({
                    url: appConfig.ResourceUrl + '/main/getauthor/' + news.author,
                    method: 'GET',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                }).success(function(data, status, headers, config) {
                    $scope.author = data;
                }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });

                $scope.close = function() {
                    $modalInstance.close();
                }
                if (news.imagethumb != "") {
                    $scope.news.cover = appConfig.bucket + "uploads/newsimage/" + news.imagethumb;
                    $scope.news.coverType = 'image';
                } else {
                    var video = news.videothumb;
                    if (video !== undefined) {
                        var queryString = video.substring(video.indexOf('?') + 1);
                        var param = parseQueryString(queryString);
                        if (param.hasOwnProperty('v')) {
                            $scope.video = video;
                            $scope.news.coverType = 'video';
                            $scope.videodisplay = $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" style="width:500px; height:300px;" allowfullscreen></iframe>');
                        } else {
                            $scope.news.coverType = 'video';
                            $scope.videodisplay = $sce.trustAsHtml(video.replace('width="560" height="315"', 'style="width:500px; height:300px;"'));
                        }
                    } else {
                        $scope.videodisplay = '';
                    }
                }

            },
            resolve: {
                news: function() {
                    return news;
                }
            }
        });
    }

    var viewNews = function(news) {
        $scope.viewNews(news);
    }

    var loadednews = 3;

    $scope.viewmorenews = function() {
        var modalInstance = $modal.open({
            templateUrl: 'viewNewsList.html',
            controller: function($scope, $modalInstance, appConfig) {
                $scope.title = "News";
                var loadnews = function() {
                    loadednews += 2;

                    $http({
                        url: appConfig.ResourceUrl + '/index/newslist/' + loadednews,
                        method: 'GET',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    }).success(function(data, status, headers, config) {
                        $scope.news = data.news;
                        console.log($scope.news);
                        $scope.count = data.count;
                        angular.forEach($scope.news, function(value, key) {
                            if (value['imagethumb'] == '' || value['imagethumb'] == null) {
                                $scope.news[key]['coverType'] = 'video';
                                $scope.news[key]['cover'] = value['videothumb'];
                            } else {
                                $scope.news[key]['cover'] = appConfig.bucket + "uploads/newsimage/" + value['imagethumb'];
                                $scope.news[key]['coverType'] = 'image';
                            }
                        });
                    }).error(function(data, status, headers, config) {
                        console.log("error");
                    });
                }

                loadnews();

                $scope.loadmore = function() {
                    loadnews();
                }

                $scope.viewnews = function(news) {
                    viewNews(news);
                }

                $scope.close = function() {
                    $modalInstance.dismiss();
                }
            }
        });
    }
})
