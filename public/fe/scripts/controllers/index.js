'use strict';

/* Controllers */

app.controller('IndexCtrl', function($scope, Login, store, $window, ServiceMain, appConfig, $state, $http, $interval, Notification, $modal, $anchorScroll, $sce, toaster){
    console.log("HITTING CONTROLLERS INDEX");
    ServiceMain.indexlist(function(data){
        console.log(data);
        $scope.mainfeature = data.featured[0];
        $scope.bucketUrl = appConfig.bucket;

        $scope.featured = data.featured;
        $scope.featured.splice(0, 1);

        $scope.greatest = data.greatest;
        $scope.latest = data.latest;
        $scope.popular = data.popular;
        //$scope.categories = data.categories;
        $scope.news = data.news;
    })
})
