'use strict';

/* Controllers */

app.controller('AboutPiCtrl', function($timeout,$scope, $state, $anchorScroll, $location, store, Maps, appConfig) {
    $scope.total = {
        action: 0,
        agents: 0,
        ideas: 0
    };

    $scope.map = {
        center: {
            latitude: 0,
            longitude: 0
        },
        zoom: 2,
        events: {
            click: function(event, a, b) {
                if (addpin) {
                    createMarker(b[0].latLng.lat(), b[0].latLng.lng());
                }
            }
        },
        refresh: false
    };

    $scope.options = {
        styles: appConfig.googleMapStyle
    };

    //put markers to map
    var pushmarker = function(data){
        if(data.show){
            $scope.missionmarkers.push({
                id: data.marker.id,
                idKey: data.marker.id,
                agentid: data.marker.agent,
                agentname: (data.marker.hide_agent==1 ? data.marker.username.length > 9 ? data.marker.username.substr(0, 9) + "..." : data.marker.username : "Anonymous"),
                agentpic: (data.marker.profile_pic_name != null && data.marker.hide_agent==1 ? appConfig.bucket + "uploads/agentpic/" + data.marker.agent + "/" + data.marker.profile_pic_name : appConfig.BaseUrl + "/img/agentdefault.png"),
                coords: {
                    latitude: data.marker.lat,
                    longitude: data.marker.long
                },
                options: {
                    draggable: data.draggable,
                    animation: google.maps.Animation.DROP,
                    icon: data.pin
                },
                events: data.event,
                show: false,
                markerInfo: {},
                cancel: true,
                WindowWithCustomClass: {
                    options: {
                        boxClass: 'custom-info-window view',
                        disableAutoPan: true
                    }
                }
            });
        }else {
            console.log(data);
            $scope.markers.push({
                id: data.marker.id,
                idKey: data.marker.id,
                agentid: data.marker.agent,
                agentname: "",
                coords: {
                    latitude: data.marker.lat,
                    longitude: data.marker.long
                },
                options: {
                    draggable: data.draggable,
                    animation: google.maps.Animation.DROP,
                    icon: data.pin
                },

                events: data.event,
                show: false,
                markerInfo: {},
                cancel: true,
                WindowWithCustomClass: {
                    options: {
                        boxClass: 'custom-info-window view',
                        disableAutoPan: true
                    }
                }
            });
        }
    }

    Maps.getallpin(function(data){
        console.log("==============get all pins");
        console.log(data);


        angular.forEach(data, function(value, key){
            var pin = '';
            if (value['pin'] == 0 && value['status'] == 1) {
                pin = Maps.pin;
            } else if(value['pin'] == 1 && value['status'] == 1){
                pin = Maps.kingpin;
            } else if(value['pin'] == 0 && value['status'] == 0){
                pin = Maps.pendingpin;
            }

            var marker = {
                marker: data[key],
                pin: pin,
                event: {
                    click: function(marker, eventName, args) {
                        var modalInstance = $modal.open({
                            templateUrl: 'viewPin.html',
                            controller: viewPinCtrl,
                            size: 'lg',
                            resolve: {
                                id: function() {
                                    return args.idKey;
                                }
                            }
                        }).result.finally(function(){
                                getcomments();
                            });
                    },
                    mouseover: function(marker, eventName, args){
                        angular.forEach($scope.missionmarkers, function(value, key) {
                            if(value['id'] == args.idKey){
                                $scope.missionmarkers[key].show = true;
                            }
                        });
                    },
                    mouseout: function(marker, eventName, args) {
                        angular.forEach($scope.missionmarkers, function(value, key) {
                            if(value['id'] == args.idKey){
                                $scope.missionmarkers[key].show = false;
                            }
                        });
                    }
                },
                draggable: false,
                show: true
            }
            if(value['status'] == 1 || (value['status'] == 0 && Login.getCurrentUser() != null && (Login.getCurrentUser().id == data.map.agent || Login.getCurrentUser().id == value['agent']))){
                pushmarker(marker);
            }
        });
        Maps.missionmarkers = $scope.missionmarkers;

    })

    Maps.getcounts(function(data){
        console.log("==============get counts");
        console.log(data);
        $scope.total = data
    })


    $scope.searchbox = {
        template:'searchbox.tpl.html',
        position:'TOP_LEFT',
        //position: 'BOTTOM',
        options: { bounds: {} } ,
        //parentdiv:'searchBoxParent',
        events: {
            places_changed: function (searchBox) {
                var place = searchBox.getPlaces();
                if (!place || place == 'undefined' || place.length == 0) {
                    console.log('no place data :(');
                    return;
                }

                $scope.map.center.latitude = place[0].geometry.location.lat();
                $scope.map.center.longitude = place[0].geometry.location.lng();
                $scope.map.zoom = 5;
            }
        }
    };




    var getmap = function() {
        $scope.missionmarkers = [];
        $scope.PinImages = [];
        $scope.mapdetails = [];
        Maps.getallpin(function(data) {

            angular.forEach(data.markers, function(value, key){
                var pin = '';
                if (value['pin'] == 0 && value['status'] == 1) {
                    pin = Maps.pin;
                } else if(value['pin'] == 1 && value['status'] == 1){
                    pin = Maps.kingpin;
                } else if(value['pin'] == 0 && value['status'] == 0){
                    pin = Maps.pendingpin;
                }

                var marker = {
                    marker: data.markers[key],
                    pin: pin,
                    event: {
                        click: function(marker, eventName, args) {
                            var modalInstance = $modal.open({
                                templateUrl: 'viewPin.html',
                                controller: viewPinCtrl,
                                size: 'lg',
                                resolve: {
                                    id: function() {
                                        return args.idKey;
                                    }
                                }
                            }).result.finally(function(){
                                    getcomments();
                                });
                        },
                        mouseover: function(marker, eventName, args){
                            angular.forEach($scope.missionmarkers, function(value, key) {
                                if(value['id'] == args.idKey){
                                    $scope.missionmarkers[key].show = true;
                                }
                            });
                        },
                        mouseout: function(marker, eventName, args) {
                            angular.forEach($scope.missionmarkers, function(value, key) {
                                if(value['id'] == args.idKey){
                                    $scope.missionmarkers[key].show = false;
                                }
                            });
                        }
                    },
                    draggable: false,
                    show: true
                }
                if(value['status'] == 1 || (value['status'] == 0 && Login.getCurrentUser() != null && (Login.getCurrentUser().id == data.map.agent || Login.getCurrentUser().id == value['agent']))){
                    pushmarker(marker);
                }
            });
            Maps.missionmarkers = $scope.missionmarkers;

            $timeout(function(){
                FB.XFBML.parse();
            }, 100);
        });
    }

    getmap();


})
