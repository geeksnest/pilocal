app.filter('trusted', ['$sce', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);
app.filter('trustedHtml', ['$sce', function ($sce) {
    return function(elm) {
        return $sce.trustAsHtml(elm);
    };
}]);
app.filter('returnYoutubeThumb', function(){
    return function (video) {
        var parseQueryString = function(queryString) {
            var params = {},
                queries, temp, i, l;

            // Split into key/value pairs
            queries = queryString.split("&");

            // Convert the array of strings into an object
            for (i = 0, l = queries.length; i < l; i++) {
                temp = queries[i].split('=');
                params[temp[0]] = temp[1];
            }

            return params;
        };
        if(video){
            var queryString = video.substring(video.indexOf('?') + 1);
            var param = parseQueryString(queryString);
            return 'http://img.youtube.com/vi/' + param.v + '/hqdefault.jpg';
        }else{
            return x;
        }
    };
});

app.filter('returnImageThumb', function(appConfig){
    return function (item) {
        if(item){
            return appConfig.bucket + "/uploads/newsimage/" + item;
        }else{
            return item;
        }
    };
})

app.filter('returnYoutubeEmbed', function($sce) {
    return function(video) {
        var parseQueryString = function(queryString) {
            var params = {},
                queries, temp, i, l;

            // Split into key/value pairs
            queries = queryString.split("&");

            // Convert the array of strings into an object
            for (i = 0, l = queries.length; i < l; i++) {
                temp = queries[i].split('=');
                params[temp[0]] = temp[1];
            }

            return params;
        };

        if (video !== undefined) {
            var queryString = video.substring(video.indexOf('?') + 1);
            var param = parseQueryString(queryString);
            if (param.hasOwnProperty('v')) {
                return $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" width="100%" height="auto" allowfullscreen></iframe>');
            }
        }
    }
});

app.filter('formatdate',function() {
    return function(input) {
        if (input) {
            return  moment(input).format("MMMM DD, YYYY");
        }
    }
});

app.filter('formatdatetime',function() {
    return function(input) {
        if (input) {
            // var m = moment(input).subtract(-6, 'hours');
            // return  m.subtract(-10, 'days').days() > 0 ? moment(input).format("LLLL") : moment(input).subtract('hours', -6).fromNow();
            return moment(input).format("LLLL");
        }
    }
});
