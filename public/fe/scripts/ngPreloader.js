/**
 * Created by ebautistajr on 4/7/15.
 */
angular.module('ngMaterialPreloader', []).directive('ngMaterialPreloader', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element = new $.materialPreloader(scope.$eval(attrs.materialPreloader));

      element.on();
    }
  };
});