'use strict';
var init_module = [
    'ngAnimate',
    'ngCookies',
    'angular-storage',
    'ngResource',
    'uuid',
    'ngFileUpload',
    'ui.router',
    'ui.bootstrap',
    'ui.bootstrap.datepicker',
    'uiGmapgoogle-maps',
    'vcRecaptcha',
    'angular-jwt',
    'videosharing-embed',
    'ui.load',
    'ngMaterialPreloader',
    'angularUtils.directives.dirPagination',
    'toaster',
    'ngSanitize',
    'angular.chosen',
    'ui.select',
    'angularMoment',
    'xeditable',
    // 'ngCkeditor'
];

var app = angular.module('PiApp', init_module)
    .run(['$rootScope', '$state', '$stateParams', 'store', 'jwtHelper', '$http', 'appConfig', 'editableOptions', 'editableThemes',
        function($rootScope, $state, $stateParams, store, jwtHelper, $http, appConfig, editableOptions, editableThemes) {
            editableOptions.theme = 'bs3';
            editableThemes.bs3.inputClass = 'input-sm';
            editableThemes.bs3.buttonsClass = 'btn-sm';
            editableThemes['bs3'].submitTpl = '<button class="btn btn-sm btn-icon btn-pi"><icon class="glyphicon glyphicon-ok"></icon></button>';
            editableThemes['bs3'].cancelTpl = '<button type="button" class="btn btn-sm btn-icon btn-danger"  ng-click="$form.$cancel()"><icon class="fa fa-ban"></icon></button>';
            
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            console.log('============================');
            console.log(store.get('AccessToken'));

            if (store.get('AccessToken') == null) {
                console.log('== Getting Access Log ==');
            }else{
                $http({
                    url: appConfig.ResourceUrl + '/authenticate/get',
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization': 'Bearer ' + store.get('AccessToken')
                    }
                }).success(function(data, status, headers, config) {
                    console.log(data);
                })
            }

            $rootScope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams) {
                    $('#materialPreloader').fadeIn(200);
                });

            $rootScope.$on('$stateChangeSuccess',
                function(event, toState, toParams, fromState, fromParams) {
                    $('#materialPreloader').fadeOut(200);
                });
        }
    ])
    .config(
        ['$locationProvider', '$logProvider', '$httpProvider', '$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$interpolateProvider', '$parseProvider', '$resourceProvider', 'jwtInterceptorProvider', 'uiGmapGoogleMapApiProvider',
            function($locationProvider, $logProvider, $httpProvider, $stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $interpolateProvider, $parseProvider, $resourceProvider, jwtInterceptorProvider, uiGmapGoogleMapApiProvider) {

                // lazy controller, directive and service
                app.controller = $controllerProvider.register;
                app.directive = $compileProvider.directive;
                app.factory = $provide.factory;
                app.constant = $provide.constant;
                app.value = $provide.value;
                app.filter = $filterProvider.register;

                $interpolateProvider.startSymbol('{[{');
                $interpolateProvider.endSymbol('}]}');
                $resourceProvider.defaults.stripTrailingSlashes = false;

                 $httpProvider.interceptors.push(['$q', '$location', 'store', function($q, $location, store) {
                     return {
                         'request': function (config) {
                             config.headers = config.headers || {};
                             if (store.get('AccessToken')) {
                                 $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + store.get('AccessToken');
                             }
                             return config;
                         },
                         'responseError': function(response) {
                             if(response.status === 401 || response.status === 403) {
                                 store.remove('AccessToken')
                                 window.location = '/';
                             }
                             return $q.reject(response);
                         }
                     };
                 }]);

                $urlRouterProvider
                    .otherwise('/');

                $stateProvider
                    //home page
                    .state('/', {
                        url: "/",
                        templateUrl: "/page/mainpage",
                        resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                            deps: ['uiLoad',
                                function(uiLoad) {
                                    return uiLoad.load([
                                        '/fe/scripts/controllers/index.js',
                                        '/fe/scripts/factory/user.js',
                                        '/fe/scripts/factory/mapQuery.js'
                                    ]);
                                }
                            ]
                        }
                    })
                    //about
                    .state('about', {
                        url: "/about",
                        templateUrl: "/page/about",
                        controller: 'AboutPiCtrl',
                        resolve: {
                            deps: ['uiLoad',
                                function(uiLoad) {
                                    return uiLoad.load([
                                        '/fe/scripts/controllers/about.js',
                                        '/fe/scripts/factory/maps.js',
                                    ]);
                                }
                            ]
                        }
                    })
                    //contact us
                    .state('contactus', {
                        url: "/contact-us",
                        controller: 'ContactusCtrl',
                        templateUrl: "/page/contactus",
                        resolve: {
                            deps: ['uiLoad',
                                function(uiLoad) {
                                    return uiLoad.load([
                                        '/fe/scripts/controllers/contactus.js',
                                        '/fe/scripts/factory/contactus.js'
                                    ]);
                                }
                            ]
                        }
                    })
                    //search
                    .state('search', {
                        url: "/search",
                        templateUrl: "/page/search",
                        controller: "searchCtrl",
                        resolve: {
                            deps: ['uiLoad', 
                                function(uiLoad) {
                                    return uiLoad.load([
                                        '/fe/scripts/controllers/search.js',
                                        '/fe/scripts/factory/mapQuery.js'
                                    ]);
                                }
                            ]
                        }
                    })
                    //privacy policy
                    .state('policy', {
                        url: "/privacy-policy",
                        templateUrl: "/page/policy"
                    })
                    //Terms of Service
                    .state('termofservice', {
                        url: "/terms-of-service",
                        templateUrl: "/page/termsofservice"
                    })
                    //login
                    .state('login', {
                        url: "/login",
                        templateUrl: '/page/login',
                        resolve: {
                            deps: ['uiLoad',
                                function(uiLoad) {
                                    return uiLoad.load([
                                        '/fe/scripts/controllers/login.js',
                                        '/fe/scripts/factory/user.js'
                                    ]);
                                }
                            ]
                        }
                    })
                    //registration
                    .state('registration', {
                        url: "/registration",
                        templateUrl: '/page/registration',
                        resolve: {
                            deps: ['uiLoad',
                                function(uiLoad) {
                                    return uiLoad.load([
                                        '/fe/scripts/controllers/user/registration.js',
                                        '/fe/scripts/factory/birthday.js',
                                        '/fe/scripts/factory/user.js',
                                        '/fe/scripts/directives/validations.js',
                                        '/vendors/pass-strength/src/strength.js'
                                    ]);
                                }
                            ]
                        }
                    })
                    //registration success
                    .state('success', {
                        url: "/success",
                        templateUrl: "/page/success"
                    })
                    //registration activation
                    .state('activate', {
                        url:"/activation/:code",
                        templateUrl: "/page/activation",
                        controller: function($scope, $stateParams, $http, appConfig){
                            var data = {
                                code: $stateParams.code
                            }
                            $http({
                                url: appConfig.ResourceUrl + '/user/activation',
                                method: 'post',
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                data: $.param(data)
                            }).success(function(data, status, headers, config) {

                                $scope.data = {
                                    msg: data.hasOwnProperty('success') ? data.success : data.error,
                                    type: data.hasOwnProperty('success') ? "success" : "danger"
                                }
                            }).error(function(data, status, headers, config) {
                                console.log(data);
                            });
                        }
                    })
                    //create map start
                    .state('mapstart', {
                        url: "/map/start",
                        templateUrl: "/page/startmission",
                        controller: function($scope, $state, Login){
                            $scope.create = function(){
                                if(Login.getCurrentUser() != null){
                                    $state.go('mapcreate');
                                }else {
                                    $state.go('login');
                                }
                            }
                        }
                    })
                    //create
                    .state('mapcreate', {
                        url: "/map/create",
                        controller: 'MapsCreateCtrl',
                        templateUrl: '/page/createmission',
                        resolve: {
                            deps: ['uiLoad',
                                function(uiLoad) {
                                    return uiLoad.load([
                                        '/fe/scripts/controllers/login.js',
                                        '/fe/scripts/controllers/map/create.js',
                                        '/fe/scripts/controllers/map/infowindow.js',
                                        '/fe/scripts/factory/user.js',
                                        '/fe/scripts/factory/maps.js',
                                        '/fe/scripts/factory/mapQuery.js',
                                        '/fe/scripts/directives/validations.js',
                                        '/fe/scripts/directives/maps.js'
                                    ]);
                                }
                            ],
                            currentLocation: function($q) {
                                var q = $q.defer();
                                navigator.geolocation.getCurrentPosition(function(pos) {
                                      console.log('Position=')
                                      console.log(pos);
                                      q.resolve(pos);

                                  }, function(error) {
                                      console.log('Got error!');
                                      console.log(error);
                                      var latLong = {coords : { 'latitude': 41.504189 , 'longitude': -97.778320}};
                                      q.resolve(latLong);
                                  });
                                  return q.promise;
                            }
                        }
                    })
                    //list
                    .state('missionmaps', {
                        url: "/mission-maps",
                        templateUrl: "/page/listmission",
                        controller: 'listCtrl',
                        resolve: {
                            deps:['uiLoad',
                                function(uiLoad){
                                    return uiLoad.load([
                                        '/fe/scripts/controllers/map/list.js',
                                        '/fe/scripts/factory/maps.js',
                                        '/fe/scripts/factory/mapQuery.js'
                                    ]);
                                }
                            ]
                        }
                    })
                    //mission map details
                    .state('mapview', {
                        url: "/action-map/:map",
                        templateUrl: function (stateParams){
                            return '/page/viewmission/' + stateParams.map;
                        },
                        controller: 'MapsViewCtrl',
                        resolve: {
                            deps: ['uiLoad',
                                function(uiLoad) {
                                    return uiLoad.load([
                                        '/fe/scripts/controllers/map/view.js',
                                        '/fe/scripts/factory/user.js',
                                        '/fe/scripts/factory/maps.js',
                                        '/fe/scripts/factory/comments.js',
                                        '/fe/scripts/factory/mapQuery.js',
                                        '/fe/scripts/directives/directives.js',
                                        '/fe/scripts/directives/validations.js',
                                        '/fe/scripts/filter.js'
                                    ]);
                                }
                            ]
                        }
                    })
                    .state('profile', {
                        url: "/profile/:username",
                        controller: 'ProfileCtrl',
                        templateUrl: '/page/profile',
                        resolve: {
                            deps: ['uiLoad',
                                function(uiLoad) {
                                    return uiLoad.load([
                                        '/fe/scripts/controllers/user/profile.js',
                                        '/fe/scripts/factory/birthday.js',
                                        '/fe/scripts/factory/user.js',
                                        '/fe/scripts/factory/mapQuery.js',
                                        '/fe/scripts/directives/validations.js',
                                        '/vendors/pass-strength/src/strength.min.js'
                                    ]);
                                }
                            ]
                        }
                    })
                    .state('missionmanager', {
                        url: "/mission-manager",
                        controller: 'MissionMgrCtrl',
                        templateUrl: '/page/missionmanager',
                        resolve: {
                            deps: ['uiLoad',
                                function(uiLoad){
                                    return uiLoad.load([
                                        '/fe/scripts/controllers/user/missionmanager.js',
                                        '/fe/scripts/factory/login.js',
                                        '/fe/scripts/factory/maps.js',
                                        '/fe/scripts/factory/mapQuery.js',
                                        '/fe/scripts/factory/comments.js',
                                        '/fe/scripts/directives/directives.js'
                                    ]);
                                }
                            ]
                        }
                    })
                    .state('account', {
                        url: "/account",
                        controller: 'accountCtrl',
                        templateUrl: '/page/account',
                        resolve: {
                            deps: ['uiLoad',
                                function(uiLoad){
                                    return uiLoad.load([
                                        '/fe/scripts/controllers/user/account.js',
                                        '/fe/scripts/factory/user.js',
                                        '/fe/scripts/directives/validations.js',
                                        '/vendors/pass-strength/src/strength.js'
                                    ]);
                                }
                            ]
                        }
                    })
                    .state('category', {
                        url: "/category/:catslug",
                        controller: 'categoryCtrl',
                        templateUrl: '/page/category',
                        resolve: {
                            deps: ['uiLoad',
                                function(uiLoad){
                                    return uiLoad.load([
                                        '/fe/scripts/controllers/map/category.js',
                                        '/fe/scripts/factory/mapQuery.js',
                                        '/fe/scripts/factory/maps.js'
                                    ]);
                                }
                            ]
                        }
                    });

                // use the HTML5 History API
                $locationProvider.html5Mode(true);

                uiGmapGoogleMapApiProvider.configure({
                    key: 'AIzaSyAkgfUylY29OvxzKm5uh8j2NHc9hhBMjW0',
                    v: '3.17',
                    libraries: 'places,weather,geometry,visualization'
                });

            }
        ]
    )
