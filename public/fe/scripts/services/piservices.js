/**
 * Created by Apple on 9/18/15.
 */
app.service('PiServices', function($sce) {
    var parseQueryString = function(queryString) {
        var params = {},
            queries, temp, i, l;

        // Split into key/value pairs
        queries = queryString.split("&");

        // Convert the array of strings into an object
        for (i = 0, l = queries.length; i < l; i++) {
            temp = queries[i].split('=');
            params[temp[0]] = temp[1];
        }

        return params;
    };
    this.viddisp = function(video) {
        if (video !== undefined) {
            var queryString = video.substring(video.indexOf('?') + 1);
            var param = parseQueryString(queryString);
            if (param.hasOwnProperty('v')) {
                return $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" width="100%" height="100%" allowfullscreen></iframe>');
            } else {
                return '';
            }
        } else {
            return '';
        }
    }
    this.setMetaTags = function(){
        $("meta[property='og\\:title']").attr("content", 'TITLE FB');
        $("meta[property='og\\:type']").attr("content", 'website');
        $("meta[property='og\\:description']").attr("content", 'DESCRIPTION FB');
        $("meta[property='og\\:image']").attr("content", '/img/frontend/logo.png');
        $("meta[name='keywords']").attr("content", 'KEYWORDS DITO ITO');
        $("meta[name='description']").attr("content", 'DESCRIPTION ITO');
        $("meta[name='author']").attr("content", 'AUTHOR ITO');
    }
});