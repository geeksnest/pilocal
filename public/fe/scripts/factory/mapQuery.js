/**
 * Created by ebautistajr on 3/30/15.
 */

app.factory('mapQuery', function($http, store, jwtHelper, $window, appConfig) {
    return {
        titleValidation: function (data, callback) {
            $http({
                url: appConfig.ResourceUrl + '/map/titleexist/' + data.title,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
            return;
        },
        getMaps: function(num, off, status, cat ,  callback) {
            $http({
                url: appConfig.ResourceUrl + '/agent/missions/list/' + num +'/' + off + '/' + status + '/' + cat,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        getPins: function(title, agent, callback) {
            $http({
                url: appConfig.ResourceUrl + '/map/getpins/' + title + '/' + agent,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        savePins: function(data, callback) {
            $http({
                url: appConfig.ResourceUrl + '/map/savepin',
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        deletePin: function(id, callback) {
            $http({
                url: appConfig.ResourceUrl + '/map/deletepins/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        getpinAgent: function(id, callback) {
            $http({
                url: appConfig.ResourceUrl + '/agent/getpinAgent/' + id,
                method: 'get',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        slides: function(images, pinid, mapid, callback) {
            var slides =[];
            var x = 0;
            angular.forEach(images, function(value, key) {
                if(pinid == value['marker_id']){
                    slides.push({ id:value['id'], image: appConfig.bucket + 'uploads/maps/' + mapid + '/' + pinid + '/' + value['filename'], type: "image" });
                    x++;
                }
            });
            callback(slides);
        },
        addpinview: function(id, callback) {
            $http({
                url: appConfig.ResourceUrl + '/pin/addview/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        setKingPin: function(id, mapid, callback) {
            $http({
                url: appConfig.ResourceUrl + '/pin/setkingpin/' + id + '/' + mapid,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        getSlides: function(pinid, callback) {
            $http({
                url: appConfig.ResourceUrl + '/getpin/images/' + pinid,
                method: 'get',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        updatePinDesc: function(id, desc, callback) {
            $http({
                url: appConfig.ResourceUrl + '/pin/editdesc/' + id + '/' + desc,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        getFblikes: function(slug, callback) {
            $http({
                url: "https://api.facebook.com/method/fql.query?query=select%20total_count,like_count,share_count,click_count%20from%20link_stat%20where%20url=%22" + appConfig.BaseUrl + "/%23/action-map/" + slug +"%22&format=json",
                method: 'get',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        // getpinComments: function(id, callback) {
        //     $http({
        //         url: appConfig.ResourceUrl + "/getpincomments/" + id,
        //         method: 'get',
        //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        //     }).success(function(data, status, headers, config) {
        //         callback(data);
        //     }).error(function(data, status, headers, config) {
        //             // called asynchronously if an error occurs
        //             // or server returns response with an error status.
        //     });
        // },
        getpin: function(id, callback) {
            $http({
                url: appConfig.ResourceUrl + "/getpin/" + id,
                method: "get",
                headers: {"Content-Type" : "application/x-www-form-urlencoded"}
            }).success(function(data, status, headers, config) {    
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        },
        editmapinfo: function(id, data, callback) {
            $http({
                url: appConfig.ResourceUrl + '/map/update/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        updateCover: function(id, data, type, callback) {
            $http({
                url: appConfig.ResourceUrl + '/map/updatecover/' + id + '/' + type,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        deletepinimg: function(id, callback) {
            $http({
                url: appConfig.ResourceUrl + '/pin/deleteimg/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        updatevid: function(id, data, callback){
            $http({
                url: appConfig.ResourceUrl + '/pin/updatevideo/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({video:data})
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        hideagent: function(id, hideagent, callback) {
            $http({
                url: appConfig.ResourceUrl + '/pin/hideagent/' + id + '/' + hideagent,
                method: 'get',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        approvepins: function(id, val, callback) {
            $http({
                url: appConfig.ResourceUrl + '/map/approvepins/' + id + '/' + val,
                method: 'get',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        },
        maphideagent: function(id, hideagent, callback) {
            $http({
                url: appConfig.ResourceUrl + '/map/hideagent/' + id + '/' + hideagent,
                method: 'get',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        pendingpinAction: function(id, action, callback){
            $http({
                url: appConfig.ResourceUrl + '/pin/pendingpinAction/' + id + '/' + action,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        getagentMaps: function(id, callback) {
            $http({
                url: appConfig.ResourceUrl + '/getagentMaps/' + id,
                method: 'get',
                headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        getmapCatTags: function(id, callback) {
            $http({
                url: appConfig.ResourceUrl + '/getmapCatTags/' + id,
                method: 'get',
                headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        getTags: function(callback) {
            $http({
                url: appConfig.ResourceUrl + '/getpopulartags',
                method: 'get',
                headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        searchMap: function(off, keyword, category, tag, callback) {
            $http({
                url: appConfig.ResourceUrl + '/searchMap/' + off + '/' + keyword + '/' + category + '/' + tag,
                method: 'get',
                headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        changeStatus: function(id, stat, callback) {
            $http({
                url: appConfig.ResourceUrl + '/map/changestatus/' + id + '/' + stat,
                method: 'post',
                headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        saveNews: function(id, news, callback) {
            $http({
                url: appConfig.ResourceUrl + '/mapnews/save/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(news)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        getnewslist: function(id, callback) {
            $http({
                url: appConfig.ResourceUrl + '/map/getnewslist/' + id,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        getnews: function(mapid, limit, callback) {
            $http({
                url: appConfig.ResourceUrl + '/map/getnews/' + mapid + '/' + limit,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        deletenews: function(id, callback) {
            $http({
                url: appConfig.ResourceUrl + '/map/deletenews/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        updateNews: function(id, data, callback) {
            $http({
                url: appConfig.ResourceUrl + '/map/updatenews/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        loadgallery: function(id, callback) {
            $http({
                url: appConfig.ResourceUrl + '/superagent/loadmapgallery/' + id,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        saveCover: function(fileout, id, type, callback){
            $http({
                url: appConfig.ResourceUrl + "/map/saveimage/" + id + '/' + type,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fileout)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        deleteMedia: function(fileout, id, type, callback){

            $http({
                url: appConfig.ResourceUrl + "/map/deletemapmedia/" + id + '/' + type,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fileout)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }
    }
});
