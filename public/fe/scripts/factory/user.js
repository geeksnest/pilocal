app.factory('User', function($http, store, jwtHelper, appConfig, Upload){
    return {
        usernameExists: function(data, callback) {

            $http({
                url: appConfig.ResourceUrl + '/agent/usernameexist/' + data.username,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
            return;
        },
        emailExists: function(data, callback) {
            $http({
                url: appConfig.ResourceUrl + '/agent/emailexist/' + data.email,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
            return;
        },
        register:function(user, callback){
            $http({
                url: appConfig.ResourceUrl+'/agent/register',
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(user)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
            return;
        },
        login: function(log, callback){
            $http({
                url: appConfig.ResourceUrl+'/agent/login',
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(log)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
            return;
        },
        getInfo: function(id, callback) {
            $http({
                url: appConfig.ResourceUrl + '/agent/get/' + id,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        },
        uploadagentpic: function(file, id, callback) {
            var promises;
            promises = Upload.upload({
                url: appConfig.bucket, //S3 upload url including bucket name
                method: 'POST',
                transformRequest: function (data, headersGetter) {
                    //Headers change here
                    var headers = headersGetter();
                    delete headers['Authorization'];
                    return data;
                },
                fields: {
                    key: 'uploads/agentpic/' + id + '/' + file.name, // the key to store the file on S3, could be file name or customized
                    AWSAccessKeyId: appConfig.AWSAccessKeyId,
                    acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                    policy: appConfig.policy, // base64-encoded json policy (see article below)
                    signature: appConfig.signature, // base64-encoded signature based on policy string (see article below)
                    "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                },
                file: file
            }).then(function () {
                $http({
                    url: appConfig.ResourceUrl+'/agent/updateprofilepic',
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param({ id: id, filename:file.name})
                }).success(function (data, status, headers, config) {
                    callback(data);
                }).error(function (data, status, headers, config) {
                    callback({'error':true});
                });
            })
        },
        updateprofile: function(data, callback) {
            $http({
                url: appConfig.ResourceUrl+'/agent/updateprofile',
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
        },
        changeusername: function(id, data, callback) {
            $http({
                url: appConfig.ResourceUrl+'/agent/frontend/updateAcct/' + id,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
        },
        chckpassword: function(id, data, callback) {
            $http({
                url: appConfig.ResourceUrl+'/agent/checkpassword/' + id,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
        },
        changePass: function(id, data, callback) {
            $http({
                url: appConfig.ResourceUrl+'/agent/changepassword/' + id,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
        },
        loadgallery: function(id, callback) {
            $http({
                url: appConfig.ResourceUrl+'/agent/loadgallery/' + id,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
        },
        deleteMedia: function(id, data, callback){
            $http({
                url: appConfig.ResourceUrl + "/agent/deleteprofpic/" + id,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }
    }
});
