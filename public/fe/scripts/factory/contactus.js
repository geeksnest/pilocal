app.factory('Contactus', function($http, store, jwtHelper, Upload, $q, rfc4122, appConfig) {
    return {
        save: function(data, callback) {
            $http({
                url: appConfig.ResourceUrl + '/frontend/savefeedback',
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        }

    }
});