/**
 * Created by ebautistajr on 3/30/15.
 */

app.factory('Login', function($http, store, jwtHelper, $window,appConfig) {
    return {
        loginmenu: "",
        getUserFromToken: function (callback) {
            if (store.get('AccessToken')) {
                var token = store.get('AccessToken');
                callback(jwtHelper.decodeToken(token));
            } else {
                callback({'error': 'No login yet.'});
            }
        },
        redirectToMainifLogin: function(){
            if (store.get('AccessToken')) {
                if(!jwtHelper.isTokenExpired(store.get('AccessToken'))){
                    $window.location = '/';
                }else{
                    store.remove('AccessToken');
                }
            }
        }
        ,getCurrentUser: function (){
            if (store.get('AccessToken')) {
                var token = store.get('AccessToken');
                return jwtHelper.decodeToken(token);
            } else {
                return null;
            }
        },
        logout: function(callback){
            $http({
                url: appConfig.ResourceUrl + '/agent/logout',
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data, status, headers, config) {
                callback(data);
            })
        }
    }
});
