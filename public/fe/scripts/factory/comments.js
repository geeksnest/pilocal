app.factory('Comments', function($http, store, jwtHelper, $window, appConfig) {
    return {
    	add: function(id, agent, data, type, pinid, callback) {
    		$http({
                url: appConfig.ResourceUrl + '/savecomment/' + id + '/' + agent + '/' + type + '/' + pinid,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
    	},
        addpinreply: function(marker_id, agent, map_id, data, callback) {
            $http({
                url: appConfig.ResourceUrl + '/savepinreply/' + marker_id + '/' + agent + '/' + map_id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        replytocomment: function(commentid, map_id, agent, type, marker_id, data, offset, count, commentload, callback) {
            $http({
                url: appConfig.ResourceUrl + '/savecommentreply/' + commentid + '/' + agent + '/' + map_id + '/' + type + '/' + marker_id + '/' + offset + '/' + count + '/' + commentload,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        getComments: function(id, limit, callback) {
            $http({
                url: appConfig.ResourceUrl + '/getmapcomments/' + id + '/' + limit,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        loadreply: function(id, offset, count, callback) {
            $http({
                url: appConfig.ResourceUrl + '/getcommentreply/' + id + '/' + offset + '/' + count,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        deleteComments: function(id, type, callback) {
            $http({
                url: appConfig.ResourceUrl + '/deletecomment/' + id + '/' + type,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        editcomment: function(id, type, data, callback) {
            $http({
                url: appConfig.ResourceUrl + '/editcomment/' + id + '/' + type,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        deleteres: function(id, callback) {
            $http({
                url: appConfig.ResourceUrl + '/deleteresponse/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        editres: function(id, data, callback) {
            $http({
                url: appConfig.ResourceUrl + '/editresponse/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        }
    }
})
