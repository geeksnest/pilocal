app.factory('ServiceMain', function($http, store, jwtHelper, Upload, $q, rfc4122, appConfig) {
    var description = 'Planet Impossible';
    var keyswords = '';
    var author = 'Planet Impossible';
    var url = '/';
    var type= 'website';
    var title = 'Planet Impossible';
    var image = '/img/frontend/logo.png';
        return {
        metadescription: function () {return description },
        metakeywords: function () {return keyswords },
        metaauthor: function () {return author },
        metafburl: function () {return url },
        metafbtype: function () {return type },
        metafbtitle: function () {return type },
        metafbdescription: function () {return description },
        metafbimage: function () {return image },
        setCurrentPin: function (val) {
            this.currentPin = val;
        },
        indexlist: function(callback){
            $http({
                url: appConfig.ResourceUrl + '/index/missions',
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        },
        getCategories: function(callback){
            $http({
                url: appConfig.ResourceUrl + '/missions/category/get',
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        },
        getNews: function(callback) {
            $http({
                url: appConfig.ResourceUrl + '/missions/news/get',
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        }
    }
});