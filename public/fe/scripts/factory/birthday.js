/**
 * Created by ebautistajr on 3/19/15.
 */
app.factory('BirthDayMonthYear', function () {
    return {
        month: function () {
            return [
                {name: 'Month', val: ''},
                {name: 'January', val: 1},
                {name: 'February', val: 2},
                {name: 'March', val: 3},
                {name: 'April', val: 4},
                {name: 'May', val: 5},
                {name: 'June', val: 6},
                {name: 'July', val: 7},
                {name: 'August', val: 8},
                {name: 'September', val: 9},
                {name: 'October', val: 10},
                {name: 'November', val: 11},
                {name: 'December', val: 12}
            ]
        },
        day: function () {
            var day = [];
            day.push({'name': 'Day','val': ''})
            for (var x = 1; x <= 31; x++) {
                day.push({'name': x,'val': x})
            }
            return day;
        },
        year: function () {

            var year = [];
            year.push({'name': 'Year','val': ''})
            for (var y = 2013; y >= 1950; y--) {
                year.push({'name': y,'val': y})
            }
            return year;
        }
    }
});