app.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if(event.which === 13 && event.shiftKey === false) {
                scope.$apply(function(){
                    scope.$eval(attrs.ngEnter, {'event': event});
                });

                event.preventDefault();
            }
        });
    };
})
.directive('lightSlider', function() {
    return {
        restrict: 'A',
        template: '<li ng-repeat="pinimage in PinImages" ng-if="comment.marker_id==pinimage.marker_id" style="position: relative;" >'+
                        ' <a class="fancybox" ng-if="pinimage.type==\'image\'" fancybox href="{[{pinimage.image}]}"> <img id="lightslider-img" ng-src="{[{pinimage.image}]}" /> </a>' +
                        ' <a class="fancybox-media" ng-if="pinimage.type==\'video\'" fancybox-media href="https://www.youtube.com/watch?v={[{pinimage.vidid}]}"> <img id="lightslider-img" ng-src="{[{pinimage.image}]}" /> <img src="/img/youtubeplay.png" style="margin-left: 35%; margin-top: -55%; position: absolute;"> </a> ' +
                  '</li>',
        link: function(scope, el, atts){
            el.lightSlider({
                item:3,
                loop:true,
                slideMove:2,
                easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                speed:600,
                closable: true,
                escKey: true
            });
        }
    };
}).directive('fancybox', function(){
    return {
        restrict: 'A',

        link: function(scope, element, attrs){
            $(element).fancybox();
        }
    }
}).directive('fancyboxMedia', function(){
        return {
            restrict: 'A',

            link: function(scope, element, attrs){
                $(element).fancybox({
                    openEffect  : 'none',
                    closeEffect : 'none',
                    helpers : {
                        media: {}
                    }
                    });
            }
        }
    });
