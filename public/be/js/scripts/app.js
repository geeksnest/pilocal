'use strict';


// Declare app level module which depends on filters, and services
var app = angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'pascalprecht.translate',
    'app.directives',
    'app.controllers',
    'angular-jwt',
    'angular-storage',
    'toaster',
    'ngFileUpload',
    'angularMoment',
    'xeditable',
    'angular.chosen',
    'ui.select',
    'uiGmapgoogle-maps'
  ])
.run(function ($rootScope,   $state,   $stateParams, store, Config, $http, editableOptions, editableThemes) {
        editableOptions.theme = 'bs3';
        editableThemes.bs3.inputClass = 'input-sm';
        editableThemes.bs3.buttonsClass = 'btn-sm';
        editableThemes['bs3'].submitTpl = '<button class="btn btn-sm btn-icon btn-success"><icon class="fa fa-save"></icon></button>';
        editableThemes['bs3'].cancelTpl = '<button type="button" class="btn btn-sm btn-icon btn-warning"  ng-click="$form.$cancel()"><icon class="fa fa-ban"></icon></button>';

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        console.log(store.get('pi-access-token'));
        //store.remove('pi-access-token');
        if (store.get('pi-access-token') == null) {
            console.log('== Getting Access Log ==');
            window.location = '/superagent';
        }else{
            $http({
                url: Config.ApiURL + '/authenticate/get',
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Bearer ' + store.get('pi-access-token')
                }
            }).success(function(data, status, headers, config) {
                console.log(data);
            })
        }

    })
.config(
  [ '$httpProvider','$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$interpolateProvider', 'uiGmapGoogleMapApiProvider', 
    function ($httpProvider, $stateProvider,   $urlRouterProvider,   $controllerProvider,   $compileProvider,   $filterProvider,   $provide, $interpolateProvider, uiGmapGoogleMapApiProvider) {

        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.factory    = $provide.factory;
        app.constant   = $provide.constant;
        app.value      = $provide.value;

        $interpolateProvider.startSymbol('{[{');
        $interpolateProvider.endSymbol('}]}');

         $httpProvider.interceptors.push(['$q', '$location', 'store', function($q, $location, store) {
             return {
                 'request': function (config) {
                     config.headers = config.headers || {};
                     if (store.get('pi-access-token')) {
                         $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + store.get('pi-access-token');
                     }
                     return config;
                 },
                 'responseError': function(response) {
                     if(response.status === 401 || response.status === 403) {
                         store.remove('pi-access-token')
                         window.location = '/superagent';
                     }
                     return $q.reject(response);
                 }
             };
         }]);

        $urlRouterProvider
            .otherwise('/dashboard');

        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: '/superagent/admin/dashboard',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/dashboard.js'
                            ]);
                        }]
                }
            })
            .state('missions', {
                url: '/missions',
                templateUrl: '/superagent/missions',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/missions.js',
                                '/be/js/scripts/factory/missions.js'
                            ]);
                        }]
                }
            })
            .state('viewmap', {
                url: '/viewmap/:slugs',
                templateUrl: '/superagent/missions/view',
                controller: 'ViewMissionCtrl',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/viewmissions.js',
                                '/be/js/scripts/factory/missions.js',
                                '/be/js/scripts/factory/maps.js'
                            ]);
                        }]
                }
            })
            .state('categories', {
                url: '/categories',
                templateUrl: '/superagent/missions/categories',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/missioncategory.js',
                                '/be/js/scripts/factory/missioncategory.js'
                            ]);
                        }]
                }
            })
            // news/blog
            .state('createnews', {
                url: '/createnews',
                templateUrl: '/superagent/news/createnews',
                controller: 'Createnews',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad){
                            return uiLoad.load([
                                '/be/js/scripts/controllers/news/createnews.js',
                                '/be/js/scripts/factory/news/news.js',
                                '/be/js/scripts/factory/news/category.js'
                            ]);
                        }
                    ]
                }
            })
            .state('managenews', {
                url: '/managenews',
                templateUrl: '/superagent/news/managenews',
                controller: 'Managenews',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/news/managenews.js',
                                '/be/js/scripts/factory/news/news.js'
                            ]);
                        }
                    ]
                }
            })
            .state('editnews', {
                url: '/editnews/:newsid',
                controller: 'Editnews',
                templateUrl: '/superagent/news/editnews',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/news/editnews.js',
                        '/be/js/scripts/factory/news/news.js',
                        '/be/js/scripts/factory/news/tags.js',
                        '/be/js/scripts/factory/news/category.js'
                      ]);
                  }]
                }

            })
            .state('createauthor', {
                url: '/createauthor',
                templateUrl: '/superagent/news/createauthor',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/news/createauthor.js',
                                '/be/js/scripts/factory/news/author.js'
                            ]);
                        }
                    ]
                }
            })
            .state('manageauthor', {
                url: '/manageauthor',
                templateUrl: '/superagent/news/manageauthor',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/news/manageauthor.js',
                                '/be/js/scripts/factory/news/author.js'
                            ]);
                        }
                    ]
                }
            })
            .state('editauthor', {
                url: '/editauthor/:authorid',
                templateUrl: '/superagent/news/editauthor',
                resolve: {
                  deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                          '/be/js/scripts/controllers/news/editauthor.js',
                          '/be/js/scripts/factory/news/author.js'
                          ]);
                    }
                  ]
                }
            })
            .state('newscategory', {
                url: '/category',
                templateUrl: '/superagent/news/category',
                controller: 'Category',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/news/category.js',
                                '/be/js/scripts/factory/news/category.js'
                            ]);
                        }
                    ]
                }
            })
            .state('newstags', {
                url: '/tags',
                templateUrl: '/superagent/news/tags',
                controller: 'Tags',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/news/tags.js',
                                '/be/js/scripts/factory/news/tags.js'
                            ]);
                        }
                    ]
                }
            })
            .state('contactus', {
                url: '/contactus',
                templateUrl: '/superagent/contacts/index',
                controller: 'ContactusCtrl',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad){
                            return uiLoad.load([
                                '/be/js/scripts/controllers/contacts.js',
                                '/be/js/scripts/factory/contacts.js',
                            ]);
                        }
                    ]
                }
            });
    }
  ]
)

.config(['$translateProvider', function($translateProvider){

  // Register a loader for the static files
  // So, the module will search missing translation tables under the specified urls.
  // Those urls are [prefix][langKey][suffix].
  $translateProvider.useStaticFilesLoader({
    prefix: '/be/js/jsons/',
    suffix: '.json'
  });

  // Tell the module what language to use by default
  $translateProvider.preferredLanguage('en');

  // Tell the module to store the language in the local storage
  $translateProvider.useLocalStorage();

}])

/**
 * jQuery plugin config use ui-jq directive , config the js and css files that required
 * key: function name of the jQuery plugin
 * value: array of the css js file located
 */
.constant('JQ_CONFIG', {
    easyPieChart:   ['/be/js/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
    sparkline:      ['/be/js/jquery/charts/sparkline/jquery.sparkline.min.js'],
    plot:           ['/be/js/jquery/charts/flot/jquery.flot.min.js',
                        '/be/js/jquery/charts/flot/jquery.flot.resize.js',
                        '/be/js/jquery/charts/flot/jquery.flot.tooltip.min.js',
                        '/be/js/jquery/charts/flot/jquery.flot.spline.js',
                        '/be/js/jquery/charts/flot/jquery.flot.orderBars.js',
                        '/be/js/jquery/charts/flot/jquery.flot.pie.min.js'],
    slimScroll:     ['/be/js/jquery/slimscroll/jquery.slimscroll.min.js'],
    sortable:       ['/be/js/jquery/sortable/jquery.sortable.js'],
    nestable:       ['/be/js/jquery/nestable/jquery.nestable.js',
                        '/be/js/jquery/nestable/nestable.css'],
    filestyle:      ['/be/js/jquery/file/bootstrap-filestyle.min.js'],
    slider:         ['/be/js/jquery/slider/bootstrap-slider.js',
                        '/be/js/jquery/slider/slider.css'],
    chosen:         ['/be/js/jquery/chosen/chosen.jquery.min.js',
                        '/be/js/jquery/chosen/chosen.css'],
    TouchSpin:      ['/be/js/jquery/spinner/jquery.bootstrap-touchspin.min.js',
                        '/be/js/jquery/spinner/jquery.bootstrap-touchspin.css'],
    wysiwyg:        ['/be/js/jquery/wysiwyg/bootstrap-wysiwyg.js',
                        '/be/js/jquery/wysiwyg/jquery.hotkeys.js'],
    dataTable:      ['/be/js/jquery/datatables/jquery.dataTables.min.js',
                        '/be/js/jquery/datatables/dataTables.bootstrap.js',
                        '/be/js/jquery/datatables/dataTables.bootstrap.css'],
    vectorMap:      ['/be/js/jquery/jvectormap/jquery-jvectormap.min.js',
                        '/be/js/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
                        '/be/js/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
                        '/be/js/jquery/jvectormap/jquery-jvectormap.css'],
    footable:       ['/be/js/jquery/footable/footable.all.min.js',
                        '/be/js/jquery/footable/footable.core.css']
    }
)


.constant('MODULE_CONFIG', {
    select2:        ['/be/js/jquery/select2/select2.css',
                        '/be/js/jquery/select2/select2-bootstrap.css',
                        '/be/js/jquery/select2/select2.min.js',
                        '/be/js/modules/ui-select2.js']
    }
)
;
