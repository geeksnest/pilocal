'use strict';


// Declare app level module which depends on filters, and services
var app = angular.module('app', ['angular-jwt','angular-storage']).config(
        function ($interpolateProvider) {
            $interpolateProvider.startSymbol('{[{');
            $interpolateProvider.endSymbol('}]}');
        })
    .run(
    [ '$rootScope', 'store', '$location',
        function ($rootScope,  store, $location) {

            console.log(store.get('pi-access-token'));

            if (store.get('pi-access-token') != null) {
                console.log('== Getting Access Log ==');
                window.location='/superagent/admin';
            }
        }
    ]
    )
    .controller('LoginCtrl', function($scope, $http, Login,store) {
        console.log('Login Controller');
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        $scope.message = '';

        if(getParameterByName('logout')){
            $scope.message = 'You have successfully logged out.';
        }

        $scope.submit = function(login){
            Login.requestAccess(login, function(res){
                console.log(res);
                if (!res.hasOwnProperty('error')) {
                    store.set('pi-access-token', res.access);
                    store.set('pi-refresh-token', res.refresh);
                    console.log("Login Success");
                    window.location='/superagent/admin';
                } else {
                    $scope.message = res.error;
                    console.log(res.error);
                }
            });
        }
    });