/**
 * Created by Apple on 10/6/15.
 */
app.controller('MissionCategoryCTRL',function($scope, Config, $modal, MissionsCategory, toaster) {

    $scope.currentEdit = {};
    $scope.editing = false;
    $scope.currentId = '';

    var num = 10;
    var off = 1;
    var keyword = null;
    $scope.loading = false;

    var loadList = function(num, off, keyword){
        $scope.loading = true;
        MissionsCategory.list(num, off, keyword, function(data){
            $scope.loading = false;
            $scope.data = data;
        });
    }

    loadList(num, off, keyword);

    $scope.clear = function(){
        $scope.keyword=null;
        loadList(num, off, keyword);
    }

    $scope.search = function (keyword) {
        var off = 0;
        $scope.keyword = keyword;
        loadList(num, off, keyword);
        $scope.searchkeyword='';
        $scope.formmanagenews.$setPristine();
    }

    $scope.setPage = function (num) {
        loadList(num, off, keyword);
    };

    $scope.editCat = function(ss){
        $scope.currentEdit = ss;
        $scope.editing = true;
        $scope.currentId = ss.id;
    }

    $scope.cancelEdit = function(){
        $scope.currentEdit = {};
        $scope.editing = false;
        $scope.currentId = '';
    }

    $scope.updateCat = function(data, id){
        console.log(data);
        data['id'] = id;
        MissionsCategory.update(data, function(res){
            $scope.cancelEdit();
            console.log(res);
            if(res.hasOwnProperty('200')){
                toaster.clear();
                toaster.pop('success', "", 'Your category has been updated.');
                loadList(10, 1, null);
            }else if(res.hasOwnProperty('error')){
                toaster.clear();
                toaster.pop('warning', "", res.error);
            }else{
                toaster.clear();
                toaster.pop('warning', "", 'Something went wrong is updating your category.');
            }

        });
    }

    $scope.open = function () {
        var modalInstance = $modal.open({
            templateUrl: 'addCategory.html',
            controller: function ($scope, $modalInstance) {
                $scope.alerts = [];
                $scope.category = {};
                $scope.success = false;
                $scope.save = function (data , catform) {
                    MissionsCategory.save(data, function(res){
                        if(res.hasOwnProperty('200')){
                            toaster.clear();
                            toaster.pop('success', "", 'Your category has been added.');
                            $scope.success = true;
                            $scope.category = {};
                            catform.$setPristine();
                            //$modalInstance.close();
                            loadList(10, 1, null);
                        }else if(res.hasOwnProperty('error')){
                            toaster.clear();
                            toaster.pop('warning', "", res.error);
                        }else{
                            toaster.clear();
                            toaster.pop('warning', "", 'Something went wrong is adding your category.');
                        }
                    });

                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    };

    $scope.delete = function(catid){
        var modalInstance = $modal.open({
            templateUrl: 'deleteCategory.html',
            controller: function ($scope, $modalInstance, catid) {
                $scope.ok = function () {
                    MissionsCategory.delete(catid, function(res){
                        if(res.hasOwnProperty('200')){
                            toaster.clear();
                            toaster.pop('success', "", 'Your category has been deleted.');
                            $scope.success = true;
                            $scope.category = {};
                            loadList(10, 1, null);
                            $modalInstance.close();
                        }else{
                            toaster.clear();
                            toaster.pop('warning', "", 'Something went wrong when deleting your category.');
                        }
                    });

                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve : { // This fires up before controller loads and templates rendered
                catid : function() {
                    return catid;
                }
            }
        });
    }

});
