app.controller('MissionsCTRL',function($scope, Config, Missions, $modal, $anchorScroll, $state) {

    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    $scope.currentstatusshow = '';
    $scope.alerts = [];
    $scope.searchkeyword = '';

    var alert = function(data) {
        $scope.alerts = [{type: data.type, msg: data.msg }];
    }

    $scope.closeAlert = function() {
        $scope.alerts = [];
    };

    var loadlist = function(num, off, keyword, status){
        $scope.loading = true;
        Missions.list(num,off, keyword, status, function(data){
            console.log(data);
            $scope.data = data;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
            $scope.loading = false;
        });
    }

    loadlist(num, off, keyword, null);

    $scope.clearsearch = function(){
        $scope.keyword=null;
        loadlist(num, off, null, null);
    }

    $scope.search = function (keyword, status) {
        var off = 0;
        $scope.keyword = keyword;
        loadlist(num, off, keyword, status);
        $scope.searchkeyword=null;
    }

    $scope.sort = function(status) {
        $scope.keyword=null;
        loadlist(num, off, null ,status);
    }

    $scope.setPage = function (pageNo) {
        loadlist(num, pageNo, keyword);
    };

    $scope.setFGClick = function(val, id, type){
        console.log(id);
        console.log(val);
        Missions.setFG(val, id, type,function(data){
            console.log(data);
        })
    }

    $scope.setstatus = function (status,mapid, offset) {

        $scope.currentstatusshow = mapid;
        status = status == 1 ? 0 : 1;
        Missions.setstatus(status, mapid, function(data){
            var i = 2;
            setInterval(function(){
                i--;
                if(i == 0)
                {
                    loadlist(num, offset, keyword, null);
                    $scope.currentstatusshow = '';
                    $scope.keyword=null;
                }
            },1000)

        });

    }

    $scope.deletemap = function(id) {
        console.log(id);
        var modalInstance = $modal.open({
            templateUrl: 'deleteMission.html',
            controller: function($scope, $modalInstance, id, Missions) {
                $scope.msg = "Are you sure you want to delete this map?";

                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }

                $scope.ok = function() {
                    Missions.delete(id, function(data) {
                        alert(data);
                        $anchorScroll();
                        $modalInstance.dismiss();
                        $scope.keyword=null;
                    });
                }
            },
            resolve: {
                id: function() {
                    return id;
                }
            }
        });
    }

    $scope.viewmap = function(slugs) {
        $state.go('viewmap', {slugs: slugs });
    }
});
