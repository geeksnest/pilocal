app.controller('ContactusCtrl',function($scope, Config, Contacts, $modal, $timeout, $filter, toaster) {

    var num = 10;
    var off = 1;
    var keyword = null;
    var date = null;

    var loadlist = function(num, off, keyword, date){
        $scope.loading = true;
        Contacts.list(num, off, keyword, date, function(data){
            $scope.data = data;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
            $scope.loading = false;
        });
    }

    loadlist(num, off, keyword, date);

    $scope.setPage = function (pageNo) {
        loadlist(num, pageNo, $scope.searchtext, $scope.searchdate);
    };

    $scope.delete = function(id) {
        var modalInstance = $modal.open({
            templateUrl: 'deleteContact.html',
            controller: function($scope, $modalInstance, id, Contacts, $timeout) {
                $scope.msg = "Are you sure you want to delete this contact?";
                $scope.showbtn = true;
                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }

                $scope.ok = function() {
                    Contacts.delete(id, function(data) {
                        $scope.msg = data;
                        $scope.showbtn = false;
                        $timeout(function() {
                            $scope.cancel();
                            loadlist(num, off, keyword, date);
                        }, 2000);
                    });
                }
            },
            resolve: {
                id: function() {
                    return id;
                }
            }
        });
    }

    $scope.search = function(keyword, searchdate) {
        var off = 1;
        console.log('Searching...');
        if (searchdate) {
            searchdate = $filter('date')(searchdate, 'yyyy-MM-dd');
            console.log(searchdate);
        } else {
            searchdate = null;
        }
        if(keyword==""){
            keyword = null;
        }
        $scope.keyword = keyword;
        $scope.searchdate = searchdate;
        loadlist(num, off, $scope.keyword, searchdate);

        $scope.searchtext = '';
        $scope.searchdate = '';
        $scope.form.$setPristine();
    }

    $scope.clear = function() {
        $scope.dt = null;
    };

    /* Date picker */
    $scope.today = function() {
        $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function() {
        $scope.dt = null;
    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.clear = function() {
        $scope.keyword = null;
        $scope.searchdate = null;
        loadlist(10, 1, $scope.keyword, $scope.searchdate);
    }

    var viewContactCtrl = function($scope, $modalInstance, id) {
        $scope.reply = [];
        Contacts.getContact(id, function(data) {
            if(data.hasOwnProperty("reply")){
                $scope.contact = data.contact;
                $scope.reply = data.reply;
            }else {
                $scope.contact = data;
            }
        });

        $scope.submitreply = function(reply) {
            Contacts.reply(id, reply, function(data){
                if(data.hasOwnProperty('success')){
                    $scope.reply.push({ message: reply, created_at: new Date() });
                    toaster.pop('success', '', 'Reply has been sent.');
                }else {
                    toaster.pop('error', '', data.error);
                }
                $modalInstance.dismiss();
            });
        }
    }

    $scope.view = function(id) {
        var modalInstance = $modal.open({
            templateUrl: "viewContact.html",
            controller: viewContactCtrl,
            resolve: {
                id: function() {
                    return id;
                }
            }
        }).result.finally(function() {
            loadlist(num, off, keyword, date);
        });
    }
});
