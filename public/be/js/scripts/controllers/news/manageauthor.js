'use strict';

/* Controllers */

app.controller('Manageauthor', function($scope, $state ,$q, $http, Config, $log, Author, $interval, $modal, toaster){


  $scope.keyword=null;
  $scope.loading = false;
  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;
  $scope.currentstatusshow = '';

  var paginate = function(num, off, keyword){
    $scope.loading = true;
    Author.list(num,off, keyword, function(data){
      $scope.data = data;
      $scope.maxSize = num;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
      $scope.loading = false;
    });
  }

  paginate(num, off, keyword);

  $scope.search = function (keyword) {
      $scope.keyword = keyword;
      paginate(10, 1, $scope.keyword);
      $scope.searchtext='';
  }

  $scope.clear = function(){
    $scope.keyword=null;
    paginate(10,1, null);
  }

  $scope.setPage = function (pageNo) {
    paginate(10, pageNo, $scope.keyword);
  };

  $scope.deleteauthor = function(authorid) {
    var modalInstance = $modal.open({
      templateUrl: 'authorDelete.html',
      controller: function($scope, $modalInstance, authorid) {
        var datalength = 0;

        var getconflict = function(){
            Author.loadauthornews(authorid, function(data){
                $scope.authconflict = data.dataconflict;
                $scope.authors = data.newsauthors;
                datalength = data.dataconflict.length
                if(datalength > 0){
                    $scope.viewconflicts = true;
                    $scope.disabledelete = true;
                }else {
                    $scope.viewconflicts = false;
                    $scope.disabledelete = false;
                }
            });
        }

        getconflict();
        
        $scope.editauthor = function(news) {
          if(news.authorid != undefined){
              Author.updatenewsauthors(news, function(){
                  getconflict();
              });
          }else {
              console.log("undefined");
          }
        }

        $scope.ok = function() {
          Author.delete(authorid, function(data){
            paginate(10, 1, $scope.keyword);
            $modalInstance.close();
            toaster.pop('success','','Author successfully Deleted!');
          });
        };

        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        };
      },
      resolve: {
        authorid: function() {
          return authorid
        }
      }
    });
  }


  $scope.editauthor = function(authorid) {
    $scope.authorid = authorid;
    console.log(authorid);
    var modalInstance = $modal.open({
      templateUrl: 'authorEdit.html',
      controller:  function($scope, $modalInstance, authorid, $state) {
        $scope.authorid = authorid;
        $scope.ok = function(authorid) {
          $scope.authorid = authorid;
          console.log(authorid);
          $state.go('editauthor', {authorid: authorid });
          $modalInstance.dismiss('cancel');
        };
        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      },
      resolve: {
        authorid: function() {
          return authorid
        }
      }
    });
  }





})