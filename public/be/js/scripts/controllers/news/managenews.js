'use strict';

/* Controllers */

app.controller('Managenews', function($scope, $state ,$q, $http, Config, $log, News, $interval, $modal, $sce, $stateParams, toaster){
    $scope.keyword=null;
    $scope.loading = false;
    // $scope.vidpath = $scope.video.video;
    $scope.sort = "updated_at";

    $scope.paste = function(video){
        $scope.vidpath=$sce.trustAsHtml($scope.video.video);
        console.log("video");
    }

    $scope.sortpage = function(sort){
        loadlist(num, off, keyword, sort);
        console.log(sort);
    }

    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    $scope.currentstatusshow = '';


    var loadlist = function(num, off, keyword, sort){
        $scope.loading = true;
        News.list(num,off, keyword, sort, function(data){
            $scope.data = data;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
            $scope.loading = false;
        });
    }

    loadlist(num, off, keyword, $scope.sort);

    $scope.clear = function(){
        $scope.keyword=null;
        loadlist(num, off, keyword, $scope.sort);
    }

    $scope.search = function (keyword) {
        var off = 0;
        $scope.keyword = keyword;
        loadlist(num, off, keyword, $scope.sort);
        $scope.searchtext='';
        $scope.formmanagenews.$setPristine();
    }

    $scope.setPage = function (pageNo) {
        loadlist(num, off, keyword, $scope.sort);
    };

    $scope.setstatus = function (status,newsid,keyword,newslocation,newsslugs) {

        $scope.currentstatusshow = newsslugs;
        status = status == 1 ? 0 : 1;
        News.updateNewsStatus(status, newsid, function(data){
            var i = 2;
            setInterval(function(){
                i--;
                if(i == 0)
                {
                    loadlist(10, 1, keyword, $scope.sort);
                    $scope.currentstatusshow = 0;
                }
            },1000)

        });

    }
    
    $scope.featured = function(newsid, featured) {
        featured = featured == true ? 1 : 0;
        News.updateFeatured(newsid, featured, function(data) {
            if(data.hasOwnProperty('error')){
                console.log(data);
            }
        });
    }

    $scope.deletenews = function(newsid) {
        var modalInstance = $modal.open({
            templateUrl: 'newsDelete.html',
            controller: function($scope, $modalInstance, newsid) {
                $scope.ok = function() {
                    News.delete(newsid, function(data){
                        loadlist(10, 1, keyword, "updated_at");
                        $modalInstance.close();
                        toaster.pop('success', '', 'News successfully Deleted!');
                    });
                };
                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                };

            },
            resolve: {
                newsid: function() {
                    return newsid
                }
            }
        });
    }

    $scope.editnews = function(newsid) {
        $scope.newsid
        var modalInstance = $modal.open({
            templateUrl: 'newsEdit.html',
            controller: function($scope, $modalInstance, newsid, $state) {
                $scope.newsid = newsid;
                $scope.ok = function(newsid) {
                    $scope.newsid = newsid;
                    $state.go('editnews', {newsid: newsid });
                    $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve: {
                newsid: function() {
                    return newsid
                }
            }
        });
    }

})