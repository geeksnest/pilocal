'use strict';

/* Controllers */


app.controller('Editnews', function($scope, $state, Upload ,$q, $http, Config, News, $stateParams ,$modal, $compile, $sce, Category, Tags, $anchorScroll, $filter, toaster){

    $scope.imageloader=false;
    $scope.imagecontent=true;
    $scope.videocontent=true;
    $scope.contentvideo='';
    $scope.thumbnails = "image";
    var addedcategory = '';

    $scope.editslug = false;
    $scope.editedslug = false;
    var slugstorage = "";

    $scope.onnewstitle = function convertToSlug(Text)
    {
        if(Text != null  && $scope.editedslug == false && $scope.editslug == false)
        {
            var text1 = Text.replace(/[^\w ]+/g,'');
            $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
        }else {
            $scope.news.slugs = null;
        }
    }

    $scope.onslugs = function(Text){
        if(Text != null)
        {
            $scope.news.slugs = Text.replace(/\s+/g, '-').toLowerCase();
        }
    }

    $scope.editnewsslug = function(){
        $scope.editslug = true;
        slugstorage = $scope.news.slugs;
    }

    $scope.cancelnewsslug = function(title) {
        if(title != null)
        {
            $scope.editedslug = false;
            $scope.news.slugs = slugstorage;
        }else {
            $scope.news.slugs = '';
            $scope.editedslug = false;
        }
        $scope.editslug = false;
    }

    $scope.setslug = function(slug){
        $scope.editedslug = true;
        $scope.editslug = false;
        if(slug != null)
        {
            slug = slug.replace(/\s+/g, '-').toLowerCase();
            News.validateSlug(slug, $scope.news.newsid, function(data) {
                if(data.exist == true) {
                    $scope.news.slugs = slugstorage;
                    toaster.pop('error','','News slug already exist');
                }else {
                    $scope.news.slugs = slug;
                }
            });
        }
    }

    $scope.clearslug = function(title){
        if(title != null)
        {
            $scope.editedslug = false;
            $scope.news.slugs = title.replace(/\s+/g, '-').toLowerCase();
        }else {
            $scope.news.slugs = '';
            $scope.editedslug = false;
        }
    }

    $http({
        url: Config.ApiURL + "/news/newsedit/" + $stateParams.newsid ,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function (data, status, headers, config) {
        News.loadcategory(function(data){
            $scope.category = data;
        });


        var newtaglist = [];
        data.tag.map(function(val){
            newtaglist.push(val.tags);
        });
        var catlist = [];
        data.category.map(function(val){
            catlist.push(val.categoryid);
        });
        data.category = catlist;
        $scope.news = data;
        $scope.news.tag = newtaglist;

        if(data.videothumb){
            $scope.news.featuredthumbtype='video';
            // data.videothumb = $sce.trustAsHtml(data.videothumb);
            $scope.vidpath = $sce.trustAsHtml(data.videothumb);
            $scope.news.featuredthumb = data.videothumb;
            $scope.news.imagethumb = '';
        }else{
            $scope.vidpath = '';
            $scope.news.featuredthumbtype='image';
            $scope.news.featuredthumb = data.imagethumb;
        }

    }).error(function (data, status, headers, config) {
        $scope.status = status;
    });

    $scope.news = {
        title: ""
    };

    var loadcategory = function(){
        News.loadcategory(function(data){
            $scope.category = data;
            console.log(addedcategory);
            $scope.news.category.push(addedcategory);
        });
    }

    $scope.preview = function(news){
        $scope.author.map(function(val){
            if(news.author == val.authorid){
                news['authorname'] = val.name;
                news['authorimage'] = val.image;
                news['authorabout'] = val.about;
            }
        });
        var catlist = [];
        $scope.category.map(function(val){
            news.category.map(function(val2){
                if(val['categoryid'] == val2){
                    catlist.push(val['categoryname']);
                }
            });
        });
        news['categoryname'] = catlist.join();
        news['tagname'] = news['tag'].join();
        var returnYoutubeThumb = function(item){
            var x= '';
            var thumb = {};
            if(item){
                var newdata = item;
                var x;
                x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
                thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
                thumb['yid'] = x[1];
                return x[1];
            }else{
                return x;
            }
        }
        if(news['featuredthumbtype'] == 'video'){
            news['featuredthumb'] = returnYoutubeThumb(news['featuredthumb']);
        }
        news.date = $filter('date')(news.date,'yyyy-MM-dd');
        console.log(news);
        window.open(Config.BaseURL + "/blog/preview?" + $.param(news));
    }

    var loadauthor = function()
    {
        News.loadauthor(function(data){
            $scope.author = data;
        });
    }
    loadauthor();

    var loadtags = function()
    {
        News.loadtags(function(data){
            var newlist = [];
            data.map(function(val){
                newlist.push(val.tags);
            });
            $scope.select2Options = {
                'multiple': true,
                'simple_tags': true,
                'tags': newlist  // Can be empty list.
            };
            $scope.tag = newlist;
        });
    }
    loadtags();

    $scope.updateNews = function(news, status)
    {
        $scope.news.status = status;
        news.date = $filter('date')(news.date,'yyyy-MM-dd');
        //news['category'] = news.category.categoryid;
        // news['tag'] = news.tag.id;
        $scope.isSaving = true;
        News.updateNews(news, function(data){
            if(data.hasOwnProperty('error')){
                if(data.error.hasOwnProperty('existTitle')){
                    toaster.pop('error' , '', 'News Title already exists.');
                    $scope.isSaving = false;
                    $anchorScroll();
                }
            }else {
                $scope.isSaving = false;
                toaster.pop('success' , '', 'News successfully saved!');
                $anchorScroll();
            }
        })
    }

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };



    $scope.media = function(type){
        var modalInstance = $modal.open({
            templateUrl: 'mediagallery.html',
            controller: function($scope, $modalInstance,Config, type) {
                $scope.imageloader=false;
                $scope.imagecontent=true;
                $scope.directory = 'newsimage';
                $scope.currentDeleting = '';
                $scope.currentSelected = '';
                $scope.videoenabled=true;
                $scope.mediaGallery = 'all';
                $scope.type = type;
                $scope.s3link = Config.amazonlink;
                console.log($scope.s3link);

                $scope.set = function(id){
                    $scope.currentSelected = id;
                    $scope.contentvideo = id;
                    if(type=='content'){
                        $scope.copy();
                    }
                }

                var returnYoutubeThumb = function(item){
                    var x= '';
                    var thumb = {};
                    if(item){
                        var newdata = item;
                        var x;
                        x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
                        thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
                        thumb['yid'] = x[1];
                        return thumb;
                    }else{
                        return x;
                    }
                }

                $scope.returnImageThumb = function(){
                    return function (item) {
                        if(item){
                            return Config.amazonlink + "/uploads/newsimage/" + item;
                        }else{
                            return item;
                        }
                    };
                }

                var loadimages = function() {
                    News.loadimages(function(data){
                        console.log(data);
                        $scope.imagelist = data;
                        $scope.imagelength = data.length;
                    });
                }

                loadimages();

                var loadvideo = function() {
                    News.loadVideo(function(data){
                        for (var x in data){
                            var newd = returnYoutubeThumb(data[x].video);
                            data[x].videourl = newd.url;
                            data[x].youtubeid = newd.yid;
                        }
                        console.log(data);

                        $scope.videolist = data;
                        $scope.videoslength = data.length;
                    })
                }

                loadvideo();

                $scope.$watch('files', function () {
                    $scope.upload($scope.files);

                });

                $scope.upload = function (files)
                {

                    var filename
                    var filecount = 0;
                    if (files && files.length)
                    {
                        $scope.imageloader=true;
                        $scope.imagecontent=false;

                        for (var i = 0; i < files.length; i++)
                        {
                            var file = files[i];

                            if (file.size >= 2000000)
                            {
                                toaster.pop('error' , '', 'File ' + file.name + ' is too big');
                                filecount = filecount + 1;

                                if(filecount == files.length)
                                {
                                    $scope.imageloader=false;
                                    $scope.imagecontent=true;
                                }


                            }
                            else

                            {

                                var promises;

                                promises = Upload.upload({

                                    url: Config.amazonlink, //S3 upload url including bucket name
                                    method: 'POST',
                                    transformRequest: function (data, headersGetter) {
                                        //Headers change here
                                        var headers = headersGetter();
                                        delete headers['Authorization'];
                                        return data;
                                    },
                                    fields : {
                                        key: 'uploads/newsimage/' + file.name, // the key to store the file on S3, could be file name or customized
                                        AWSAccessKeyId: Config.AWSAccessKeyId,
                                        acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                        policy: Config.policy, // base64-encoded json policy (see article below)
                                        signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                        "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                    },
                                    file: file
                                })
                                promises.then(function(data){

                                    filecount = filecount + 1;
                                    filename = data.config.file.name;
                                    var fileout = {
                                        'imgfilename' : filename
                                    };
                                    News.saveImage(fileout, function(data){
                                        loadimages();
                                        if(filecount == files.length)
                                        {
                                            $scope.imageloader=false;
                                            $scope.imagecontent=true;
                                        }
                                    });
                                });
                            }

                        }
                    }
                };

                $scope.savevid = function(newsvid) {
                    if(newsvid){
                        var x = newsvid.match(/<iframe width="(.*?)" height="(.*?)" src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
                        console.log(x);
                        if(x==null){
                            $scope.invalidvideo = true;
                            $scope.video='';
                        }else{
                            var newslink = { 'newsvid': newsvid }
                            News.saveVid(newslink,function(data){
                                loadvideo();
                                $scope.video='';
                                $scope.invalidvideo = false;
                                console.log(data);
                            });
                        }
                    }
                };

                $scope.deletevideo = function (videoid, $event)
                {
                    var datavideo = {
                        'videoid' : videoid
                    };
                    News.deleteVideo(datavideo, function(data){
                        loadvideo();
                    });
                    $event.stopPropagation();
                }

                $scope.deletenewsimg = function (dataimg, $event)
                {
                    var fileout = {
                        'imgfilename' : dataimg
                    };
                    News.deleteImage(fileout, function(data) {
                        loadimages();

                    });
                    $event.stopPropagation();
                }

                $scope.copy = function() {
                    console.log($scope.contentvideo);
                    var text = '';
                    if($scope.contentvideo.filename){
                        text = Config.amazonlink + '/uploads/newsimage/' + $scope.contentvideo.filename;
                    }else if($scope.contentvideo.videoid){
                        text = $scope.contentvideo.video;
                    }
                    var pp = window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
                    if(pp != "" && pp !== null) {
                        $modalInstance.dismiss('cancel');
                    }
                }

                $scope.ok = function(category) {
                    $modalInstance.close($scope.contentvideo);
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                }

            },
            resolve: {
                type: function(){
                    return type;
                }
            },
            windowClass: 'xxx-modal-window'
        }).result.then(function(res) {
                console.log("============================");
                console.log(res);
                if(res.filename){
                    $scope.news.imagethumb = res.filename;
                    $scope.vidpath = '';
                    $scope.news.featuredthumbtype='image';
                    $scope.news.featuredthumb = res.filename;
                }else if(res.videoid){
                    $scope.news.imagethumb = '';
                    $scope.news.featuredthumbtype='video';
                    $scope.vidpath=$sce.trustAsHtml(res.video);
                    $scope.news.featuredthumb = res.video;
                }
            });
    }


    $scope.addcategory = function() {
        var modalInstance = $modal.open({
            templateUrl: 'categoryAdd.html',
            controller: function($scope, $modalInstance) {

                var categoryslugs = '';


                $scope.oncategorytitle = function convertToSlug(Text)
                {
                    if(Text == null)
                    {
                        toaster.pop('error' , '', 'Category Title is required.');
                    }
                    else
                    {
                        var text1 = Text.replace(/[^\w ]+/g,'');
                        categoryslugs = angular.lowercase(text1.replace(/ +/g,'-'));
                    }

                }

                $scope.ok = function(category) {

                    category['slugs'] = categoryslugs;
                    Category.addcategory(category, function(data){
                        addedcategory = data.categoryid;
                        if(data.hasOwnProperty('error')){
                            if(data.error.hasOwnProperty('existCategory')){
                                toaster.pop('error' , '', 'Category Title already exists.');
                            }
                        }else {
                            loadcategory();
                            $modalInstance.dismiss('cancel');
                        }
                    })
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                }

            },
            resolve: {

            }
        });
    }

    /* Add Tags*/
    $scope.addtags = function() {
        var modalInstance = $modal.open({
            templateUrl: 'tagsAdd.html',
            controller: function($scope, $modalInstance) {
                var tagsslugs = '';
                console.log('addtags');

                $scope.ontagstitle = function convertToSlug(Text)
                {
                    if(Text == null)
                    {
                        toaster.pop('error' , '', 'Tag Title is required.');
                    }
                    else
                    {
                        var text1 = Text.replace(/[^\w ]+/g,'');
                        tagsslugs = angular.lowercase(text1.replace(/ +/g,'-'));
                    }
                }

                $scope.ok = function(tags) {
                    Tags.addtags(tags, function(data){
                        if(data.hasOwnProperty('error')){
                            if(data.error.hasOwnProperty('existTags')){
                                toaster.pop('error' , '', 'Tag Title already exists.');
                            }
                        }else {
                            loadtags();
                            $modalInstance.close();
                        }
                    });
                    tags['slugs'] = tagsslugs;
                }
                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                }

            },
            resolve: {
            }
        });
    }

})
