'use strict';

/* Controllers */

app.controller('Editauthor', function($scope, $state, Upload ,$q, $http, Config, Author, $stateParams ,$modal, $anchorScroll, toaster){

    $scope.imageloader=false;
    $scope.imagecontent=true;

    $scope.author = {
      name: ""
    };

    var oriauthor = angular.copy($scope.author);


    // console.log(authorid);
    $http({
        url: Config.ApiURL + "/news/editauthor/" + $stateParams.authorid ,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
        $scope.author = data;
        
    }).error(function (data, status, headers, config) {
        $scope.status = status;
    });


    $scope.cutlink = function convertToSlug(Text)
    {
        var texttocut = Config.amazonlink + '/uploads/saveauthorimage/';
        $scope.author.photo = Text.substring(texttocut.length); 
    }


    $scope.updateAuthor = function(author)
    {

        $scope.isSaving = true;
        $http({
            url: Config.ApiURL + "/news/editauthor",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(author)
        }).success(function (data, status, headers, config) {
            console.log(data);
            $scope.isSaving = false;
            toaster.pop('success', '', 'Author successfully updated!');
            $anchorScroll();
        }).error(function (data, status, headers, config) {
            toaster.pop('error', '', 'Something went wrong please check your fields');
        });
     
    }

    var loadimages = function() {

        $http({
            url: Config.ApiURL + "/news/authorlistimages",
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            $scope.imagelist = data;
        }).error(function(data) {
            $scope.status = status;
        });
    }

    loadimages();

    //media
    $scope.media = function(type){
        var modalInstance = $modal.open({
            templateUrl: 'mediagallery.html',
            controller: function($scope, $modalInstance,Config, type) {
                $scope.imageloader=false;
                $scope.imagecontent=true;
                $scope.directory = 'saveauthorimage';
                $scope.videoenabled=false;
                $scope.mediaGallery = 'images';
                $scope.invalidvideo = false;
                $scope.currentSelected = '';
                $scope.currentDeleting = '';
                $scope.type = type;
                $scope.s3link = Config.amazonlink;
                console.log($scope.s3link);

                $scope.set = function(id){
                    $scope.currentSelected = id;
                    $scope.contentvideo = id;
                    if(type=='content'){
                        $scope.copy();
                    }
                }


                $scope.returnImageThumb = function(){
                    return function (item) {
                        if(item){
                            return Config.amazonlink + "/uploads/saveauthorimage/" + item;
                        }else{
                            return item;
                        }
                    };
                }

                var loadimages = function() {
                    Author.loadimages(function(data){
                        console.log(data);
                        $scope.imagelist = data;
                        $scope.imagelength = data.length;
                        });
                }

                loadimages();

                $scope.$watch('files', function () {
                    $scope.upload($scope.files);

                });

                $scope.upload = function (files)
                {

                    var filename
                    var filecount = 0;
                    if (files && files.length)
                    {
                        $scope.imageloader=true;
                        $scope.imagecontent=false;

                        for (var i = 0; i < files.length; i++)
                        {
                            var file = files[i];

                            if (file.size >= 2000000)
                            {
                                toaster.pop('danger', '', 'File ' + file.name + ' is too big');
                                filecount = filecount + 1;

                                if(filecount == files.length)
                                {
                                    $scope.imageloader=false;
                                    $scope.imagecontent=true;
                                }


                            }
                            else

                            {

                                var promises;

                                promises = Upload.upload({

                                    url: Config.amazonlink, //S3 upload url including bucket name
                                    method: 'POST',
                                    transformRequest: function (data, headersGetter) {
                                        //Headers change here
                                        var headers = headersGetter();
                                        delete headers['Authorization'];
                                        return data;
                                    },
                                    fields : {
                                        key: 'uploads/saveauthorimage/' + file.name, // the key to store the file on S3, could be file name or customized
                                        AWSAccessKeyId: Config.AWSAccessKeyId,
                                        acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                        policy: Config.policy, // base64-encoded json policy (see article below)
                                        signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                        "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                    },
                                    file: file
                                })
                                promises.then(function(data){

                                    filecount = filecount + 1;
                                    filename = data.config.file.name;
                                    var fileout = {
                                        'imgfilename' : filename
                                    };
                                    Author.saveImage(fileout, function(data){
                                        loadimages();
                                        if(filecount == files.length)
                                        {
                                            $scope.imageloader=false;
                                            $scope.imagecontent=true;
                                        }
                                    });
                                });
                            }

                        }
                    }
                };

                $scope.deletenewsimg = function (dataimg, $event)
                {
                    var fileout = {
                        'imgfilename' : dataimg
                    };
                    Author.deleteImage(fileout, function(data) {
                        loadimages();

                    });
                    $event.stopPropagation();
                }

                $scope.copy = function() {
                    console.log($scope.contentvideo);
                    var text = '';
                    if($scope.contentvideo.filename){
                        text = Config.amazonlink + '/uploads/saveauthorimage/' + $scope.contentvideo.filename;
                    }else if($scope.contentvideo.videoid){
                        text = $scope.contentvideo.video;
                    }
                    var pp = window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
                    if(pp != "" && pp !== null) {
                        $modalInstance.dismiss('cancel');
                    }
                }

                $scope.ok = function(category) {
                    $modalInstance.close($scope.contentvideo);
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                }

            },
            resolve: {
                type: function(){
                    return type;
                }
            },
            windowClass: 'xxx-modal-window'
        }).result.then(function(res) {
                console.log("============================");
                console.log(res);
                if(res.filename){
                    $scope.author.photo = res.filename;
                    $scope.vidpath = '';
                    $scope.author.featuredthumbtype='image';
                    $scope.author.featuredthumb = res.filename;
                }else if(res.videoid){
                    $scope.author.imagethumb = '';
                    $scope.author.featuredthumbtype='video';
                    $scope.vidpath=$sce.trustAsHtml(res.video);
                    $scope.author.featuredthumb = res.video;
                }
            });
    }
    
 

})
