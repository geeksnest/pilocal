app.controller('ViewMissionCtrl',function($scope, Config, Missions, $modal, $timeout, Upload, $filter, $stateParams, Maps, toaster, $sce) {
    var addedcategory = {};
    $scope.disable = false;
    var comments = [];
    var pincomments = [];
    $scope.commentload = 4;
    $scope.comments = [];

    var parseQueryString = function(queryString) {
        var params = {},
            queries, temp, i, l;

        // Split into key/value pairs
        queries = queryString.split("&");

        // Convert the array of strings into an object
        for (i = 0, l = queries.length; i < l; i++) {
            temp = queries[i].split('=');
            params[temp[0]] = temp[1];
        }

        return params;
    };

    var videocover = function(video) {
        if (video != undefined && video != '' && video != null) {
            var queryString = video.substring(video.indexOf('?') + 1);
            var param = parseQueryString(queryString);

            if (param.hasOwnProperty('v')) {
                $scope.vidpath =  $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" width="100%" height="240px" allowfullscreen></iframe>');
            }
        }
    }

    var viewPinCtrl = function($scope, $modalInstance, pin, slugs) {
        $scope.pin = pin;
        $scope.slides = [];
        $scope.showdelete = false;
        $scope.showsetkingpin = false;
        $scope.showeditpin = false;
        $scope.showmedia = false;
        $scope.spinner = false;
        $scope.showedit = false;
        $scope.showdeleteComment = false;
        $scope.showeditComment = false;
        $scope.deleting = false; 
        $scope.commentload = 0;
        $scope.comments = []; 
        var loadcount = 0;      

        var getpinimages = function(id) {
            Missions.getPinImages(id, function(data) {
                if(data.hasOwnProperty('error')){
                    $modalInstance.dismiss();
                    toaster.pop('error', '', data.error);
                }else {
                    $scope.agent = data.agent;
                    $scope.pin.username = pin.hide_agent == 1 ? data.agent.first_name + " " + data.agent.last_name : "Anonymous Agent"; 
                    $scope.commentcount = data.commentcount;
                    filterslides(data.pinimages, pin.id, pin.map_id);
                    pincomments = data.comments;
                    $scope.loadcomment();
                }
            });
        }

        var viddisp = function(video) {
            if (video != undefined && video != '' && video != null) {
                var queryString = video.substring(video.indexOf('?') + 1);
                var param = parseQueryString(queryString);
                if (param.hasOwnProperty('v')) {
                    $scope.slides.push({ videodisplay: $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" width="100%" height="400px" allowfullscreen></iframe>'), type: "video", image: "http://img.youtube.com/vi/" + param.v + "/hqdefault.jpg", video: video});
                } else {
                    $scope.videodisplay = '';
                    toaster.clear();
                    toaster.pop('error', "", 'Invalid youtube link.');
                }
            } else {
                $scope.videodisplay = '';
            }
        }

        var filterslides = function(markerimages, id, map_id){
            Missions.slides(markerimages, id, map_id, function(data) {
                viddisp($scope.pin.video);
                angular.forEach(data, function(value, key) {
                    $scope.slides[$scope.slides.length] = data[key];
                })
            });

            $timeout(function () {
                $('#imageGallery').lightSlider({
                    gallery:true,
                    item:1,
                    loop:true,
                    thumbItem:9,
                    slideMargin:0,
                    enableDrag: false,
                    currentPagerPosition:'left'
                });
            }, 500);
        }

        getpinimages(pin.id);

        $scope.cancel = function() {
            $modalInstance.dismiss();
        }

        $scope.setkingpin = function() {
            $scope.showsetkingpin = true;
        }   

        $scope.deletemission = function() {
            $scope.showdelete = true;
        }

        $scope.showalbum = function() {
            $scope.showmedia = true;
        }

        $scope.setPin = function() {
            Missions.setKingPin(pin.id, $scope.pin.map_id, function(data) {
                getmap(slugs);
                $modalInstance.dismiss();
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                }else {
                    toaster.pop('error', "", data.error);
                }
            });
        }

        $scope.deletePin = function() {
           Missions.deletePin(pin.id, function(data) {
                getmap(slugs);
                $modalInstance.dismiss();
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                }else {
                    toaster.pop('error', "", data.error);
                }
            });
        }

        $scope.updateyt = function(video) {
            Missions.updatevid(pin.id, video, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                    var queryString = video.substring(video.indexOf('?') + 1);
                    var param = parseQueryString(queryString);
                    if (param.hasOwnProperty('v')) {
                        $scope.slides[0] = {
                            videodisplay: $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" width="100%" height="400px" allowfullscreen></iframe>'),
                            type: "video",
                            image: "http://img.youtube.com/vi/" + param.v + "/hqdefault.jpg",
                            video: video
                        }
                        getmap(slugs);
                    }
                }else {
                    toaster.pop('error', "", data.error);
                }
            });
        }

        $scope.prepare = function(files) {
            var upload = [];
            if (files && files.length) {
                files.map(function(file) {
                    if (file.size >= 2000000) {
                        toaster.clear();
                        toaster.pop('danger', "", "File is too big");
                    } else {
                        $scope.spinner = true;
                        upload.push(file);
                    }
                });
                Missions.saveMarkerPics(upload, $scope.pin.id, $scope.pin.map_id, function(data) {
                    toaster.clear();
                    toaster.pop('success', "", 'Image/s has been successfully uploaded.');
                    Missions.getSlides(pin.id, function(data) {
                        $scope.slides= [];
                        filterslides(data, pin.id, $scope.pin.map_id);
                    });
                    $scope.spinner = $scope.showupload = false;
                });
                $timeout(function() {
                    $scope.flashError = [];
                }, 10000);
            }
        };

        $scope.deleteimg = function(id) {
            $scope.deleting = true;
            Missions.deletepinimg(id, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    angular.forEach($scope.slides, function(value, key) {
                        if(value['id'] == id){
                            $scope.slides.splice(key, 1);
                        }
                    });
                    toaster.pop('success', "", data.success);
                    $scope.deleting = false;
                }else {
                    toaster.pop('danger', "", data.error);
                }
            });
        }

        $scope.loadcomment = function(){
            $scope.commentload += 2;
            angular.forEach(pincomments, function(value, key) {
                if(key <= $scope.commentload && key >= loadcount){
                    loadcount++;
                    $scope.comments.push(value);
                    $scope.comments[key].reply =  $sce.trustAsHtml($scope.comments[key].reply);
                    $scope.comments[key].profile_pic_name = (value['profile_pic_name'] != null ? Config.amazonlink + "/uploads/agentpic/" + value['agent'] + "/" + value['profile_pic_name'] : Config.BaseURL + "/img/agentdefault.png");
                    $scope.comments[key].first_name = $scope.comments[key].first_name != null ? $scope.comments[key].first_name :  "Superagent";
                    $scope.comments[key].last_name = $scope.comments[key].last_name != null ? $scope.comments[key].last_name :  "";
                }
            });
        }

         $scope.addresponse = function(reply) {
            if(reply!=null && reply!=''){
                Missions.addpinreply(pin.id, 'superagent', $scope.pin.map_id, {reply:reply}, function(data) {
                    $scope.commentload +=1;
                    angular.forEach(data.reply, function(value, key) {
                        if(key <= $scope.commentload){
                            $scope.comments[key] = value;
                            $scope.comments[key].reply =  $sce.trustAsHtml($scope.comments[key].reply);
                            $scope.comments[key].profile_pic_name = ($scope.comments[key].profile_pic_name != null ? Config.amazonlink + "/uploads/agentpic/" + $scope.comments[key].agent + "/" + $scope.comments[key].profile_pic_name : Config.BaseURL + "/img/agentdefault.png");
                            $scope.comments[key].first_name = $scope.comments[key].first_name != null ? $scope.comments[key].first_name :  "Superagent";
                            $scope.comments[key].last_name = $scope.comments[key].last_name != null ? $scope.comments[key].last_name :  "";
                        }
                    });
                    $scope.commentcount = data.reply.length;
                    toaster.clear();
                    if(data.hasOwnProperty('success')){
                        toaster.pop('success', "", data.success);
                        $scope.comment = undefined;
                        $(".input.comment").val(undefined);
                        getmap(slugs);
                    }else {
                        toaster.pop('danger', "", data.error);
                    }
                });
            }
        }

        $scope.editResponse = function(id, reply) {
            $scope.id = id;
            $scope.reply = reply;
            $scope.showeditComment = true;
        }

        $scope.editcomment = function() {
            Missions.editres($scope.id, {reply:$scope.reply}, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                }else {
                    toaster.pop('danger', "", data.error);
                }
                getmap(slugs);
                $modalInstance.dismiss();
            });
        }

        $scope.deleteComment = function(id) {
            $scope.replyid = id;
            $scope.showdeleteComment = true;
        }

        $scope.deletec = function() {
            Missions.deleteres($scope.replyid, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                }else {
                    toaster.pop('danger', "", data.error);
                }
                getmap(slugs, null);
                $modalInstance.dismiss();
            });
        }
    }

    var commentthumb = function(pinimages, mapId) {
        angular.forEach(pinimages, function(value, key) {
            $scope.PinImages.push({ image: Config.amazonlink + '/uploads/maps/' + mapId + '/' + value['marker_id'] + '/' + value['filename'], marker_id : value['marker_id']});
        });
    }

    var commentvideo = function(markers, mapid, markerimages) {
        angular.forEach(markers, function(key, value) {
            if(value['video'] != '' && value['video'] != null){
                if (video != undefined && video != '' && video != null) {
                    var queryString = video.substring(video.indexOf('?') + 1);
                    var param = parseQueryString(queryString);
                    if (param.hasOwnProperty('v')) {
                        $scope.PinImages.push({ image: "http://img.youtube.com/vi/" + param.v + "/hqdefault.jpg", marker_id: value['id']});
                    }
                }
            }
        });
        commentthumb(markerimages, mapid);
    }

    var showreplyid = "";

    var getcomments = function() {
        Missions.getComments($scope.mapdetails.id, $scope.commentload, function(data){
            $scope.spinner = false;
            $scope.commentcount = data.commentcount;
            $scope.replies = data.reply;
            $scope.replycount = data.replycount;
            comments = data.comments;
            loadcomment();

            angular.forEach($scope.comments, function(value, key) {
                if(value['showreply']){
                    showreplyid = value['id'];
                }
            });

            angular.forEach(comments, function(value, key) {
                $scope.comments[key] = value;
                if(value['id'] == showreplyid){
                    $scope.comments[key]['showreply'] = true;
                }else {
                    $scope.comments[key]['showreply'] = false;
                }
            });
        });
    }

    $scope.loadcomment = function() {
        $scope.spinner = true;
        $scope.commentload += 3;
        getcomments();
    };

    var loadcomment = function() {
        angular.forEach(comments, function(value, key) {
            if(key <= $scope.commentload){
                $scope.comments[key] = comments[key];
                $scope.comments[key]['profile_pic_name'] = (comments[key].profile_pic_name != null && (comments[key].hide_agent == null ||  comments[key].hide_agent == 1) ? Config.amazonlink + "/uploads/agentpic/" + comments[key].agent + "/" + comments[key].profile_pic_name : Config.BaseURL + "/img/agentdefault.png");
                $scope.comments[key].first_name = $scope.comments[key].first_name != null ? "Agent " + $scope.comments[key].first_name :  "Superagent";
                $scope.comments[key].last_name = $scope.comments[key].last_name != null ? $scope.comments[key].last_name :  "";
               if(key > 0){
                    $scope.comments[key]['showreply'] = false;
                }else {
                    $scope.comments[key]['showreply'] = true;
                }
            }
        });
        angular.forEach($scope.replies, function(value, key) {
            if(key <= $scope.replies.length){
                $scope.replies[key].profile_pic_name = ($scope.replies[key].profile_pic_name != null ? Config.amazonlink + "/uploads/agentpic/" + $scope.replies[key].agent + "/" + $scope.replies[key].profile_pic_name : Config.BaseURL + "/img/agentdefault.png");
            }
        });
    }

    var getmap = function(slug) {
        $scope.missionmarkers = [];
        $scope.tags = [];
        $scope.PinImages = [];
        Missions.getmap(slug, function(data) {
            $scope.mapdetails = data.map;
            $scope.commentcount = data.commentcount;
            $scope.mapactions = data.markers.length;
            $scope.mapdetails.featured = (data.map.featured == 1 ? true : false);
            $scope.mapdetails.status = (data.map.status == 1 ? true : false);
            $scope.mapdetails.coverType=='video' ? videocover($scope.mapdetails.cover) : '';
            $scope.data = data;
            commentvideo(data.markers, data.map.id, data.pinimages);
            comments = data.comments;
            $scope.replies = data.reply;
            loadcomment();
            $scope.replycount = data.replycount;
            $scope.news = data.mapnews;
            $scope.map = {
                center: {
                    latitude: data.markers[0].lat,
                    longitude: data.markers[0].long
                },
                zoom: 10,
                // events: {
                //     click: function(event, a, b) {
                //         if (addpin) {
                //             createMarker(b[0].latLng.lat(), b[0].latLng.lng());
                //         }
                //     }
                // },
                refresh: false,
                options : {
                    styles: [{"featureType":"all","elementType":"all","stylers":[{"invert_lightness":true},{"saturation":10},{"lightness":30},{"gamma":0.5},{"hue":"#435158"}]}]
                }
            };

            angular.forEach(data.markers, function(value, key){
                var pin = '';
                if (value['pin'] == 0 && value['status'] == 1) {
                    pin = Maps.pin;
                } else if(value['pin'] == 1 && value['status'] == 1){
                    pin = Maps.kingpin;
                } else if(value['pin'] == 0 && value['status'] == 0){
                    pin = Maps.pendingpin;
                }

                $scope.missionmarkers.push({
                    id: value['id'],
                    idKey: value['id'],
                    agentid: value['agent'],
                    agentname: (data.markers[key].hide_agent==1 ? data.markers[key].username.length > 9 ? data.markers[key].username.substr(0, 9) + "..." : data.markers[key].username : "Anonymous"),
                    agentpic: (data.markers[key].profile_pic_name != null && data.markers[key].hide_agent==1 ? Config.amazonlink + "/uploads/agentpic/" + data.markers[key].agent + "/" + data.markers[key].profile_pic_name : Config.BaseURL + "/img/agentdefault.png"),
                    coords: {
                        latitude: value['lat'],
                        longitude: value['long']
                    },
                    options: {
                        draggable: false,
                        animation: google.maps.Animation.DROP,
                        icon: pin
                    },
                    WindowWithCustomClass: {
                        options: {
                            boxClass: 'custom-info-window view',
                            disableAutoPan: true
                        }
                    },
                    events: {
                        click: function(marker, eventName, args) {
                            var modalInstance = $modal.open({
                              templateUrl: 'viewPin.html',
                              controller: viewPinCtrl,
                              size: 'lg',
                              resolve: {
                                pin: function() {
                                  return data.markers[key];
                                },
                                slugs: function() {
                                    return $stateParams.slugs;
                                }
                              }
                            });
                        },
                        mouseover: function(marker, eventName, args){
                            angular.forEach($scope.missionmarkers, function(value, key) {
                                if(value['id'] == args.idKey){
                                    $scope.missionmarkers[key].show = true;
                                }
                            });
                        },
                       mouseout: function(marker, eventName, args) {
                            angular.forEach($scope.missionmarkers, function(value, key) {
                                if(value['id'] == args.idKey){
                                    $scope.missionmarkers[key].show = false;
                                }
                            });
                        }
                    },
                    show: false,
                    markerInfo: {},
                    cancel: true
                });
            });

            data.tags.map(function(val){
                $scope.tags.push(val.tag);
            });
            
        });
    }

    getmap($stateParams.slugs);

    var loadcategory = function() {
        Missions.loadcategory(function(data) {
            $scope.data.categories = data;
            console.log($scope.mapdetails.categories);
            console.log($scope.data.categories);
        });
    }

    $scope.addcategory = function() {
        var modalInstance = $modal.open({
            templateUrl: 'addCategory.html',
            controller: function ($scope, $modalInstance, toaster) {
                $scope.save = function (data , catform) {
                    Missions.saveCategory(data, function(res){
                        if(res.hasOwnProperty('success')){
                            addedcategory = res.category;
                            toaster.clear();
                            toaster.pop('success', "", 'Category has been added.');
                            loadcategory();
                            catform.$setPristine();
                            $modalInstance.dismiss();
                        }else if(res.hasOwnProperty('error')){
                            toaster.clear();
                            toaster.pop('warning', "", res.error);
                        }
                    });

                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }).result.finally(function(){
            console.log($scope.mapdetails.categories);
            console.log($scope.data.categories);
            if(addedcategory.hasOwnProperty('id')){
                $scope.mapdetails.categories.push(addedcategory.id);
                addedcategory = {};
            }
        });
    }

    $scope.onnewstitle = function convertToSlug(title, id)
    {
        Missions.titleValidation(id, title, function(data){
            if(data.exists == true){
                toaster.pop('error','',data.error);
                $scope.disable = true;
            } else {
                title = title.replace(/[^\w ]+/g,'');
                $scope.mapdetails.mapslugs = angular.lowercase(title.replace(/ +/g,'-'));
                $scope.disable = false;
            }
        });
    }

    $scope.media = function(type){
        var modalInstance = $modal.open({
            templateUrl: 'mediagallery.html',
            controller: function($scope, $modalInstance,Config, type, mapid) {
                $scope.imageloader=false;
                $scope.imagecontent=true;
                $scope.directory = 'maps/' + mapid;
                $scope.videoenabled=true;
                $scope.mediaGallery = 'all';
                $scope.invalidvideo = false;
                $scope.currentSelected = '';
                $scope.currentDeleting = '';
                $scope.type = type;
                $scope.s3link = Config.amazonlink;

                $scope.set = function(id){
                    console.log(id);
                    $scope.currentSelected = id;
                    $scope.contentvideo = id;
                    if(type=='content'){
                        $scope.copy();
                    }
                }

                var returnYoutubeThumb = function(item){
                    var x= '';
                    var thumb = {};
                    if(item){
                        var newdata = item;
                        var x;
                        x = newdata.video.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
                        thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
                        thumb['yid'] = x[1];
                        return thumb;
                    }else{
                        return x;
                    }
                }



                $scope.returnImageThumb = function(){
                    return function (item) {
                        if(item){
                            return Config.amazonlink + "/uploads/maps/" + mapid + "/" + item;
                        }else{
                            return item;
                        }
                    };
                }

                var loadgallery = function() {
                    Missions.loadgallery(mapid, function(data){
                        if(data.hasOwnProperty('image')){
                            $scope.imagelist = data.image;
                            $scope.imagelength = data.image.length;
                        }
                                console.log(data);
                        if(data.hasOwnProperty('video')){
                            for (var x in data.video){
                                var newd = returnYoutubeThumb(data.video[x]);
                                data.video[x].videourl = newd.url;
                                data.video[x].youtubeid = newd.yid;
                            }

                            $scope.videolist = data.video;
                            $scope.videoslength = data.video.length;
                        }
                    });
                }

                loadgallery();

                $scope.$watch('files', function () {
                    $scope.upload($scope.files);

                });

                $scope.upload = function (files)
                {

                    var filename
                    var filecount = 0;
                    if (files && files.length)
                    {
                        $scope.imageloader=true;
                        $scope.imagecontent=false;

                        for (var i = 0; i < files.length; i++)
                        {
                            var file = files[i];

                            if (file.size >= 2000000)
                            {
                                toaster.pop('error', '', 'File ' + file.name + ' is too big');
                                filecount = filecount + 1;

                                if(filecount == files.length)
                                {
                                    $scope.imageloader=false;
                                    $scope.imagecontent=true;
                                }


                            }
                            else

                            {

                                var promises;

                                promises = Upload.upload({

                                    url: Config.amazonlink, //S3 upload url including bucket name
                                    method: 'POST',
                                    transformRequest: function (data, headersGetter) {
                                        //Headers change here
                                        var headers = headersGetter();
                                        delete headers['Authorization'];
                                        return data;
                                    },
                                    fields : {
                                        key: 'uploads/maps/' + mapid + '/' + file.name, // the key to store the file on S3, could be file name or customized
                                        AWSAccessKeyId: Config.AWSAccessKeyId,
                                        acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                        policy: Config.policy, // base64-encoded json policy (see article below)
                                        signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                        "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                    },
                                    file: file
                                })
                                promises.then(function(data){

                                    filecount = filecount + 1;
                                    filename = data.config.file.name;
                                    var fileout = {
                                        'imgfilename' : filename
                                    };
                                    Missions.saveCover(fileout, mapid, 'image', function(data){
                                        loadgallery();
                                        if(filecount == files.length)
                                        {
                                            $scope.imageloader=false;
                                            $scope.imagecontent=true;
                                        }
                                    });
                                });
                            }

                        }
                    }
                };

                $scope.savevid = function(newsvid) {
                    if(newsvid){
                        var x = newsvid.match(/<iframe width="(.*?)" height="(.*?)" src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
                        console.log(x);
                        if(x==null){
                            $scope.invalidvideo = true;
                            $scope.video='';
                        }else{
                            var newslink = { 'newsvid': newsvid }
                            Missions.saveCover(newslink, mapid, 'video', function(data){
                                loadgallery();
                                $scope.video='';
                                $scope.invalidvideo = false;
                                console.log(data);
                            });
                        }
                    }
                };



                $scope.deletevideo = function (videoid, $event)
                {
                    var datavideo = {
                        'videoid' : videoid
                    };
                    Missions.deleteMedia(datavideo, mapid, 'video', function(data){
                        loadgallery();
                    });
                    $event.stopPropagation();
                }

                $scope.deletenewsimg = function (dataimg, $event)
                {
                    var fileout = {
                        'imgfilename' : dataimg
                    };
                    Missions.deleteMedia(fileout, mapid, 'image', function(data) {
                        loadgallery();

                    });
                    $event.stopPropagation();
                }

                $scope.copy = function() {
                    console.log($scope.contentvideo);
                    var text = '';
                    if($scope.contentvideo.filename){
                        text = Config.amazonlink + '/uploads/newsimage/' + $scope.contentvideo.filename;
                    }else if($scope.contentvideo.videoid){
                        text = $scope.contentvideo.video;
                    }
                    var pp = window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
                    if(pp != "" && pp !== null) {
                        $modalInstance.dismiss('cancel');
                    }
                }

                $scope.ok = function(category) {
                    $modalInstance.close($scope.contentvideo);
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                }

            },
            resolve: {
                type: function(){
                    return type;
                },
                mapid : function() {
                    return $scope.mapdetails.id;
                }
            },
            windowClass: 'xxx-modal-window'
        }).result.then(function(res) {
            if(res.filename){
                $scope.mapdetails.cover = res.filename;
                $scope.mapdetails.coverType = "image";
            }else if(res.videoid){
                $scope.mapdetails.coverType = "video";
                $scope.mapdetails.cover = "https://www.youtube.com/watch?v=" + res.youtubeid;
                $scope.vidpath=videocover("https://www.youtube.com/watch?v=" + res.youtubeid);
            }
        });
    }

    $scope.updateMap = function(data){
        Missions.updatemap($scope.mapdetails.id, data, function(data) {
            if(data.hasOwnProperty('success')) {
                toaster.pop('success', '', data.success);
                getmap($scope.mapdetails.mapslugs);
            }else {
                toaster.pop('error', '', data.error);
            }
        });
    }

    $scope.commentlistener = function(comment) {
        comments = [];
        if(comment!=null && comment!=''){
            Missions.addcomment($scope.mapdetails.id, "superagent", {comment:comment}, "map", null, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                    $scope.comment = undefined;
                    $(".input.comment").val(undefined);
                    getcomments();
                }else {
                    toaster.pop('danger', "", data.error);
                }
            });
        }
    }

    $scope.loadmorereply = function(id) {
        var i = $scope.replies.length;
        while (i--){
            if (id == $scope.replies[i].mapcomment_id){
                $scope.replies.splice(i, 1);
            }
        }

        angular.forEach($scope.replycount, function(value, key) {
            if(id == value['id']){
                $scope.replycount[key].loaded = value['loaded'] +3;
                Missions.loadreply(id, $scope.replycount[key].loaded, value['count'], function(data) {
                    angular.forEach(data, function(value, key) {
                        value['profile_pic_name'] = (value['profile_pic_name'] != null ? Config.amazonlink + "/uploads/agentpic/" + value['agent'] + "/" + value['profile_pic_name'] : Config.BaseURL + "/img/agentdefault.png");
                        $scope.replies.push(value);
                    });
                });
            }
        });
    }

    var deleteCommentCtrl = function($scope, $modalInstance, toaster, id, type) {
        $scope.msg = "Are you sure you want to delete this " + type + "?";

        $scope.delete = function() {
            Missions.deleteComments(id, type, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                    getcomments();
                }else {
                    toaster.pop('danger', "", data.error);
                }
                $modalInstance.dismiss();
            });
        }

        $scope.close = function() {
            $modalInstance.dismiss();
        }
    }

    $scope.deleteComment = function(id, type) {
        var modalInstance = $modal.open({
          templateUrl: 'deleteComment.html',
          controller: deleteCommentCtrl,
          resolve: {
            id: function() {
              return id;
            },
            type: function() {
                return type;
            }
          }
        });
    }

    var editCommentCtrl = function($scope, $modalInstance, toaster, id, msg, type) {
        $scope.msg = "Are you sure you want to edit this " + type + "?";

        $scope.edit = function() {
            Missions.editcomment(id, type, {msg:msg}, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', "", data.success);
                    getcomments();
                }else {
                    toaster.pop('danger', "", data.error);
                }
                $modalInstance.dismiss();
            });
        }

        $scope.close = function() {
            $modalInstance.dismiss();
        }
    }

    $scope.editComment = function(id, msg, type) {
        var modalInstance = $modal.open({
            templateUrl: 'editComment.html',
            controller: editCommentCtrl,
            resolve: {
                id: function() {
                    return id;
                },
                msg: function() {
                    return msg;
                },
                type: function() {
                    return type;
                }
            }
        });
    }

    $scope.showreplybox = function(id) {
        angular.forEach($scope.comments, function(value, key) {
            if(value['id']==id){
                $scope.comments[key]['showreply'] = true;
            }else {
                $scope.comments[key]['showreply'] = false;
            }
        });
    }

    //reply to comment
    $scope.replytocomment = function(reply, commentid, type, marker_id) {
        var i = $scope.replies.length;
        while (i--){
            if (commentid == $scope.replies[i].mapcomment_id){
                $scope.replies.splice(i, 1);
            }
        }
        angular.forEach($scope.replycount, function(value, key) {
            if(value['id'] == commentid){
                $scope.replycount[key].loaded = value['loaded'] + 1;
                $scope.replycount[key].count = value['count'] + 2;
                Missions.replytocomment(commentid, $scope.mapdetails.id, "superagent", type, marker_id, {reply: reply}, $scope.replycount[key].loaded, $scope.replycount[key].count, $scope.commentload, function(data) {
                    toaster.clear();
                    if(data.hasOwnProperty('success')){
                        toaster.pop('success', "", data.success);
                        $scope.comment = undefined;
                        $(".input.reply").val(undefined);
                        // getcomments();
                        angular.forEach($scope.comments, function(value, key) {
                            if(value['showreply']){
                                showreplyid = value['id'];
                            }
                        });

                        angular.forEach(data.comments, function(value, key) {
                            $scope.comments[key] = data.comments[key];
                            $scope.comments[key]['profile_pic_name'] = (value['profile_pic_name'] != null && comments[key].hide_agent == null || comments[key].hide_agent == 1 ? Config.amazonlink + "/uploads/agentpic/" + value['agent'] + "/" + value['profile_pic_name'] : Config.BaseURL + "/img/agentdefault.png");
                            if(value['id'] == showreplyid){
                                $scope.comments[key]['showreply'] = true;
                            }else {
                                $scope.comments[key]['showreply'] = false;
                            }
                        });
                        angular.forEach(data.reply, function(value, key) {
                            value['profile_pic_name'] = (value['profile_pic_name'] != null ? Config.amazonlink + "/uploads/agentpic/" + value['agent'] + "/" + value['profile_pic_name'] : Config.BaseURL + "/img/agentdefault.png");
                            $scope.replies.push(value);
                        });
                    }else {
                        toaster.pop('danger', "", data.error);
                    }
                });
            }
        });
    }

    var loadannouncements = function() {
        Missions.getnewslist($scope.mapdetails.id, function(data) {
            $scope.news = data;
        });
    }

    // news
    var viewMapNewsCtrl = function($scope, $modalInstance, news, agentid, agent, $timeout) {
        $scope.news = news;
        if($scope.news.coverType == 'image'){
           $scope.news.cover = Config.amazonlink + "/uploads/mapnews/" + news.mapid + "/" + news.cover;
        }
        console.log($scope.news.cover);
        $scope.agentid = agentid;
        $scope.agent = agent;
        $scope.deleteshow = false;
        $scope.editshow = false;
        $scope.close = function() {
            $modalInstance.close();
        }
        if(news.coverType=='video'){
            var video = news.cover;
            if (video !== undefined) {
                var queryString = video.substring(video.indexOf('?') + 1);
                var param = parseQueryString(queryString);
                if (param.hasOwnProperty('v')) {
                    $scope.video=video;
                    $scope.videodisplay = $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" style="width:500px; height:300px;" allowfullscreen></iframe>');
                } else {
                    $scope.video='';
                    $scope.videodisplay = '';
                    toaster.clear();
                    toaster.pop('error', "", 'Invalid youtube link.');
                }
            } else {
                $scope.videodisplay = '';
            }
        }

        $scope.close = function() {
            $modalInstance.dismiss();
        }

        $scope.deleteNews = function() {
            $scope.deleteshow = true;
        }

        $scope.delete = function() {
            Missions.deletenews(news.id, function(data) {
                toaster.clear();
                if(data.hasOwnProperty('success')){
                    loadannouncements();
                    $modalInstance.dismiss();
                    toaster.pop('success', "", data.success);
                }else {
                    toaster.pop('error', "", data.error);
                }
            });
        }

        $scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
        $scope.generateThumb = function(file) {
            if (file != null) {
                $scope.file=file;
                if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
                    $timeout(function() {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function(e) {
                            $timeout(function() {
                                file.dataUrl = e.target.result;
                                $scope.disable = false;
                            });
                        }
                    });
                }
            }
        };
    }

    $scope.viewmapnews = function(news) {
        var modalInstance = $modal.open({
            templateUrl: 'viewNews.html',
            controller: viewMapNewsCtrl,
            resolve: {
                news: function() {
                    return news;
                },
                agentid: function() {
                    return $scope.mapdetails.agent;
                },
                agent: function() {
                    return $scope.agentid;
                }
            }
        }).result.finally(function(){
            loadannouncements();
        });
    }


    var viewmapnews = function(news) {
        $scope.viewmapnews(news);
    }

    var loadednews = 3;
    $scope.seemore = function() {
        var modalInstance = $modal.open({
            templateUrl: 'viewNewsList.html',
            controller: function($scope, $modalInstance, mapid) {
                $scope.amazonlink = Config.amazonlink;
                var loadnews = function(){
                    loadednews +=2;
                    Missions.getnews(mapid, loadednews, function(data) {
                        $scope.news = data.news;
                        console.log($scope.news);
                        $scope.count = data.count;
                    });
                }

                loadnews();

                $scope.loadmore = function() {
                    loadnews();
                }

                $scope.viewnews = function(news) {
                    viewmapnews(news);
                }
                $scope.close = function() {
                    $modalInstance.dismiss();
                }
            },
            resolve: {
                mapid: function() {
                    return $scope.mapdetails.id;
                }
            }
        });
    }
});
