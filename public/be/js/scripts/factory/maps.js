app.factory('Maps', function($http, store, jwtHelper, Upload, $q, Config) {
	return {
		currentPin: null,
		currentMap: null,
		mapDetails: {title: '', description: '', category: '', tags:''},
		markerPanel: false,
		markers: [],
		missionmarkers:[],
		pinImages: [],
		showDelete: false,
		showMapInfo: false,
		showMapCover: false,
		showMakeKingpin: false,
		coverType: '',
		coverVal: '',
		mapSlugs:'',
		mapCategory:'',
		mapTags: '',
		setCurrentPin: function(val) {
			this.currentPin = val;
		},
		setCurrentMap: function(val) {
			this.currentMap = val;
		},
		getMarkerKey: function(obj, val) {
			for (var i in obj) {
				if (obj[i].hasOwnProperty['id']) {
					if (obj[i][prop] == val) {
						return i;
					}
				}
				if (obj[i].hasOwnProperty['idKey']) {
					if (obj[i][prop] == val) {
						return i;
					}
				}

			}
		},
		getToggleMarkerInfo: function() {
			return this.markerPanel;
		},
		setToggleMarkerInfo: function(bol) {
			return this.markerPanel = bol;
		},
		saveCoverPic: function(type , file, mapid, callback) {
			if(type=='image'){
				var promises = Upload.upload({
					url: Config.bucket, //S3 upload url including bucket name
					method: 'POST',
					transformRequest: function(data, headersGetter) {
						//Headers change here
						var headers = headersGetter();
						delete headers['Authorization'];
						return data;
					},
					fields: {
						key: 'uploads/maps/' + mapid + '/' + file.name, // the key to store the file on S3, could be file name or customized
						AWSAccessKeyId: Config.AWSAccessKeyId,
						acl: 'private', // sets the access to the uploaded file in the bucket: private or public
						policy: Config.policy, // base64-encoded json policy (see article below)
						signature: Config.signature, // base64-encoded signature based on policy string (see article below)
						"Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
					},
					file: file
				});

				promises.then(function(data) {
					return callback(mapid);
				});
			}else{
				return callback(mapid);
			}
		},
		saveNewsPic: function(type, file, mapid, callback) {
			if(type=='image'){
				var promises = Upload.upload({
					url: Config.bucket, //S3 upload url including bucket name
					method: 'POST',
					transformRequest: function(data, headersGetter) {
						//Headers change here
						var headers = headersGetter();
						delete headers['Authorization'];
						return data;
					},
					fields: {
						key: 'uploads/mapnews/' + mapid + '/' + file.name, // the key to store the file on S3, could be file name or customized
						AWSAccessKeyId: Config.AWSAccessKeyId,
						acl: 'private', // sets the access to the uploaded file in the bucket: private or public
						policy: Config.policy, // base64-encoded json policy (see article below)
						signature: Config.signature, // base64-encoded signature based on policy string (see article below)
						"Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
					},
					file: file
				});

				promises.then(function(data) {
					return callback(mapid);
				});
			}else{
				return callback(mapid);
			}
		},
		saveMarkerPics: function(files, idKey, mapId, callback) {
			var promises;
			var x = 0;
			var success = false;
			files.map(function(file) {
				promises = Upload.upload({
					url: Config.bucket, //S3 upload url including bucket name
					method: 'POST',
					transformRequest: function(data, headersGetter) {
						//Headers change here
						var headers = headersGetter();
						delete headers['Authorization'];
						return data;
					},
					fields: {
						key: 'uploads/maps/' +  mapId + "/" + idKey + '/' + file.name, // the key to store the file on S3, could be file name or customized
						AWSAccessKeyId: Config.AWSAccessKeyId,
						acl: 'private', // sets the access to the uploaded file in the bucket: private or public
						policy: Config.policy, // base64-encoded json policy (see article below)
						signature: Config.signature, // base64-encoded signature based on policy string (see article below)
						"Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
					},
					file: file
				});

				promises.then(function(data) {
					console.log('THEN PROMISES');
					x++;
					var imagefile = {
						id : idKey,
						file : file.name
					}
					$http({
		                url: Config.ResourceUrl + '/save/markerimage',
		                method: 'post',
		                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
						data : $.param(imagefile)
		            }).success(function(data, status, headers, config) {
						return callback(data);
		            }).error(function(data, status, headers, config) {
		                    // called asynchronously if an error occurs
		                    // or server returns response with an error status.
		            });
					if (x === files.length) {
						return callback(data);
					}
				});
			});
		},
		showTrueMarker: function(id) {
			for (var x in this.markers) {
				if (this.markers[x].idKey == id) {
					this.markers[x].show = true;
					return x;
				}
			}
		},
		convertImageSize: function(bytes) {
			if (bytes == 0) return 0;
			var k = 1000;
			var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
			var i = Math.floor(Math.log(bytes) / Math.log(k));
			return (bytes / Math.pow(k, i)).toPrecision(3);
		},
		pin: {
			url: '/img/pin.png',
			// This marker is 20 pixels wide by 32 pixels tall.
			size: new google.maps.Size(23, 30),
			// The origin for this image is 0,0.
			origin: new google.maps.Point(0, 0),
			// The anchor for this image is the base of the flagpole at 0,32.
			anchor: new google.maps.Point(9, 34)
		},
		kingpin: {
			url: '/img/kingpin.png',
			// This marker is 20 pixels wide by 32 pixels tall.
			size: new google.maps.Size(30, 45),
			// The origin for this image is 0,0.
			origin: new google.maps.Point(0, 0),
			// The anchor for this image is the base of the flagpole at 0,32.
			anchor: new google.maps.Point(11, 43)
		},
		categories: function(callback) {
			$http({
				url: Config.ResourceUrl + '/missions/category/get',
				method: 'get',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				return callback(data);
			}).error(function(data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});
		},
		tags: function(callback) {
			$http({
				url: Config.ResourceUrl + '/missions/tags/get',
				method: 'get',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				return callback(data);
			}).error(function(data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});
		},
		pendingpin: {
			url: '/img/pendingpin.png',
			// This marker is 20 pixels wide by 32 pixels tall.
			size: new google.maps.Size(23, 34),
			// The origin for this image is 0,0.
			origin: new google.maps.Point(0, 0),
			// The anchor for this image is the base of the flagpole at 0,32.
			anchor: new google.maps.Point(10, 34)
		}
	}
})
