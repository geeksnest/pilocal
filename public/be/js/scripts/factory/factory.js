/**
 * Created by ebautistajr on 3/30/15.
 */

app.factory('Login', function($http, Config) {
    return {
        requestAccess: function (login, callback) {
            $http({
                url: Config.ApiURL + '/superagent/login',
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data : $.param(login)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        },
        logout: function(callback){
            $http({
                url: Config.ApiURL + '/user/logout',
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data, status, headers, config) {
                callback(data);
            })
        }
    }
});