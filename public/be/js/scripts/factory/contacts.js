app.factory('Contacts', function($http, Config) {
    return {
        list: function(num, off, keyword, date, callback) {
    	 	$http({
	            url: Config.ApiURL +"/contacts/list/" + num + '/' + off + '/' + keyword + '/' + date,
	            method: "GET",
	            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	        }).success(function (data, status, headers, config) {
	           callback(data);
	        }).error(function (data, status, headers, config) {
	           callback(data);
	        });
        },
        delete: function(id, callback) {
            $http({
                url: Config.ApiURL + "/contacts/delete/" + id,
                method: "post",
                headers: {"Content-Type" : "application/x-www-form-urlencoded"}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        getContact: function(id, callback) {
    	 	$http({
	            url: Config.ApiURL +"/contacts/get/" + id,
	            method: "GET",
	            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	        }).success(function (data, status, headers, config) {
	           callback(data);
	        }).error(function (data, status, headers, config) {
	           callback(data);
	        });
        },
        reply: function(id, data, callback) {
            $http({
                url: Config.ApiURL + "/contacts/reply/" + id,
                method: "post",
                headers: {"Content-Type" : "application/x-www-form-urlencoded"},
                data: $.param({ reply: data })
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        }
    }
});
