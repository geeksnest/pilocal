/**
 * Created by ebautistajr on 3/30/15.
 */

app.factory('Missions', function($http, Config, Upload) {
    return {
        list: function(num, off, keyword, status, callback){
            $http({
                url: Config.ApiURL + '/superagent/missions/list/' + num + '/' + off + '/' + keyword + '/' + status,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        },
        setFG: function(val, id, type,callback){
            $http({
                url: Config.ApiURL + '/superagent/missions/fg/' + val + '/' + id + '/' + type,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        },
        delete: function(id, callback) {
            $http({
                url: Config.ApiURL + '/map/delete/' + id,
                method: 'POST',
                headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        setstatus: function(status, id, callback) {
            $http({
                url: Config.ApiURL + '/map/changestatus/' + id + '/' + status,
                method: 'POST',
                headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        getmap:function(slug, callback) {
            $http({
                url: Config.ApiURL + '/superagent/missionmap/' + slug,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        },
        saveCategory: function(category, callback) {
            $http({
                url: Config.ApiURL + '/superagent/missions/addcategory',
                method: 'POST',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(category)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        },
        loadcategory: function(callback){
            $http({
                url: Config.ApiURL + '/superagent/getcategories',
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        },
        titleValidation: function (id, title, callback) {
            $http({
                url: Config.ApiURL + '/superagent/titleexist/' + id + "/" + title,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        loadgallery: function(id, callback) {
            $http({
                url: Config.ApiURL + '/superagent/loadmapgallery/' + id,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        saveCover: function(fileout, id, type, callback){
            $http({
                url: Config.ApiURL + "/map/saveimage/" + id + '/' + type,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fileout)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        deleteMedia: function(fileout, id, type, callback){
            $http({
                url: Config.ApiURL + "/map/deletemapmedia/" + id + '/' + type,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fileout)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        updatemap: function(id, data, callback) {
            $http({
                url: Config.ApiURL + "/superagent/map/update/" + id,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        getPinImages: function(id, callback) {
            $http({
                url: Config.ApiURL + '/superagent/getPinImages/' + id,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        slides: function(images, pinid, mapid, callback) {
            var slides =[];
            var x = 0;
            angular.forEach(images, function(value, key) {
                if(pinid == value['marker_id']){
                    slides.push({ id:value['id'], image: Config.amazonlink + '/uploads/maps/' + mapid + '/' + pinid + '/' + value['filename'], type: "image" });
                    x++;
                }
            });
            callback(slides);
        },
        setKingPin: function(id, mapid, callback) {
            $http({
                url: Config.ApiURL + '/pin/setkingpin/' + id + '/' + mapid,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        deletePin: function(id, callback) {
            $http({
                url: Config.ApiURL + '/map/deletepins/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        updatevid: function(id, data, callback){
            $http({
                url: Config.ApiURL + '/pin/updatevideo/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({video:data})
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        saveMarkerPics: function(files, idKey, mapId, callback) {
            var promises;
            var x = 0;
            var success = false;
            files.map(function(file) {
                promises = Upload.upload({
                    url: Config.amazonlink, //S3 upload url including amazonlink name
                    method: 'POST',
                    transformRequest: function(data, headersGetter) {
                        //Headers change here
                        var headers = headersGetter();
                        delete headers['Authorization'];
                        return data;
                    },
                    fields: {
                        key: 'uploads/maps/' +  mapId + "/" + idKey + '/' + file.name, // the key to store the file on S3, could be file name or customized
                        AWSAccessKeyId: Config.AWSAccessKeyId,
                        acl: 'private', // sets the access to the uploaded file in the amazonlink: private or public
                        policy: Config.policy, // base64-encoded json policy (see article below)
                        signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                        "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                    },
                    file: file
                });

                promises.then(function(data) {
                    console.log('THEN PROMISES');
                    x++;
                    var imagefile = {
                        id : idKey,
                        file : file.name
                    }
                    $http({
                        url: Config.ApiURL + '/save/markerimage',
                        method: 'post',
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data : $.param(imagefile)
                    }).success(function(data, status, headers, config) {
                        return callback(data);
                    }).error(function(data, status, headers, config) {
                            // called asynchronously if an error occurs
                            // or server returns response with an error status.
                    });
                    if (x === files.length) {
                        return callback(data);
                    }
                });
            });
        },
        getSlides: function(pinid, callback) {
            $http({
                url: Config.ApiURL + '/getpin/images/' + pinid,
                method: 'get',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        deletepinimg: function(id, callback) {
            $http({
                url: Config.ApiURL + '/pin/deleteimg/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        getComments: function(id, limit, callback) {
            $http({
                url: Config.ApiURL + '/getmapcomments/' + id + '/' + limit,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        deleteres: function(id, callback) {
            $http({
                url: Config.ApiURL + '/deleteresponse/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        addpinreply: function(marker_id, agent, map_id, data, callback) {
            $http({
                url: Config.ApiURL + '/savepinreply/' + marker_id + '/' + agent + '/' + map_id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        editres: function(id, data, callback) {
            $http({
                url: Config.ApiURL + '/editresponse/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        addcomment: function(id, agent, data, type, pinid, callback) {
            $http({
                url: Config.ApiURL + '/savecomment/' + id + '/' + agent + '/' + type + '/' + pinid,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        loadreply: function(id, offset, count, callback) {
            $http({
                url: Config.ApiURL + '/getcommentreply/' + id + '/' + offset + '/' + count,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        deleteComments: function(id, type, callback) {
            $http({
                url: Config.ApiURL + '/deletecomment/' + id + '/' + type,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        editcomment: function(id, type, data, callback) {
            $http({
                url: Config.ApiURL + '/editcomment/' + id + '/' + type,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        replytocomment: function(commentid, map_id, agent, type, marker_id, data, offset, count, commentload, callback) {
            $http({
                url: Config.ApiURL + '/savecommentreply/' + commentid + '/' + agent + '/' + map_id + '/' + type + '/' + marker_id + '/' + offset + '/' + count + '/' + commentload,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        getnews: function(mapid, limit, callback) {
            $http({
                url: Config.ApiURL + '/map/getnews/' + mapid + '/' + limit,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        deletenews: function(id, callback) {
            $http({
                url: Config.ApiURL + '/map/deletenews/' + id,
                method: 'post',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        },
        getnewslist: function(id, callback) {
            $http({
                url: Config.ApiURL + '/map/getnewslist/' + id,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
        }
    }
});