/**
 * Created by ebautistajr on 3/30/15.
 */

app.factory('News', function($http, Config) {
    return {
        loadcategory: function(callback){
            $http({
                url: Config.ApiURL + "/news/listcategory",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },

        loadtags: function(callback){
            $http({
                url: Config.ApiURL + "/news/listtags",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {         
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },

    	loadauthor: function(callback){
    	 	$http({
    	 		url: Config.ApiURL + "/news/listauthor",
    	 		method: "GET",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		}
    	 	}).success(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	});
    	 },
        add: function(news,callback){
            $http({
                url: Config.ApiURL + "/news/create",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(news)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({error: { msg: 'Something went wrong please check your fields'}});
            });
        },
        updateNews: function(news, callback) {
            $http({
                url: Config.ApiURL + "/news/updatenews",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(news)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                console.log(data)
            });
        },
        loadimages: function(callback){
            $http({
                url: Config.ApiURL + "/news/listimages",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
        loadVideo: function(callback){
            $http({
                url: Config.ApiURL + "/news/listvideo",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
        saveImage: function(fileout, callback){
            $http({
                url: Config.ApiURL + "/news/saveimage",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fileout)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        saveVid: function(newslink, callback){
            $http({
                url: Config.ApiURL + "/news/addvid",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(newslink)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        deleteVideo: function(datavideo, callback){
            $http({
                url: Config.ApiURL + "/news/deletevideo",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(datavideo)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        deleteImage: function(fileout, callback){

            $http({
                url: Config.ApiURL + "/news/deletenewsimg",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fileout)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        list: function(num, off, keyword, sort, callback){
    	 	$http({
	            url: Config.ApiURL +"/news/managenews/" + num + '/' + off + '/' + keyword + '/' + sort,
	            method: "GET",
	            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	        }).success(function (data, status, headers, config) {
	           data = data;
	           pagetotalitem = data.total_items;
	           currentPage = data.index;
	           callback(data);
	        }).error(function (data, status, headers, config) {
	           callback(data);
	        });
    	 },
		 delete: function(newsid, callback){
			$http({
				url: Config.ApiURL + "/news/newsdelete/" + newsid,
				method: "POST",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		 },
		 updateNewsStatus: function(status,newsid, callback){
			 $http({
				 url: Config.ApiURL + "/news/updatenewsstatus/"+status+"/" + newsid,
				 method: "POST",
				 headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			 }).success(function (data, status, headers, config) {
				 callback(data);
			 }).error(function (data, status, headers, config) {
				 callback(data);
			 });
		 },
         updateFeatured: function(newsid, featured, callback) {
             $http({
                 url: Config.ApiURL + "/news/updatenewsfeatured/"+newsid+"/" + featured,
                 method: "POST",
                 headers: {'Content-Type': 'application/x-www-form-urlencoded'}
             }).success(function (data, status, headers, config) {
                 callback(data);
             }).error(function (data, status, headers, config) {
                 callback(data);
             });
         },
         validateSlug: function(slug, id, callback) {
            $http({
                url: Config.ApiURL +"/news/validateSlug/" + slug + '/' + id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
               data = data;
               pagetotalitem = data.total_items;
               currentPage = data.index;
               callback(data);
            }).error(function (data, status, headers, config) {
               callback(data);
            }); 
         }
    }
});