app.factory('Author', function($http, $q, Config){
  	return {
    	loadcategory: function(callback){
    	 	$http({
    	 		url: Config.ApiURL + "/news/listcategory",
    	 		method: "GET",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		}
    	 	}).success(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	});
    	},
        loadimages: function(callback){
            $http({
                url: Config.ApiURL + "/news/authorlistimages",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
        deleteImage: function(fileout, callback){
            $http({
                url: Config.ApiURL + "/news/deleteauthorimg",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fileout)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        saveImage: function(fileout, callback){
           $http({
                url: Config.ApiURL + "/news/saveauthorimage",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fileout)
            }).success(function (data, status, headers, config) {
                callback(data);
                
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        list: function(num, off, keyword , callback){
            $http({
                        url: Config.ApiURL +"/news/manageauthor/" + num + '/' + off + '/' + keyword,
                        method: "GET",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {
                       data = data;
                       callback(data);
                       pagetotalitem = data.total_items;
                       currentPage = data.index;
                       
                    }).error(function (data, status, headers, config) {
                       callback(data);
                    });
         },
        delete: function(authorid, callback){
            $http({
                url: Config.ApiURL + "/news/authordelete/" + authorid,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        loadauthornews: function(authorid, callback){
            $http({
                url: Config.ApiURL + "/news/loadauthornews/" + authorid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        updatenewsauthors: function(data, callback){
            $http({
                url: Config.ApiURL + "/news/updatenewsauthors",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        }

    }
   
})