app.factory('Tags', function($http, $q, Config){
    return {
        loadtags: function(callback){
            $http({
                url: Config.ApiURL + "/news/listtags",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
        },

        loadauthor: function(callback){
            $http({
                url: Config.ApiURL + "/news/listauthor",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
        },

        addtags: function(tags, callback){
            $http({
                url: Config.ApiURL + "/news/savetags",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(tags)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
            });
        },

        list: function(num, off, keyword, callback){
            $http({
                url: Config.ApiURL + "/news/managetags/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },

        delete: function(id, callback){
            $http({
                url: Config.ApiURL + "/news/tagsdelete/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },

        update: function(data, memid, callback){
            $http({
                url: Config.ApiURL + "/news/updatetags/" + data + "/"+memid,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        conflicts: function(id, callback) {
            $http({
                url: Config.ApiURL + "/tags/loadconflict/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        updateconflict: function(data, callback){
            $http({
                url: Config.ApiURL + "/tags/updateconflict",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        }
    }

})
