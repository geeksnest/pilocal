/**
 * Created by ebautistajr on 3/30/15.
 */

app.factory('MissionsCategory', function($http, Config) {
    return {
        save: function(data, callback){
            $http({
                url: Config.ApiURL + '/superagent/missions/category/add',
                method: 'POST',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        },
        update: function(data, callback){
            $http({
                url: Config.ApiURL + '/superagent/missions/category/update',
                method: 'POST',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        },
        list: function(num, page, keyword, callback){
            $http({
                url: Config.ApiURL + '/superagent/missions/category/list/' + num + '/'+ page +'/' + keyword,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        },
        delete: function(id, callback){
            $http({
                url: Config.ApiURL + '/superagent/missions/category/delete/'+id,
                method: 'DELETE',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {

            });
        }
    }
});