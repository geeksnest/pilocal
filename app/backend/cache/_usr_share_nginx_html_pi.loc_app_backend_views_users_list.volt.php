<?php echo $this->getContent(); ?>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">User List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Users
    </div>
    <div class="row wrapper">
      <div class="col-sm-5 m-b-xs">
        <select class="input-sm form-control w-sm inline v-middle">
          <option value="0">Bulk action</option>
          <option value="1">Delete selected</option>
          <option value="2">Bulk edit</option>
          <option value="3">Export</option>
        </select>
        <button class="btn btn-sm btn-default">Apply</button>                
      </div>
      <div class="col-sm-4">
      </div>
      <div class="col-sm-3">
        <div class="input-group">
          <input class="input-sm form-control" placeholder="Search" type="text">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <input type="hidden" ng-init='userdata = <?php echo $data; ?>'>
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th style="width:20px;">
              <label class="i-checks m-b-none">
                <input type="checkbox"><i></i>
              </label>
            </th>
            <th>Name</th>
            <th>Email</th>
            <th>Username</th>
            <th>Status</th>
            <th>Task</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="user in userdata.users">
            <td><label class="i-checks m-b-none"><input name="post[]" type="checkbox"><i></i></label></td>
            <td>{[{ user.firstname }]} {[{ user.lastname }]}</td>
            <td>{[{ user.email }]}</td>
            <td>{[{ user.username }]}</td>
            <td><span ng-class="">{[{ user.agentStatus }]}</span></td>
            <td>
              <a href="" class="active" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i><i class="fa fa-times text-danger text"></i></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
        <div class="col-sm-4 hidden-xs">
          <select class="input-sm form-control w-sm inline v-middle">
            <option value="0">Bulk action</option>
            <option value="1">Delete selected</option>
            <option value="2">Bulk edit</option>
            <option value="3">Export</option>
          </select>
          <button class="btn btn-sm btn-default">Apply</button>                  
        </div>
        <div class="col-sm-4 text-center">
          <small class="text-muted inline m-t-sm m-b-sm">showing 20-30 of 50 items</small>
        </div>
        <div class="col-sm-4 text-right text-center-xs">                
          <ul class="pagination">
            <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" class="paginate_button previous disabled"><a class="fa fa-chevron-left" href=""></a></li>
            <li ng-repeat="page in userdata.pages" ng-class="{ 'active' : userdata.index == page.num  }" class="paginate_button">
              <a ng-click="numpages(page.num)"> {[{ page.num }]}</a>
            </li>
            <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" class="paginate_button next"><a href="#" class="fa fa-chevron-right"></a></li>
          </ul>
        </div>
     </div>
    </footer>
  </div>
</div>