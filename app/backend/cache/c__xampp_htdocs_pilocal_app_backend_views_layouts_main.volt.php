<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <?php echo $this->tag->getTitle(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  <?php echo $this->tag->stylesheetLink('be/css/bootstrap.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/animate.css'); ?>
  <?php echo $this->tag->stylesheetLink('vendors/font-awesome/css/font-awesome.min.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/simple-line-icons.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/font.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/app.css'); ?>
  <?php echo $this->tag->stylesheetLink('vendors/AngularJS-Toaster/toaster.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/custom.css'); ?>
  <?php echo $this->tag->stylesheetLink('vendors/angular-xeditable/dist/css/xeditable.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/js/jquery/chosen/chosen.css'); ?>
  <?php echo $this->tag->stylesheetLink('vendors/angular-ui-select/dist/select.min.css'); ?>
  <?php echo $this->tag->stylesheetLink('vendors/lightslider/dist/css/lightslider.min.css'); ?>

  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon
  <link rel="shortcut icon" href="<?php echo $this->url->getBaseUri(); ?>public/img/favicon/favicon.png">-->
</head>
<body ng-controller="AppCtrl">
<?php echo $this->getContent(); ?>
<div class="app" id="app" ng-class="{'app-header-fixed':app.settings.headerFixed, 'app-aside-fixed':app.settings.asideFixed, 'app-aside-folded':app.settings.asideFolded}">
    <div class="app-header navbar">
      <!-- navbar header -->
      <div class="navbar-header bg-white">
        <button class="pull-right visible-xs dk" ui-toggle-class="show" data-target=".navbar-collapse">
          <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle-class="off-screen" data-target=".app-aside" ui-scroll="app">
          <i class="glyphicon glyphicon-align-justify"></i>
        </button>
        <!-- brand -->
        <a href="#/" class="navbar-brand text-lt">
          <img src="/img/assets/logo.png" alt=".">
          <span class="hidden-folded m-l-xs">{[{app.name}]}</span>
        </a>
        <!-- / brand -->
      </div>
      <!-- / navbar header -->

      <!-- navbar collapse -->
      <div class="collapse navbar-collapse box-shadow {[{app.settings.navbarCollapseColor}]}">
        <!-- buttons -->
        <div class="nav navbar-nav m-l-sm hidden-xs">
          <a href class="btn no-shadow navbar-btn" ng-click="app.settings.asideFolded = !app.settings.asideFolded">
            <i class="fa {[{app.settings.asideFolded ? 'fa-indent' : 'fa-dedent'}]} fa-fw"></i>
          </a>
          <a href class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
            <i class="icon-user fa-fw"></i>
          </a>
        </div>
        <!-- / buttons -->

        <!-- link and dropdown -->
        <ul class="nav navbar-nav hidden-sm">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
              <span translate="header.navbar.new.NEW">New</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="#">
                  <span class="badge bg-info pull-right">5</span>
                  <span translate="header.navbar.new.NEWS">Task</span>
                </a>
              </li>
              <li><a href="#" translate="header.navbar.new.BLOG">User</a></li>
              <li>
                <a href="#">
                  <span class="badge bg-danger pull-right">4</span>
                  <span translate="header.navbar.new.PAGE">Email</span>
                </a>
              </li>
              <li>
                <a href="#">
                  <span class="badge bg-danger pull-right">4</span>
                  <span translate="header.navbar.new.USER">Email</span>
                </a>
              </li>
            </ul>
          </li>
        </ul>
        <!-- / link and dropdown -->

        <!-- search form -->
        <form class="navbar-form navbar-form-sm navbar-left shift" ui-shift="prependTo" target=".navbar-collapse" role="search" ng-controller="TypeaheadDemoCtrl">
          <div class="form-group">
            <div class="input-group">
              <input type="text" ng-model="selected" typeahead="state for state in states | filter:$viewValue | limitTo:8" class="form-control input-sm bg-light no-border rounded padder" placeholder="Search projects...">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-sm bg-light rounded"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </div>
        </form>
        <!-- / search form -->

        <!-- nabar right -->
        <ul class="nav navbar-nav navbar-right">
          <li class="hidden-xs">
            <a ui-fullscreen></a>
          </li>
          <li class="dropdown">
            <a href class="dropdown-toggle">
              <i class="icon-bell fa-fw"></i>
              <span class="visible-xs-inline">Notifications</span>
              <span class="badge badge-sm up bg-danger pull-right-xs">2</span>
            </a>
            <!-- dropdown -->
            <div class="dropdown-menu w-xl animated fadeInUp">
              <div class="panel bg-white">
                <div class="panel-heading b-light bg-light">
                  <strong>You have <span>2</span> notifications</strong>
                </div>
                <div class="list-group">
                  <a href class="media list-group-item">
                    <span class="pull-left thumb-sm">
                      <img src="/img/a0.jpg" alt="..." class="img-circle">
                    </span>
                    <span class="media-body block m-b-none">
                      Use awesome animate.css<br>
                      <small class="text-muted">10 minutes ago</small>
                    </span>
                  </a>
                  <a href class="media list-group-item">
                    <span class="media-body block m-b-none">
                      1.0 initial released<br>
                      <small class="text-muted">1 hour ago</small>
                    </span>
                  </a>
                </div>
                <div class="panel-footer text-sm">
                  <a href class="pull-right"><i class="fa fa-cog"></i></a>
                  <a href="#notes" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                </div>
              </div>
            </div>
            <!-- / dropdown -->
          </li>
          <li class="dropdown">
            <a href class="dropdown-toggle clear" data-toggle="dropdown">
              <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                <img src="/img/a0.jpg" alt="...">
                <i class="on md b-white bottom"></i>
              </span>
              <span class="hidden-sm hidden-md"><?php echo $username['pi_username']; ?></span> <b class="caret"></b>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu animated fadeInRight w">
              <li class="wrapper b-b m-b-sm bg-light m-t-n-xs">
                <div>
                  <p>300mb of 500mb used</p>
                </div>
                <progressbar value="60" class="progress-xs m-b-none bg-white"></progressbar>
              </li>
              <li>
                <a href>
                  <span class="badge bg-danger pull-right">30%</span>
                  <span>Settings</span>
                </a>
              </li>
              <li>
                <a ui-sref="app.page.profile">Profile</a>
              </li>
              <li>
                <a ui-sref="app.docs">
                  <span class="label bg-info pull-right">new</span>
                  Help
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a ng-click="logout()">Logout</a>
              </li>
            </ul>
            <!-- / dropdown -->
          </li>
        </ul>
        <!-- / navbar right -->

      </div>
      <!-- / navbar collapse -->
      </div>

  <!-- menu -->
  <div class="app-aside hidden-xs {[{app.settings.asideColor}]}">
<div class="aside-wrap">
  <div class="navi-wrap">
    <!-- user -->
    <div class="clearfix hidden-xs text-center hide" id="aside-user">
      <div class="dropdown wrapper">
        <a ui-sref="app.page.profile">
          <span class="thumb-lg w-auto-folded avatar m-t-sm">
            <img src="/img/a0.jpg" class="img-full" alt="...">
          </span>
        </a>
        <a href class="dropdown-toggle hidden-folded">
          <span class="clear">
            <span class="block m-t-sm">
              <strong class="font-bold text-lt">John.Smith</strong>
              <b class="caret"></b>
            </span>
            <span class="text-muted text-xs block">Art Director</span>
          </span>
        </a>
        <!-- dropdown -->
        <ul class="dropdown-menu animated fadeInRight w hidden-folded">
          <li class="wrapper b-b m-b-sm bg-info m-t-n-xs">
            <span class="arrow top hidden-folded arrow-info"></span>
            <div>
              <p>300mb of 500mb used</p>
            </div>
            <progressbar value="60" type="white" class="progress-xs m-b-none dker"></progressbar>
          </li>
          <li>
            <a href>Settings</a>
          </li>
          <li>
            <a ui-sref="app.page.profile">Profile</a>
          </li>
          <li>
            <a href>
              <span class="badge bg-danger pull-right">3</span>
              Notifications
            </a>
          </li>
          <li class="divider"></li>
          <li>
            <a ui-sref="access.signin">Logout</a>
          </li>
        </ul>
        <!-- / dropdown -->
      </div>
      <div class="line dk hidden-folded"></div>
    </div>
    <!-- / user -->

    <!-- nav -->
    <nav ui-nav class="navi">
<!-- first -->
<ul class="nav">
  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span translate="aside.nav.HEADER">Navigation</span>
  </li>
  <li>
    <a ui-sref="dashboard">
      <i class="glyphicon glyphicon-stats icon text-primary-dker"></i>
      <span class="font-bold" translate="aside.nav.DASHBOARD">Dashboard</span>
    </a>
  </li>
  <li ng-class="{active:$state.includes('app.users')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-user icon"></i>
      <span class="font-bold" translate="aside.nav.users.USERS">Users</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="userlist">
          <span translate="aside.nav.users.USER_LIST">User List</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.users.CREATE_USER">Create User</span>
        </a>
      </li>
    </ul>
  </li>
  <li ui-sref-active="active">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-map-marker icon text-info-dker"></i>
      <span class="font-bold" translate="aside.nav.MISSION.missions">Users</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="missions">
          <span translate="aside.nav.MISSION.sub.MissionMaps">User List</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="categories">
          <span translate="aside.nav.MISSION.sub.MissionCategories">User List</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.MISSION.sub.MissionTags">Create User</span>
        </a>
      </li>
    </ul>

  </li>
  <li ui-sref-active="active">
    <a href class="auto">
      <!-- <b class="badge bg-info pull-right">9</b> -->
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-edit icon text-info-lter"></i>
      <span class="font-bold" translate="aside.nav.NEWS.news">News</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="createnews">
          <span translate="aside.nav.NEWS.sub.CreateNews">Create News</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="managenews">
          <span translate="aside.nav.NEWS.sub.ManageNews">Manage News</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="createauthor">
          <span translate="aside.nav.NEWS.sub.CreateAuthor">Create Author</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="manageauthor">
          <span translate="aside.nav.NEWS.sub.ManageAuthor">Manage Author</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="newscategory">
          <span translate="aside.nav.NEWS.sub.NewsCategory">News Category</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="newstags">
          <span translate="aside.nav.NEWS.sub.NewsTags">News Tags</span>
        </a>
      </li>
    </ul>
  </li>
  <li ng-class="active">
    <a ui-sref="contactus">
      <i class="glyphicon glyphicon-earphone"></i>
      <span class="font-bold" translate="aside.nav.CONTACTUS.contactus">contact us</span>
    </a>
  </li>
  <li ui-sref-active="active">
    <a ui-sref="app.widgets">
      <!-- <b class="badge bg-success dk pull-right">16</b> -->
      <i class="glyphicon glyphicon-th-large icon text-success"></i>
      <span class="font-bold" translate="aside.nav.PAGES">Pages</span>
    </a>
  </li>
  <li class="line dk"></li>

</ul>
<!-- / third -->

    </nav>
    <!-- nav -->

    <!-- aside footer -->
    <div class="wrapper m-t">
      <div class="text-center-folded">
        <span class="pull-right pull-none-folded">60%</span>
        <span class="hidden-folded" translate="aside.MEMORY">Milestone</span>
      </div>
      <progressbar value="60" class="progress-xxs m-t-sm dk" type="info"></progressbar>
      <div class="text-center-folded">
        <span class="pull-right pull-none-folded">35%</span>
        <span class="hidden-folded" translate="aside.DISK">Release</span>
      </div>
      <progressbar value="35" class="progress-xxs m-t-sm dk" type="primary"></progressbar>
    </div>
    <!-- / aside footer -->
  </div>
</div>
  </div>
  <!-- / menu -->
  <!-- content -->
  <div class="app-content">
    <div ui-butterbar></div>
    <a href class="off-screen-toggle hide" ui-toggle-class="off-screen" data-target=".app-aside" ></a>
    <div class="app-content-body fade-in-up" ui-view></div>
  </div>
  <!-- /content -->
  <!-- aside right -->
  <div class="app-aside-right pos-fix no-padder w-md w-auto-xs bg-white b-l animated fadeInRight hide">
    <div class="vbox">
      <div class="wrapper b-b b-light m-b">
        <a href class="pull-right text-muted text-md" ui-toggle-class="show" target=".app-aside-right"><i class="icon-close"></i></a>
        Chat
      </div>
      <div class="row-row">
        <div class="cell">
          <div class="cell-inner padder">
            <!-- chat list -->
            <div class="m-b">
              <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                  <span class="arrow left pull-up"></span>
                  <p class="m-b-none">Hi John, What's up...</p>
                </div>
                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i> 2 minutes ago</small>
              </div>
            </div>
            <div class="m-b">
              <a href class="pull-right thumb-xs avatar"><img src="/img/a3.jpg" class="img-circle" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm bg-light r m-r-sm">
                  <span class="arrow right pull-up arrow-light"></span>
                  <p class="m-b-none">Lorem ipsum dolor :)</p>
                </div>
                <small class="text-muted">1 minutes ago</small>
              </div>
            </div>
            <div class="m-b">
              <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                  <span class="arrow left pull-up"></span>
                  <p class="m-b-none">Great!</p>
                </div>
                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i>Just Now</small>
              </div>
            </div>
            <!-- / chat list -->
          </div>
        </div>
      </div>
      <div class="wrapper m-t b-t b-light">
        <form class="m-b-none">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Say something">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">SEND</button>
            </span>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- / aside right -->

  <!-- footer -->
  <div class="app-footer wrapper b-t bg-light">
    <span class="pull-right">{[{app.version}]} <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
    &copy; 2015 Copyright.
  </div>
  <!-- / footer -->

</div>
<!-- JS -->
<?php echo $this->tag->javascriptInclude('be/js/jquery/jquery.min.js'); ?>
<!-- angular -->
<?php echo $this->tag->javascriptInclude('vendors/angular/angular.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/angular-cookies/angular-cookies.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/angular-animate/angular-animate.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/angular-ui-router/release/angular-ui-router.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/angular-translate.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ngStorage.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-load.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-jq.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-validate.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-bootstrap-tpls.min.js'); ?>

<?php echo $this->tag->javascriptInclude('vendors/angular-jwt/dist/angular-jwt.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/a0-angular-storage/dist/angular-storage.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/ng-file-upload/ng-file-upload-shim.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/ng-file-upload/ng-file-upload.min.js'); ?>

<?php echo $this->tag->javascriptInclude('vendors/angular-moment/angular-moment.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/moment/min/moment.min.js'); ?>
<!-- <?php echo $this->tag->javascriptInclude('vendors/moment-range/dist/moment-range.min.js'); ?> -->
<?php echo $this->tag->javascriptInclude('vendors/angular-xeditable/dist/js/xeditable.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/angular-ui-select/dist/select.min.js'); ?>

<!-- lightslider -->
<?php echo $this->tag->javascriptInclude('vendors/lightslider/dist/js/lightslider.min.js'); ?>

<!-- CKeditor -->
<?php echo $this->tag->javascriptInclude('be/js/ckeditor/ckeditor.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/ckeditor/styles.js'); ?>

<!-- For Toaster -->
<?php echo $this->tag->javascriptInclude('vendors/AngularJS-Toaster/toaster.js'); ?>

<!-- include angular-chosen -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script>
<script src="/vendors/angular-chosen/angular-chosen.js"></script>

<!-- For angular Maps V3-->
<script src="/vendors/lodash/lodash.min.js"></script>
<script src="/vendors/angular-google-maps/dist/angular-google-maps.min.js"></script>
<script src='//maps.googleapis.com/maps/api/js?sensor=false&libraries=places'></script>

<!-- APP -->
<?php echo $this->tag->javascriptInclude('be/js/scripts/app.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/controllers/controllers.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/factory/factory.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/directives/directives.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/filter.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/config.js'); ?>

</body>
</html>
