<script type="text/ng-template" id="addCategory.html">
    <div ng-include="'/be/tpl/addCategory.html'"></div>
</script>
<script type="text/ng-template" id="deleteCategory.html">
    <div ng-include="'/be/tpl/deleteCategory.html'"></div>
</script>
<toaster-container ></toaster-container>
<div class="hbox hbox-auto-xs hbox-auto-sm" ng-controller="MissionCategoryCTRL">

    <!-- main -->
    <div class="col">
        <!-- main header -->
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">Mission Categories</h1>
                    <small class="text-muted">Listing of all mission categories.</small>
                </div>
                <div class="col-sm-6 col-xs-12 pull-right text-right">
                    <button class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="open(23)"><i class="fa fa-plus"></i>Add Category</button>
                </div>
            </div>
        </div>
        <!-- / main header -->

        <!-- / main header -->
        <div class="hbox hbox-auto-xs hbox-auto-sm" >
            <div class="col wrapper-md">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Category List
                    </div>
                    <div class="row wrapper">
                        <div class="col-sm-4 m-b-xs" >
                            <span ng-show="keyword"><strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button></span>
                        </div>
                        <div class="col-sm-4 pull-right text-right">
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" ng-model="searchkeyword" placeholder="Search">
                              <span class="input-group-btn">
                                <button class="btn btn-sm btn-default" type="button" ng-click="search(searchkeyword, searchStatus);">Go!</button>
                              </span>
                            </div>

                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped b-t b-light">
                            <thead>
                            <tr>
                                <th>
                                </th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Date Last Updated</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody ng-show="loading">
                            <tr>
                                <td colspan="5">Loading List</td>
                            </tr>
                            </tbody>
                            <tbody ng-hide="loading">
                            <tr  ng-show="bigTotalItems==0"> <td colspan="5"> No records found! </td></tr>
                            <tr ng-repeat="ss in data.data" >
                                <td>
                                </td>
                                <td><span ng-hide="currentId == ss.id">{[{ ss.cattitle }]}</span> <input type="text" class="form-control" name="cat[$index].title" ng-model="cat[$index].title" ng-init="cat[$index].title=ss.cattitle" ng-show="currentId == ss.id"></td>
                                <td><span ng-hide="currentId == ss.id">{[{ ss.catdescription }]}</span> <input type="text" class="form-control" name="cat[$index].description" ng-model="cat[$index].description" ng-init="cat[$index].description=ss.catdescription" ng-show="currentId == ss.id"></td>
                                <td>{[{ ss.updated_at }]}</td>
                                <td>
                                    <div ng-show="currentId == ss.id">
                                        <button class="btn btn-sm btn-icon btn-success" ng-click="updateCat(cat[$index], ss.id)"><icon class="fa fa-save"></icon></button>
                                        <button class="btn btn-sm btn-icon btn-warning" ng-click="cancelEdit()"><icon class="fa fa-ban"></icon></button>
                                    </div>
                                    <div ng-hide="currentId == ss.id">
                                        <button class="btn btn-sm btn-icon btn-info" ng-click="editCat(ss)"><icon class="fa fa-pencil-square"></icon></button>
                                        <button class="btn btn-sm btn-icon btn-danger" ng-click="delete(ss.id)"><icon class="fa fa-times-circle"></icon></button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-4 hidden-xs">
                            </div>
                            <div class="col-sm-4 text-center" ng-hide="bigTotalItems==0 || loading">
                                <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                                <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </div>
    <!-- / main -->
</div>
