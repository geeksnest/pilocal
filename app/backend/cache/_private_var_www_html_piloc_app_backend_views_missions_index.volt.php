<script type="text/ng-template" id="addLabel.html">
    <div ng-include="'/be/tpl/addLabel.html'"></div>
</script>
<div class="hbox hbox-auto-xs hbox-auto-sm" ng-controller="MissionsCTRL">
    <!-- main -->
    <div class="col">
        <!-- main header -->
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">Missions</h1>
                    <small class="text-muted">Listing of all agent missions.</small>
                </div>
            </div>
        </div>
        <!-- / main header -->
        <div class="hbox hbox-auto-xs hbox-auto-sm" >
            <div class="col wrapper-md">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Mission List
                    </div>
                    <div class="row wrapper">
                        <div class="col-sm-4 m-b-xs" >
                            <span ng-show="keyword"><strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clearsearch()">Clear</button></span>
                            <br><span ng-show="txtdate">Filter by: <strong> "{[{ txtdate }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clearfilter()">Clear</button></span>
                        </div>

                        <div class="col-sm-4 pull-right text-right">
                            <div class="btn-group m-b-xs" ng-init="searchStatus = 'ALL'">
                                <label class="btn btn-xs ng-valid ng-dirty active" ng-model="searchStatus" btn-radio="'ALL'"><i class="fa fa-check text-active"></i> All </label>
                                <label class="btn btn-xs btn-primary ng-valid ng-dirty" ng-model="searchStatus" btn-radio="'FEATURED'"><i class="fa fa-check text-active"></i> Featured</label>
                                <label class="btn btn-xs btn-info ng-valid ng-dirty" ng-model="searchStatus" btn-radio="'LATEST'"><i class="fa fa-check text-active"></i> Latest</label>
                                <label class="btn btn-xs btn-success ng-valid ng-dirty" ng-model="searchStatus" btn-radio="'GREATEST'"><i class="fa fa-check text-active"></i> Greatest</label>
                                <label class="btn btn-xs btn-warning ng-valid ng-dirty" ng-model="searchStatus" btn-radio="'POPULAR'"><i class="fa fa-check text-active"></i> Popular</label>
                            </div>
                            <br>
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" ng-model="searchkeyword" placeholder="Search">
                              <span class="input-group-btn">
                                <button class="btn btn-sm btn-default" type="button" ng-click="search(searchkeyword, searchStatus);">Go!</button>
                              </span>
                            </div>

                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped b-t b-light">
                            <thead>
                            <tr>
                                <th>
                                </th>
                                <th>Title</th>
                                <th>Agent</th>
                                <th>Date Created</th>
                                <th>Date Last Updated</th>
                                <th>Views</th>
                                <th>Featured</th>
                                <th>Greatest</th>
                            </tr>
                            </thead>
                            <tbody ng-show="loading">
                            <tr>
                                <td colspan="5">Loading List</td>
                            </tr>
                            </tbody>
                            <tbody ng-hide="loading">
                            <tr  ng-show="bigTotalItems==0"> <td colspan="5"> No records found! </td></tr>
                            <tr ng-repeat="ss in data.data">
                                <td>
                                    <icon ng-show="ss.featured" class="fa fa-star" style="color: green"></icon>
                                    <icon ng-show="ss.greatest" class="fa fa-asterisk" style="color: darkgreen"></icon>
                                </td>
                                <td>{[{ ss.title }]}</td>
                                <td>{[{ ss.first_name }]} {[{ ss.last_name }]}</td>
                                <td>{[{ ss.created_at }]}</td>
                                <td>{[{ ss.updated_at }]}</td>
                                <td>{[{ ss.views }]}</td>
                                <td>
                                    <label class="i-switch bg-primary m-t-xs m-r">
                                        <input checked="" type="checkbox" ng-model="featured" ng-init="featured= ss.featured==1 ? true: false;" ng-click="setFGClick(featured, ss.mapid, 'f')">
                                        <i></i>
                                    </label>
                                </td>
                                <td>
                                    <label class="i-switch bg-success m-t-xs m-r">
                                        <input checked="" type="checkbox" ng-model="greatest" ng-init="greatest= ss.greatest==1 ? true: false;" ng-click="setFGClick(greatest, ss.mapid, 'g')">
                                        <i></i>
                                    </label>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-4 hidden-xs">
                            </div>
                            <div class="col-sm-4 text-center" ng-hide="bigTotalItems==0 || loading">
                                <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                                <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </div>
    <!-- / main -->
</div>
