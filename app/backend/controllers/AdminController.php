<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class AdminController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->request->isPost()) {
            die('BOOM!');    
        }

    }
    public function dashboardAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function boomAction()
    {
        echo "boom";
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);   
    }

}

