<?php

namespace Modules\Backend\Controllers;  

use Phalcon\Mvc\View;
use Phalcon\Tag;
use Modules\Backend\Models\Users as PI;

class IndexController extends ControllerBase
{
    public function initialize()
    {
    	parent::initialize();
    }	
    public function indexAction()
    {	

    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);	
    }

    private function _registerSession($user)
    {
        $this->session->set('auth', array(
            'pi_id' => $user->userid,
            'pi_fullname' => $user->firstname .' '.$user->lastname,
            'pi_username' => $user->username
        ));

        //Set SuperAdmin
        if($user->userLevel){
            $this->session->set('SuperAgent', true );
        }
    }
}

