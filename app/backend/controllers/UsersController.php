<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Modules\Backend\Forms\CreateuserForm;
use Modules\Backend\Models\Piagents as PI;

class UsersController extends ControllerBase
{

    public function indexAction()
    {

    }
    public function createAction()
    {
    	$form = new CreateuserForm();
    	$this->view->form = $form;

            $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

    }
    public function createSaveAction(){
        $form = new CreateuserForm();
        $data = array();
        if ($this->request->isPost()) {   
                if ($form->isValid($this->request->getPost()) != false) {
                    $userName = PI::findFirst("username='".$this->request->getPost('username', 'striptags')."'");
                    $userEmail = PI::findFirst("email='".$this->request->getPost('emailaddress', 'striptags')."'");
                    if($userName==true || $userEmail==true){
                        ($userName==true) ? $data["usernametaken"] = "Username already taken." : '';
                        ($userEmail==true) ? $data["emailtaken"] = "Email already taken." : '';
                    }else{
                        $user = new PI();
                        $password = sha1($this->request->getPost('password'));
                        $user->assign(array(
                            'username' => $this->request->getPost('username', 'striptags'),
                            'email' => $this->request->getPost('emailaddress', 'striptags'),
                            'password' => $password,
                            'hdyha' => $this->request->getPost('firstname', 'striptags'),
                            'referal' => 'superagent',
                            'firstname' => $this->request->getPost('firstname', 'striptags'),
                            'lastname' => $this->request->getPost('lastname', 'striptags'),
                            'birthday' => $this->request->getPost('birthday', 'striptags'),
                            'gender' => $this->request->getPost('gender', 'striptags'),
                            'state' => $this->request->getPost('state', 'striptags'),
                            'country' => 'country',
                            'agentType' => 'agent',
                            'agentStatus' => 'active'
                        ));
                        if (!$user->save()) {
                            $data['error'] = "Something went wrong saving the data, please try again.";
                        } else {
                            $data['success'] = "User profile has been stored.";
                        }  
                    }           
                }else{
                    $data['error'] = "Something went wrong in validating the data, please try again.";
                }
        }
        echo json_encode($data);

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
    }

    public function listAction(){
        $numberPage = $this->request->getQuery("page", "int");

        $builder = $this->modelsManager->createBuilder()
            ->columns('username , userid, email, firstname, lastname, agentStatus, agentType')
            ->from('Modules\Backend\Models\Piagents');

        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
          "builder" => $builder,
          "limit"=> 10,
          "page" => $numberPage
          ));
        // Get the paginated results
        $page = $paginator->getPaginate();
        $data = array();
        $list = array();
        foreach($page->items as $item){
            $list[] = array(
                'username' => $item->username,
                'userid' => $item->userid,
                'email' => $item->email,
                'firstname' => $item->firstname,
                'lastname' => $item->lastname,
                'agentStatus' => $item->agentStatus,
                'agentType' => $item->agentType
            );
        }
        $data['users'] = $list;

        $p = array();
        for($x = 1; $x <= $page->total_pages; $x++){
            $p[] = array('num' => $x, 'link' => 'user');
        }
        $data['pages'] = $p;
        $data['index'] = $page->current;
        $this->view->data = json_encode($data);
        //echo json_encode($data);
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}

