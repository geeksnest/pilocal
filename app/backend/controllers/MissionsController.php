<?php

namespace Modules\Backend\Controllers;  

use Phalcon\Mvc\View;
use Phalcon\Tag;
use Modules\Backend\Models\Users as PI;

class MissionsController extends ControllerBase
{
    public function initialize()
    {
    	parent::initialize();
    }	
    public function indexAction()
    {	

    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);	
    }
    public function viewAction() 
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function categoriesAction()
    {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}

