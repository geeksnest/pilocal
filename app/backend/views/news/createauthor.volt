{{ content() }}


<script type="text/ng-template" id="mediagallery.html">
  <div ng-include="'/be/tpl/mediagallery.html'"></div>
</script>

<toaster-container ></toaster-container>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Authors</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="saveAuthor(author)" name="formpage" ng-controller="Createauthor">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-8">
          <div class="panel panel-default">

            <div class="panel-heading font-bold">
              Authors Information
            </div>


              <div class="panel-body">
                
                <div class="form-group hiddenoverflow">
                  <label class="col-sm-2 control-label">
                    <label for="title">Name</label>
                  </label>
                  <div class="col-sm-10">
                    <input type="text" id="name" name="name" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="author.name" required="required">
                  </div> 
                </div>
                
                <div class="line line-dashed b-b line-lg"></div>
                
                <div class="form-group hiddenoverflow">
                  <label class="col-sm-2 control-label">
                    <label for="title">Location</label>
                  </label>
                  <div class="col-sm-10">
                    <input type="text" id="location" name="location" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="author.location" required="required">
                  </div> 
                </div>
                
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group hiddenoverflow">
                  <label class="col-sm-2 control-label">
                    <label for="title">Occupation</label>
                  </label>
                  <div class="col-sm-10">
                    <input type="text" id="occupation" name="occupation" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="author.occupation" required="required">
                  </div> 
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">
                    <label for="title">Since</label>
                  </label>
                  <div class="col-sm-4">
                    <span class="input-group-btn">
                      <input id="date" name="date" class="form-control" datepicker-popup="yyyy-MM-dd" ng-model="author.since" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled style="display:inline-block">
                      <button type="button" class="btn btn-default" ng-click="open($event)" style="top:-5px"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>
                  </div> 
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                
                <div class="form-group hiddenoverflow">
                  <label class="col-sm-2 control-label">
                    <label for="title">About the Author</label>
                  </label>
                  <div class="col-sm-10">
                    <textarea class="ck-editor" ng-model="author.about" required="required"></textarea>
                  </div>
                </div>
              </div>

          </div>
        </div>


        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Photo of the Author
              <a class="btn btn-default btn-xs pull-right" ng-click="media('featured')"><i class="fa  fa-folder-open"></i> Media Library </a>
            </div>
            <div class="panel-body">
              <img ng-show="author.photo" src="<?php echo $this->config->application->amazonlink; ?>/uploads/saveauthorimage/{[{ author.photo }]}" style="width: 100%">
            </div>
          </div>
        </div>



      </div>


      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Submit</button>
            </footer>
        </div>
      </div>

  </div>
</fieldset>
</form>