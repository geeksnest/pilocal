{{ content() }}

<script type="text/ng-template" id="newsDelete.html">
  <div ng-include="'/be/tpl/newsDelete.html'"></div>
</script>

<script type="text/ng-template" id="newsEdit.html">
  <div ng-include="'/be/tpl/newsEdit.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">News List</h1>
  <a id="top"></a>
</div>

<toaster-container ></toaster-container>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formmanagenews">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">

      <div class="row">

        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              News List
            </div>


              <div class="panel-body">

                <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs" ng-show="keyword">
                        <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
                    </div>
                    <div class="col-sm-5 m-b-xs pull-right">
                      <div class="input-group full-width">
                        <div class="pull-right">
                          <div class="btn-group m-b-xs" ng-init="searchStatus = 'ALL'">
                              <label class="btn btn-xs ng-valid ng-dirty active" ng-model="sort" btn-radio="'dateedited'"  ng-click="sortpage('updated_at')"><i class="fa fa-check text-active"></i> All </label>
                              <label class="btn btn-xs btn-primary ng-valid ng-dirty" ng-model="sort" btn-radio="'title'" ng-click="sortpage('title')"><i class="fa fa-check text-active"></i> Title</label>
                              <label class="btn btn-xs btn-info ng-valid ng-dirty" ng-model="sort" btn-radio="'author.name'" ng-click="sortpage('author.name')"><i class="fa fa-check text-active"></i> Author</label>
                              <label class="btn btn-xs btn-success ng-valid ng-dirty" ng-model="sort" btn-radio="'featurednews'" ng-click="sortpage('featurednews')"><i class="fa fa-check text-active"></i> Featured</label>
                              <label class="btn btn-xs btn-warning ng-valid ng-dirty" ng-model="sort" btn-radio="'date'" ng-click="sortpage('date')"><i class="fa fa-check text-active"></i> Date Published</label>
                          </div>
                        </div>
                      </div>
                      <div class="input-group">
                          <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                          <span class="input-group-btn">
                          <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                          </span>
                      </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th style="width:40%">Title</th>
                                <th>Author</th>
                                <th>Status</th>
                                <th>Featured</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody ng-show="loading">
                            <tr colspan="4">
                                <td>Loading News</td>
                            </tr>
                        </tbody>
                        <tbody ng-hide="loading">
                            <tr colspan="4" ng-show="bigTotalItems==0"> <td> No records found! </td></tr>
                            <tr ng-repeat="mem in data.data">

                                <td><a href="<?php echo $this->config->application->BaseURL.'/blog/'; ?>{[{ mem.newsslugs }]}" target="_blank">{[{ mem.title }]}</a></td>
                                <td>{[{ mem.name }]}</td>
                                <td  ng-if="mem.status == 1">
                                  <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>
                                  <div class="checkstatuscontent">
                                    <label class="i-switch bg-info m-t-xs m-r">
                                      <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.newsid,searchtext,mem.newslocation,mem.newsslugs)">
                                      <i></i>
                                    </label>
                                    
                                  </div>
                                  <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.newsslugs"><i class="fa fa-check"></i></spand></div>
                                </td>
                                <td  ng-if="mem.status == 0">
                                  <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Deactivated</span></div>
                                  <div class="checkstatuscontent">
                                    <label class="i-switch bg-info m-t-xs m-r">
                                      <input type="checkbox" ng-click="setstatus(mem.status,mem.newsid,searchtext,mem.newslocation,mem.newsslugs)">
                                      <i></i>
                                    </label>
                                   
                                  </div>
                                   <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow ==mem.newsslugs"><i class="fa fa-check"></i></spand></div>
                                </td>
                                <td>
                                    <label class="i-switch bg-primary m-t-xs m-r">
                                        <input checked="" type="checkbox" ng-model="featurednews" ng-init="featurednews= mem.featurednews==1 ? true: false;" ng-click="featured(mem.newsid, featurednews)">
                                        <i></i>
                                    </label>
                                </td>
                                <td>
                                    <button class="btn btn-sm btn-icon btn-info" ng-click="editnews(mem.newsid)"><icon class="fa fa-pencil-square"></icon></button>
                                    <button class="btn btn-sm btn-icon btn-danger" ng-click="deletenews(mem.newsid)"><icon class="fa fa-times-circle"></icon></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

              </div>


          </div>
        </div>

      </div>

      <div class="row" ng-hide="bigTotalItems==0 || loading">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
                <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
</fieldset>
</form>