{{ content() }}

<script type="text/ng-template" id="categoryAdd.html">
  <div ng-include="'/be/tpl/categoryAdd1.html'"></div>
</script>

<script type="text/ng-template" id="mediagallery.html">
  <div ng-include="'/be/tpl/mediagallery.html'"></div>
</script>

<script type="text/ng-template" id="tagsAdd.html">
  <div ng-include="'/be/tpl/tagsAdd.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create News</h1>
  <a id="top"></a>
</div>

<script type="text/ng-template" id="uploadVideo.html">
  <div ng-include="'/be/tpl/uploadVideo.html'"></div>
</script>

<toaster-container ></toaster-container>

<style type="text/css">
  iframe{
    width: 100%;
    height: 250px;
    /*max-width:600px;*/
  }
  video {
    width: 100% !important;
    height: 250px;
    max-width:600px;
  }
</style>


<form class="form-validation form-horizontal  ng-pristine ng-invalid ng-invalid-required" name="formnews" id="formnews">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">

      <div class="row">

        <div class="col-sm-8">
          <div class="panel panel-default">

            <div class="panel-heading font-bold">
              News Information
            </div>


            <div class="panel-body">
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <label for="title">Title</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.title" required="required" ng-change="onnewstitle(news.title)" ng-change="onnewstitle(news.title)">
                  <br>
                  <b class="pull-left">News Slugs: </b>
                  <input type="text" ng-show="editslug" id="pageslugs" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern pull-left" ng-model="news.slugs" ng-change="onslugs(news.slugs)"><span ng-bind="news.slugs" class="pull-left mg-left"></span>
                  <div ng-show="editslug">
                    <a class="btn btn-danger btn-xs pull-right mg-left" ng-click="cancelnewsslug(news.title)">cancel</a>
                    <a class="btn btn-primary btn-xs pull-right mg-left" ng-click="setslug(news.slugs)">ok</a>
                  </div>
                  <div ng-if="news.title.length > 0">
                    <a class="btn btn-danger btn-xs pull-right mg-left" ng-hide="editslug" ng-click="clearslug(news.title)">clear</a>
                    <a class="btn btn-primary btn-xs pull-right mg-left" ng-hide="editslug" ng-click="editnewsslug()">edit slug</a>
                  </div>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <label for="title">Body Content</label>
                </label>
                <div class="col-sm-10">
                  <a class="btn btn-default btn-xs pull-right" ng-click="media('content')"><i class="fa  fa-folder-open"></i> Media Library </a> <br> <br>
                  <textarea class="ck-editor" ng-model="news.body" required="required"></textarea>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <label for="title">Summary</label>
                </label>

                <div class="col-sm-10">
                  <textarea ng-model="news.summary" name="summary" required="required" class="form-control" style="resize:vertical;" ng-maxlength="1000"></textarea>
                  <span ng-if="formnews.summary.$error.maxlength">Maximum of 1000 characters.</span>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  Author
                </label>
                <div class="col-sm-10">
                  <select chosen allow-single-deselect class="form-control" options="author" ng-model="news.author" ng-options="mem.authorid as mem.name for mem in author"  required="required">
                  </select>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <label for="title">Meta Title</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="metatitle" name="metatitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.metatitle" required="required">
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <label for="title">Meta Description</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="metadesc" name="metadesc" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.metadesc" required="required">
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <label for="title">Meta Keyword</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="metakeyword" name="metakeyword" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.metakeyword" required="required">
                </div>
              </div>
            </div>

          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Publish
            </div>
            <div class="panel-body">

              Publish Immediatley:
              <div class="input-group col-sm-10">
                <span class="input-group-btn">
                  <input id="date" name="date" class="form-control" datepicker-popup="yyyy-MM-dd" ng-model="news.date" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                  <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                </span>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <button type="submit" ng-click="saveNews(news,0)" class="btn btn-default btn-sm" ng-disabled="formnews.$invalid">Save Draft</button>
              <a href="" ng-click="preview(news)" class="btn btn-default btn-sm" ng-disabled="formnews.$invalid">Preview</a>
              <button type="submit" ng-click="saveNews(news,1)" class="btn btn-success btn-sm pull-right" ng-disabled="formnews.$invalid">Publish</button>
            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Category
              <a class="btn btn-default btn-xs pull-right" ng-click="addcategory()"><i class="fa fa-plus"></i> Add</a>
            </div>
            <div class="panel-body">
              <select chosen multiple class=" chosen-choices form-control w-md" ng-model="news.category" options="category" ng-options="cat.categoryid as cat.categoryname for cat in category" required="required">
              </select>
              <label><em class="text-muted">Click add to create more categories to choose from.</em></label>
            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Tags
            </div>
            <div class="panel-body">
              <ui-select multiple tagging tagging-label="false" ng-model="news.tag" theme="bootstrap" ng-disabled="disabled" title="Choose a color">
                <ui-select-match placeholder="Select tags...">{[{ $item }]} </ui-select-match>
                <ui-select-choices repeat="ta in tag">
                  {[{ ta }]}
                </ui-select-choices>
              </ui-select>
              <label><em class="text-muted">For tags that doesnt exist just type and press enter. This will be added to your database of tags once the page is published.</em></label>
            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Select featured
              <a class="btn btn-default btn-xs pull-right" ng-click="media('featured')"><i class="fa  fa-folder-open"></i> Media Library</a>
            </div>
            <div class="panel-body">
              <input type="hidden" name="featuredthumb" ng-model="news.featuredthumb" required>
              <label><em class="text-muted">Click select to add videos and image or select an existing image or video.</em></label>
                <img ng-show="news.imagethumb" src="<?php echo $this->config->application->amazonlink; ?>/uploads/newsimage/{[{ news.imagethumb }]}" style="width: 100%">
                <div style="padding:10px;" ng-bind-html="vidpath"></div>
                <div ng-model="previewvideo">
                  {[{ upVid.path}]}
                </div>

            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Configuration
            </div>
            <div class="panel-body">
              <div class="form-group">
                <label class="col-sm-3 control-label">Featured</label>
                <div class="col-sm-8">
                  <label class="i-switch i-switch-md bg-primary m-t-xs m-r">
                    <input type="checkbox" checked="" ng-model="news.featurednews">
                    <i></i>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>
  </fieldset>
</form>
