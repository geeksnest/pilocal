<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  {{ get_title() }}
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  {{ stylesheet_link('be/css/bootstrap.css') }}
  {{ stylesheet_link('be/css/animate.css') }}
  {{ stylesheet_link('be/css/font-awesome.min.css') }}
  {{ stylesheet_link('be/css/simple-line-icons.css') }}
  {{ stylesheet_link('be/css/font.css') }}
  {{ stylesheet_link('be/css/app.css') }}
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon 
  <link rel="shortcut icon" href="{{url.getBaseUri()}}public/img/favicon/favicon.png">-->
</head>

<body >
  <div class="container w-xxl w-auto-xs" ng-controller="LoginCtrl">
  <a href class="navbar-brand block m-t">Planet Impossible</a>
  <div class="m-b-lg">
    <div class="wrapper text-center">
      <img src='/img/pilogo2 (2).png'>
    </div>
    <form name="form" class="form-validation" method="post">
      <div class="text-danger wrapper text-center" ng-show="message" ng-bind="message">
      </div>
      <div class="list-group list-group-sm">
        <div class="list-group-item">
          {{ text_field('username', 'size': "30", 'class': "form-control no-border", 'id': "inputUsername", 'placeholder': 'Username', 'ng-model': 'user.username', 'required':'required') }}
        </div>
        <div class="list-group-item">
          {{ password_field('password', 'size': "30", 'class': "form-control no-border", 'id': "inputPassword", 'placeholder': 'Password', 'ng-model': 'user.password', 'required': 'required') }}
        </div>
      </div>
      <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="submit(user)" ng-disabled='form.$invalid'>Log in</button>
      <div class="text-center m-t m-b"><a ui-sref="access.forgotpwd">Forgot password?</a></div>
      <div class="line line-dashed"></div>
      <p class="text-center"><small>Only the SuperAgent can access beyond this point.</small></p>
    </form>
  </div>

</div>
<!-- JS -->
{{ javascript_include('vendors/jquery/dist/jquery.min.js') }}

<!-- angular -->
{{ javascript_include('vendors/angular/angular.js') }}
{{ javascript_include('be/js/angular/angular-cookies.min.js') }}
{{ javascript_include('be/js/angular/angular-animate.min.js') }}
{{ javascript_include('be/js/angular/angular-ui-router.min.js') }}
{{ javascript_include('be/js/angular/angular-translate.js') }}
{{ javascript_include('be/js/angular/ngStorage.min.js') }}
{{ javascript_include('be/js/angular/ui-load.js') }}
{{ javascript_include('be/js/angular/ui-jq.js') }}
{{ javascript_include('be/js/angular/ui-validate.js') }}
{{ javascript_include('be/js/angular/ui-bootstrap-tpls.min.js') }}
  {{ javascript_include('vendors/angular-jwt/dist/angular-jwt.min.js') }}
  {{ javascript_include('vendors/a0-angular-storage/dist/angular-storage.min.js') }}

<!-- APP -->
  <script src="/be/js/scripts/loginapp.js"></script>
  <script src="/be/js/scripts/controllers/controllers.js"></script>
  <script src="/be/js/scripts/factory/factory.js"></script>
  <script src="/be/js/scripts/config.js"></script>

</body>
</html>