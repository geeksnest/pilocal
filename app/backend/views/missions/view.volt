<script type="text/ng-template" id="addCategory.html">
    <div ng-include="'/be/tpl/addCategory.html'"></div>
</script>

<script type="text/ng-template" id="mediagallery.html">
  <div ng-include="'/be/tpl/mediagallery.html'"></div>
</script>

<script type="text/ng-template" id="viewPin.html">
  <div ng-include="'/be/tpl/viewPin.html'"></div>
</script>

<script type="text/ng-template" id="deleteComment.html">
  <div ng-include="'/be/tpl/deleteComment.html'"></div>
</script>

<script type="text/ng-template" id="editComment.html">
  <div ng-include="'/be/tpl/editComment.html'"></div>
</script>

<script type="text/ng-template" id="viewNewsList.html">
  <div ng-include="'/be/tpl/viewNewsList.html'"></div>
</script>

<script type="text/ng-template" id="viewNews.html">
  <div ng-include="'/be/tpl/viewNews.html'"></div>
</script>

<toaster-container ></toaster-container>

<form class="form-validation form-horizontal  ng-pristine ng-invalid ng-invalid-required" ng-submit="updateMap(mapdetails)" name="form" id="form">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <div class="row">
        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Map Information
            </div>

            <div class="panel-body">
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <label for="title">Title</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="mapdetails.title" required="required" ng-change="onnewstitle(mapdetails.title, mapdetails.id)">
                  <br>
                  <b class="pull-left">Map Slugs: </b><span ng-bind="mapdetails.mapslugs" class="pull-left mg-left"></span>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <label for="title">Description</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="mapdetails.description" required="required">
                  
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              <div class="gmap">
                  <!-- Google Maps. Replace the below iframe with your Google Maps embed code -->
                  <ui-gmap-google-map center='map.center' zoom='map.zoom' events="map.events" options="map.options">
                      <!-- <ui-gmap-search-box template="searchbox.template" events="searchbox.events" position="searchbox.position"></ui-gmap-search-box> -->
                      <ui-gmap-marker ng-repeat="m in missionmarkers" coords="m.coords" idkey="m.id" options="m.options" events="m.events">
                          <ui-gmap-window show="m.show" options="m.WindowWithCustomClass.options">
                              <div>
                                  <div style="background-image: url('{[{m.agentpic}]}')" alt="" class="agent-winimg pull-left bg-image" ng-cloak></div>
                                  <p class="full-width pull-left text-center">{[{ m.agentname }]}</p>
                              </div>
                          </ui-gmap-window>
                      </ui-gmap-marker>
                  </ui-gmap-google-map>
              </div>

              <div class="socialmedia-btn">
                <div class="btn-container">
                  <div>
                    <!-- facebook -->
                    <div class="fb-like pull-left social-media" data-href="<?php echo $base_url; ?>/action-map/{[{mapdetails.mapslugs}]}" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                    <!-- twitter -->
                    <a href="https://twitter.com/share" class="twitter-share-button social-media pull-left">Tweet</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

                    <div class="pull-right map-report-btn" ng-cloak>
                      <div class="pull-left">
                        <i class="fa fa-eye fa-lg"></i> {[{ mapdetails.views }]}
                      </div>
                      <div class="pull-left">
                         <i class="fa fa-comments-o fa-lg"></i> {[{ commentcount }]}
                      </div>
                      <div class="pull-left">
                          <i class="fa fa-map-marker fa-lg"></i> {[{ mapactions }]}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="comment-container pull-left full-width">
                  <p class="pull-left full-width"><i class="fa fa-comment-o fa-2x"></i> All Action Responses({[{ commentcount}]})</p>
                  <div class="full-width hiddenoverflow">
                      <div class="commentbox full-width hiddenoverflow">
                          <img class="image pull-left" src="/img/pilogo.png" alt="">
                          <textarea class="input form-control pull-left comment mg-bottom" ng-model="comment" placeholder="Add a comment..." style="width:731px;"></textarea>
                          <a ng-click="commentlistener(comment)" class="btn btn-pi pull-right"><i class="fa fa-commenting-o"></i> Post</a>
                      </div>
                      <hr>
                  </div>
                  <div class="commentlist hiddenoverflow" ng-repeat="comment in comments">
                      <!-- <img class="image pull-left" src="{[{comment.profile_pic_name}]}" ng-if="comment.superagent==0" alt=""> -->
                      <div class="image pull-left bg-image" style="background-image: url('{[{comment.profile_pic_name}]}')" ng-if="comment.superagent==0"></div>
                      <img class="image pull-left" src="/img/pilogo.png" ng-if="comment.superagent==1" alt="">
                      <div class="comment pull-left">
                          <h6 class="no-margin mg-bottom"  ng-cloak>
                              <span ng-if="comment.hide_agent != 0">{[{ comment.first_name + " " + comment.last_name }]}</span>
                              <span ng-if="comment.hide_agent == 0">Anonymous Agent</span>
                               | <small>{[{ comment.created_at | date: 'MMMM dd, yyyy hh:mm a' }]}</small>
                              <div class="btn-group pull-right" dropdown is-open="status.isopen" ng-if="comment.type=='map'">
                                <button type="button" class="btn btn-link" dropdown-toggle ng-disabled="disabled"><span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="single-button">
                                  <li role="menuitem" ng-if="comment.superagent==1">
                                      <a ng-click="comment.showedit=true" class="btn btn-link pull-left full-width text-left" ng-if="comment.showedit==false">Edit</a>
                                      <a ng-click="comment.showedit=false" class="btn btn-link pull-left full-width text-left" ng-if="comment.showedit">Cancel</a>
                                  </li>
                                  <li role="menuitem"><a ng-click="deleteComment(comment.id, 'comment')" class="btn btn-link pull-left full-width text-left">Delete</a></li>
                                </ul>
                              </div>
                          </h6>
                          <p  ng-cloak ng-if="comment.showedit==false">
                            {[{ comment.comment }]}
                          </p>
                          <!-- edit comment -->
                          <div class="full-width pull-left" ng-if="comment.showedit">
                              <textarea class="input form-control pull-left full-width mg-bottom" ng-model="comment.comment" placeholder="Add a comment..."></textarea>
                              <a ng-click="editComment(comment.id, comment.comment, 'comment')" class="btn btn-pi pull-right"><i class="fa fa-pencil-square-o"></i> Edit</a>
                          </div>

                          <ul light-slider ng-if="comment.type=='pin'"></ul>
                          <a href="" class="pull-left btn btn-link" ng-if="comment.showreply==false" ng-click="showreplybox(comment.id)"><i class="fa fa-reply"></i> Reply</a>
                      </div>
                      <div class="pull-right reply-loadmore">
                          <a ng-click="loadmorereply(comment.id)" class="btn btn-link pull-right" ng-repeat="r in replycount" ng-if="r.count > r.loaded && r.id==comment.id"> load more</a>
                      </div>
                      <div class="pull-right commentreplylist" ng-repeat="reply in replies" ng-if="reply.mapcomment_id==comment.id ">
                          <div class="commentreply hiddenoverflow">
                              <!-- <img class="image pull-left" src="{[{reply.profile_pic_name}]}" alt="" ng-if="reply.superagent==0"> -->
                              <div class="image pull-left bg-image" style="background-image: url('{[{reply.profile_pic_name}]}')" ng-if="reply.superagent==0"></div>
                              <img class="image pull-left" src="/img/pilogo.png" alt="" ng-if="reply.superagent==1">
                              <div class="comment pull-left">
                                  <h6 class="no-margin mg-bottom"  ng-cloak>
                                      {[{ reply.first_name + " " + reply.last_name }]} | <small>{[{ reply.created_at | date: 'MMMM dd, yyyy hh:mm a' }]}</small>
                                      <div class="btn-group pull-right" dropdown>
                                        <button type="button" class="btn btn-link" dropdown-toggle><span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="single-button">
                                          <li role="menuitem">
                                              <a ng-click="reply.showedit=true" class="btn btn-link pull-left full-width text-left" ng-if="reply.showedit==false">Edit</a>
                                              <a ng-click="reply.showedit=false" class="btn btn-link pull-left full-width text-left" ng-if="reply.showedit">Cancel</a>
                                          </li>
                                          <li role="menuitem"><a ng-click="deleteComment(reply.id, 'response')" class="btn btn-link pull-left full-width text-left">Delete</a></li>
                                        </ul>
                                      </div>
                                  </h6>
                                  <!-- <p  ng-cloak ng-bind-html="reply.reply" ng-if="reply.showedit==false"></p> -->
                                  <p  ng-cloak ng-if="reply.showedit==false">{[{ reply.reply }]}</p>
                                  <!-- edit comment -->
                                  <div class="full-width pull-left" ng-if="reply.showedit">
                                      <textarea class="input form-control pull-left mg-bottom full-width" ng-model="reply.reply"></textarea>
                                      <a ng-click="editComment(reply.id, reply.reply, 'response')" class="btn btn-pi pull-right"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="pull-right commentreplylist tbox">
                          <div class="replybox full-width hiddenoverflow" ng-if="comment.showreply">
                              <textarea class="input form-control pull-left reply full-width mg-bottom" ng-model="reply" placeholder="Add a response..."></textarea>
                              <a ng-click="replytocomment(reply, comment.id, comment.type, comment.marker_id)" class="btn btn-pi pull-right mg-bottom"><i class="fa fa-reply"></i> Reply</a>
                          </div>
                      </div>
                  </div>
                  <a ng-if="commentcount >= 4 && commentcount > commentload" class="btn btn-block loadmore btn-pi" ng-click="loadcomment();">
                  <div class="spinner pull-left" ng-if="spinner"></div> Load more</a>
              </div>

            </div>
          </div>
        </div>

        <div class="col-sm-4 mg-bottom">
          <div class="panel-body no-padding">
              <footer class="panel-footer text-right bg-light lter">
                <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
                <button type="submit" class="btn btn-success" ng-disabled="form.$invalid || disable">Submit</button>
              </footer>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Map Cover
              <a class="btn btn-default btn-xs pull-right" ng-click="media('featured')"><i class="fa  fa-folder-open"></i> Media Library</a>
            </div>
            <div class="panel-body">
              <label><em class="text-muted">Click select to add videos and image or select an existing image or video.</em></label>
              <img ng-if="mapdetails.coverType=='image'" src="<?php echo $this->config->application->amazonlink; ?>/uploads/maps/{[{ mapdetails.id }]}/{[{ mapdetails.cover}]}" style="width: 100%">
              <div ng-bind-html="vidpath" ng-if="mapdetails.coverType=='video'"></div>
            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Category
              <a class="btn btn-default btn-xs pull-right" ng-click="addcategory()"><i class="fa fa-plus"></i> Add</a>
            </div>
            <div class="panel-body">
              <select chosen multiple class=" chosen-choices form-control w-md" ng-model="mapdetails.categories" options="data.categories" ng-options="cat.id as cat.cattitle for cat in data.categories" required="required">
              </select>
              <label><em class="text-muted">Click add to create more categories to choose from.</em></label>
            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Tags
            </div>
            <div class="panel-body">
              <ui-select multiple tagging tagging-label="(custom 'new' label)" ng-model="mapdetails.tags" theme="bootstrap" sortable="true" ng-disabled="disabled" title="Choose a color">
                <ui-select-match placeholder="Select Tags...">{[{$item}]}</ui-select-match>
                <ui-select-choices repeat="tags in tags">
                  {[{tags}]}
                </ui-select-choices>
              </ui-select>

              <label><em class="text-muted">For tags that doesnt exist just type and press enter. This will be added to your database of tags once the page is published.</em></label>
            </div>
          </div>
        </div>
        
        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Settings
            </div>
            <div class="panel-body">
              <div class="form-group">
                <label class="col-sm-3 control-label">Featured</label>
                <div class="col-sm-9">
                  <label class="i-switch bg-primary m-t-xs m-r">
                    <input type="checkbox" name='featured' ng-model="mapdetails.featured">
                    <i></i>
                  </label>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Status</label>
                <div class="col-sm-9">
                  <label class="i-switch bg-primary m-t-xs m-r">
                    <input type="checkbox" name='status' ng-model="mapdetails.status">
                    <i></i>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Announcement
            </div>
            <div class="panel-body">
              <div class="mapnews" ng-repeat="news in news">
                <a href="#" ng-click="viewmapnews(news)">
                  <h5>{[{ news.created_at | formatdatetime }]}</h5>
                  <blockquote>
                    <p>{[{ news.news | limitTo: 200}]} <span ng-if="news.news.length > 200">...</span></p>
                  </blockquote>
                </a>
              </div>
              <h5 ng-if="news.length == 0">No announcement.</h5>
              <a class="pull-right" ng-click="seemore()" ng-if="news.length > 0">see more</a>
            </div>
          </div>
        </div>
        
    </div>
  </fieldset>
</form>
