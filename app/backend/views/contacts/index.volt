{{ content() }}

<script type="text/ng-template" id="deleteContact.html">
    <div ng-include="'/be/tpl/deleteContact.html'"></div>
</script>

<script type="text/ng-template" id="viewContact.html">
    <div ng-include="'/be/tpl/viewContact.html'"></div>
</script>

<toaster-container ></toaster-container>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Contact Us</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="form">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">

      <div class="row">

        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Contact List
            </div>


              <div class="panel-body">

                <div class="row wrapper">
                    <div class="col-sm-4 m-b-xs" ng-show="keyword">
                        <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
                    </div>
                    <div class="col-sm-3 m-b-xs pull-right">
                        <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext, searchdate)">Go!</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3 m-b-xs pull-right">
                        <div class="input-group">
                          <span class="input-group-btn">
                            <input id="date" name="date" class="input-sm form-control" datepicker-popup="yyyy-MM-dd" ng-model="searchdate" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                            <button type="button" class="btn btn-sm btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                          </span>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th width="30%">Subject</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody ng-show="loading">
                            <tr colspan="6">
                                <td>Loading Contacts</td>
                            </tr>
                        </tbody>
                        <tbody ng-hide="loading">
                            <tr colspan="6" ng-show="bigTotalItems==0"> <td> No records found! </td></tr>
                            <tr ng-repeat="mem in data.data">
                                <td>
                                    <span class="label label-warning" ng-if="mem.status == 1"><i class="fa fa-envelope"></i></span>
                                    <span class="label label-info" ng-if="mem.status == 2"><i class="fa fa-envelope-o"></i></span>
                                    <span class="label label-primary" ng-if="mem.status == 3"><i class="fa fa-reply"></i></span>
                                </td>
                                <td>{[{ mem.firstname + " " + mem.lastname }]}</td>
                                <td>{[{ mem.email }]}</td>
                                <td>{[{ mem.subject }]}</td>
                                <td>{[{ mem.created_at }]}</td>
                                <td>
                                    <button class="btn btn-sm btn-icon btn-info" ng-click="view(mem.id)"><icon class="fa fa-reply"></icon></button>
                                    <button class="btn btn-sm btn-icon btn-danger" ng-click="delete(mem.id)"><icon class="fa fa-times-circle"></icon></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

              </div>


          </div>
        </div>

      </div>

      <div class="row" ng-hide="bigTotalItems==0 || loading">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
                <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
</fieldset>
</form>
