
<script type="text/ng-template" id="deletePin.html">
  <div ng-include="'/fe/tpl/deletePin.html'"></div>
</script>
<script type="text/ng-template" id="setKingPin.html">
    <div ng-include="'/fe/tpl/setKingPin.html'"></div>
</script>
<script type="text/ng-template" id="mapInfo.html">
    <div ng-include="'/fe/tpl/mapInfo.html'"></div>
</script>
<script type="text/ng-template" id="mapCover.html">
    <div ng-include="'/fe/tpl/mapCover.html'"></div>
</script>
<toaster-container ></toaster-container>

<!-- banner -->
<div class="small-banner">
    <div class="container">
        <ul class="breadcrumbs h-list">
            <li><a href="/">Home</a></li>
            <li class="active">Create Mission Map</li>
        </ul>
        <h1>Create Mission Map</h1>
    </div>
</div>
<!-- banner -->

<!-- Page content starts -->
<div class="content create-mission-map">
    <div class="container">
        <div class="row" ng-hide="loader">
            <div class="col-md-8">
                <h3 ng-show="mapTitle"><a href="" ng-click="showMapInfo()"> {[{ mapTitle }]} <icon class="fa fa-pencil"></icon></a></h3>
                <p><a href="" ng-click="showMapInfo()"><span ng-show="mapCategory"><icon class="fa fa-bookmark"></icon> {[{ mapCategory }]}</span> <span ng-show="mapTags"><icon class="fa fa-tag"></icon> {[{ mapTags }]}</span></a></p>
                <p ng-show="mapDescription"> <a ng-click="showMapInfo()"> {[{ mapDescription }]} <icon class="fa fa-pencil"></icon></a></p>
            </div>
            <div class="col-md-4 text-right" style="padding-top: 18px;">
                <img src="{[{agentpic}]}" alt="" class="agent-pic" style="float: right">
                <div style="padding: 10px 5px; float: right;">
                    <span ng-hide="showauthor">Anonymous</span>
                    <span ng-show="showauthor">{[{ agentName }]}</span>
                    <br>
                    <span class="text-muted"><input type="checkbox" ng-model="showauthor" name="showauthor" id="showauthor"> <label for="showauthor">Show my info as creator.</label></span>
                </div>
            </div>
        </div>

        <div class="row" ng-hide="loader">
            <div class="col-md-12">
                <!-- Contact -->
                <div ng-show="markers.length == 0"  >

                </div>
                <!-- Google maps -->
                <script type="text/ng-template" id="control.tpl.html">
                    <button class="btn btn-sm btn-primary togglebutton" ng-click="controlClick()">  <i class="fa fa-bars"></i>  </button>
                </script>
                <script type="text/ng-template" id="control.info.html">
                    <a href="" popover-placement="right" popover="Map Information" popover-trigger="mouseenter" ng-click="clickMapInfo()" style=" margin-top: 30px;" >
                        <img src="/img/mapcontrols/info.png" style="margin-left: 20px;" class="mission-map-controls">
                    </a>
                </script>
                <script type="text/ng-template" id="control.coverphoto.html">
                    <a href="" popover-placement="right" popover="Cover Photo" popover-trigger="mouseenter" ng-click="clickMapCover()">
                        <img src="{[{ mapcover }]}" style="margin-left: 20px; margin-top:10px" class="mission-map-controls">
                    </a>
                </script>
                <script type="text/ng-template" id="control.noti.html">
                    <a href="" ng-init="showNoti=true" ng-click="showNoti = !showNoti" ng-show="showNoti" style="margin-top: 10px;">
                        <div class="alert alert-info alert-dismissible pin-pic-error" role="alert" >
                            <i class="fa fa-info-circle"></i>
                            A kingpin is where your mission started. For more faqs visit link.
                        </div>
                    </a>
                </script>
                <script type="text/ng-template" id="searchbox.tpl.html">
                    <input id="search-box" type="text" class="search-box" name="q" placeholder="Search Place..." />
                </script>
                <div class="gmap">
                    <!-- Google Maps. Replace the below iframe with your Google Maps embed code -->
                    <ui-gmap-google-map center='map.center' zoom='map.zoom' events="map.events" options="options">
                        <ui-gmap-search-box template="searchbox.template" events="searchbox.events" position="searchbox.position"></ui-gmap-search-box>
                        <ui-gmap-map-control template="control.tpl.html" position="right-top" controller="InfoWindowCtrl" index="-1"></ui-gmap-map-control>
                        <ui-gmap-map-control template="control.noti.html" position="bottom"  controller="InfoWindowCtrl" index="-1"></ui-gmap-map-control>
                        <ui-gmap-map-control template="control.info.html" position="left" style="left: 100px;" controller="InfoWindowCtrl" index="-1"></ui-gmap-map-control>
                        <ui-gmap-map-control template="control.coverphoto.html" position="left" style="left: 100px;" controller="InfoWindowCtrl" index="-1"></ui-gmap-map-control>


                        <ui-gmap-marker ng-repeat="m in markers" coords="m.coords" idkey="m.id" options="m.options" events="m.events">
                        
                            <ui-gmap-window show="m.show" options="map.infoWindowWithCustomClass.options">
                                <div ng-controller="InfoWindowCtrl">
                                    <span dynamic = "buttons"></span>
                                    <a class="markercontrol" ng-click="deleteMarker(m.id)"><i class="fa fa-minus-circle"></i> Delete </a>
                                    <a class="markercontrol" ng-class="{ 'control-disabled' : m.kingpin}" ng-click="setKingPin(m.id, m.kingpin)"><i class="fa fa-map-marker"></i> Kingpin  </a>

                                </div>
                            </ui-gmap-window>>

                        </ui-gmap-marker>
                    </ui-gmap-google-map>
                </div>
                <div class="gmap-marker-info sidebar" style="display: none; width: 38%; position: relative; float: left; height: 500px;" >
                    <div class="panel">
                        <div class="panel-header">
                            <p class="text-center category">Pin Information</p>
                        </div>
                        <div class="panel-content">
                            <form name="form1" class="fomr-horizontal">
                                <div class="col-md-12">
                                    <tabset justified="true">
                                        <tab heading="Marker Info">
                                            <br>
                                            <div class="alert alert-success" ng-show="notiCheck" style="margin-bottom: 5px;">
                                                <icon class="fa fa-check"> Your changes have been saved.</icon>
                                            </div>
                                            <div class="form-group" id="markerInfo">
                                                <label for="exampleInputEmail1">Description </label>
                                                <input class="form-control" ng-model="mk.description" ng-change="saveMarkerInfo(mk)">
                                            </div>
                                            <div class="form-group" style="margin: 0px;">
                                                <label for="name">Video Link</label>
                                                <input type="text" class="form-control" id="name" ng-model="mk.videolink" ng-change="saveMarkerInfo(mk); viddisp(mk.videolink)">
                                            </div>
                                            <div ng-bind-html="videodisplay"> </div>
                                        </tab>
                                        <tab heading="Picture" id="pictureTab">
                                            <div ngf-drop ngf-select ng-model="files" class="drop-box"
                                                    ngf-drag-over-class="dragover" ngf-multiple="true" ngf-change="prepare($files)" ngf-allow-dir="true" accept="image/*">Drop marker images here or click to upload
                                            </div>
                                            <div class="alert alert-danger alert-dismissible pin-pic-error" role="alert" ng-repeat="fe in flashError">
                                              <button type="button" ng-click="removeerror($index)" class="close pin-pic-error-close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <strong>{[{ fe.name }]}</strong> image size is too big.
                                            </div>
                                            <ul class="pin-pic-list">
                                                <li ng-repeat="f in currentPinImages" style="font:smaller">{[{ f.name }]}
                                                <a href="" class="remove-pin-pic" ng-click="removepinpic($index)"><i class="fa fa-remove"></i></a></li>
                                            </ul>
                                            <div ngf-no-file-drop>File Drag/Drop is not supported for this browser</div>
                                        </tab>
                                    </tabset>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 footer">
                <div class="pull-right">
                    <button type="submit" class="btn btn-pi" ng-disabled="form.$invalid||markers.length==0" ng-click="saveMap(showauthor)">Map this Mission</button>
                </div>
            </div>
        </div>


        
        <div class="row" ng-show="loader">
            <div class="cwell text-center">
                <img src="/img/preloader3.gif"/>
            </div>
            <div class="cwell text-center">
                <h4 class="context-loader">{[{ loaderText }]}</h4>
            </div>
        </div>
    </div>
</div>

<!-- Page content ends -->
