<div class="content create-map-page about-pi">
	<div class="container">
		<h3 class="title">WHAT IS PLANET IMPOSSIBLE?</h3>
		<p>Planet impossible is Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus impedit deserunt debitis veniam quibusdam, praesentium soluta harum. Ducimus, inventore incidunt, consequatur dolorem magnam aperiam labore modi iusto omnis maiores itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

		<div class="video-container">
			<iframe width="560" height="315" src="https://www.youtube.com/embed/gOW_azQbOjw" class="center-block" frameborder="0" allowfullscreen></iframe>
		</div>

		<h3 class="title">YOUR MISSION</h3>
		<p>Planet impossible has successfully Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus impedit deserunt debitis veniam quibusdam, praesentium soluta harum. Ducimus, inventore incidunt, consequatur dolorem magnam aperiam labore modi iusto omnis maiores itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

		<div class="row about-maps">
			<div class="col-lg-4">
				<h4 class="text-center">ACTIONS</h4>
				<h2 class="text-center">104, 506, 332</h2>
			</div>
			<div class="col-lg-4">
				<h4 class="text-center">AGENTS</h4>
				<h2 class="text-center">3, 590, 051</h2>
			</div>
			<div class="col-lg-4">
				<h4 class="text-center">MISSIONS/IDEAS</h4>
				<h2 class="text-center">204</h2>
			</div>
		</div>

		<div class="gmap">
			<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d12325604.853620162!2d-99.40748758246184!3d41.04317354921439!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sph!4v1441614645612" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>

		<h3 class="title">WHO IS AGENT PI?</h3>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus impedit deserunt debitis veniam quibusdam, praesentium soluta harum. Ducimus, inventore incidunt, consequatur dolorem magnam aperiam labore modi iusto omnis maiores itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus impedit deserunt debitis veniam quibusdam, praesentium soluta harum. Ducimus, inventore incidunt, consequatur dolorem magnam aperiam labore modi iusto omnis maiores itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	</div>
</div>