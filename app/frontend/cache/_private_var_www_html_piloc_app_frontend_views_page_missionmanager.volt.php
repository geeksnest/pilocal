<script type="text/ng-template" id="viewPin.html">
  <div ng-include="'/fe/tpl/viewPin.html'"></div>
</script>

<script type="text/ng-template" id="editMap.html">
  <div ng-include="'/fe/tpl/editMap.html'"></div>
</script>

<script type="text/ng-template" id="viewMapList.html">
  <div ng-include="'/fe/tpl/viewMapList.html'"></div>
</script>

<script type="text/ng-template" id="deleleComment.html">
  <div ng-include="'/fe/tpl/deleleComment.html'"></div>
</script>

<script type="text/ng-template" id="editComment.html">
  <div ng-include="'/fe/tpl/editComment.html'"></div>
</script>

<script type="text/ng-template" id="mapNews.html">
  <div ng-include="'/fe/tpl/mapNews.html'"></div>
</script>

<script type="text/ng-template" id="agentSidebar.html">
  <div ng-include="'/fe/tpl/agentSidebar.html'"></div>
</script>

<script type="text/ng-template" id="viewNews.html">
  <div ng-include="'/fe/tpl/viewNews.html'"></div>
</script>

<script type="text/ng-template" id="viewNewsList.html">
  <div ng-include="'/fe/tpl/viewNewsList.html'"></div>
</script>

<script type="text/ng-template" id="searchbox.tpl.html">
    <input id="search-box" type="text" class="search-box" name="q" placeholder="Search Place..." />
</script>

<script type="text/ng-template" id="mediagallery.html">
  <div ng-include="'/fe/tpl/mediagallery.html'"></div>
</script>

<toaster-container ></toaster-container>
<!-- banner -->
<div class="pull-left full-width">
    <div class="container">
        <ul class="breadcrumbs h-list">
            <li><a href="/">Home</a></li>
            <li><a href="#">My Account</a></li>
            <li class="active">Mission Manager</li>
        </ul>
    </div>
</div>
<!-- banner -->
<div class="content userprofile">
    <div class="container">
        <hr>
        <div class="sidebar leftsidebar">
            <div class="panel" ng-include="'agentSidebar.html'">
            </div>
            <div class="mg-bot" ng-if="!default">
                <a href="" class="btn btn-link no-hover btn-link-pi pull-right" ng-click="addNews()">Add Announcement</a>
            </div>

            <div class="panel" ng-if="!default">
                <div class="panel-header">
                    <p class="full-width text-center mission-announcement">Mission Announcement</p>
                </div>
                <div class="panel-content announcement">
                    <div class="list" ng-repeat="news in mapnews">
                        <a href="" ng-click="viewmapnews(news)">
                            <div class="full-width">
                                <strong>{[{ news.created_at | formatdate }]}</strong>
                            </div>
                            <p class="full-width">&#34;{[{ news.news }]}.&#34;</p>
                        </a>
                    </div>
                    <a href="#" class="viewmore" ng-if="mapnews.length > 0" ng-click="viewmorenews()">view more</a>
                    <div class="list" ng-if="mapnews.length == 0">
                        <h6 class="text-center">No announcement</h6>
                    </div>
                </div>
            </div>

        </div>

        <div class="rightcontent">
            <h3 class="no-margin">Mission Manager 
                <a ng-click="viewlist()" class="btn btn-pi pull-right" ng-if="mapcount > 0"><span class="fa fa-list"></span> List of Mission Maps</a>
                <a ng-click="viewglobe()" class="btn btn-pi pull-right"><span class="fa fa-globe"></span></a>
            </h3>
            <div>
                <div class="gmap mg-top mg-bottom">
                    <!-- Google Maps. Replace the below iframe with your Google Maps embed code -->
                    <ui-gmap-google-map center='map.center' zoom='map.zoom' events="map.events" options="options">
                        <ui-gmap-search-box template="searchbox.template" events="searchbox.events" position="searchbox.position"></ui-gmap-search-box>
                        <ui-gmap-marker ng-repeat="m in missionmarkers" coords="m.coords" idkey="m.id" options="m.options" events="m.events">
                            <ui-gmap-window show="m.show" options="m.WindowWithCustomClass.options">
                                <div>
                                    <div style="background-image: url('{[{m.agentpic}]}')" alt="" class="agent-winimg pull-left bg-image" ng-cloak></div>
                                    <p class="full-width pull-left text-center">{[{ m.agentname }]}</p>
                                </div>
                            </ui-gmap-window>
                        </ui-gmap-marker>
                    </ui-gmap-google-map>
                </div>
                <div ng-if="!default">
                    <div class="col-lg-4">
                        <img src="{[{mapdetails.cover}]}" alt="" class="full-width mg-top" ng-if="mapdetails.coverType=='image'">
                        <div ng-if="mapdetails.coverType=='video'" ng-bind-html="mapdetails.cover" class="mg-top">
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <h3 ng-bind-html="mapdetails.title"></h3>
                        <p ng-bind-html="mapdetails.description"></p>
                    </div>
                    <div class="col-lg-12">
                        <p class="no-margin mg-top">
                            <small>Categories: <a href="" ng-repeat="category in categories">{[{category.cattitle}]}<span ng-if="$last!=true">, </span></a></small>
                        </p>
                        <p>
                            <small>Tags: <a href="" ng-repeat="tag in tags">{[{tag.tag}]}<span ng-if="$last!=true">, </span></a></small>
                        </p>
                    </div>
                    <div class="socialmedia-btn">
                        <div class="pull-right map-report-btn" ng-cloak>
                            <div class="pull-left">
                                Accept all pins: <input type="checkbox" ng-model="approvepins" ng-change="approvingpins(approvepins)">
                            </div>
                            <div class="pull-left">
                               <i class="fa fa-eye fa-lg"></i> {[{ mapviews }]}
                            </div>
                            <div class="pull-left">
                               <i class="fa fa-comments-o fa-lg"></i> {[{ commentcount }]}
                            </div>
                            <div class="pull-left">
                                <i class="fa fa-map-marker fa-lg"></i> {[{ mapactions }]}
                            </div>
                            <div class="pull-left">
                                <a href="" class="no-hover no-margin" ng-click="editMap()">
                                    <i class="fa fa-pencil fa-lg"></i> Edit
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="" class="no-hover no-margin" ng-click="changestat(1)" ng-if="mapdetails.status==0">
                                    <i class="fa fa-check-square-o fa-lg"></i> Activate
                                </a>
                                <a href="" class="no-hover no-margin" ng-click="changestat(0)" ng-if="mapdetails.status==1">
                                    <i class="fa fa-ban fa-lg"></i> Deactivate
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="" class="no-hover no-margin" ng-click="changecover()">
                                    <i class="fa fa-file-image-o fa-lg"></i> Change cover
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="comment-container pull-left full-width">
                        <p class="pull-left full-width"><i class="fa fa-comment-o fa-2x"></i> All Action Responses({[{ commentcount}]})</p>
                        <div class="full-width">
                            <div class="commentbox hiddenoverflow" style="width: 100%">
                                <!--<div class="image pull-left bg-image" style="background-image: url('{[{ agentprofpic }]}')"></div>-->
                                <div style="float: left; width: 13%;">
                                    <img class="image pull-left" src="{[{agentprofpic}]}" alt="">
                                </div>
                                <!--<textarea class="input form-control pull-left comment mg-bottom" ng-disabled="agentid==null" ng-model="comment" placeholder="Add a comment..."></textarea>-->
                                <div style="float: left; width: 86%;">
                                    <textarea ckeditor="editorOptions" name="editor" ng-model="comment" ng-disabled="agentid==null"></textarea>
                                </div>
                                <a ng-click="commentlistener(comment)" class="btn btn-pi pull-right" style="margin-top: 10px;"><i class="fa fa-commenting-o"></i> Post</a>
                            </div>
                            <hr>
                        </div>
                        <div class="commentlist hiddenoverflow" ng-repeat="comment in comments">
                            <!-- <img class="image pull-left" src="{[{comment.profile_pic_name}]}" alt="" ng-if="comment.superagent==0"> -->
                            <div class="image pull-left bg-image" style="background-image: url('{[{comment.profile_pic_name}]}')" ng-if="comment.superagent==0"></div>
                            <img class="image pull-left" src="/img/pilogo.png" alt="" ng-if="comment.superagent==1">
                            <div class="comment pull-left">
                                <h6 class="no-margin mg-bottom"  ng-cloak>
                                    <span ng-if="comment.hide_agent != 0">{[{ comment.username }]}</span>
                                    <span ng-if="comment.hide_agent == 0">Anonymous Agent</span>
                                     | <small>{[{ comment.created_at | formatdatetime }]}</small>
                                    <div class="btn-group pull-right" dropdown is-open="status.isopen" ng-if="comment.type=='map' && comment.superagent==0">
                                      <button type="button" class="btn btn-link" dropdown-toggle ng-disabled="disabled"><span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu" aria-labelledby="single-button">
                                        <li role="menuitem" ng-if="comment.agent==agentid">
                                            <a ng-click="comment.showedit=true" class="btn btn-link pull-left full-width text-left" ng-if="comment.showedit==false">Edit</a>
                                            <a ng-click="comment.showedit=false" class="btn btn-link pull-left full-width text-left" ng-if="comment.showedit">Cancel</a>
                                        </li>
                                        <li role="menuitem"><a ng-click="deleteComment(comment.id, 'comment')" class="btn btn-link pull-left full-width text-left">Delete</a></li>
                                      </ul>
                                    </div>
                                </h6>
                                <p  ng-cloak ng-bind-html="comment.comment" ng-if="comment.showedit==false"></p>
                                <!-- edit comment -->
                                <div class="full-width pull-left" ng-if="comment.showedit">
                                    <!--<textarea class="input form-control pull-left full-width mg-bottom" ng-model="comment.comment" placeholder="Add a comment..."></textarea>-->
                                    <textarea ckeditor="editorOptions" name="editorcomment" class="input form-control pull-left full-width mg-bottom" ng-model="comment.comment">

                                    </textarea>
                                    <a ng-click="editComment(comment.id, comment.comment, 'comment')" style="margin-top: 10px" class="btn btn-pi pull-right"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                </div>

                                <ul light-slider ng-if="comment.type=='pin'"></ul>
                                <a href="" class="pull-left btn btn-link" ng-if="comment.showreply==false && agentid!=null" ng-click="showreplybox(comment.id)"><i class="fa fa-reply"></i> Reply</a>
                            </div>
                            <div class="pull-right reply-loadmore">
                                <a ng-click="loadmorereply(comment.id)" class="btn btn-link pull-right" ng-repeat="r in replycount" ng-if="r.count > r.loaded && r.id==comment.id"> load more</a>
                            </div>
                            <div class="pull-right commentreplylist" ng-repeat="reply in replies" ng-if="reply.mapcomment_id==comment.id ">
                                <div class="commentreply hiddenoverflow">
                                    <!-- <img class="image pull-left" src="{[{reply.profile_pic_name}]}" alt="" ng-if="reply.superagent==0"> -->
                                    <div class="image pull-left bg-image" style="background-image: url('{[{reply.profile_pic_name}]}')" ng-if="reply.superagent==0"></div>
                                    <img class="image pull-left" src="/img/pilogo.png" alt="" ng-if="reply.superagent==1">
                                    <div class="comment pull-left">
                                        <h6 class="no-margin mg-bottom"  ng-cloak>
                                            {[{ reply.username  }]} | <small>{[{ reply.updated_at | formatdatetime }]}</small>
                                            <div class="btn-group pull-right" dropdown ng-if="reply.superagent==0">
                                              <button type="button" class="btn btn-link" dropdown-toggle><span class="caret"></span>
                                              </button>
                                              <ul class="dropdown-menu" role="menu" aria-labelledby="single-button">
                                                <li role="menuitem" ng-if="agentid==reply.agent">
                                                    <a ng-click="reply.showedit=true" class="btn btn-link pull-left full-width text-left" ng-if="reply.showedit==false">Edit</a>
                                                    <a ng-click="reply.showedit=false" class="btn btn-link pull-left full-width text-left" ng-if="reply.showedit">Cancel</a>
                                                </li>
                                                <li role="menuitem"><a ng-click="deleteComment(reply.id, 'response')" class="btn btn-link pull-left full-width text-left">Delete</a></li>
                                              </ul>
                                            </div>
                                        </h6>
                                        <p  ng-cloak ng-bind-html="reply.reply" ng-if="reply.showedit==false"></p>
                                        <!-- edit comment -->
                                        <div class="full-width pull-left" ng-if="reply.showedit">
                                            <!--<textarea class="input form-control pull-left mg-bottom full-width" ng-model="reply.reply"></textarea>-->
                                            <textarea ckeditor="editorOptions" name="editoredit" style="margin-top: 10px" class="input form-control pull-left mg-bottom full-width" ng-model="reply.reply"></textarea>
                                            <a ng-click="editComment(reply.id, reply.reply, 'response')" class="btn btn-pi pull-right"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right commentreplylist tbox" ng-if="agentid!=null">
                                <div class="replybox full-width hiddenoverflow" ng-if="comment.showreply" style="padding-right: 20px;">
                                    <!--<textarea class="input form-control pull-left reply full-width mg-bottom" ng-disabled="agentid==null" ng-model="reply" placeholder="Add a response..."></textarea>-->
                                    <textarea ckeditor="editorOptions" name="editorreply" class="input form-control pull-left reply full-width mg-bottom" ng-disabled="agentid==null" ng-model="reply" placeholder="Add a response..."></textarea>
                                    <a ng-click="replytocomment(reply, comment.id, comment.type, comment.marker_id)" style="margin-top: 10px" class="btn btn-pi pull-right mg-bottom"><i class="fa fa-reply"></i> Reply</a>
                                </div>
                            </div>
                        </div>
                        <button ng-if="commentcount > 4 && commentcount > commentload" class="btn btn-block loadmore btn-pi" ng-click="loadcomment();">
                        <div class="spinner pull-left" ng-if="spinner"></div> Load more</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
