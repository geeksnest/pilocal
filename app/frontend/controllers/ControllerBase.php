<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    protected function initialize()
    {

        $this->view->metadescription = '';
        $this->view->metakeywords = '';
        $this->view->metaauthor = '';

        $this->view->fbtitle = '';
        $this->view->fbdesc = '';
        $this->view->fbcover = '';

        $this->view->base_url = $this->config->application->BaseURL;
        $this->view->api_url = $this->config->application->ApiURL;
        $this->view->amasonLink = $this->config->application->amazonlink;
    }

    public function angularLoader($ang){
        $modules = array();
        $scripts = '';
        foreach($ang as $key => $val){
            $scripts .= $this->tag->javascriptInclude($val);
            $modules[] = $key;
        }
        $this->view->modules = (!empty($modules) ? $modules : array());
        $this->view->otherjvascript = $scripts;
    }
    public function httpPost($url,$params)
    {
        $postData = '';
        //create name value pairs seperated by &
        foreach($params as $k => $v)
        {
            $postData .= $k . '='.$v.'&';
        }
        rtrim($postData, '&');

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output=curl_exec($ch);

        curl_close($ch);
        return $output;
    }
}
