<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class IndexController extends ControllerBase
{
    public function indexAction()
    {
        $this->angularLoader(array(
            'userfactory' => 'fe/scripts/factory/user.js'
        ));

        if(isset($_GET["_escaped_fragment_"])){

            var_dump($_GET);
            $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
            die();
        }
        //$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function mainPageAction(){

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function sharePageAction($slugs){

        $service_url_news = $this->config->application->ApiURL. '/map/getpins/' . $slugs . '/null';

        $curl = curl_init($service_url_news);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->metaauthor = $decoded->map->name;

        $this->view->fbtitle = $decoded->map->title;
        $this->view->fbdesc = $decoded->map->description;

        $this->view->fbcover = $this->config->application->amazonlink.'/uploads/maps/'.$decoded->map->id.'/'.$decoded->map->cover;

    }
}

