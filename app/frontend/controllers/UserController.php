<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;
class UserController extends ControllerBase
{
    public function indexAction()
    {
        echo "Boom Panes!";
    }

    public function loginAction()
    {
        $this->angularLoader(array(
            'login' => 'fe/scripts/controllers/login.js',
            'userfactory' => 'fe/scripts/factory/user.js'
        ));
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function profileAction($username)
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function missionmanagerAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function accountAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}

