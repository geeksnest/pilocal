<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;
class PageController extends ControllerBase
{
    public function indexAction()
    {

    }
    public function aboutAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function contactusAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function searchAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function policyAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function termsofserviceAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function mainpageAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function startmissionAction()
    {
        $this->angularLoader(array(
            'login' => 'fe/scripts/controllers/login.js',
            'userfactory' => 'fe/scripts/factory/user.js',
            'maps' => 'fe/scripts/controllers/maps.js'
        ));
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function createmissionAction()
    {
        $this->angularLoader(array(
            'login' => 'fe/scripts/controllers/login.js',
            'userfactory' => 'fe/scripts/factory/user.js',
            'maps' => 'fe/scripts/controllers/maps.js',
            'mapsFactory' => 'fe/scripts/factory/maps.js',
            'mapsDirective' => 'fe/scripts/directives/maps.js'
        ));
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function listmissionAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function loginAction()
    {
        $this->angularLoader(array(
            'login' => 'fe/scripts/controllers/login.js',
            'userfactory' => 'fe/scripts/factory/user.js'
        ));
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function profileAction($username)
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function missionmanagerAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function viewmissionAction($slug)
    {
        $this->view->title = $slug;
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function registrationAction()
    {
        $this->angularLoader(array(
            'registration' => 'fe/scripts/controllers/registration.js',
            'birthdayfactory' => 'fe/scripts/factory/birthday.js',
            'userfactory' => 'fe/scripts/factory/user.js',
            'validation' => 'fe/scripts/directives/validations.js',
            'passstrength' => 'vendors/pass-strength/src/strength.min.js'
        ));
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function successAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function activationAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function accountAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function categoryAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}
