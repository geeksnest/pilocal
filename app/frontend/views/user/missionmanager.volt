<script type="text/ng-template" id="viewPin.html">
  <div ng-include="'/fe/tpl/viewPin.html'"></div>
</script>

<script type="text/ng-template" id="editMap.html">
  <div ng-include="'/fe/tpl/editMap.html'"></div>
</script>

<script type="text/ng-template" id="viewMapList.html">
  <div ng-include="'/fe/tpl/viewMapList.html'"></div>
</script>

<script type="text/ng-template" id="deleleComment.html">
  <div ng-include="'/fe/tpl/deleleComment.html'"></div>
</script>

<script type="text/ng-template" id="editComment.html">
  <div ng-include="'/fe/tpl/editComment.html'"></div>
</script>

<script type="text/ng-template" id="mapCover.html">
  <div ng-include="'/fe/tpl/mapCover.html'"></div>
</script>

<toaster-container ></toaster-container>
<!-- banner -->
<div class="pull-left full-width">
    <div class="container">
        <ul class="breadcrumbs h-list">
            <li><a href="/">Home</a></li>
            <li><a href="#">My Account</a></li>
            <li class="active">Mission Manager</li>
        </ul>
    </div>
</div>
<!-- banner -->
<div class="content userprofile">
    <div class="container">
        <hr>
        <div class="sidebar leftsidebar">
            <div class="panel">
                <div class="panel-header">
                    <p class="full-width text-center">{[{ agent.username }]}</p>
                </div>
                <div class="panel-content">
                    <ul class="no-list-style">
                        <li><a href="" class="no-hover">Dashboard</a></li>
                        <li><a ui-sref="account" class="no-hover">Account</a></li>
                        <li><a ui-sref="profile({ username: '{[{ username }]}' })" class="no-hover">Profile</a></li>
                        <li><a ui-sref="missionmanager" class="no-hover">Mission Manager  <span class="badge badge-pi">{[{notification.pendingpin}]}</span></a></li>
                        <li><a href="" class="no-hover">Community</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="rightcontent">
            <h3 class="no-margin">Mission Manager <a ng-click="viewlist()" class="btn btn-pi pull-right" ng-if="nomap==false"><span class="fa fa-list"></span> List of Mission Maps</a></h3>
            <div ng-if="nomap==false">
                <div class="gmap mg-top mg-bottom">
                    <!-- Google Maps. Replace the below iframe with your Google Maps embed code -->
                    <ui-gmap-google-map center='map.center' zoom='map.zoom' events="map.events" options="options">
                        <ui-gmap-marker ng-repeat="m in missionmarkers" coords="m.coords" idkey="m.id" options="m.options" events="m.events">
                            <ui-gmap-window show="m.show">
                                <div>
                                    <img src="{[{m.agentpic}]}" alt="" class="agent-winimg" ng-cloak> {[{ m.agentname }]}
                                </div>
                            </ui-gmap-window>
                        </ui-gmap-marker>
                    </ui-gmap-google-map>
                </div>
                <div class="col-lg-4">
                    <img src="{[{mapdetails.cover}]}" alt="" class="full-width mg-top" ng-if="mapdetails.coverType=='image'">
                    <div ng-if="mapdetails.coverType=='video'" ng-bind-html="mapdetails.cover" class="mg-top">
                    </div>
                </div>
                <div class="col-lg-8">
                    <h3 ng-bind-html="mapdetails.title"></h3>
                    <p ng-bind-html="mapdetails.description"></p>
                </div>
                <div class="socialmedia-btn">
                    <div class="pull-right map-report-btn" ng-cloak>
                        <div class="pull-left">
                           <i class="fa fa-eye fa-lg"></i> {[{ mapviews }]}
                        </div>
                        <div class="pull-left">
                           <i class="fa fa-comments-o fa-lg"></i> {[{ commentcount }]}
                        </div>
                        <div class="pull-left">
                            <i class="fa fa-map-marker fa-lg"></i> {[{ mapactions }]}
                        </div>
                        <div class="pull-left">
                            <a href="" class="no-hover no-margin" ng-click="editMap()">
                                <i class="fa fa-pencil fa-lg"></i> Edit
                            </a>
                        </div>
                        <div class="pull-left">
                            <a href="" class="no-hover no-margin" ng-click="changestat(1)" ng-if="mapdetails.status==0">
                                <i class="fa fa-check-square-o fa-lg"></i> Activate
                            </a>
                            <a href="" class="no-hover no-margin" ng-click="changestat(0)" ng-if="mapdetails.status==1">
                                <i class="fa fa-ban fa-lg"></i> Deactivate
                            </a>
                        </div>
                        <div class="pull-left">
                            <a href="" class="no-hover no-margin" ng-click="changecover()">
                                <i class="fa fa-file-image-o fa-lg"></i> Change cover
                            </a>
                        </div>
                    </div>
                </div>
                <div class="comment-container pull-left full-width">
                    <p class="pull-left full-width"><i class="fa fa-comment-o fa-2x"></i> All Action Responses({[{ commentcount}]})</p>
                    <div class="full-width">
                        <div class="commentbox hiddenoverflow">
                            <img class="image pull-left" src="{[{agentprofpic}]}" alt="">
                            <textarea class="input form-control pull-left comment mg-bottom" ng-disabled="agentid==null" ng-model="comment" placeholder="Add a comment..."></textarea>
                            <a ng-click="commentlistener(comment)" class="btn btn-pi pull-right"><i class="fa fa-commenting-o"></i> Post</a>
                        </div>
                        <hr>
                    </div>
                    <div class="commentlist hiddenoverflow" ng-repeat="comment in comments">
                        <img class="image pull-left" src="{[{comment.profile_pic_name}]}" alt="">
                        <div class="comment pull-left">
                            <h6 class="no-margin mg-bottom"  ng-cloak>
                                <span ng-if="comment.hide_agent != 0">Agent {[{ comment.first_name + " " + comment.last_name }]}</span>
                                <span ng-if="comment.hide_agent == 0">Anonymous Agent</span>
                                 | <small>{[{ comment.created_at | date: 'MMMM dd, yyyy hh:mm a' }]}</small>
                                <div class="btn-group pull-right" dropdown is-open="status.isopen" ng-if="comment.type=='map'">
                                  <button type="button" class="btn btn-link" dropdown-toggle ng-disabled="disabled"><span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" role="menu" aria-labelledby="single-button">
                                    <li role="menuitem" ng-if="comment.agent==agentid">
                                        <a ng-click="comment.showedit=true" class="btn btn-link pull-left full-width text-left" ng-if="comment.showedit==false">Edit</a>
                                        <a ng-click="comment.showedit=false" class="btn btn-link pull-left full-width text-left" ng-if="comment.showedit">Cancel</a>
                                    </li>
                                    <li role="menuitem"><a ng-click="deleteComment(comment.id, 'comment')" class="btn btn-link pull-left full-width text-left">Delete</a></li>
                                  </ul>
                                </div>
                            </h6>
                            <p  ng-cloak ng-bind-html="comment.comment" ng-if="comment.showedit==false"></p>
                            <!-- edit comment -->
                            <div class="full-width pull-left" ng-if="comment.showedit">
                                <textarea class="input form-control pull-left full-width mg-bottom" ng-model="comment.comment" placeholder="Add a comment..."></textarea>
                                <a ng-click="editComment(comment.id, comment.comment, 'comment')" class="btn btn-pi pull-right"><i class="fa fa-pencil-square-o"></i> Edit</a>
                            </div>

                            <ul light-slider ng-if="comment.type=='pin'"></ul>
                            <a href="" class="pull-left btn btn-link" ng-if="comment.showreply==false && agentid!=null" ng-click="showreplybox(comment.id)"><i class="fa fa-reply"></i> Reply</a>
                        </div>
                        <div class="pull-right reply-loadmore">
                            <a ng-click="loadmorereply(comment.id)" class="btn btn-link pull-right" ng-repeat="r in replycount" ng-if="r.count > r.loaded && r.id==comment.id"> load more</a>
                        </div>
                        <div class="pull-right commentreplylist" ng-repeat="reply in replies" ng-if="reply.mapcomment_id==comment.id ">
                            <div class="commentreply hiddenoverflow">
                                <img class="image pull-left" src="{[{reply.profile_pic_name}]}" alt="">
                                <div class="comment pull-left">
                                    <h6 class="no-margin mg-bottom"  ng-cloak>
                                        Agent {[{ reply.first_name + " " + reply.last_name }]} | <small>{[{ reply.created_at | date: 'MMMM dd, yyyy hh:mm a' }]}</small>
                                        <div class="btn-group pull-right" dropdown>
                                          <button type="button" class="btn btn-link" dropdown-toggle><span class="caret"></span>
                                          </button>
                                          <ul class="dropdown-menu" role="menu" aria-labelledby="single-button">
                                            <li role="menuitem" ng-if="agentid==reply.agent">
                                                <a ng-click="reply.showedit=true" class="btn btn-link pull-left full-width text-left" ng-if="reply.showedit==false">Edit</a>
                                                <a ng-click="reply.showedit=false" class="btn btn-link pull-left full-width text-left" ng-if="reply.showedit">Cancel</a>
                                            </li>
                                            <li role="menuitem"><a ng-click="deleteComment(reply.id, 'response')" class="btn btn-link pull-left full-width text-left">Delete</a></li>
                                          </ul>
                                        </div>
                                    </h6>
                                    <p  ng-cloak ng-bind-html="reply.reply" ng-if="reply.showedit==false"></p>
                                    <!-- edit comment -->
                                    <div class="full-width pull-left" ng-if="reply.showedit">
                                        <textarea class="input form-control pull-left mg-bottom full-width" ng-model="reply.reply"></textarea>
                                        <a ng-click="editComment(reply.id, reply.reply, 'response')" class="btn btn-pi pull-right"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pull-right commentreplylist tbox" ng-if="agentid!=null">
                            <div class="replybox full-width hiddenoverflow" ng-if="comment.showreply">
                                <textarea class="input form-control pull-left reply full-width mg-bottom" ng-disabled="agentid==null" ng-model="reply" placeholder="Add a response..."></textarea>
                                <a ng-click="replytocomment(reply, comment.id, comment.type, comment.marker_id)" class="btn btn-pi pull-right mg-bottom"><i class="fa fa-reply"></i> Reply</a>
                            </div>
                        </div>
                    </div>
                    <button ng-if="commentcount > 4 && commentcount > commentload" class="btn btn-block loadmore" ng-click="loadcomment();">
                    <div class="spinner pull-left" ng-if="spinner"></div> Load more</button>
                </div>
            </div>
            <div ng-if="nomap">
                <h4 class="text-center mg-top">No map found</h4>
            </div>
        </div>
    </div>
</div>
