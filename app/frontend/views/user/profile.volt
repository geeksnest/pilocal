<toaster-container ></toaster-container>
<!-- banner -->
<div class="pull-left full-width">
    <div class="container">
        <ul class="breadcrumbs h-list">
            <li><a href="/">Home</a></li>
            <li><a href="#">My Account</a></li>
            <li class="active">Edit Profile</li>
        </ul>
    </div>
</div>
<!-- banner -->
<div class="content userprofile">
    <div class="container">
        <hr>
        <div class="sidebar leftsidebar">
            <div class="panel">
                <div class="panel-header">
                    <p class="full-width text-center">{[{ agent.username }]}</p>
                </div>
                <div class="panel-content">
                    <ul class="no-list-style">
                        <li><a href="" class="no-hover">Dashboard</a></li>
                        <li><a ui-sref="account" class="no-hover">Account</a></li>
                        <li><a ui-sref="profile({ username: '{[{ username }]}' })" class="no-hover">Profile</a></li>
                        <li><a ui-sref="missionmanager" class="no-hover">Mission Manager <span class="badge badge-pi">{[{notification.pendingpin}]}</span></a></li>
                        <li><a href="" class="no-hover">Community</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="rightcontent">
            <form class="form-horizontal" name="form" id="form" ng-submit="updateagent(agent)">
                <h3>Profile</h3>
                <div class="form-container">
                    <div class="form-group hiddenoverflow">
                        <label class="col-sm-4 control-label">Profile Picture</label>
                        <div class="col-sm-6">
                          <img ngf-src="file[0]" id="profpic" src="{[{profpic}]}" ngf-accept="'image/*'">
                          <label class="label_profile_pic btn btn-pi" id="change-picture center-block" ngf-change="prepare(file)" ngf-select ng-model="file" ngf-multiple="false">Change Picture</label>
                        </div>
                  </div>

                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="inputName" class="col-lg-4 control-label">* First Name</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="firstname" type="text" name="firstname" ng-model="agent.first_name" required>
                            <span class="help-block" ng-if="form.firstname.$error.required">Your first name is required</span>
                            <span class="help-block" ng-if="form.firstname.$error.maxlength">Maximum of 100 characters only.</span>
                        </div>
                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="inputName" class="col-lg-4 control-label">* Last Name</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="lastname" type="text" name="lastname" ng-model="agent.last_name" required>
                            <span class="help-block" ng-if="form.lastname.$error.required">Your last name is required</span>
                            <span class="help-block" ng-if="form.lastname.$error.maxlength">Maximum of 100 characters only.</span>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label for="inputName" class="col-lg-4 control-label">* Birthday</label>
                        <div class="col-lg-2 no-padding" show-errors='{ showSuccess: true }' style="padding-left:15px;">
                                <select class="form-control" ng-model="agent.month" name="month" id="bdmonth"  ng-options="m.val as m.name for m in months" required>
                                </select>
                        </div>
                        <div class="col-lg-2 no-padding" show-errors='{ showSuccess: true }'>
                                <select class="form-control" ng-model="agent.day" name="day" id="bdday" ng-options="d.val as d.name for d in day" required>
                                </select>
                            </div>
                        <div class="col-lg-2 no-padding" show-errors='{ showSuccess: true }' style="padding-right:15px;">
                                <select class="form-control" ng-model="agent.year" name="year" id="bdyear" ng-options="y.val as y.name for y in year" required>
                                </select>
                            </div>
                        <span class="help-block" ng-if="form.month.$error.required || form.bdday.$error.required || form.bdyear.$error.required">Please complete your birth date.</span>
                    </div>
                    <div class="form-group" ng-class="{'has-error': agent.gendererror}" >
                        <label for="dropdown" class="col-lg-4 control-label">* Gender</label>
                        <div class="col-lg-4">
                            <label><input type="radio" name="gender" ng-model="agent.gender" value="male" >Male</label><br>
                            <label><input type="radio" name="gender" ng-model="agent.gender" value="female" >Female</label><br>
                            <label><input type="radio" name="gender" ng-model="agent.gender" value="others" >Others</label>
                            <br>
                            <span class="help-block ng-scope" ng-if="agent.gendererror">Please select your gender.</span>
                        </div>

                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="dropdown" class="col-lg-4 control-label" >* Country</label>
                        <div class="col-lg-6">
                            <select onchange="print_state('state', this.selectedIndex);" id="country" name ="country" ng-model="agent.country" class="form-control" required>
                            </select>
                            <span class="help-block" ng-if="form.country.$error.required">Please select a country.</span>
                        </div>
                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="dropdown" class="col-lg-4 control-label">* State</label>
                        <div class="col-lg-6">
                            <select name ="state" id ="state" class="form-control" ng-model="agent.state" required>
                            </select>
                            <span class="help-block" ng-if="form.state.$error.required">Please select a state.</span>
                        </div>
                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="dropdown" class="col-lg-4 control-label">My mood</label>
                        <div class="col-lg-6">
                            <textarea ng-model="agent.mood" class="form-control" id="mood" name="mood" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-4 col-lg-8">
                            <button type="submit" class="btn btn-pi pull-left" ng-disabled="form.$invalid" style="margin-right: 10px;">Save Changes</button>
                            <div class="spinner pull-left" ng-if="form-status"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        if($('#country').length){
            print_country("country");
        }        
    });
</script>