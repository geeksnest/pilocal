
<!-- banner -->
<div class="small-banner">
    <div class="container">
        <ul class="breadcrumbs h-list">
            <li><a href="/">Home</a></li>
            <li class="#/registration">Become an Agent</li>
            <li class="active">Activation</li>
        </ul>
        <h1>Activation</h1>
    </div>
</div>
<!-- banner -->

<div class="content main-body reg-success">
    <div class="container">
        <div class="alert alert-{[{ data. type }]}" role="alert">{[{ data.msg }]}</div>
    </div>
</div>