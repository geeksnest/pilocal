
<!-- banner -->
<div class="small-banner">
    <div class="container">
        <ul class="breadcrumbs h-list">
            <li><a href="/">Home</a></li>
            <li class="active">Become an Agent</li>
        </ul>
        <h1>Become an Agent</h1>
    </div>
</div>
<!-- banner -->

<div class="content main-body" ng-controller="RegistrationCtrl" id="registrationpage">
    <div class="container">
        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
            architecto beatae vitae dicta sunt explicabo.</p>

        <!-- left content -->
        <div class="left-content">
            <form class="form-horizontal" name="registrationform" id="registrationform" ng-submit="register(reg)">
                <h3>Account</h3>
                <hr>
                <div class="form-container">
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="inputName" class="col-lg-5 control-label">* Username</label>
                        <div class="col-lg-6 ">
                            <input class="form-control" id="username" ng-model="reg.username"  type="text" name="username" required="required" ng-minlength="4" ng-pattern="phoneNumberPattern" user-exists-validator>
                            <span class="help-block" ng-if="registrationform.username.$error.required">The user's name is required</span>
                            <span class="help-block" ng-if="registrationform.username.$error.minlength || registrationform.username.$error.maxlength">Username should have at least of 3-20 characters in length</span>
                            <span class="help-block" ng-if="registrationform.username.$error.pattern">Only Letters and Numbers are allowed.</span>
                            <span class="help-block" ng-if="registrationform.username.$error.exists">Sorry but the username has already been taken.</span>
                        </div>
                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="inputEmail1" class="col-lg-5 control-label">* Email Address</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="email"  type="email" name="email" ng-model="reg.email" required="required" email-exists-validator>
                            <span class="help-block" ng-if="registrationform.email.$error.required">Email is required</span>
                            <span class="help-block" ng-if="registrationform.email.$error.email">Email is invalid</span>
                            <span class="help-block" ng-if="registrationform.email.$error.exists">Sorry but that email has already been taken.</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword1" class="col-lg-offset-5 col-lg-6 control-label">* Weak passwords are not allowed</label>
                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="inputPassword1" class="col-lg-5 control-label">* Password</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="password"  type="password" ng-model="reg.password" name="password"  required="required" ng-minlength="8" ng-keyup="pstrength()" pass-strength>
                            <span class="help-block" ng-if="registrationform.password.$error.required">Please enter your password.</span>
                            <span class="help-block" ng-if="registrationform.password.$error.minlength">Minimum of 8 characters.</span>
                        </div>
                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="inputPassword1" class="col-lg-5 control-label">* Retype Password</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="retypepassword" type="password" ng-model="reg.repassword" name="retypepassword"  required="required" pw-check="password">
                              <span class="msg-error" ng-show="registrationform.retypepassword.$error.pwmatch">
                                Passwords don't match.
                              </span>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label for="dropdown" class="col-lg-5 control-label">How did you hear about us?</label>
                        <div class="col-lg-6">
                            <select class="form-control" ng-model="reg.source" name="source"  required="required">
                                <option value=""></option>
                                <option value="news">News</option>
                                <option value="tv">TV</option>
                                <option value="others">Other Things</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputName" class="col-lg-5 control-label">Referal Code (<a href="">What is this?</a>)</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="referralcode" type="text" name="referalcode" ng-model="reg.referalcode" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="dropdown" class="col-lg-5 control-label">Subscribe to Newsletter</label>
                        <div class="col-lg-6">
                            <label><input type="radio" name="newsletter" ng-model="reg.newsletter" value="1" checked>Yes</label>
                            <label><input type="radio" name="newsletter" ng-model="reg.newsletter" value="0">No</label>
                        </div>
                    </div>
                </div>

                <h3>Profile</h3>
                <hr>
                <div class="form-container">
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="inputName" class="col-lg-5 control-label">* First Name</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="firstname" type="text" name="firstname" ng-model="reg.firstname"  required="required" ng-maxlength="100">
                            <span class="help-block" ng-if="registrationform.firstname.$error.required">Your first name is required</span>
                            <span class="help-block" ng-if="registrationform.firstname.$error.maxlength">Maximum of 100 characters only.</span>
                        </div>
                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="inputName" class="col-lg-5 control-label">* Last Name</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="lastname" type="text" name="lastname" ng-model="reg.lastname"  required="required" ng-maxlength="100">
                            <span class="help-block" ng-if="registrationform.lastname.$error.required">Your last name is required</span>
                            <span class="help-block" ng-if="registrationform.lastname.$error.maxlength">Maximum of 100 characters only.</span>
                        </div>
                    </div>
                    <div class="form-group birthday">
                        <label for="inputName" class="col-lg-5 control-label">* Birthday</label>
                        <div class="col-lg-2 month" show-errors='{ showSuccess: true }'>
                                <select class="form-control" ng-model="reg.month" ng-init="reg.month = ''" name="month" id="bdmonth"  ng-options="m.val as m.name for m in months"  required="required">
                                </select>
                        </div>
                        <div class="col-lg-2 day" show-errors='{ showSuccess: true }'>
                                <select class="form-control" ng-model="reg.day" name="day" id="bdday" ng-init="reg.day=''" ng-options="d.val as d.name for d in day"  required="required">
                                </select>
                            </div>
                        <div class="col-lg-2 year" show-errors='{ showSuccess: true }'>
                                <select class="form-control" ng-model="reg.year" name="year" id="bdyear" ng-init="reg.year=''" ng-options="y.val as y.name for y in year"  required="required">
                                </select>
                            </div>
                        <span class="help-block" ng-if="registrationform.month.$error.required || registrationform.bdday.$error.required || registrationform.bdyear.$error.required">Please complete your birth date.</span>
                    </div>
                    <div class="form-group gender" ng-class="{'has-error': reg.gendererror}" >
                        <label for="dropdown" class="col-lg-5 control-label">* Gender</label>
                        <div class="col-lg-6">
                            <label><input type="radio" name="gender" ng-model="reg.gender" value="male"  required="required">Male</label>
                            <label><input type="radio" name="gender" ng-model="reg.gender" value="female"  required="required">Female</label>
                            <label><input type="radio" name="gender" ng-model="reg.gender" value="others"  required="required">Others</label>
                            <br>
                            <span class="help-block ng-scope" ng-if="reg.gendererror">Please select your gender.</span>
                        </div>

                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="dropdown" class="col-lg-5 control-label" >* Country</label>
                        <div class="col-lg-6">
                            <select onchange="print_state('state', this.selectedIndex);" id="country" name ="country" ng-model="reg.country" class="form-control"  required="required">
                            </select>
                            <span class="help-block" ng-if="registrationform.country.$error.required">Please select a country.</span>
                        </div>
                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="dropdown" class="col-lg-5 control-label">* State</label>
                        <div class="col-lg-6">
                            <select name ="state" id ="state" class="form-control" ng-model="reg.state"  required="required">
                            </select>
                            <span class="help-block" ng-if="registrationform.state.$error.required">Please select a state.</span>
                        </div>
                    </div>
                    <div class="form-group" ng-class="{'has-error ng-scope': captcharesponse}">
                        <label for="inputName" class="col-lg-5 control-label">Please click on the Captcha to be verified.</label>
                        <div class="col-lg-5">
                            <div
                                vc-recaptcha
                                theme="'light'"
                                key="model.key"
                                on-create="setWidgetId(widgetId)"
                                on-success="setResponse(response)"
                                ></div>
                        </div>

                    </div>
                    <div class="form-group" ng-class="{'has-error': reg.termserror}">
                        <div class="col-lg-offset-4 col-lg-8">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="termsconditions" ng-model="reg.terms" required="required">* I Agree Terms &amp; Conditions
                                </label>
                            </div>
                            <span class="help-block ng-scope" ng-if="reg.termserror">You have to accept the terms and agreement.</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-4 col-lg-8">
                            <button type="submit" class="btn btn-primary pull-left" ng-disabled="registrationform.$invalid||passstrength==true" style="margin-right: 10px;">Register</button>
                            <button type="reset" ng-click="reset()" class="btn btn-default pull-left">Reset</button>
                            <div class="spinner pull-left" ng-if="formStatus"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- left content -->

        <!-- right content -->
        <div class="sidebar right-content">
            <div class="panel">
                <div class="panel-header">
                    <p>List of Benefits go here</p>
                    <p>Tagline goes here</p>
                </div>
                <div class="panel-content">
                    <div>
                        <h5>Lorem ipsum dolor sit amet</h5>
                        <p>Ut enim ad minim veniam, quis nostrud exercit ation ullamco laboris nisi ut aliquip ex ea commo do consequat.</p>
                    </div>
                    <div>
                        <h5>Lorem ipsum dolor sit amet</h5>
                        <p>Ut enim ad minim veniam, quis nostrud exercit ation ullamco laboris nisi ut aliquip ex ea commo do consequat.</p>
                    </div>
                    <div>
                        <h5>Lorem ipsum dolor sit amet</h5>
                        <p>Ut enim ad minim veniam, quis nostrud exercit ation ullamco laboris nisi ut aliquip ex ea commo do consequat.</p>
                    </div>
                    <div>
                        <h5>Lorem ipsum dolor sit amet</h5>
                        <p>Ut enim ad minim veniam, quis nostrud exercit ation ullamco laboris nisi ut aliquip ex ea commo do consequat.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- right content -->

    </div>
</div>
<script>
    $(document).ready(function(){
        if($('#country').length){
            print_country("country");
        }        
    });
</script>