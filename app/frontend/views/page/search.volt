
<!-- banner -->
<div class="small-banner">
    <div class="container">
        <ul class="breadcrumbs h-list">
            <li><a href="/">Home</a></li>
            <li class="active">Search</li>
        </ul>
        <h1>Search</h1>
    </div>
</div>
<!-- banner -->

<div class="content main-body">
    <div class="container">
        
        <div class="row mg-top mg-bottom">
          <div class="col-lg-offset-3 col-lg-6">
            <div class="input-group">
              <input type="text" class="form-control" ng-model="keyword" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-default" ng-click="search(keyword)" type="button"><i class="glyphicon glyphicon-search"></i></button>
              </span>
            </div><!-- /input-group -->
          </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <hr>
        <div class="tags-container" ng-if="showtags">
            <p class="title">Popular Tags</p>
            <p class="tags mg-bottom">
              <a ng-click="selecttags(tag.tag)" ng-repeat="tag in tags" class="btn btn-link no-hover no-padding">{[{ tag.tag }]}<span ng-if="$last==false">, </span></a>
            </p>
        </div>
        <h5 ng-if="showtags==false">{[{msg}]} <a class="btn btn-pi btn-xs" ng-click="search('')">clear</a></h5>
        <div class="row mg-top">

          <div class="col-lg-4 mission-group search mg-bottom" ng-repeat="map in maps">
            <a href="/#/action-map/{[{ map.mapslugs }]}" class="no-hover">
              <div class="mission" >
                  <div ng-if="map.coverType=='image'" class="map" style="background: url('{[{ amazonurl }]}uploads/maps/{[{ map.id }]}/{[{ map.cover }]}')"></div>
                  <div ng-if="map.coverType=='video'" class="map" style="background: url('{[{ map.cover | returnYoutubeThumb }]}')">
                  </div>
                  <div class="mission-details">
                      <div ng-if="map.profile_pic_name!=null && map.hide_agent==1" style="background-image: url('{[{ amazonurl }]}uploads/agentpic/{[{ map.agent }]}/{[{ map.profile_pic_name }]}')" class="agent-pic bg-image"></div>
                      <img ng-if="map.profile_pic_name==null || map.hide_agent==0" src="/img/user.png" class="agent-pic">
                      <p class="agent-name" ng-if="map.hide_agent==1">{[{map.username}]}</p>
                      <p class="agent-name" ng-if="map.hide_agent==0"><span>Anonymous Agent</span></p>
                      <div class="mission-desc-container pull-left">
                        <p class="mission-title full-width pull-left">{[{map.title}]}</p>
                        <p class="mission-desc full-width pull-left" ng-bind-html="map.description | limitTo:215"></p>
                      </div>
                      <div class="btn-container pull-left full-width">
                          <p class="actions" > <span ng-bind="map.actions"></span> <i class="fa fa-map-marker"></i></p>
                          <div class="pull-right">
                              <a href="#"><i class="fa fa-eye fa-lg"></i> {[{ map.views }]}</a>
                              <a href="#"><i class="fa fa-comment fa-lg"></i> {[{map.countComments}]}</a>
                          </div>
                      </div>
                  </div>
              </div>
            </a>
          </div>
          <div class="col-lg-offset-3 col-lg-6">
            <div class="center-block" style="width:50%;">
              <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </div>
          </div>
          <div class="col-lg-12" ng-if="maps.length == 0">
              <h4 class="text-center">No mission maps found</h4>
          </div>

        </div>
    </div>
</div>