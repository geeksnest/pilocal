<script type="text/ng-template" id="viewPin.html">
  <div ng-include="'/fe/tpl/viewPin.html'"></div>
</script>

<script type="text/ng-template" id="addPin.html">
  <div ng-include="'/fe/tpl/addPin.html'"></div>
</script>

<script type="text/ng-template" id="editMap.html">
  <div ng-include="'/fe/tpl/editMap.html'"></div>
</script>

<script type="text/ng-template" id="deleleComment.html">
  <div ng-include="'/fe/tpl/deleleComment.html'"></div>
</script>

<script type="text/ng-template" id="editComment.html">
  <div ng-include="'/fe/tpl/editComment.html'"></div>
</script>

<script type="text/ng-template" id="mapCover.html">
  <div ng-include="'/fe/tpl/mapCover.html'"></div>
</script>

<script type="text/ng-template" id="viewNews.html">
  <div ng-include="'/fe/tpl/viewNews.html'"></div>
</script>

<script type="text/ng-template" id="viewNewsList.html">
  <div ng-include="'/fe/tpl/viewNewsList.html'"></div>
</script>

<script type="text/ng-template" id="searchbox.tpl.html">
    <input id="search-box" type="text" class="search-box" name="q" placeholder="Search Place..." />
</script>

<script type="text/ng-template" id="mediagallery.html">
  <div ng-include="'/fe/tpl/mediagallery.html'"></div>
</script>

<toaster-container ></toaster-container>
<div class="content no-margin">
    <div class="container">
        <h3 class="title" ng-cloak>{[{ mapdetails.title }]} <a href="" ng-click="editMap()" ng-if="agentid==mapdetails.agent"><icon class="fa fa-pencil"></icon></a></h3>
        <div class="pull-left full-width mg-bottom missiondate">
            <small ng-cloak>Started on {[{ mapdetails.created_at | formatdate }]}</small>
        </div>
        <!-- main content -->
        <div class="main-content">
            <div class="gmap">
                <div class="gmap">
                    <!-- Google Maps. Replace the below iframe with your Google Maps embed code -->
                    <ui-gmap-google-map center='map.center' zoom='map.zoom' events="map.events" options="options">
                        <ui-gmap-search-box template="searchbox.template" events="searchbox.events" position="searchbox.position"></ui-gmap-search-box>
                        <ui-gmap-marker ng-repeat="m in missionmarkers" coords="m.coords" idkey="m.id" options="m.options" events="m.events">
                            <ui-gmap-window show="m.show" options="m.WindowWithCustomClass.options">
                                <div>
                                    <div style="background-image: url('{[{m.agentpic}]}')" alt="" class="agent-winimg pull-left bg-image" ng-cloak></div> 
                                    <p class="full-width pull-left text-center">{[{ m.agentname }]}</p>
                                </div>
                            </ui-gmap-window>
                        </ui-gmap-marker>
                    </ui-gmap-google-map>
                </div>
            </div>
            <div class="pull-left mg-top full-width" ng-if="agentid!=undefined">
                <a ng-click="addpin()" class="btn btn-{[{btn}]} btn-lg btn-block" ng-cloak>
                    {[{btnmsg}]}
                </a>
            </div>
            <div class="pull-left full-width" ng-if="agentid==mapdetails.agent">
                <div class="pull-right">
                    <label>
                        <!--<input type="checkbox" ng-model="hide_agent" ng-change="hideagent(hide_agent)"> -->
                        Show my name as creator:
                        <select name="hide_agent" ng-model="hide_agent" ng-change="hideagent(hide_agent)">
                            <option value="true" selected="selected">Username</option>
                            <option value="false">Secret Agent</option>
                        </select>
                        Accept all pins: <input type="checkbox" ng-model="approvepins" ng-change="approvingpins(approvepins)">
                    </label>
                </div>
            </div>
            <div class="full-width pull-left">
                <p class="no-margin mg-top">
                    <small>Categories: <a href="" ng-repeat="category in categories">{[{category.cattitle}]}<span ng-if="$last!=true">, </span></a></small>
                </p>
                <p>
                    <small>Tags: <a href="" ng-repeat="tag in tags">{[{tag.tag}]}<span ng-if="$last!=true">, </span></a></small>
                </p>
            </div>
            <div class="socialmedia-btn">
                <div class="btn-container">
                    <div>
                        <!-- facebook -->
                        <div class="fb-like pull-left social-media" data-href="<?php echo $base_url; ?>/action-map/{[{mapdetails.mapslugs}]}" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                        <!-- twitter -->
                        <a href="https://twitter.com/share" class="twitter-share-button social-media pull-left">Tweet</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

                        <div class="pull-right map-report-btn" ng-cloak>
                            <div class="pull-left">
                               <i class="fa fa-eye fa-lg"></i> {[{ mapviews }]}
                            </div>
                            <div class="pull-left">
                               <i class="fa fa-comments-o fa-lg"></i> {[{ commentcount }]}
                            </div>
                            <div class="pull-left">
                                <i class="fa fa-map-marker fa-lg"></i> {[{ mapactions }]}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comment-container pull-left full-width">
                <p class="pull-left full-width"><i class="fa fa-comment-o fa-2x"></i> All Action Responses({[{ commentcount}]})
                    <span class="pull-right" ng-if="agentid==null"><a ui-sref="login">Sign in</a> or <a ui-sref="registration" >register</a> to post a comment.</span>
                </p>
                <div class="full-width">
                    <div class="commentbox hiddenoverflow" style="width: 100%">
                        <div style="float: left; width: 13%;">
                            <img class="image pull-left" src="{[{agentprofpic}]}" alt="">
                        </div>
                        <!--<textarea class="input form-control pull-left comment mg-bottom" ng-disabled="agentid==null"  placeholder="Add a comment..."></textarea>-->
                        <div style="float: left; width: 86%;">
                            <textarea ckeditor="editorOptions" name="editor" ng-model="comment" ng-disabled="agentid==null"></textarea>
                        </div>


                        <a ng-click="commentlistener(comment)" class="btn btn-pi pull-right" style="margin-top: 10px;"><i class="fa fa-commenting-o"></i> Post</a>
                    </div>
                    <hr>
                </div>
                <div class="commentlist hiddenoverflow" ng-repeat="comment in comments">
                    <img class="image pull-left bg-image" style="background-image: url('{[{comment.profile_pic_name}]}')" alt="" ng-if="comment.superagent==0">
                    <img class="image pull-left" src="/img/pilogo.png" alt="" ng-if="comment.superagent==1">
                    <div class="comment pull-left">
                        <h6 class="no-margin mg-bottom"  ng-cloak>
                            <span ng-if="comment.hide_agent != 0">{[{ comment.username }]}</span>
                            <span ng-if="comment.hide_agent == 0">Anonymous Agent</span>
                             | <small>{[{ comment.created_at | formatdatetime }]}</small>
                            <div class="btn-group pull-right" dropdown is-open="status.isopen" ng-if="comment.superagent != 1 && comment.type=='map' && (agentid==comment.agent || mapdetails.agent==agentid)">
                              <button type="button" class="btn btn-link" dropdown-toggle ng-disabled="disabled"><span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu" aria-labelledby="single-button">
                                <li role="menuitem" ng-if="agentid==comment.agent">
                                    <a ng-click="comment.showedit=true" class="btn btn-link pull-left full-width text-left" ng-if="comment.showedit==false">Edit</a>
                                    <a ng-click="comment.showedit=false" class="btn btn-link pull-left full-width text-left" ng-if="comment.showedit">Cancel</a>
                                </li>
                                <li role="menuitem"><a ng-click="deleteComment(comment.id, 'comment')" class="btn btn-link pull-left full-width text-left">Delete</a></li>
                              </ul>
                            </div>
                        </h6>
                        <p  ng-cloak ng-bind-html="comment.comment" ng-if="comment.showedit==false"></p>
                        <!-- edit comment -->
                        <div class="full-width pull-left" ng-if="comment.showedit">
                            <textarea ckeditor="editorOptions" name="editorcomment" class="input form-control pull-left full-width mg-bottom" ng-model="comment.comment">

                            </textarea>
                            <a ng-click="editComment(comment.id, comment.comment, 'comment')" class="btn btn-pi pull-right" style="margin-top: 10px"><i class="fa fa-pencil-square-o"></i> Save</a>
                        </div>

                        <ul light-slider ng-if="comment.type=='pin'"></ul>
                        <a href="" class="pull-left btn btn-link" ng-if="comment.showreply==false && agentid!=null" ng-click="showreplybox(comment.id)"><i class="fa fa-reply"></i> Reply</a>
                    </div>
                    <div class="pull-right reply-loadmore">
                        <a ng-click="loadmorereply(comment.id)" class="btn btn-link pull-right" ng-repeat="r in replycount" ng-if="r.count > r.loaded && r.id==comment.id"> load more</a>
                    </div>
                    <div class="pull-right commentreplylist" ng-repeat="reply in replies" ng-if="reply.mapcomment_id==comment.id ">
                        <div class="commentreply hiddenoverflow">
                            <div class="image pull-left bg-image" style="background-image: url('{[{reply.profile_pic_name}]}')" ng-if="reply.superagent==0"></div>
                            <img class="image pull-left" src="/img/pilogo.png" alt="" ng-if="reply.superagent==1">
                            <div class="comment pull-left">
                                <h6 class="no-margin mg-bottom"  ng-cloak>
                                    {[{ reply.username }]} | <small>{[{ reply.created_at | formatdatetime }]}</small>
                                    <div class="btn-group pull-right" dropdown ng-if="reply.superagent != 1 && (agentid==reply.agent || mapdetails.agent==agentid)">
                                      <button type="button" class="btn btn-link" dropdown-toggle><span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu" aria-labelledby="single-button">
                                        <li role="menuitem" ng-if="reply.agent==agentid">
                                            <a ng-click="reply.showedit=true" class="btn btn-link pull-left full-width text-left" ng-if="reply.showedit==false">Edit</a>
                                            <a ng-click="reply.showedit=false" class="btn btn-link pull-left full-width text-left" ng-if="reply.showedit">Cancel</a>
                                        </li>
                                        <li role="menuitem"><a ng-click="deleteComment(reply.id, 'response')" class="btn btn-link pull-left full-width text-left">Delete</a></li>
                                      </ul>
                                    </div>
                                </h6>
                                <p  ng-cloak ng-bind-html="reply.reply" ng-if="reply.showedit==false"></p>
                                <!-- edit comment -->
                                <div class="full-width pull-left" ng-if="reply.showedit" >
                                    <textarea ckeditor="editorOptions" name="editoredit" class="input form-control pull-left mg-bottom full-width" ng-model="reply.reply"></textarea>
                                    <a ng-click="editComment(reply.id, reply.reply, 'response')" class="btn btn-pi pull-right" style="margin-top: 10px;"><i class="fa fa-pencil-square-o"></i> Save</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right commentreplylist tbox" ng-if="agentid!=null">
                        <div class="replybox full-width hiddenoverflow" ng-if="comment.showreply" style="padding-right: 20px;">
                            <textarea ckeditor="editorOptions" name="editorreply" class="input form-control pull-left reply full-width mg-bottom" ng-disabled="agentid==null" ng-model="reply" placeholder="Add a response..."></textarea>
                            <a ng-click="replytocomment(reply, comment.id, comment.type, comment.marker_id)" style="margin-top: 10px" class="btn btn-pi pull-right mg-bottom"><i class="fa fa-reply"></i> Reply</a>
                        </div>
                    </div>
                </div>
                <button ng-if="commentcount > 4 && commentcount > commentload" class="btn btn-block btn-pi loadmore" ng-click="loadcomment();">
                <div class="spinner pull-left" ng-if="spinner"></div> Load more</button>
            </div>
        </div>
        <!-- main content -->

        <!-- sidebar -->
        <div class="sidebar">
            <a class="agent mg-bottom" href="#">
                <div class="agent-pic bg-image" style="background-image:url('{[{profpic}]}')"></div>
                <div class="details">
                    <p class="agent-name"  ng-cloak>{[{ mapdetails.hide_agent == 1 ? "Agent " + agent.username : "Anonymous Agent" }]}</p>
                    <p  ng-cloak>{[{ mapcount }]} Mission Maps</p>
                    <p  ng-cloak>{[{ actioncount }]} Action Pins</p>
                </div>
            </a>
            <div class="media-container mg-bottom">
                <a class="fancybox-button" fancybox href="{[{mapdetails.cover}]}" >
                    <div ng-if="mapdetails.coverType=='image'" class="bg-image" width="100%" style="background-image: url('{[{mapdetails.cover}]}'); height:150px"></div>
                </a>
                <a class="fancybox-media" fancybox-media href="https://www.youtube.com/watch?v={[{mapdetails.cover}]}">
                    <div ng-if="mapdetails.coverType=='video'" class="bg-image" width="100%"  style="background: url('http://img.youtube.com/vi/{[{mapdetails.cover}]}/hqdefault.jpg'); height:150px">
                        <img src="/img/youtubeplay.png" style="margin-left: 35%; margin-top: 18%;">
                    </div>
                </a>

                <a ng-click="changecover()" class="btn btn-pi btn-block" ng-if="agentid==mapdetails.agent">Change cover</a>
            </div>

            <div class="mission-details pull-left full-width">
                <p class="desc" ng-cloak ng-bind-html="mapdetails.description"></p>
            </div>

            <!-- mission news -->
            <!-- <div class="panel">
                <div class="panel-header">
                    <img src="/img/frontend/von.png" alt="">
                    <p>Hello Agent!</p>
                    <p class="agent-alias">Captain Planet</p>
                </div>
                <div class="panel-content">
                    <div class="list">
                        <div class="date">
                            <strong>6</strong>11
                        </div>
                        <p>&#34;Well, call me Captain Combustion, a spontaneous kinda guy.&#34;</p>
                    </div>
                    <div class="list">
                        <div class="date">
                            <strong>6</strong>11
                        </div>
                        <p>&#34;Well, call me Captain Combustion, a spontaneous kinda guy.&#34;</p>
                    </div>
                    <div class="list">
                        <div class="date">
                            <strong>6</strong>11
                        </div>
                        <p>&#34;Well, call me Captain Combustion, a spontaneous kinda guy.&#34;</p>
                    </div>
                    <a href="#" class="viewmore">view more</a>
                </div>
            </div> -->

            <div class="panel" ng-if="mapnews.length > 0">
                <div class="panel-header">
                    <p class="full-width text-center mission-announcement">Mission Announcement</p>
                </div>
                <div class="panel-content announcement">
                    <div class="list" ng-repeat="news in mapnews">
                        <a href="" ng-click="viewmapnews(news)">
                            <div class="full-width">
                                <strong>{[{ news.created_at | formatdate }]}</strong>
                            </div>
                            <p class="full-width">&#34;{[{ news.news | limitTo : 60 }]}...&#34;</p>
                            <img src="{[{ amazonlink }]}uploads/mapnews/{[{ news.mapid }]}/{[{ news.cover }]}" alt="" class="full-width" ng-if="news.coverType=='image'" />
                        </a>
                        <div class="full-width" ng-bind-html="news.cover | returnYoutubeEmbed" ng-if="news.coverType=='video'"></div>
                    </div>
                    <a href="#" class="viewmore" ng-if="mapnews.length > 0" ng-click="viewmorenews()">view more</a>
                </div>
            </div>

        </div>
        <!-- sidebar -->
    </div>
</div>
