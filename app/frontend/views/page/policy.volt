<!-- banner -->
<div class="small-banner">
    <div class="container">
        <ul class="breadcrumbs h-list">
            <li><a href="/">Home</a></li>
            <li class="active">Privacy Policy</li>
        </ul>
        <h1>Privacy Policy</h1>
    </div>
</div>
<!-- banner -->

<div class="content policy-terms">
	<div class="container">
		<p>Thank you for visiting our web site. This web site belongs to and is operated by Planet Impossible, LLC d/b/a Planet Impossible or one of its subsidiary or affiliated companies ("Planet Impossible"). Planet Impossible is committed to protecting and safeguarding your privacy on this web site. This Privacy Policy outlines the information Planet Impossible may be collecting from visitors to this web site and how we will use and protect that information. Effective as of February 2012. This Privacy Policy applies to Planet Impossible’s U.S. web site only and does not apply to third party web sites accessible via hyperlinks from this web site.</p>
		<ol>
			<li>
				Consent to this Privacy Policy
				<p>By using our web site in any way, such as browsing or interacting, you signify that you agree with the terms of our current Privacy Policy as posted here. If you do not agree with this Privacy Policy, you should not use this web site. Planet Impossible reserves the right to change or remove this Privacy Policy at our discretion, without prior notice to you. We encourage you to visit this area frequently to stay informed as your continued use of our web site following the posting of changes to these terms means that you consent to such changes. To make this policy easy to find, we make it available on our home page and at every point where personally identifiable information may be requested.</p>
			</li>
			<li>
				What Information Is Collected
				<p>We collect two types of information about visitors:</p>
				<ol type="a">
					<li>
						<p>
							Personally Identifiable Information: Personally identifiable information is any information which can be used to identify an individual, such as a name, address, telephone number, social security number, billing and shipping information, credit card information, e-mail address or other information that you voluntarily provide to us about yourself. When you visit our web site, we will not collect any personally identifiable information about you unless you provide it to us voluntarily. You can visit our web sites without telling us who you are or revealing personally identifiable information about yourself, in which case you may however be unable to participate in certain product promotions or receive product information. From time to time, Planet Impossible may offer sweepstakes, contests and surveys. Participation in these promotions is completely voluntary and the user therefore has a choice whether or not to disclose this information. In the event you wish to enter a sweepstakes, contest or survey but do not wish us to maintain your personal information for any purpose unrelated to the sweepstakes, contest or survey, you may notify us, and provide a description of the specific sweepstakes, contest or survey to: support@ Planet Impossible.com.
							Please note that we will only collect personally identifiable information revealing social security numbers, driver’s license information, racial or ethnic origin, financial information, political opinions, religious or philosophical beliefs, union membership, health information, sex life or criminal convictions with your explicit opt in consent, and only to the extent necessary for the provision of the services offered by our web site. If you apply for a US employment position via our careers center you may be asked to provide information on your gender or race where permitted by law. You should understand that, if provided, this information will only be used in accordance with applicable law. Providing this information is strictly voluntary and you will not be subject to any adverse action or treatment if you choose not to provide this information.
						</p>
					</li>
					<li>
						<p>
							Aggregate and Statistical Data: Planet Impossible may collect certain non-personally identifiable aggregate data called web log information (such as your web browser, operating system, pages visited, etc.) and use cookies or web beacons (see definitions below) when you visit some of our web pages. For instance, when you visit one of our web sites, our webserver will automatically recognize some non-personal information, including but not limited to the date and time you visited our site, the pages you visited, the referring website you came from, the type of browser you are using, the type of operating system you are using and the domain name and address of your internet server. We also may collect your Internet Protocol (IP) address. An IP address is a number that is assigned to your computer when you use the Internet. The IP address data that we collect does not contain any personally identifiable information about you and is used to administer our site, to determine the numbers of different visitors to the site and to gather broad demographic data. We do not use cookies to retrieve information that is unrelated to your visit to or your interaction with this web site.
							In some cases we may also collect information that you voluntarily submit, such as general statistical information (e.g., age, gender, household size, zip/postal code, preferences or interests).
						</p>
					</li>
				</ol>
			</li>
			<li>
				How Information Is Collected
				<p>
					Personally Identifiable Information: Planet Impossible collects personally identifiable information from our web site visitors only on a voluntary basis. Such information may be collected from you if you choose to participate in the following via our web site: web site registrations, contests, sweepstakes, questionnaires, surveys, consumer service contacts, employment inquiries, product purchases, or other similar activities requiring the submission of personally identifiable information. We may enhance or merge the information that you provide with information about you obtained from third parties for the same purpose.
					Aggregate and Statistical Data: Cookies and web beacons are used to collect non-personal information automatically from visitors to our site. A "cookie" is a small piece of data that can be sent by a webserver to your computer, which then may be stored by your browser on your computer´s hard drive. Cookies help us in many ways to make your visit to our web site more convenient and meaningful to you. For example, cookies allow us to tailor a web site or advertisement to better match your interests and preferences, or to save you the trouble of re-entering certain information in some registration areas. Web beacons are electronic files on our web site that allow us to count users who have visited that page or to access certain cookies.
					Most Internet browsers enable you to erase cookies from your computer hard drive, block all cookies, or receive a warning before a cookie is stored. Please refer to your browser instructions or help screen to utilize or learn more about these functions. If you disable cookies, however, you may not be able to use certain personalized functions of this web site.
					Statistical non-personally identifying information (e.g., gender, zip code, etc.) is only collected if you voluntarily submit it to us.
				</p>
			</li>
			<li>
				Use of Information
				<p>
					Personally Identifiable Information: We collect, maintain and use personally identifiable information you have voluntarily submitted to our web site for the following purposes:<br>
					-helping to establish and verify the identity of users;<br>
					-opening, maintaining, administering and servicing users profiles, accounts or memberships;<br>
                    -processing, servicing or enforcing transactions and sending related communications;<br>
					-providing services and support to users;<br>
					-providing you with information about employment opportunities, administering the application process and considering you for employment if you apply for a job or an internship;<br>
					-to conduct sweepstakes, surveys and contests, and to provide the results thereof;<br>
					-improving the web site, including tailoring it to users´ preferences;<br>
					-providing users with product or service updates, promotional notices and offers, and other information about Planet Impossible;
					-responding to your questions, inquiries, comments and instructions; and<br>
					-maintaining the security and integrity of our..
				</p>
			</li>
		</ol>
	</div>
</div>