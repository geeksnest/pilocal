<script type="text/ng-template" id="otherCategories.html">
    <div ng-include="'/fe/tpl/otherCategories.html'"></div>
</script>
<script type="text/ng-template" id="sidebarCategory.html">
    <div ng-include="'/fe/tpl/sidebarCategories.html'"></div>
</script>
<script type="text/ng-template" id="sidebarNews.html">
    <div ng-include="'/fe/tpl/sidebarNews.html'"></div>
</script>
<script type="text/ng-template" id="viewNews.html">
  <div ng-include="'/fe/tpl/viewNews.html'"></div>
</script>
<script type="text/ng-template" id="viewNewsList.html">
  <div ng-include="'/fe/tpl/viewNewsList.html'"></div>
</script>

<!-- banner -->
    <div class="banner">
        <div class="container">
            <p class="text-center"><strong>One</strong> person can help <strong>everyone</strong></p>
            <h1 class="text-center">Where&#39s your next mission?</h1>
            <button  ui-sref="mapstart" class="banner-btn center-element">Map out your mission</button>
        </div>
    </div>
    <!-- banner -->

    <!-- content -->
    <div class="content" ng-controller="IndexCtrl">
        <div class="container">
             <!-- main content -->
            <div class="main-content">

                <!-- featured missions -->
                <div class="mission-group">
                    <div class="content-header">
                        <p><img src="/img/frontend/featuredicon.png" alt=""> <strong>Featured</strong> Missions</p>
                    </div>
                    <div class="missions-container">
                        <div class="two-column left">
                            <div class="mission">
                                <a href="/action-map/{[{ mainfeature.mapslugs }]}" class="no-hover">
                                <div class="map" style="background: url('{[{ bucketUrl }]}uploads/maps/{[{ mainfeature.mapid }]}/{[{ mainfeature.cover }]}')" ng-if="mainfeature.coverType=='image'"></div>
                                <div class="map" style="background: url('{[{ mainfeature.cover | returnYoutubeThumb }]}')" ng-if="mainfeature.coverType=='video'"></div>
                                <div class="mission-details">
                                    <div ng-if="mainfeature.profile_pic_name && mainfeature.hide_agent=='1'" style="background-image: url('{[{ bucketUrl }]}uploads/agentpic/{[{ mainfeature.agentid }]}/{[{ mainfeature.profile_pic_name }]}')" class="agent-pic bg-image"></div>
                                    <img ng-if="!mainfeature.profile_pic_name || mainfeature.hide_agent=='0'" src='/img/user.png' class="agent-pic">
                                    <p ng-if="mainfeature.hide_agent==1" class="agent-name"><span ng-bind="mainfeature.first_name"></span> <span ng-bind="mainfeature.last_name"></span></p>
                                    <p ng-if="mainfeature.hide_agent==0" class="agent-name"><span>Anonymous Agent</span></p>
                                    <p class="mission-desc"><span ng-bind="mainfeature.title"></span></p>
                                    <div class="btn-container">
                                        <p class="actions"><span ng-bind="mainfeature.actions"></span> <i class="fa fa-map-marker"></i></p>
                                        <div class="pull-right">
                                            <a href="#" class=""><i class="fa fa-eye"></i> {[{ mainfeature.views }]}</a>
                                            <a href="#" class=""><i class="fa fa-comment"></i> {[{ addComments(mainfeature.countComments, mainfeature.countRepComments)}]}</a>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="two-column right">
                            <div class="mission" ng-repeat="ff in featured">
                                <a href="/action-map/{[{ ff.mapslugs }]}" class="no-hover">
                                    <!-- background-image: url('http://img.youtube.com/vi/NIPXpNBJ4G0/hqdefault.jpg ')-->
                                    <div ng-if="ff.coverType=='image'" class="map" style="background: url('{[{ bucketUrl }]}uploads/maps/{[{ ff.mapid }]}/{[{ ff.cover }]}')">
                                    </div>
                                    <div ng-if="ff.coverType=='video'" class="map" style="background: url('{[{ ff.cover | returnYoutubeThumb }]}')">
                                    </div>
                                    <div ng-if="ff.coverType!='image' && ff.coverType!='video'" class="map" >
                                    </div>
                                    <div class="mission-details">
                                        <p class="mission-desc"><span ng-bind="ff.title | limitTo:17"></span></p>
                                        <p ng-if="ff.hide_agent==1" class="agent-name"><span ng-bind="ff.username"></span></p>
                                        <p ng-if="ff.hide_agent==0" class="agent-name"><span>Anonymous Agent</span></p>
                                        <div class="btn-container">
                                            <a href="/action-map/{[{ ff.mapslugs }]}" class="pull-left no-margin">
                                                <p class="actions pull-left"><span ng-bind="ff.actions"></span> <i class="fa fa-map-marker"></i></p>
                                            </a>
                                            <a href="#" class=""><i class="fa fa-eye"></i> {[{ ff.views }]}</a>
                                            <a href="#" class=""><i class="fa fa-comment"></i> {[{ addComments(ff.countComments, ff.countRepComments)}]}</a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="pull-left full-width">
                        <a href="" class="seemore" ng-click="seeMore('FEATURED')">see more <i class="fa fa-play"></i></a>
                    </div>
                </div>
                <!-- featured missions -->


                <!-- latest missions -->
                <div class="mission-group">
                    <div class="content-header">
                        <p><img src="/img/frontend/latesticon.png" alt=""> <strong>Latest</strong> Missions</p>
                    </div>
                    <div class="missions-container">
                        <div class="three-column" ng-repeat="ll in latest">
                            <a href="/action-map/{[{ ll.mapslugs }]}" class="no-hover">
                            <div class="mission" >
                                <div ng-if="ll.coverType=='image'" class="map" style="background: url('{[{ bucketUrl }]}uploads/maps/{[{ ll.mapid }]}/{[{ ll.cover }]}')">
                                </div>
                                <div ng-if="ll.coverType=='video'" class="map" style="background: url('{[{ ll.cover | returnYoutubeThumb }]}')">
                                </div>
                                <div ng-if="ll.coverType!='image' && ll.coverType!='video'" class="map" >
                                </div>
                                <div class="mission-details">
                                    <div ng-if="ll.profile_pic_name && ll.hide_agent=='1'" style=" background-image: url('{[{ bucketUrl }]}uploads/agentpic/{[{ ll.agentid }]}/{[{ ll.profile_pic_name }]}')" class="agent-pic bg-image"></div>
                                    <img ng-if="!ll.profile_pic_name || ll.hide_agent=='0'" src='/img/user.png' class="agent-pic">
                                    <p ng-if="ll.hide_agent==1" class="agent-name"><span ng-bind="ll.username"></span></p>
                                    <p ng-if="ll.hide_agent==0" class="agent-name"><span>Anonymous Agent</span></p>
                                    <p class="mission-desc" ng-bind="ll.title | limitTo:31"></p>
                                    <div class="btn-container">
                                        <p class="actions" > <span ng-bind="ll.actions"></span> <i class="fa fa-map-marker"></i></p>
                                        <div class="pull-right">
                                            <a href="#" class=""><i class="fa fa-eye"></i> {[{ ll.views }]}</a>
                                            <a href="#" class=""><i class="fa fa-comment"></i> {[{ addComments(ll.countComments, ll.countRepComments)}]}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                </a>
                        </div>
                    </div>
                    <div class="pull-left full-width">
                        <a href="" class="seemore" ng-click="seeMore('LATEST')">see more <i class="fa fa-play"></i></a>
                    </div>
                </div>
                <!-- latest missions -->
                
                <!-- greatest missions -->
                <div class="mission-group">
                    <div class="content-header">
                        <p><img src="/img/frontend/greatesticon.png" alt=""> <strong>Greatest</strong> Missions</p>
                    </div>
                    <div class="missions-container">
                        <div class="three-column" ng-repeat="gg in greatest">
                            <a href="/action-map/{[{ gg.mapslugs }]}" class="no-hover">
                                <div class="mission" >
                                    <div ng-if="gg.coverType=='image'" class="map" style="background: url('{[{ bucketUrl }]}uploads/maps/{[{ gg.mapid }]}/{[{ gg.cover }]}')">
                                    </div>
                                    <div ng-if="gg.coverType=='video'" class="map" style="background: url('{[{ gg.cover | returnYoutubeThumb }]}')">
                                    </div>
                                    <div ng-if="gg.coverType!='image' && gg.coverType!='video'" class="map" >
                                    </div>
                                    <div class="mission-details">
                                        <div ng-if="gg.profile_pic_name && gg.hide_agent=='1'" style="background-image: url('{[{ bucketUrl }]}uploads/agentpic/{[{ gg.agentid }]}/{[{ gg.profile_pic_name }]}')" class="agent-pic bg-image"></div>
                                        <img ng-if="!gg.profile_pic_name || gg.hide_agent=='0'" src='/img/user.png' class="agent-pic">
                                        <p ng-if="gg.hide_agent==1" class="agent-name"><span ng-bind="gg.username"></span></p>
                                        <p ng-if="gg.hide_agent==0" class="agent-name"><span>Anonymous Agent</span></p>
                                        <p class="mission-desc" ng-bind="gg.title | limitTo:31"></p>
                                        <div class="btn-container">
                                            <p class="actions" > <span ng-bind="gg.actions"></span> <i class="fa fa-map-marker"></i></p>
                                            <div class="pull-right">
                                                <a href="#" class=""><i class="fa fa-eye"></i> {[{ gg.views }]}</a>
                                                <a href="#" class=""><i class="fa fa-comment"></i> {[{ addComments(gg.countComments, gg.countRepComments)}]}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="pull-left full-width">
                        <a href="" class="seemore" ng-click="seeMore('GREATEST')">see more <i class="fa fa-play"></i></a>
                    </div>
                </div>
                <!-- greatest missions -->

                <!-- popular missions -->
                <div class="mission-group">
                    <div class="content-header">
                        <p><img src="/img/frontend/popularicon.png" alt=""> <strong>Popular</strong> Missions</p>
                    </div>
                    <div class="missions-container">
                        <div class="three-column" ng-repeat="pp in popular">
                            <a href="/action-map/{[{ pp.mapslugs }]}" class="no-hover">
                                <div class="mission" >
                                    <div ng-if="pp.coverType=='image'" class="map" style="background: url('{[{ bucketUrl }]}uploads/maps/{[{ pp.mapid }]}/{[{ pp.cover }]}')">
                                    </div>
                                    <div ng-if="pp.coverType=='video'" class="map" style="background: url('{[{ pp.cover | returnYoutubeThumb }]}')">
                                    </div>
                                    <div ng-if="pp.coverType!='image' && pp.coverType!='video'" class="map" >
                                    </div>
                                    <div class="mission-details">
                                        <div ng-if="pp.profile_pic_name && pp.hide_agent=='1'" style="background-image: url('{[{ bucketUrl }]}uploads/agentpic/{[{ pp.agentid }]}/{[{ pp.profile_pic_name }]}')" class="agent-pic bg-image"></div>
                                        <img ng-if="!pp.profile_pic_name || pp.hide_agent=='0'" src='/img/user.png' class="agent-pic">
                                        <p ng-if="pp.hide_agent==1" class="agent-name"><span ng-bind="pp.username"></span> </p>
                                        <p ng-if="pp.hide_agent==0" class="agent-name"><span>Anonymous Agent</span></p>
                                        <p class="mission-desc" ng-bind="pp.title | limitTo:31"></p>
                                        <div class="btn-container">
                                            <p class="actions" > <span ng-bind="pp.actions"></span> <i class="fa fa-map-marker"></i></p>
                                            <div class="pull-right">
                                                <a href="#" class=""><i class="fa fa-eye"></i> {[{ pp.views }]}</a>
                                                <a href="#" class=""><i class="fa fa-comment"></i> {[{ addComments(pp.countComments, pp.countRepComments)}]}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="pull-left full-width">
                        <a href="" class="seemore" ng-click="seeMore('POPULAR')">see more <i class="fa fa-play"></i></a>
                    </div>
                </div>
                <!-- popular missions -->

                <!-- recommended missions
                <div class="mission-group">
                    <div class="content-header">
                        <p><img src="/img/frontend/recommendedicon.png" alt=""> <strong>Recommended</strong> Missions</p>
                    </div>
                    <div class="missions-container">
                        <div class="three-column">
                            <div class="mission">
                                <div class="map">
                                </div>
                                <div class="mission-details">
                                    <img src="/img/frontend/von.png" alt="" class="agent-pic">
                                    <p class="agent-name">John Jeevon Ang</p>
                                    <p class="mission-desc">SHOW AND THROW A BAG</p>
                                    <div class="btn-container">
                                        <p class="actions">12, 090 Actions</p>
                                        <div class="pull-right">
                                            <a href="#"><i class="fa fa-heart fa-lg"></i></a>
                                            <a href="#"><i class="fa fa-share fa-lg"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="three-column">
                            <div class="mission">
                                <div class="map">
                                </div>
                                <div class="mission-details">
                                    <img src="/img/frontend/von.png" alt="" class="agent-pic">
                                    <p class="agent-name">John Jeevon Ang</p>
                                    <p class="mission-desc">SHOW AND THROW A BAG</p>
                                    <div class="btn-container">
                                        <p class="actions">12, 090 Actions</p>
                                        <div class="pull-right">
                                            <a href="#"><i class="fa fa-heart fa-lg"></i></a>
                                            <a href="#"><i class="fa fa-share fa-lg"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="three-column">
                            <div class="mission">
                                <div class="map">
                                </div>
                                <div class="mission-details">
                                    <img src="/img/frontend/von.png" alt="" class="agent-pic">
                                    <p class="agent-name">John Jeevon Ang</p>
                                    <p class="mission-desc">SHOW AND THROW A BAG</p>
                                    <div class="btn-container">
                                        <p class="actions">12, 090 Actions</p>
                                        <div class="pull-right">
                                            <a href="#"><i class="fa fa-heart fa-lg"></i></a>
                                            <a href="#"><i class="fa fa-share fa-lg"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="seemore">see more <i class="fa fa-play"></i></a>
                </div>
                <!-- recommended missions -->
            </div>   
            <!-- main content -->

            <!-- sidebar -->
            <div class="sidebar">
                <div class="panel" ng-include="'sidebarNews.html'">
                </div>
                <div class="panel" ng-include="'sidebarCategory.html'">
                </div>
                <a ui-sref="contactus">
                    <img src="/img/frontend/contactus.png" alt="">
                </a>
            </div>
            <!-- sidebar -->
        </div>
    </div>
    <!-- content -->