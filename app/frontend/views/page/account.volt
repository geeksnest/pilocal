<toaster-container ></toaster-container>

<script type="text/ng-template" id="editAcct.html">
  <div ng-include="'/fe/tpl/editAcct.html'"></div>
</script>

<script type="text/ng-template" id="changePass.html">
  <div ng-include="'/fe/tpl/changePass.html'"></div>
</script>

<script type="text/ng-template" id="agentSidebar.html">
  <div ng-include="'/fe/tpl/agentSidebar.html'"></div>
</script>

<!-- banner -->
<div class="pull-left full-width">
    <div class="container">
        <ul class="breadcrumbs h-list">
            <li><a href="/">Home</a></li>
            <li><a href="#">My Account</a></li>
            <li class="active">Account</li>
        </ul>
    </div>
</div>
<!-- banner -->
<div class="content userprofile">
    <div class="container">
        <hr>
        <div class="sidebar leftsidebar">
            <div class="panel" ng-include="'agentSidebar.html'">
            </div>
        </div>

        <div class="rightcontent">
            <div class="form-horizontal" name="form" id="form" ng-submit="updateagent(agent)">
                <h3>Account</h3>
                <div class="form-container">
                    
                    <div class="form-group">
                        <label for="inputName" class="col-lg-5 control-label">Username</label>
                        <div class="col-lg-6 ">
                            <span editable-text="agent.username" e-form="textBtnForm" onaftersave="update($data, 'username')" oncancel="cancel()">
                                {[{ agent.username }]}
                            </span>
                            <button class="btn btn-pi btn-xs pull-right" ng-click="textBtnForm.$show()" ng-hide="textBtnForm.$visible">
                                change
                            </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">Email Address</label>
                        <div class="col-lg-6">
                            <span editable-text="agent.email" e-form="textBtnForm2" onbeforesave="update($data, 'email')">
                                {[{ agent.email }]}
                            </span>
                            <button class="btn btn-pi btn-xs pull-right" ng-click="textBtnForm2.$show()" ng-hide="textBtnForm2.$visible">
                                change
                            </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword1" class="col-lg-5 control-label">Password</label>
                        <div class="col-lg-6">
                            <button class="btn btn-pi btn-sm" ng-click="changepass()"> Change password</button>
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label for="inputName" class="col-lg-5 control-label">Referal Code (<a href="">What is this?</a>)</label>
                        <div class="col-lg-6">
                            <input class="form-control" id="referralcode" type="text" name="referalcode" ng-model="reg.referalcode" readonly="readonly">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>