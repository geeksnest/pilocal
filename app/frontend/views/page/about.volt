<script type="text/ng-template" id="searchbox.tpl.html">
	<input id="search-box" type="text" class="search-box" name="q" placeholder="Search Place..." />
</script>
<div class="content create-map-page about-pi">
	<div class="container">
		<h3 class="title">WHAT IS PLANET IMPOSSIBLE?</h3>
		<p>Planet impossible is Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus impedit deserunt debitis veniam quibusdam, praesentium soluta harum. Ducimus, inventore incidunt, consequatur dolorem magnam aperiam labore modi iusto omnis maiores itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

		<div class="video-container">
			<iframe width="560" height="315" src="https://www.youtube.com/embed/gOW_azQbOjw" class="center-block" frameborder="0" allowfullscreen></iframe>
		</div>

		<h3 class="title">YOUR MISSION</h3>
		<p>Planet impossible has successfully Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus impedit deserunt debitis veniam quibusdam, praesentium soluta harum. Ducimus, inventore incidunt, consequatur dolorem magnam aperiam labore modi iusto omnis maiores itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

		<div class="row about-maps">
			<div class="col-lg-4">
				<h4 class="text-center">ACTIONS</h4>
				<h2 class="text-center">{[{total.markers[0].markercount}]}</h2>
			</div>
			<div class="col-lg-4">
				<h4 class="text-center">AGENTS</h4>
				<h2 class="text-center">{[{total.agents[0].agentcount}]}</h2>
			</div>
			<div class="col-lg-4">
				<h4 class="text-center">MISSIONS/IDEAS</h4>
				<h2 class="text-center">{[{total.maps[0].mapscount}]}</h2>
			</div>
		</div>
		<div class="gmap">
		<div class="gmap">
			<!-- Google Maps. Replace the below iframe with your Google Maps embed code -->
			<ui-gmap-google-map center='map.center' zoom='map.zoom' events="map.events" options="options">
				<ui-gmap-search-box template="searchbox.template" events="searchbox.events" position="searchbox.position"></ui-gmap-search-box>
				<ui-gmap-marker ng-repeat="m in missionmarkers" coords="m.coords" idkey="m.id" options="m.options" events="m.events">
				</ui-gmap-marker>
			</ui-gmap-google-map>
		</div>
			</div>

		<h3 class="title">WHO IS AGENT PI?</h3>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus impedit deserunt debitis veniam quibusdam, praesentium soluta harum. Ducimus, inventore incidunt, consequatur dolorem magnam aperiam labore modi iusto omnis maiores itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus impedit deserunt debitis veniam quibusdam, praesentium soluta harum. Ducimus, inventore incidunt, consequatur dolorem magnam aperiam labore modi iusto omnis maiores itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	</div>
</div>