<!-- banner -->
<div class="small-banner">
    <div class="container">
        <ul class="breadcrumbs h-list">
            <li><a href="/">Home</a></li>
            <li class="active">Terms of Service</li>
        </ul>
        <h1>Terms of Service</h1>
    </div>
</div>
<!-- banner -->

<div class="content policy-terms">
	<div class="container">
		<p>
			You must read and agree to our Subscriber Agreement to become a subscriber to Planet Impossible.com
			This Subscriber Agreement and terms of use govern your use of Planet Impossible.com, and unless other terms and conditions expressly govern, any other content services from Planet Impossible LLC (doing business as Planet Impossible.com) that may be made available from time to time (each, a "service"). If you agree to be bound by the terms of this Agreement, you should click on the "I agree" button at the end of this Agreement. If you do not agree to be bound by the terms of this Agreement, you should click "I disagree." If you click "I disagree," you will not be able to proceed with the registration process for the respective service and become a Subscriber. If you are reading a paper copy of this Agreement, please check the appropriate box on the associated form. To the extent you have access to, or are using, a service without having completed our registration process or clicked on an "I agree" button, you are hereby notified that your continued use of a service is subject to many of the terms and conditions of this Agreement as explained in section 5 below.
		</p>

		<ol>
			<li>
				<a href="#">Changes to Subscriber Agreement.</a>
			</li>
			<li>
				<a href="#">Privacy and your account.</a>
			</li>
			<li>
				<a href="#">Fees and payments.</a>
			</li>
			<li>
				<a href="#">Renewal.</a>
			</li>
			<li>
				<a href="#">Limitations on use.</a>
			</li>
			<li>
				<a href="#">Community; user-generated content.</a>
			</li>
			<li>
				<a href="#">Disclaimers of warranties and limitations on liability.</a>
			</li>
			<li>
				<a href="#">General.</a>
			<li>
				<a href="#">Cancellation.</a>
			</li>
		</ol>

		<ol class="terms">
			<li>
				<p>
					Changes to Subscriber Agreement. We may change the terms of this Agreement at any time by notifying you of the change in writing or electronically (including without limitation, by E-mail or by posting a notice on the service that the terms have been "updated"). The changes also will appear in this document, which you can access at any time by going to the Terms and Conditions link at the bottom of every page. You signify that you agree to be bound by such changes by using a service after changes are made to this Agreement.
				</p>
			</li>
			<li>
				<p>
					Privacy and your account. Registration data and other information about you are subject to our privacy policy: "The information provided to Planet Impossible.com to process your subscription is strictly confidential and will never be shared with any advertiser or third-party. It will be used solely to deliver to you the content requested." If you access a service using a password, you are solely responsible for maintaining the confidentiality of that password. If you provide someone else with access to your password to a service, they will have the ability to view information about your account and make changes through the Web site for the service. Similarly, if you tell someone the answer to your security question for a service, they will be able to request information about your account and make changes through customer service. You agree to notify us promptly if you change your address or E-mail so we can continue to contact you and send any notices required hereunder. If you fail to notify us promptly of a change, then any notice we send to your old address or E-mail shall be deemed sufficient notice.
				</p>
			</li>
			<li>
				<p>
					Fees and payments. You agree to pay the subscription fees and any other charges incurred in connection with your user name and password for a service (including any applicable taxes) at the rates in effect when the charges were incurred. If your subscription includes access to areas containing Premium content or services, your access to such areas may be subject to additional fees, terms and conditions, which will be separately disclosed in such areas. We will bill all charges automatically to your credit card. Subscription fees will be billed at the beginning of your subscription or any renewal. Unless we state in writing otherwise, all fees and charges are nonrefundable. We may change the fees and charges then in effect, or add new fees or charges, by giving you notice in advance. If you want to use a different credit card or there is a change in credit card validity or expiration date, or if you believe someone has accessed a service using your user name and password without your authorization, you must contact Planet Impossible.com to alert us to the problem. You are responsible for any fees or charges incurred to access a service through an Internet access provider or other third-party service.
				</p>
			</li>
			<li>
				<p>
					Renewal. Your subscription will renew automatically, unless we terminate it or you notify us by telephone, mail, or E-mail (receipt of which must be confirmed by E-mail reply from us) of your decision to terminate your subscription. For annual subscriptions, we will notify you of the pending renewal of your subscription at least 30 days prior to the date your subscription renews, except as otherwise required by law. You must cancel your subscription before it renews to avoid billing of subscription fees for the renewal term to your credit card. 
				</p>
			</li>
			<li>
				<p>
					Limitations on use.
				</p>
				<ol type="a">
					<li>
						Only one individual may access a service at the same time using the same user name or password, unless we agree otherwise.
					</li>
					<li>
						The text, graphics, images, video, metadata, design, organization, compilation, look and feel, advertising and all other protectable intellectual property (the "content") available through the services are our property or the property of our advertisers and licensors and are protected by copyright and other intellectual property laws. Unless you have our written consent, you may not sell, publish, distribute, retransmit or otherwise provide access to the content received through the services to anyone, including, if applicable, your fellow students or employees, with the following exceptions:
							<ol type="i">
								<li>
									You may occasionally distribute a copy of an article, or a portion of an article, from a service in non-electronic form to a few individuals without charge, provided you include all copyright and other proprietary rights notices in the same form in which the notices appear in the service, original source attribution, and the phrase "used with permission from Planet Impossible.com.".
								</li>
								<li>
									You may occasionally use our "forward" service to E-mail an article from our Thursday report or Monthly Reports to a few individuals, without charge. You are not permitted to use this service for the purpose of regularly providing other users with access to content from a service.
								</li>
								<li>
									While you may download, store and create an archive of articles from the service for your personal use, you may not otherwise provide access to such an archive to more than a few individuals on an occasional basis. The foregoing does not apply to any sharing functionality we provide through the service that expressly allows you to share articles or links to articles with others. In addition, you may not use such an archive to develop or operate an automated trading system or for data or text mining.
								</li>
							</ol>
					</li>
					<li>
						You agree not to rearrange or modify the content. You agree not to create abstracts from, scrape or display our content for use on another Web site or service (other than headlines from our RSS feed with active links back to the full article on the service). You agree not to post any content from the services (other than headlines from our RSS feed with active links back to the full article on the service) to Weblogs, newsgroups, mail lists or electronic bulletin boards, without our written consent.
					</li>
					<li>
						You agree not to use the services for any unlawful purpose. We reserve the right to terminate or restrict your access to a service if, in our opinion, your use of the service may violate any laws, regulations or rulings, infringe upon another person's rights or violate the terms of this Agreement. Also, we may refuse to grant you a user name that impersonates someone else, is protected by trademark or other proprietary right law, or is vulgar or otherwise offensive. 
					</li>
				</ol>
			</li>
			<li>
				<p>
					Community; user-generated content.
				</p>
				<ol type="a">
					<li>
						User name. We require you to register to have access to our community area. It is your responsibility to choose your password wisely. If you have concerns or believe that someone is using your password without your authority, please contact customer service. We reserve the right to disclose any information about you, including registration data, in order to comply with any applicable laws and/or requests under legal process, to operate our systems properly, to protect our property or rights, and to safeguard the interests of others.
					</li>
					<li>
						User-generated content.
						<ol type="i">
							<li>
								User content. We offer you the opportunity to comment on and engage in discussions regarding articles, companies and various topics. Any content, information, graphics, audio, images, and links you submit as part of creating your profile or in connection with any of the foregoing activities is referred to as "user content" in this Agreement and is subject to various terms and conditions as set forth below.
							</li>
							<li>
								Cautions regarding other users and user content. You understand and agree that user content includes information, views, opinions, and recommendations of many individuals and organizations and is designed to help you gather the information you need to help you make your own decisions. Importantly, you are responsible for your own decisions and for properly analyzing and verifying any information you intend to rely upon. We do not endorse any recommendation or opinion made by any user. We reserve the right to monitor or remove any user content from the services at any time without notice. You should also be aware that other users may use our services for personal gain. As a result, please approach messages with appropriate skepticism. User content may be misleading, deceptive, or in error. 
							</li>
							<li>
								Grant of rights and representations by you. If you upload, post or submit any user content on a service, you represent to us that you have all the necessary legal rights to upload, post or submit such user content and that it will not violate any law or the rights of any person. You agree that upon uploading, posting or submitting information on the services, you grant Planet Impossible.com, and our respective affiliates and successors a non-exclusive, transferable, worldwide, fully paid-up, royalty-free, perpetual, irrevocable right and license to use, distribute, publicly perform, display, reproduce, and create derivative works from your user content in any and all media, in any manner, in whole or part, without any duty to compensate you. You also grant us the right to authorize the use of user content, or any portion thereof, by users and other users in accordance with the Terms and Conditions of this Agreement, including the rights to feature your user content specifically on the services and to allow other users to request access to your user content, such as for example through an RSS feed.
							</li>
							<li>
								We may also remove any user content for any reason and without notice to you. This includes all materials related to your use of the services or membership, including E-mail accounts, postings, profiles or other personalized information you have created while on the services. v. Rules of conduct. Users must not post material that is offensive, contains personal attacks, is misleading or demonstrably false or is solely (or overwhelmingly) self-promotional.
							</li>
						</ol>
					</li>
				</ol>
			</li>
			<li>
				Disclaimers of warranties and limitations on liability. You agree that your access to, and use of, the services and the content available through the services is on an "as-is", "as available" basis and we specifically disclaim any representations or warranties, express or implied, including, without limitation, any representations or warranties of merchantability or fitness for a particular purpose. Planet Impossible.com and its subsidiaries, affiliates, shareholders, directors, officers, employees and licensors will not be liable (jointly or severally) to you or any other person as a result of your access or use of the services for indirect, consequential, special, incidental, punitive, or exemplary damages, including, without limitation, lost profits and lost revenues (collectively, the "excluded damages"), whether or not characterized in negligence, tort, contract, or other theory of liability, even if any of the Planet Impossible.com parties have been advised of the possibility of or could have foreseen any of the excluded damages, and irrespective of any failure of an essential purpose of a limited remedy.
			</li>
			<li>
				General. This Agreement contains the final and entire Agreement between us regarding your use of the services and supersedes all previous and contemporaneous oral or written Agreements regarding your use of the services. We may discontinue or change the services, or their availability to you, at any time. This Agreement is personal to you, which means that you may not assign your rights or obligations under this Agreement to anyone. No third party is a beneficiary of this Agreement. You agree that this Agreement, as well as any and all claims arising from this Agreement will be governed by and construed in accordance with the laws of the State of Arizona, United States of America applicable to contracts made entirely within Arizona and wholly performed in Arizona, without regard to any conflict or choice of law principles. The sole jurisdiction and venue for any litigation arising out of this Agreement will be an appropriate federal or state court located in Arizona. This Agreement will not be governed by the United Nations convention on contracts for the international sale of goods.
			</li>
			<li>
				Cancellation. When a purchase is made, Planet Impossible.com will send you an E-mail link to verify your E-mail address. By verifying the address, you are agreeing to the subscription terms. If you subsequently change E-mail addresses, you acknowledge that it is your responsibility to change your E-mail address at Planet Impossible.com. If you stop receiving the content, you must contact Planet Impossible.com to investigate. Your also pledge to do everything in your power to make sure that the E-mails can get through your company's E-mail filters. You also agree that you are voluntarily choosing to purchase this content and that Planet Impossible.com makes no promises that you will like the content or agree with the content. If you choose to cancel your subscription, you must do so in writing and you understand that no refunds will be granted for any content you have received. If you cancel a subscription before it's completed, you understand that you will only be refunded for the unused portion of the subscription, which will be billed based on how you actually subscribed, since Planet Impossible.com subscription months are more heavily discounted if the Subscriber commits to a longer subscription.
			</li>
		</ol>
	</div>
</div>