<toaster-container ></toaster-container>

<!-- banner -->
<div class="small-banner">
    <div class="container">
        <ul class="breadcrumbs h-list">
            <li><a href="/">Home</a></li>
            <li class="active">Contact Us</li>
        </ul>
        <h1>Contact Us</h1>
    </div>
</div>
<!-- banner -->

<div class="content main-body">
    <div class="container">
        <p>Fill out the form below if you have any questions, comments or concerns or email us at <a href="mailto:contact@planetimpossible.com" class="btn btn-link no-hover no-padding">contact@planetimpossible.com</a></p>

        <!-- left content -->
        <div class="left-content">
            <form class="form-horizontal" name="contactusForm" id="contactusForm" ng-submit="submit(feedback)">
                <div class="form-container">
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="inputName" class="col-lg-4 control-label">Firstname *</label>
                        <div class="col-lg-7 ">
                            <input class="form-control" id="firstname" ng-model="feedback.firstname"  type="text" name="firstname" required ng-minlength="2" ng-maxlength="255">
                            <span class="help-block" ng-if="contactusForm.firstname.$error.required">First name is required</span>
                            <span class="help-block" ng-if="contactusForm.firstname.$error.minlength || contactusForm.firstname.$error.maxlength">Invalid first name</span>
                        </div>
                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="inputName" class="col-lg-4 control-label">Lastname *</label>
                        <div class="col-lg-7 ">
                            <input class="form-control" id="lastname" ng-model="feedback.lastname"  type="text" name="lastname" required ng-minlength="2">
                            <span class="help-block" ng-if="contactusForm.lastname.$error.required">Last name is required</span>
                            <span class="help-block" ng-if="contactusForm.lastname.$error.minlength || contactusForm.username.$error.maxlength">Invalid last name</span>
                        </div>
                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="inputEmail1" class="col-lg-4 control-label">Email Address *</label>
                        <div class="col-lg-7">
                            <input class="form-control" id="email" name="email" type="email" name="email" ng-model="feedback.email" required>
                            <span class="help-block" ng-if="contactusForm.email.$error.required">Email is required</span>
                            <span class="help-block" ng-if="contactusForm.email.$error.email">Email is invalid</span>
                        </div>
                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="inputPassword1" class="col-lg-4 control-label">Subject *</label>
                        <div class="col-lg-7">
                            <input class="form-control" id="subject"  type="text" name="subject" ng-model="feedback.subject" required>
                            <span class="help-block" ng-if="contactusForm.subject.$error.required">Subject is required</span>
                        </div>
                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label for="inputPassword1" class="col-lg-4 control-label">Message *</label>
                        <div class="col-lg-7">
                            <textarea name="message" class="form-control" id="message" ng-model="feedback.message" cols="30" rows="5" required></textarea>
                            <span class="help-block" ng-if="contactusForm.message.$error.required">Message is required</span>
                        </div>
                    </div>
                    <div class="form-group" ng-class="{'has-error ng-scope': captcharesponse}">
                        <label for="inputName" class="col-lg-4 control-label">Please click on the Captcha to be verified.</label>
                        <div class="col-lg-7">
                            <div
                                vc-recaptcha
                                theme="'light'"
                                key="model.key"
                                on-create="setWidgetId(widgetId)"
                                on-success="setResponse(response)"
                                ></div>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-8 col-lg-3">
                            <button type="submit" class="btn btn-pi btn-block pull-left" ng-disabled="contactusForm.$invalid || disable" style="margin-right: 10px;">Submit</button>
                            <div class="spinner pull-left" ng-if="spinner"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- left content -->

        <!-- right content -->
        <div class="sidebar right-content">
            <!-- <div class="panel">
                <div class="panel-header">
                    <p>List of Benefits go here</p>
                    <p>Tagline goes here</p>
                </div>
                <div class="panel-content">
                    <div>
                        <h5>Lorem ipsum dolor sit amet</h5>
                        <p>Ut enim ad minim veniam, quis nostrud exercit ation ullamco laboris nisi ut aliquip ex ea commo do consequat.</p>
                    </div>
                    <div>
                        <h5>Lorem ipsum dolor sit amet</h5>
                        <p>Ut enim ad minim veniam, quis nostrud exercit ation ullamco laboris nisi ut aliquip ex ea commo do consequat.</p>
                    </div>
                    <div>
                        <h5>Lorem ipsum dolor sit amet</h5>
                        <p>Ut enim ad minim veniam, quis nostrud exercit ation ullamco laboris nisi ut aliquip ex ea commo do consequat.</p>
                    </div>
                    <div>
                        <h5>Lorem ipsum dolor sit amet</h5>
                        <p>Ut enim ad minim veniam, quis nostrud exercit ation ullamco laboris nisi ut aliquip ex ea commo do consequat.</p>
                    </div>
                </div>
            </div> -->
        </div>
        <!-- right content -->

    </div>
</div>