<script type="text/ng-template" id="otherCategories.html">
    <div ng-include="'/fe/tpl/otherCategories.html'"></div>
</script>
<script type="text/ng-template" id="sidebarCategory.html">
    <div ng-include="'/fe/tpl/sidebarCategories.html'"></div>
</script>
<script type="text/ng-template" id="sidebarNews.html">
    <div ng-include="'/fe/tpl/sidebarNews.html'"></div>
</script>

<script type="text/ng-template" id="viewNews.html">
  <div ng-include="'/fe/tpl/viewNews.html'"></div>
</script>

<script type="text/ng-template" id="viewNewsList.html">
  <div ng-include="'/fe/tpl/viewNewsList.html'"></div>
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- banner -->
<div class="small-banner">
    <div class="container">
        <ul class="breadcrumbs h-list">
            <li><a href="/">Home</a></li>
            <li class="active">Mission Maps</li>
        </ul>
        <h1>Mission Maps</h1>
    </div>
</div>
<!-- banner -->

<div class="content mission-list">
	<div class="container">
		<!-- main content -->
        <div class="main-content">
        	<div class="panel panel-default">
        		<div class="panel-heading">
        			<div class="row">
        				<label class="pull-left sort">Sort By: </label>
        				<div class="col-lg-4">
						    <Select class="form-control" ng-options="option.value as option.name for option in sortoption" ng-model="sortby" ng-change="setPage(1, sortby)"></Select>
					   	</div>
	        			<div class="col-lg-7" style="width:412px;">
	        				<nav class="pull-right">
                                <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage, sortby)"></pagination>
							</nav>
	        			</div>
        			</div>
        		</div>
        	</div>

			<div class="mission" ng-repeat = "map in maps">
				<a ui-sref="mapview({ map: '{[{ map.mapslugs }]}' })">
					<div class="map pull-left hiddenoverflow">
                        <!-- Google Maps. Replace the below iframe with your Google Maps embed code -->
                        <ui-gmap-google-map center='map.map.center' zoom='map.map.zoom' options="options">
                            <ui-gmap-marker ng-repeat="marker in map.markers" coords="{latitude:marker.lat, longitude:marker.long}" idkey="marker.id" icon="marker.icon"></ui-gmap-marker>
                        </ui-gmap-google-map>
					</div>
					<div class="mission-details pull-left">
						<p class="mission-title">{[{ map.title }]}</p>
						<small>Posted on {[{ map.created_at | formatdate }]} by {[{ map.hide_agent == true ? "Agent " + map.username : "Anonymous Agent" }]}</small>
						<p class="mission-desc" ng-bind-html="map.description | limitTo:170"><span ng-if="map.description.length > 234">...</span></p>
						<div class="btn-container">
                            <!-- facebook -->
                            <div class="fb-like pull-left social-media" data-href="<?php echo $base_url; ?>/action-map/{[{map.mapslugs}]}" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                            <!-- twitter -->
                            <a href="https://twitter.com/share" class="twitter-share-button social-media pull-left">Tweet</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                        </div>

                        <div class="btn-container pull-left full-width views-comments">
                            <div class="pull-right">
                                <a href="#" class="btn btn-lg btn-link"><i class="fa fa-eye fa-lg"></i> {[{map.views}]}</a>
                                <a href="#" class="btn btn-lg btn-link"><i class="fa fa-comment fa-lg"></i> {[{map.comments}]}</a>
                            </div>
                        </div>
					</div>
				</a>
			</div>

        	<div class="panel panel-default" ng-hide="maxSize >= bigTotalItems">
        		<div class="panel-heading">
        			<div class="row">
	        			<div class="col-lg-12">
                            <nav class="pull-right">
                                <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage, sortby)"></pagination>
                            </nav>
	        			</div>
        			</div>
        		</div>
        	</div>
		</div>

		<!-- sidebar -->
        <div class="sidebar">
            <div class="panel" ng-include="'sidebarNews.html'">
            </div>
            <div class="panel" ng-include="'sidebarCategory.html'">
            </div>
            <a ui-sref="contactus">
                <img src="/img/frontend/contactus.png" alt="">
            </a>
        </div>
        <!-- sidebar -->

	</div>
</div>
