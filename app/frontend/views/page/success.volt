
<!-- banner -->
<div class="small-banner">
    <div class="container">
        <ul class="breadcrumbs h-list">
            <li><a href="/">Home</a></li>
            <li class="#/registration">Registration</li>
            <li class="active">Success</li>
        </ul>
        <h1>Registration Success!</h1>
    </div>
</div>
<!-- banner -->

<div class="content main-body reg-success">
    <div class="container">
        <div class="alert alert-success" role="alert">Now to fully complete your registration, please check your email for the activation link.</div>
    </div>
</div>