<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script type="text/ng-template" id="viewPin.html">
  <div ng-include="'/fe/tpl/viewPin.html'"></div>
</script>

<script type="text/ng-template" id="addPin.html">
  <div ng-include="'/fe/tpl/addPin.html'"></div>
</script>

<script type="text/ng-template" id="editMap.html">
  <div ng-include="'/fe/tpl/editMap.html'"></div>
</script>

<script type="text/ng-template" id="deleleComment.html">
  <div ng-include="'/fe/tpl/deleleComment.html'"></div>
</script>

<script type="text/ng-template" id="editComment.html">
  <div ng-include="'/fe/tpl/editComment.html'"></div>
</script>

<script type="text/ng-template" id="mapCover.html">
  <div ng-include="'/fe/tpl/mapCover.html'"></div>
</script>

<toaster-container ></toaster-container>
<div class="content no-margin">
	<div class="container">
		<h3 class="title" ng-cloak>{[{ mapdetails.title }]} <a href="" ng-click="editMap()" ng-if="agentid==mapdetails.agent"><icon class="fa fa-pencil"></icon></a></h3>
		<div class="pull-left full-width mg-bottom missiondate">
			<small ng-cloak>Started on {[{ mapdetails.created_at | date: 'MMMM dd, yyyy' }]}</small>
		</div>
		<!-- main content -->
        <div class="main-content">
        	<div class="gmap">
				<div class="gmap">
                    <!-- Google Maps. Replace the below iframe with your Google Maps embed code -->
                    <ui-gmap-google-map center='map.center' zoom='map.zoom' events="map.events" options="options">
						<ui-gmap-marker ng-repeat="m in missionmarkers" coords="m.coords" idkey="m.id" options="m.options" events="m.events">
                            <ui-gmap-window show="m.show">
                                <div>
                                    <img src="{[{m.agentpic}]}" alt="" class="agent-winimg" ng-cloak> {[{ m.agentname }]}
                                </div>
                            </ui-gmap-window>
						</ui-gmap-marker>
                    </ui-gmap-google-map>
                </div>
			</div>
			<div class="pull-left mg-top full-width" ng-if="agentid!=undefined">
				<a ng-click="addpin()" class="btn btn-{[{btn}]} btn-lg btn-block" ng-cloak>
					{[{btnmsg}]}
				</a>
			</div>
            <div class="pull-left full-width" ng-if="agentid==mapdetails.agent">
                <div class="pull-right">
                    <label>
                        <input type="checkbox" ng-model="hide_agent" ng-change="hideagent(hide_agent)"> Show my name as creator.
                    </label>
                </div>
            </div>
			<div class="socialmedia-btn">
				<div class="btn-container">
                    <div>
    	                <!-- facebook -->
    	                <div class="fb-like pull-left social-media" data-href="<?php echo $base_url; ?>/action-map/{[{mapdetails.mapslugs}]}" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
    	                <!-- twitter -->
    	                <a href="https://twitter.com/share" class="twitter-share-button social-media pull-left">Tweet</a>
    					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

    					<div class="pull-right map-report-btn" ng-cloak>
                            <div class="pull-left">
    	                       <i class="fa fa-eye fa-lg"></i> {[{ mapviews }]}
                            </div>
                            <div class="pull-left">
                               <i class="fa fa-comments-o fa-lg"></i> {[{ commentcount }]}
                            </div>
                            <div class="pull-left">
                                <i class="fa fa-map-marker fa-lg"></i> {[{ mapactions }]}
                            </div>
    	                </div>
                    </div>
                </div>
			</div>
            <div class="comment-container pull-left full-width">
                <p class="pull-left full-width"><i class="fa fa-comment-o fa-2x"></i> All Action Responses({[{ commentcount}]})
                    <span class="pull-right" ng-if="agentid==null"><a ui-sref="login">Sign in</a> or <a ui-sref="registration" >register</a> to post a comment.</span>
                </p>
                <div class="full-width">
                    <div class="commentbox hiddenoverflow">
                        <img class="image pull-left" src="{[{agentprofpic}]}" alt="">
                        <textarea class="input form-control pull-left comment mg-bottom" ng-disabled="agentid==null" ng-model="comment" placeholder="Add a comment..."></textarea>
                        <a ng-click="commentlistener(comment)" class="btn btn-pi pull-right"><i class="fa fa-commenting-o"></i> Post</a>
                    </div>
                    <hr>
                </div>
                <div class="commentlist hiddenoverflow" ng-repeat="comment in comments">
                    <img class="image pull-left" src="{[{comment.profile_pic_name}]}" alt="">
                    <div class="comment pull-left">
                        <h6 class="no-margin mg-bottom"  ng-cloak>
                            <span ng-if="comment.hide_agent != 0">Agent {[{ comment.first_name + " " + comment.last_name }]}</span>
                            <span ng-if="comment.hide_agent == 0">Anonymous Agent</span>
                             | <small>{[{ comment.created_at | date: 'MMMM dd, yyyy hh:mm a' }]}</small>
                            <div class="btn-group pull-right" dropdown is-open="status.isopen" ng-if="comment.type=='map' && (agentid==comment.agent || mapdetails.agent==agentid)">
                              <button type="button" class="btn btn-link" dropdown-toggle ng-disabled="disabled"><span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu" aria-labelledby="single-button">
                                <li role="menuitem" ng-if="agentid==comment.agent">
                                    <a ng-click="comment.showedit=true" class="btn btn-link pull-left full-width text-left" ng-if="comment.showedit==false">Edit</a>
                                    <a ng-click="comment.showedit=false" class="btn btn-link pull-left full-width text-left" ng-if="comment.showedit">Cancel</a>
                                </li>
                                <li role="menuitem"><a ng-click="deleteComment(comment.id, 'comment')" class="btn btn-link pull-left full-width text-left">Delete</a></li>
                              </ul>
                            </div>
                        </h6>
                        <p  ng-cloak ng-bind-html="comment.comment" ng-if="comment.showedit==false"></p>
                        <!-- edit comment -->
                        <div class="full-width pull-left" ng-if="comment.showedit">
                            <textarea class="input form-control pull-left full-width mg-bottom" ng-model="comment.comment" placeholder="Add a comment..."></textarea>
                            <a ng-click="editComment(comment.id, comment.comment, 'comment')" class="btn btn-pi pull-right"><i class="fa fa-pencil-square-o"></i> Edit</a>
                        </div>

                        <ul light-slider ng-if="comment.type=='pin'"></ul>
                        <a href="" class="pull-left btn btn-link" ng-if="comment.showreply==false && agentid!=null" ng-click="showreplybox(comment.id)"><i class="fa fa-reply"></i> Reply</a>
                    </div>
                    <div class="pull-right reply-loadmore">
                        <a ng-click="loadmorereply(comment.id)" class="btn btn-link pull-right" ng-repeat="r in replycount" ng-if="r.count > r.loaded && r.id==comment.id"> load more</a>
                    </div>
                    <div class="pull-right commentreplylist" ng-repeat="reply in replies" ng-if="reply.mapcomment_id==comment.id ">
                        <div class="commentreply hiddenoverflow">
                            <img class="image pull-left" src="{[{reply.profile_pic_name}]}" alt="">
                            <div class="comment pull-left">
                                <h6 class="no-margin mg-bottom"  ng-cloak>
                                    Agent {[{ reply.first_name + " " + reply.last_name }]} | <small>{[{ reply.created_at | date: 'MMMM dd, yyyy hh:mm a' }]}</small>
                                    <div class="btn-group pull-right" dropdown ng-if="agentid==reply.agent || mapdetails.agent==agentid">
                                      <button type="button" class="btn btn-link" dropdown-toggle><span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu" aria-labelledby="single-button">
                                        <li role="menuitem" ng-if="reply.agent==agentid">
                                            <a ng-click="reply.showedit=true" class="btn btn-link pull-left full-width text-left" ng-if="reply.showedit==false">Edit</a>
                                            <a ng-click="reply.showedit=false" class="btn btn-link pull-left full-width text-left" ng-if="reply.showedit">Cancel</a>
                                        </li>
                                        <li role="menuitem"><a ng-click="deleteComment(reply.id, 'response')" class="btn btn-link pull-left full-width text-left">Delete</a></li>
                                      </ul>
                                    </div>
                                </h6>
                                <p  ng-cloak ng-bind-html="reply.reply" ng-if="reply.showedit==false"></p>
                                <!-- edit comment -->
                                <div class="full-width pull-left" ng-if="reply.showedit">
                                    <textarea class="input form-control pull-left mg-bottom full-width" ng-model="reply.reply"></textarea>
                                    <a ng-click="editComment(reply.id, reply.reply, 'response')" class="btn btn-pi pull-right"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right commentreplylist tbox" ng-if="agentid!=null">
                        <div class="replybox full-width hiddenoverflow" ng-if="comment.showreply">
                            <textarea class="input form-control pull-left reply full-width mg-bottom" ng-disabled="agentid==null" ng-model="reply" placeholder="Add a response..."></textarea>
                            <a ng-click="replytocomment(reply, comment.id, comment.type, comment.marker_id)" class="btn btn-pi pull-right mg-bottom"><i class="fa fa-reply"></i> Reply</a>
                        </div>
                    </div>
                </div>
                <button ng-if="commentcount > 4 && commentcount > commentload" class="btn btn-block loadmore" ng-click="loadcomment();">
                <div class="spinner pull-left" ng-if="spinner"></div> Load more</button>
            </div>
        </div>
        <!-- main content -->

        <!-- sidebar -->
        <div class="sidebar">
            <a class="agent mg-bottom" href="#">
    			<img src="{[{profpic}]}" alt="" class="agent-pic">
    			<div class="details">
    				<p class="agent-name"  ng-cloak>{[{ mapdetails.hide_agent == 1 ? "Agent " + agent.first_name + " " + agent.last_name : "Anonymous Agent" }]}</p>
    				<p  ng-cloak>{[{ mapcount }]} Mission Maps</p>
    				<p  ng-cloak>{[{ actioncount }]} Action Pins</p>
    			</div>
            </a>
            <div class="media-container mg-bottom" ng-repeat="image in images">
                <a class="fancybox-button" fancybox href="{[{mapdetails.cover}]}" title="Rodeo Dusk (_JonathanMitchellPhotography_)">
                    <img ng-if="mapdetails.coverType=='image'" width="100%" height="150" ng-src="{[{mapdetails.cover}]}">
                </a>
				<div ng-if="mapdetails.coverType=='video'" ng-bind-html="mapdetails.cover">
				</div>
                <a ng-click="changecover()" class="btn btn-pi btn-block" ng-if="agentid==mapdetails.agent">Change cover</a>
			</div>

			<div class="mission-details pull-left full-width">
				<p class="desc" ng-cloak ng-bind-html="mapdetails.description"></p>
			</div>
			<!-- mission news -->
			<div class="panel">
                <div class="panel-header">
                    <img src="/img/frontend/von.png" alt="">
                    <p>Hello Agent!</p>
                    <p class="agent-alias">Captain Planet</p>
                </div>
                <div class="panel-content">
                    <div class="list">
                        <div class="date">
                            <strong>6</strong>11
                        </div>
                        <p>&#34;Well, call me Captain Combustion, a spontaneous kinda guy.&#34;</p>
                    </div>
                    <div class="list">
                        <div class="date">
                            <strong>6</strong>11
                        </div>
                        <p>&#34;Well, call me Captain Combustion, a spontaneous kinda guy.&#34;</p>
                    </div>
                    <div class="list">
                        <div class="date">
                            <strong>6</strong>11
                        </div>
                        <p>&#34;Well, call me Captain Combustion, a spontaneous kinda guy.&#34;</p>
                    </div>
                    <a href="#" class="viewmore">view more</a>
                </div>
            </div>
        </div>
        <!-- sidebar -->
	</div>
</div>
