<!DOCTYPE html>
<html lang="en" data-ng-app="PiApp">
<head>
    <meta charset="utf-8">
    <!-- Title here -->
    <title>Planet Impossible</title>
    <base href="/">

    <meta name="description" content="{{metadescription}}">
    <meta name="keywords" content="{{metakeywords}}">
    <meta name="author" content="{{metaauthor}}">

    <!-- Here I customized the title, but you can customize any property you want -->
    <meta property="og:title" content="{{fbtitle}}"/>
    <meta property="og:description" content="{{fbdesc}}" />
    <meta property="og:image" content="{{fbcover}}" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <base href="{{ router.getRewriteUri() }}">

    <!-- Styles -->
    <!-- Font awesome CSS-->
    <link rel="stylesheet" href="/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <!-- <link href="/fe/css/style2.css" rel="stylesheet"> -->
    <link href="/fe/css/style.css" rel="stylesheet">
    <link href="/vendors/Material-Preloader/css/materialPreloader.css" rel="stylesheet">

    <link href="/vendors/AngularJS-Toaster/toaster.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/vendors/lightslider/dist/css/lightslider.min.css">
    <link rel="stylesheet" type="text/css" href="/vendors/angular-xeditable/dist/css/xeditable.css">
    <link rel="stylesheet" href="/vendors/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.css">
    {{ stylesheet_link('be/js/jquery/chosen/chosen.css') }}

    {{ stylesheet_link('be/js/jquery/select2/select2.css') }}
    {{ stylesheet_link('vendors/angular-ui-select/dist/select.css') }}



    <!-- Favicon -->
    <link rel="shortcut icon" href="/img/frontend/favicon/favicon.png">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <script src="https://apis.google.com/js/platform.js" async defer></script>

    <!-- Add fancyBox -->
    <link rel="stylesheet" href="/vendors/fancyBox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />

    <!-- CKEDITOR -->
    <link rel="stylesheet" href="/vendors/ng-ckeditor/ng-ckeditor.css">
</head>

<body ng-controller="MainCtrl">
<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

    <!-- header -->
    <header class="header">
        <div class="container">
            <a href="/"><img src="/img/frontend/logo.png" alt="Planet Impossible" class="logo pull-left"></a>
            <input type="text" class="input pull-left" id="header-input-search" placeholder="Search">
            <div>
                <div class="pull-left" style="width:404px;">
                    <a ui-sref="search" class="blue-link no-hover">Advance Search</a>
                </div>
                <ul class="header-links h-list">
                    <li><a ui-sref="about" class="b-link">What is Planet Impossible?</a></li>
                    <li>
                        <a ng-if="loginmenu==''" ui-sref="login" class="b-link">Login</a>
                        <a ng-if="loginmenu != ''" class="no-hover ng-cloak" ui-sref="profile({ username: '{[{ username }]}' })">{[{ loginmenu }]} <span class="badge badge-pi ng-cloak" ng-if="notification.total > 0">{[{notification.total}]}</span></a>
                    </li> |
                    <li>
                        <a ng-if="loginmenu==''" ui-sref="registration" class="b-link">Become an Agent</a>
                        <a ng-if="loginmenu!=''" class="btn btn-link b-link no-padding" ng-click="logout()">Log out</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <!-- header -->

    <!-- navigation -->
    <nav class="navigation">
        <div class="container">
            <ul class="h-list">
                <li>
                    <a ui-sref="missionmaps"><img src="/img/frontend/navicon1.jpg" alt="Mission Maps"> MISSION MAPS</a>
                </li>
                <li>
                    <a ui-sref="mapstart"><img src="/img/frontend/navicon2.jpg" alt="Create Mission"> CREATE MISSION</a>
                </li>
                <li>
                    <a href="#"><img src="/img/frontend/navicon3.jpg" alt="Community"> COMMUNITY</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- navigation -->
    <div ng-material-preloader="{position: 'top', height: '5px', col_1: '#159756', col_2: '#da4733', col_3: '#3b78e7', col_4: '#fdba2c', fadeIn: 200, fadeOut: 200}">
   <div ui-view></div>

    <!-- footer -->
    <footer>
        <div class="footer-content">
            <div class="container">
                <div class="col">
                    <ul>
                        <li class="footer-nav-header">NAVIGATE</li>
                        <li><a href="/">Home</a></li>
                        <li><a ui-sref="about">What is PI?</a></li>
                        <li><a ui-sref="mapstart">Create a mission</a></li>
                    </ul>
                </div>
                <div class="col">
                    <ul>
                        <li class="footer-nav-header">MAPS</li>
                        <li><a href="#" ng-click="seeMore('FEATURED')">Featured Missions</a></li>
                        <li><a href="#" ng-click="seeMore('LATEST')">Latest Missions</a></li>
                        <li><a href="#" ng-click="seeMore('GREATEST')">Greatest Missions</a></li>
                        <li><a href="#" ng-click="seeMore('POPULAR')">Most Popular Missions</a></li>
                    </ul>
                </div>
                <div class="col">
                    <ul>
                        <li class="footer-nav-header">COMMUNITY</li>
                        <li><a href="#">Blogs</a></li>
                        <li><a href="#">Community</a></li>
                        <li><a href="#">Forum</a></li>
                        <li><a href="#">News and Events</a></li>
                    </ul>
                </div>
                <div class="col">
                    <ul>
                        <li class="footer-nav-header">GET INVOLVED</li>
                        <li><a href="#">Donate to PI</a></li>
                        <li><a href="#">PI Team Efforts</a></li>
                        <li><a href="#">Learn More</a></li>
                        <li><a href="#">PI's Future</a></li>
                        <li><a href="#">Be a piece of PI</a></li>
                    </ul>
                </div>
                <div class="col">
                    <ul>
                        <li class="footer-nav-header">USEFUL LINKS</li>
                        <li><a ui-sref="registration">Become an Agent</a></li>
                        <li><a ui-sref="login">Sign in</a></li>
                        <li><a ui-sref="contactus">Contact Us</a></li>
                    </ul>
                </div>
                <div class="col">
                    <p class="footer-nav-header">NEWSLETTER</p>
                    <p>
                        Subscript to our newsletter<br>
                        and get the latest news<br>
                        straight to your inbox.<br>
                    </p>
                    <input type="text" placeholder="Enter your email" class="input">
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <p>Copyright 2015 PlanetImpossible.com | <a ui-sref="policy">Privacy Policy</a> | <a ui-sref="termofservice">Terms of Service</a></p>
                <img src="/img/frontend/footerlogo.png" alt="Planet Impossible" class="pull-right">
            </div>
        </div>
    </footer>
    <!-- footer -->

<!-- Footer ends -->

<!-- Javascript files -->
<!-- jQuery -->
<script src="/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap JS
<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
-->
<script src="/vendors/html5shiv/dist/html5shiv.min.js"></script>

<script src="/fe/scripts/modernizr.custom.28468.js"></script>

<!-- Custom JS -->
<script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer></script>
<script src="/fe/scripts/custom.js"></script>
<script src="/fe/scripts/countries3.js"></script>
<script src="/vendors/angular/angular.js"></script>
<script src="/vendors/angular-ui-router/release/angular-ui-router.min.js"></script>
<script src="/vendors/angular-animate/angular-animate.min.js"></script>
<script src="/vendors/angular-cookies/angular-cookies.min.js"></script>
<script src="/vendors/a0-angular-storage/dist/angular-storage.min.js"></script>
<script src="/vendors/angular-bootstrap/ui-bootstrap.min.js"></script>
<script src="/vendors/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
<script src="/vendors/angular-resource/angular-resource.min.js"></script>
<script src="/vendors/angular-recaptcha/release/angular-recaptcha.min.js"></script>
<script src="/vendors/angular-jwt/dist/angular-jwt.min.js"></script>
<script src="/vendors/angular-uuid-service/uuid-svc.min.js"></script>
<script src="/vendors/angularUtils-pagination/dirPagination.js"></script>
<script src="/vendors/angular-sanitize/angular-sanitize.min.js"></script>
<script src="/vendors/Material-Preloader/js/materialPreloader.js"></script>
<script src="/fe/scripts/ngPreloader.js"></script>
<script src="/vendors/lightslider/dist/js/lightslider.min.js"></script>
<script src="http://www.datejs.com/build/date.js" type="text/javascript"></script>

<!-- For Toaster -->
<script src="/vendors/AngularJS-Toaster/toaster.js"></script>

<script src="/vendors/ng-file-upload/ng-file-upload-shim.min.js"></script>
<script src="/vendors/ng-file-upload/ng-file-upload.min.js"></script>
<script src="/vendors/ng-videosharing-embed/build/ng-videosharing-embed.min.js"></script>

<script src="/vendors/angular-ui-load/ui-load.js"></script>

<script src="/vendors/angular-xeditable/dist/js/xeditable.min.js"></script>

        <!-- include angular-chosen -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script>
        <script src="/vendors/angular-chosen/angular-chosen.js"></script>

        {{ javascript_include('vendors/angular-ui-select/dist/select.js') }}
        {{ javascript_include('vendors/angular-moment/angular-moment.min.js') }}
        {{ javascript_include('vendors/moment/min/moment.min.js') }}

<!-- For angular Maps V3-->
<script src="/vendors/lodash/dist/lodash.min.js"></script>
<script src="/vendors/angular-google-maps/dist/angular-google-maps.min.js"></script>
<script src='//maps.googleapis.com/maps/api/js?sensor=false&libraries=places'></script>

        <script src="/vendors/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.js"></script>

<script src="/fe/scripts/app.js"></script>
<script src="/fe/scripts/controllers/main.js"></script>
<script src="/fe/scripts/factory/notification.js"></script>
<script src="/fe/scripts/factory/login.js"></script>
<script src="/fe/scripts/factory/factory.js"></script>
<script src="/fe/scripts/config.js"></script>
<?php 
    if(isset($otherjvascript)){
        echo $otherjvascript ;
    }
?>
<script src="/fe/scripts/directives/showErrors.js"></script>
<script src="/fe/scripts/services/piservices.js"></script>
<script src="/fe/scripts/filter.js"></script>

        <!-- Add fancyBox -->
        <script type="text/javascript" src="/vendors/fancyBox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
        <script type="text/javascript" src="/vendors/fancyBox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

        <!-- CK EDITOR -->
        <script src="/vendors/ckeditor/ckeditor.js"></script>
        <script src="/vendors/ng-ckeditor/ng-ckeditor.js"></script>
        <link rel="stylesheet" href="/vendors/ng-ckeditor/ng-ckeditor.css">

        <script>

            // This code is generally not necessary, but it is here to demonstrate
            // how to customize specific editor instances on the fly. This fits well
            // this demo because we have editable elements (like headers) that
            // require less features.
            // The "instanceCreated" event is fired for every editor instance created.
            CKEDITOR.on( 'instanceCreated', function( event ) {
                var editor = event.editor,
                        element = editor.element;
                // Customize editors for headers and tag list.
                // These editors don't need features like smileys, templates, iframes etc.
                    editor.on( 'configLoaded', function() {
                        // Remove unnecessary plugins to make the editor simpler.
                        // Rearrange the layout of the toolbar.
                        editor.config.extraPlugins = 'oembed,widget,dialog';
                        editor.config.toolbar = [
                            { name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
                            { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
                            { name: 'links', items: [ 'Link', 'Unlink', 'Image' , 'oembed'] },
                            { name: 'about', items: [ 'About' ] }
                        ];
                        editor.config.height = '100px';

                        // Remove the redundant buttons from toolbar groups defined above.
                        editor.config.removeButtons = 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar';
                    });
            });

        </script>

</body>
</html>
