<!DOCTYPE html>
<html lang="en" data-ng-app="PiApp">
<head>
    <meta charset="utf-8">
    <title>HOME | Planet Impossible</title>
    <base href="{{ router.getRewriteUri() }}">
    <meta name="description" content="{{metadescription}}">
    <meta name="keywords" content="{{metakeywords}}">
    <meta name="author" content="{{metaauthor}}">
    <meta property="og:title" content="{{fbtitle}}"/>
    <meta property="og:description" content="{{fbdesc}}" />
    <meta property="og:image" content="{{fbcover}}" />
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="/img/frontend/favicon/favicon.png">

    <!-- STYLES -->
    <link rel="stylesheet" href="/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/fe/css/style.css" rel="stylesheet">

    <!-- CSS for togglesidebar -->
    <link href="/fe/css/togglesidebar.css" rel="stylesheet">

    <link href="/vendors/Material-Preloader/css/materialPreloader.css" rel="stylesheet">
    <link href="/vendors/AngularJS-Toaster/toaster.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/vendors/lightslider/dist/css/lightslider.min.css">
    <link rel="stylesheet" type="text/css" href="/vendors/angular-xeditable/dist/css/xeditable.css">
    <link rel="stylesheet" href="/vendors/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.css">
    {{ stylesheet_link('be/js/jquery/chosen/chosen.css') }}
    {{ stylesheet_link('be/js/jquery/select2/select2.css') }}
    {{ stylesheet_link('vendors/angular-ui-select/dist/select.css') }}

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <script src="https://apis.google.com/js/platform.js" async defer></script>

    <!-- Add fancyBox -->
    <link rel="stylesheet" href="/vendors/fancyBox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />

    <!-- CKEDITOR -->
    <link rel="stylesheet" href="/vendors/ng-ckeditor/ng-ckeditor.css">

    <!-- SWIPER -->
    <link rel="stylesheet" href="/vendors/Swiper/dist/css/swiper.min.css">

</head>

<body ng-controller="MainCtrl">

    <div id="fb-root"></div>

    <!-- BEGIN HEADER -->

    <div id="header">
        <div class="col-xs-12">
            <div id="header-logo" class="col-xs-4">
                <img id="header-logo-image" src="/img/new_assets/logo.png" />
                <div id="header-logo-text">
                    <h3>Planet Impossible</h3>
                    <p>An Experiment in Collective Action</p>
                </div>
            </div>
            <div id="header-search" class="col-xs-4">
                <h4 id="header-search-text" class="col-xs-2">Search</h4>
                <div class="col-xs-9 col-xs-12 text-left">
                    <input id="header-search-input" ng-model="query" type="text" ng-keypress="search($event, query)" />
                </div>
            </div>                    
            <div id="header-login" class="col-xs-4">
                <button id="loginBtn" class="btn btn-warning">Log In</button>
                <button id="createBtn" class="btn btn-warning">Create Mission</button>
            </div>
        </div>
    </div>
    <div style="" id="navigation" class=""> 
        <div class="col-xs-1 col-xs-1 col-xs-12">
            <div id="nav-toggler">
                <a id="menu-toggle" href="#" class="btn-menu toggle" ng-click="sidebar.toggle()">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </a>
            </div>
        </div>
        <div id="nav-menu" class="col-xs-offset-2 col-xs-7 col-xs-8 col-xs-12 pulleft">
            <ul id="nav-menu-list">
                <li><a href="about">About</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Create</a></li>
                <li><a href="#">Discover</a></li>
                <li><a href="#">Events</a></li>
            </ul>
        </div>
        <div id="nav-brand">
            <img id="nav-brand-logo" src="/img/new_assets/alphaagent-logo.png">
            <span id="nav-brand-text">Alpha Agents</span>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- END NAVIGATION -->
    <div id="wrapper">
        <div id="sidebar-wrapper">
            <nav id="spy">
                <ul id="group" class="gtitles">
                    <li>
                        <span class="group-titles">Categories</span>
                        <ul id="subgroup" class="stitles">
                            <li><a href="#">Animals</a></li>
                            <li><a href="#">Bullying Violence</a></li>
                            <li><a href="#">Disasters</a></li>
                            <li><a href="#">Discrimination</a></li>
                            <li><a href="#">Education</a></li>
                            <li><a href="#">Environment</a></li>
                            <li><a href="#">Health</a></li>
                            <li><a href="#">Homelessness + Poverty</a></li>
                            <li><a href="#">Sex + Relationships</a></li>
                        </ul>
                    </li>
                    <li class="margin-xs">
                        <span class="group-titles">Tags</span>
                        <ul id="subgroup" class="stitles">
                            <li>Animals Bullying Violence</li>
                            <li>Disasters Discrimination</li>
                            <li>Education Environment</li>
                            <li>Health Homelessness +</li>
                            <li>Poverty Sex +</li>
                            <li>Relationships</li>
                        </ul>
                    </li>
                    <li class="margin-xs"><span class="group-titles">Location</span></li>
                    <div class="searchloc">
                        <input type="text">
                        <span class="fa fa-search"></span>
                    </div>
                    <li class="margin-xs"><span class="group-titles">Date</span></li>
                    <br><br>
                </ul>
            </nav>
        </div>
        <div class="swiper-container" style="margin-top:115px;">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="slider-img" style="background: url(/img/new_assets/img-2.jpg) no-repeat;background-size: 100% 100%;">
                        <div class="container">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="slider-search">
                                        <label>Search</label>
                                        <input type="text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-bottom">
                        <div class="container">
                            <div class="text-center">
                                <span>Text goes here. Description about the Call To Action goes here.</span>
                                <button class=" btn btn-info action-block-button">Become an Agent</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">

                    <div class="slider-img" style="background: url(/img/new_assets/img-1.jpg) no-repeat;background-size: 100% 100%;">
                        <div class="container">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="slider-search">
                                        <label>Search</label>
                                        <input type="text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-bottom">
                        <div class="container">
                            <div class="text-center">
                                <span>Text goes here. Description about the Call To Action goes here.</span>
                                <button class=" btn btn-info action-block-button">Become an Agent</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <section class="container">
                <div id="top-ads" class="row row-fix text-center">
                    <img src="/img/new_assets/sample-ad-1.png">
                </div>
                <div id="featured-missions" class="row row-fix">
                    <div class="col-xs-12">
                        <h2>Featured Missions</h2>
                        <sub>Agent Pi: C’mon guys we really need to fix this problem. Seems like all of you really care about fish this week.</sub>
                        <div id="mission-items" class="col-xs-12">
                            <div class="mission-card col-xs-4">
                                <div class="wrapper">
                                    <div class="flagCon">
                                        <div class="flagIcon">
                                            <label> HOT PICK </label>
                                            <span class="fa fa-flag pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="viewsCon">
                                        <div class="viewsIcon">
                                            <label> 1000 views </label><span class="fa fa-eye pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="commentsCon">
                                        <div class="commentsIcon">
                                            <label> 1000 comments </label><span class="fa fa-commenting-o  pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="pinCon">
                                        <div class="pinIcon">
                                            <label> 1000 pin </label><span class="fa fa-thumb-tack pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="card-image" style="background: url('http://img.youtube.com/vi/uVdV-lxRPFo/hqdefault.jpg');"></div>
                                    <div class="card-info-3">
                                        <img src="https://planetimpossible.s3.amazonaws.com/uploads/agentpic/40BE72F7-819C-4E19-BF38-86F3B0619140/jeevon.png" />
                                        <p>Save the boats! Save your water! Save your life!</p>
                                    </div>
                                    <div class="wrapperFooter">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-6">
                                                    <p>Jane Doe</p>
                                                </div>
                                                <div class="col-xs-6">
                                                    <p class="pull-right">22 April 2015</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mission-card col-xs-4">
                                <div class="wrapper">
                                    <div class="flagCon">
                                        <div class="flagIcon">
                                            <label> HOT PICK </label><span class="fa fa-flag pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="viewsCon">
                                        <div class="viewsIcon">
                                            <label> 1000 views </label><span class="fa fa-eye pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="commentsCon">
                                        <div class="commentsIcon">
                                            <label> 1000 comments </label><span class="fa fa-commenting-o  pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="pinCon">
                                        <div class="pinIcon">
                                            <label> 1000 pin </label><span class="fa fa-thumb-tack pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="card-image" style="background: url('https://planetimpossible.s3.amazonaws.com/uploads/maps/8BAE3442-D01B-4EF0-828C-C76C3EA7C1FD/Default-Mainthumbnail5.jpg');"></div>
                                    <div class="card-info-3">
                                        <img src="https://planetimpossible.s3.amazonaws.com/uploads/agentpic/40BE72F7-819C-4E19-BF38-86F3B0619140/jeevon.png" />
                                        <p>Save the boats! Save your water! Save your life!</p>
                                    </div>
                                    <div class="wrapperFooter">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-6">
                                                    <p>Jane Doe</p>
                                                </div>
                                                <div class="col-xs-6">
                                                    <p class="pull-right">22 April 2015</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mission-card  col-xs-4">
                                <div class="wrapper">
                                    <div class="flagCon">
                                        <div class="flagIcon">
                                            <label> HOT PICK </label><span class="fa fa-flag pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="viewsCon">
                                        <div class="viewsIcon">
                                            <label> 1000 views </label><span class="fa fa-eye pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="commentsCon">
                                        <div class="commentsIcon">
                                            <label> 1000 comments </label><span class="fa fa-commenting-o  pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="pinCon">
                                        <div class="pinIcon">
                                            <label> 1000 pin </label><span class="fa fa-thumb-tack pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="card-image" style="background: url('http://img.youtube.com/vi/GJ4Qp2xeRds/hqdefault.jpg');"></div>
                                    <div class="card-info-3">
                                        <img src="https://planetimpossible.s3.amazonaws.com/uploads/agentpic/40BE72F7-819C-4E19-BF38-86F3B0619140/jeevon.png" />
                                        <p>Save the boats! Save your water! Save your life!</p>
                                        <!-- <span>Agent CoolGuy</span> -->
                                    </div>
                                    <div class="wrapperFooter">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-6">
                                                    <p>Jane Doe</p>
                                                </div>
                                                <div class="col-xs-6">
                                                    <p class="pull-right">22 April 2015</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="latest-missions" class="row row-fix">
                    <div class="col-xs-12">
                        <h2>Latest Missions</h2>
                        <sub>Agent Pi: Take a look at what is new.</sub>
                        <div id="mission-items" class="col-xs-12">
                            <div class="mission-card col-xs-4">
                                <div class="wrapper">
                                    <div class="flagCon">
                                        <div class="flagIcon">
                                            <label> HOT PICK </label><span class="fa fa-flag pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="viewsCon">
                                        <div class="viewsIcon">
                                            <label> 1000 views </label><span class="fa fa-eye pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="commentsCon">
                                        <div class="commentsIcon">
                                            <label> 1000 comments </label><span class="fa fa-commenting-o  pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="pinCon">
                                        <div class="pinIcon">
                                            <label> 1000 pin </label><span class="fa fa-thumb-tack pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="card-image" style="background: url('http://img.youtube.com/vi/uVdV-lxRPFo/hqdefault.jpg');"></div>
                                    <div class="card-info-3">
                                        <img src="https://planetimpossible.s3.amazonaws.com/uploads/agentpic/40BE72F7-819C-4E19-BF38-86F3B0619140/jeevon.png" />
                                        <p>Save the boats! Save your water! Save your life!</p>
                                    </div>
                                    <div class="wrapperFooter">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-6">
                                                    <p>Jane Doe</p>
                                                </div>
                                                <div class="col-xs-6">
                                                    <p class="pull-right">22 April 2015</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mission-card col-xs-4">
                                <div class="wrapper">
                                    <div class="flagCon">
                                        <div class="flagIcon">
                                            <label> HOT PICK </label><span class="fa fa-flag pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="viewsCon">
                                        <div class="viewsIcon">
                                            <label> 1000 views </label><span class="fa fa-eye pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="commentsCon">
                                        <div class="commentsIcon">
                                            <label> 1000 comments </label><span class="fa fa-commenting-o  pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="pinCon">
                                        <div class="pinIcon">
                                            <label> 1000 pin </label><span class="fa fa-thumb-tack pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="card-image" style="background: url('https://planetimpossible.s3.amazonaws.com/uploads/maps/8BAE3442-D01B-4EF0-828C-C76C3EA7C1FD/Default-Mainthumbnail5.jpg');"></div>
                                    <div class="card-info-3">
                                        <img src="https://planetimpossible.s3.amazonaws.com/uploads/agentpic/40BE72F7-819C-4E19-BF38-86F3B0619140/jeevon.png" />
                                        <p>Save the boats! Save your water! Save your life!</p>
                                    </div>
                                    <div class="wrapperFooter">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-6">
                                                    <p>Jane Doe</p>
                                                </div>
                                                <div class="col-xs-6">
                                                    <p class="pull-right">22 April 2015</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mission-card col-xs-4">
                                <div class="wrapper">
                                    <div class="flagCon">
                                        <div class="flagIcon">
                                            <label> HOT PICK </label><span class="fa fa-flag pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="viewsCon">
                                        <div class="viewsIcon">
                                            <label> 1000 views </label><span class="fa fa-eye pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="commentsCon">
                                        <div class="commentsIcon">
                                            <label> 1000 comments </label><span class="fa fa-commenting-o  pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="pinCon">
                                        <div class="pinIcon">
                                            <label> 1000 pin </label><span class="fa fa-thumb-tack pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="card-image" style="background: url('http://img.youtube.com/vi/GJ4Qp2xeRds/hqdefault.jpg');"></div>
                                    <div class="card-info-3">
                                        <img src="https://planetimpossible.s3.amazonaws.com/uploads/agentpic/40BE72F7-819C-4E19-BF38-86F3B0619140/jeevon.png" />
                                        <p>Save the boats! Save your water! Save your life!</p>
                                    </div>
                                    <div class="wrapperFooter">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-6">
                                                    <p>Jane Doe</p>
                                                </div>
                                                <div class="col-xs-6">
                                                    <p class="pull-right">22 April 2015</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="recommended-missions" class="row row-fix">
                    <div class="col-xs-12">
                        <h2>Recommended Missions for You</h2>
                        <sub>Agent Pi: C’mon guys we really need to fix this problem. Seems like all of you really care about fish this week.</sub>
                        <div id="mission-items" class="col-xs-12">
                            <div class="mission-card col-xs-4">
                                <div class="wrapper">
                                    <div class="flagCon">
                                        <div class="flagIcon">
                                            <label> HOT PICK </label><span class="fa fa-flag pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="viewsCon">
                                        <div class="viewsIcon">
                                            <label> 1000 views </label><span class="fa fa-eye pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="commentsCon">
                                        <div class="commentsIcon">
                                            <label> 1000 comments </label><span class="fa fa-commenting-o  pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="pinCon">
                                        <div class="pinIcon">
                                            <label> 1000 pin </label><span class="fa fa-thumb-tack pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="card-image" style="background: url('http://img.youtube.com/vi/uVdV-lxRPFo/hqdefault.jpg');"></div>
                                    <div class="card-info-3">
                                        <img src="https://planetimpossible.s3.amazonaws.com/uploads/agentpic/40BE72F7-819C-4E19-BF38-86F3B0619140/jeevon.png" />
                                        <p>Save the boats! Save your water! Save your life!</p>
                                    </div>
                                    <div class="wrapperFooter">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-6">
                                                    <p>Jane Doe</p>
                                                </div>
                                                <div class="col-xs-6">
                                                    <p class="pull-right">22 April 2015</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mission-card col-xs-4">
                                <div class="wrapper">
                                    <div class="flagCon">
                                        <div class="flagIcon">
                                            <label> HOT PICK </label><span class="fa fa-flag pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="viewsCon">
                                        <div class="viewsIcon">
                                            <label> 1000 views </label><span class="fa fa-eye pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="commentsCon">
                                        <div class="commentsIcon">
                                            <label> 1000 comments </label><span class="fa fa-commenting-o  pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="pinCon">
                                        <div class="pinIcon">
                                            <label> 1000 pin </label><span class="fa fa-thumb-tack pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="card-image" style="background: url('https://planetimpossible.s3.amazonaws.com/uploads/maps/8BAE3442-D01B-4EF0-828C-C76C3EA7C1FD/Default-Mainthumbnail5.jpg');"></div>
                                    <div class="card-info-3">
                                        <img src="https://planetimpossible.s3.amazonaws.com/uploads/agentpic/40BE72F7-819C-4E19-BF38-86F3B0619140/jeevon.png" />
                                        <p>Save the boats! Save your water! Save your life!</p>
                                    </div>
                                    <div class="wrapperFooter">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-6">
                                                    <p>Jane Doe</p>
                                                </div>
                                                <div class="col-xs-6">
                                                    <p class="pull-right">22 April 2015</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mission-card col-xs-4">
                                <div class="wrapper">
                                    <div class="flagCon">
                                        <div class="flagIcon">
                                            <label> HOT PICK </label><span class="fa fa-flag pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="viewsCon">
                                        <div class="viewsIcon">
                                            <label> 1000 views </label><span class="fa fa-eye pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="commentsCon">
                                        <div class="commentsIcon">
                                            <label> 1000 comments </label><span class="fa fa-commenting-o  pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="pinCon">
                                        <div class="pinIcon">
                                            <label> 1000 pin </label><span class="fa fa-thumb-tack pull-right" aria-hidden="true"></span>
                                        </div>
                                    </div>

                                    <div class="card-image" style="background: url('http://img.youtube.com/vi/GJ4Qp2xeRds/hqdefault.jpg');"></div>
                                    <div class="card-info-3">
                                        <img src="https://planetimpossible.s3.amazonaws.com/uploads/agentpic/40BE72F7-819C-4E19-BF38-86F3B0619140/jeevon.png" />
                                        <p>Save the boats! Save your water! Save your life!</p>
                                    </div>
                                    <div class="wrapperFooter">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-6">
                                                    <p>Jane Doe</p>
                                                </div>
                                                <div class="col-xs-6">
                                                    <p class="pull-right">22 April 2015</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="top-ads" class="row row-fix text-center">
                    <img src="/img/new_assets/sample-ad-2.png">
                </div>
            </section>
        </div>
        <div id="PIMission">
            <div class="container">
                <h2 class="text-center">What is Planet Impossible’s Mission?</h2>
                <p>
                    Simply looking at data and statistics, we can easily see that our planet and the people living on the planet need to improve. 
                    This will only happen when each person makes a conscious choice to be active and do something different. 
                    If it is this simple, then why is it “impossible”? It isn’t. 
                    The only obstacle is the our ability to make the choice because the perception that it “is impossible” - but it is just that, perception. 
                    It is only impossible if we do not do it together. Simply put, our collaboration is the solution. You are a part of the collective. 
                    Take action now and help influence what is possible. 
                </p> <br>
                <p>
                    <i>Spread your idea and influence action. The more you reach, the more possible it becomes!</i>
                </p>
            </div>
        </div>


        <footer id="piFooter">
            <div id="footer-content" >
                <div class="container">
                    <div class="col-xs-3 content-col">
                        <h4 class="content-col-title">NAVIGATE</h4>
                        <ul>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Create</a></li>
                            <li><a href="#">Discover</a></li>
                            <li><a href="#">Events</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-3 content-col">
                        <h4 class="content-col-title">MISSIONS</h4>
                        <ul>
                            <li><a href="#">Featured</a></li>
                            <li><a href="#">Latest</a></li>
                            <li><a href="#">Recommended</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-3 content-col">
                        <h4 class="content-col-title">TOOLS</h4>
                        <ul>
                            <li><a href="#">Become an Agent</a></li>
                            <li><a href="#">Sign In</a></li>
                            <li><a href="#">Alpha Agents</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-3 content-col">
                        <h4 class="content-col-title">NEWSLETTER</h4>
                        <p id="newsletter-text">
                            Subscribe to our newsletter<br/>
                            and get the latest news<br/>
                            straight to your inbox.
                        </p>
                        <input id="newsletter-email" ng-keypress="subscribe($event, recipient)" ng-model="recipient" type="email" />
                    </div>
                </div>
            </div>
            <div id="footer-bottom">
                <span>Copyright © 2016 Planet Impossible. All rights reserved.</span>
                <ul>
                    <li><a href="#">Terms</a></li>
                    <li><a href="#">Privacy</a></li>
                    <li><a href="#">Policy & Safety</a></li>
                </ul>
                <div id="footer-logo" class="pull-right">
                    <img src="/img/new_assets/footer-logo.png" />
                    <span>Planet Impossible</span>
                </div>
            </div>
        </footer>

    </div>

    <!-- SCRIPTS -->
    <!-- jQuery -->
    <script src="/vendors/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript">
        $('#header').css('width', screen.width - 15);
        $('#navigation').css('width', screen.width - 15);
        $('#PIMission').css('width', screen.width - 15);
        $('#piFooter').css('width', screen.width - 15);
        $('.swiper-container').css('width', screen.width - 15);
    </script>
    <script src="/vendors/html5shiv/dist/html5shiv.min.js"></script>
    <script src="/fe/scripts/modernizr.custom.28468.js"></script>

    <!-- Custom JS -->
    <script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer></script>
    <script src="/fe/scripts/custom.js"></script>

    <!-- scripts for toggle sidebar -->
    <script src="/fe/scripts/togglesidebar.js"></script>

    <script src="/fe/scripts/countries3.js"></script>
    <script src="/vendors/angular/angular.js"></script>
    <script src="/vendors/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="/vendors/angular-animate/angular-animate.min.js"></script>
    <script src="/vendors/angular-cookies/angular-cookies.min.js"></script>
    <script src="/vendors/a0-angular-storage/dist/angular-storage.min.js"></script>
    <script src="/vendors/angular-bootstrap/ui-bootstrap.min.js"></script>
    <script src="/vendors/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
    <script src="/vendors/angular-resource/angular-resource.min.js"></script>
    <script src="/vendors/angular-recaptcha/release/angular-recaptcha.min.js"></script>
    <script src="/vendors/angular-jwt/dist/angular-jwt.min.js"></script>
    <script src="/vendors/angular-uuid-service/uuid-svc.min.js"></script>
    <script src="/vendors/angularUtils-pagination/dirPagination.js"></script>
    <script src="/vendors/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="/vendors/Material-Preloader/js/materialPreloader.js"></script>
    <script src="/fe/scripts/ngPreloader.js"></script>
    <script src="/vendors/lightslider/dist/js/lightslider.min.js"></script>
    <script src="http://www.datejs.com/build/date.js" type="text/javascript"></script>

    <!-- For Toaster -->
    <script src="/vendors/AngularJS-Toaster/toaster.js"></script>
    <script src="/vendors/ng-file-upload/ng-file-upload-shim.min.js"></script>
    <script src="/vendors/ng-file-upload/ng-file-upload.min.js"></script>
    <script src="/vendors/ng-videosharing-embed/build/ng-videosharing-embed.min.js"></script>
    <script src="/vendors/angular-ui-load/ui-load.js"></script>
    <script src="/vendors/angular-xeditable/dist/js/xeditable.min.js"></script>

    <!-- include angular-chosen -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script>
    <script src="/vendors/angular-chosen/angular-chosen.js"></script>
    {{ javascript_include('vendors/angular-ui-select/dist/select.js') }}
    {{ javascript_include('vendors/angular-moment/angular-moment.min.js') }}
    {{ javascript_include('vendors/moment/min/moment.min.js') }}

    <!-- For angular Maps V3-->
    <script src="/vendors/lodash/dist/lodash.min.js"></script>
    <script src="/vendors/angular-google-maps/dist/angular-google-maps.min.js"></script>
    <script src='//maps.googleapis.com/maps/api/js?sensor=false&libraries=places'></script>
    <script src="/vendors/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.js"></script>
    <script src="/fe/scripts/app.js"></script>
    <script src="/fe/scripts/controllers/main.js"></script>
    <script src="/fe/scripts/factory/notification.js"></script>
    <script src="/fe/scripts/factory/login.js"></script>
    <script src="/fe/scripts/factory/factory.js"></script>
    <script src="/fe/scripts/config.js"></script>
    <?php 
    if(isset($otherjvascript)){
    echo $otherjvascript;
}
?>
<script src="/fe/scripts/directives/showErrors.js"></script>
<script src="/fe/scripts/services/piservices.js"></script>
<script src="/fe/scripts/filter.js"></script>

<!-- Fancybox -->
<script type="text/javascript" src="/vendors/fancyBox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="/vendors/fancyBox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<!-- SWIPER -->
<script src="/vendors/Swiper/dist/js/swiper.min.js"></script>

<!-- FB PIXELS -->
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>
