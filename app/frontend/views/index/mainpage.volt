<script type="text/ng-template" id="otherCategories.html">
    <div ng-include="'/fe/tpl/otherCategories.html'"></div>
</script>
<!-- banner -->
    <div class="banner">
        <div class="container">
            <p class="text-center"><strong>One</strong> person can help <strong>everyone</strong></p>
            <h1 class="text-center">Where&#39s your next mission?</h1>
            <button  ui-sref="mapstart" class="banner-btn center-element">Map out your mission</button>
        </div>
    </div>
    <!-- banner -->

    <!-- content -->
    <div class="content" ng-init="loadindex()">
        <div class="container">
             <!-- main content -->
            <div class="main-content">

                <!-- featured missions -->
                <div class="mission-group">
                    <div class="content-header">
                        <p><img src="/img/frontend/featuredicon.png" alt=""> <strong>Featured</strong> Missions</p>
                    </div>
                    <div class="missions-container">
                        <div class="two-column left">
                            <div class="mission">
                                <a href="/action-map/{[{ mainfeature.mapslugs }]}" class="no-hover">
                                <div class="map" style="background: url('{[{ bucketUrl }]}uploads/maps/{[{ mainfeature.mapid }]}/{[{ mainfeature.cover }]}')">
                                </div>
                                <div class="mission-details">
                                    <img ng-if="mainfeature.profile_pic_name && mainfeature.hide_agent=='1'" src='{[{ bucketUrl }]}uploads/agentpic/{[{ mainfeature.agentid }]}/{[{ mainfeature.profile_pic_name }]}' class="agent-pic">
                                    <img ng-if="!mainfeature.profile_pic_name || mainfeature.hide_agent=='0'" src='/img/user.png' class="agent-pic">
                                    <p ng-if="mainfeature.status==1" class="agent-name"><span ng-bind="mainfeature.first_name"></span> <span ng-bind="mainfeature.last_name"></span></p>
                                    <p ng-if="mainfeature.status==0" class="agent-name"><span>Anonymous Agent</span></p>
                                    <p class="mission-desc"><span ng-bind="mainfeature.title | limitTo:33"></span></p>
                                    <div class="btn-container">
                                        <p class="actions"><span ng-bind="mainfeature.actions"></span> <i class="fa fa-map-marker"></i></p>
                                        <div class="pull-right">
                                            <a href="#" class=""><i class="fa fa-eye"></i> {[{ mainfeature.views }]}</a>
                                            <a href="#" class=""><i class="fa fa-comment"></i> {[{ addComments(mainfeature.countComments, mainfeature.countRepComments)}]}</a>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="two-column right">
                            <div class="mission" ng-repeat="ff in featured">
                                <a href="/action-map/{[{ ff.mapslugs }]}" class="no-hover">
                                    <!-- background-image: url('http://img.youtube.com/vi/NIPXpNBJ4G0/hqdefault.jpg ')-->
                                    <div ng-if="ff.coverType=='image'" class="map" style="background: url('{[{ bucketUrl }]}uploads/maps/{[{ ff.mapid }]}/{[{ ff.cover }]}')">
                                    </div>
                                    <div ng-if="ff.coverType=='video'" class="map" style="background: url('{[{ ff.cover | returnYoutubeThumb }]}')">
                                    </div>
                                    <div ng-if="ff.coverType!='image' && ff.coverType!='video'" class="map" >
                                    </div>
                                    <div class="mission-details">
                                        <p class="mission-desc"><span ng-bind="ff.title | limitTo:17"></span></p>
                                        <p ng-if="ff.status==1" class="agent-name"><span ng-bind="ff.first_name"></span> <span ng-bind="ff.last_name"></span></p>
                                        <p ng-if="ff.status==0" class="agent-name"><span>Anonymous Agent</span></p>
                                        <div class="btn-container">
                                            <a href="/action-map/{[{ ff.mapslugs }]}" class="pull-left no-margin">
                                                <p class="actions pull-left"><span ng-bind="mainfeature.actions"></span> <i class="fa fa-map-marker"></i></p>
                                            </a>
                                            <a href="#" class=""><i class="fa fa-eye"></i> {[{ mainfeature.views }]}</a>
                                            <a href="#" class=""><i class="fa fa-comment"></i> {[{ addComments(mainfeature.countComments, mainfeature.countRepComments)}]}</a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="pull-left full-width">
                        <a href="" class="seemore" ng-click="seeMore('FEATURED')">see more <i class="fa fa-play"></i></a>
                    </div>
                </div>
                <!-- featured missions -->


                <!-- latest missions -->
                <div class="mission-group">
                    <div class="content-header">
                        <p><img src="/img/frontend/latesticon.png" alt=""> <strong>Latest</strong> Missions</p>
                    </div>
                    <div class="missions-container">
                        <div class="three-column" ng-repeat="ll in latest">
                            <a href="/action-map/{[{ ll.mapslugs }]}" class="no-hover">
                            <div class="mission" >
                                <div ng-if="ll.coverType=='image'" class="map" style="background: url('{[{ bucketUrl }]}uploads/maps/{[{ ll.mapid }]}/{[{ ll.cover }]}')">
                                </div>
                                <div ng-if="ll.coverType=='video'" class="map" style="background: url('{[{ ll.cover | returnYoutubeThumb }]}')">
                                </div>
                                <div ng-if="ll.coverType!='image' && ll.coverType!='video'" class="map" >
                                </div>
                                <div class="mission-details">
                                    <img ng-if="ll.profile_pic_name && ll.hide_agent=='1'" src='{[{ bucketUrl }]}uploads/agentpic/{[{ ll.agentid }]}/{[{ ll.profile_pic_name }]}' class="agent-pic">
                                    <img ng-if="!ll.profile_pic_name || ll.hide_agent=='0'" src='/img/user.png' class="agent-pic">
                                    <p ng-if="ll.status==1" class="agent-name"><span ng-bind="ll.first_name"></span> <span ng-bind="ll.last_name"></span></p>
                                    <p ng-if="ll.status==0" class="agent-name"><span>Anonymous Agent</span></p>
                                    <p class="mission-desc" ng-bind="ll.title | limitTo:31"></p>
                                    <div class="btn-container">
                                        <p class="actions" > <span ng-bind="ll.actions"></span> <i class="fa fa-map-marker"></i></p>
                                        <div class="pull-right">
                                            <a href="#" class=""><i class="fa fa-eye"></i> {[{ ll.views }]}</a>
                                            <a href="#" class=""><i class="fa fa-comment"></i> {[{ addComments(ll.countComments, ll.countRepComments)}]}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                </a>
                        </div>
                    </div>
                    <div class="pull-left full-width">
                        <a href="" class="seemore" ng-click="seeMore('LATEST')">see more <i class="fa fa-play"></i></a>
                    </div>
                </div>
                <!-- latest missions -->
                
                <!-- greatest missions -->
                <div class="mission-group">
                    <div class="content-header">
                        <p><img src="/img/frontend/greatesticon.png" alt=""> <strong>Greatest</strong> Missions</p>
                    </div>
                    <div class="missions-container">
                        <div class="three-column" ng-repeat="gg in greatest">
                            <a href="/action-map/{[{ gg.mapslugs }]}" class="no-hover">
                                <div class="mission" >
                                    <div ng-if="gg.coverType=='image'" class="map" style="background: url('{[{ bucketUrl }]}uploads/maps/{[{ gg.mapid }]}/{[{ gg.cover }]}')">
                                    </div>
                                    <div ng-if="gg.coverType=='video'" class="map" style="background: url('{[{ gg.cover | returnYoutubeThumb }]}')">
                                    </div>
                                    <div ng-if="gg.coverType!='image' && gg.coverType!='video'" class="map" >
                                    </div>
                                    <div class="mission-details">
                                        <img ng-if="gg.profile_pic_name && gg.hide_agent=='1'" src='{[{ bucketUrl }]}uploads/agentpic/{[{ gg.agentid }]}/{[{ gg.profile_pic_name }]}' class="agent-pic">
                                        <img ng-if="!gg.profile_pic_name || gg.hide_agent=='0'" src='/img/user.png' class="agent-pic">
                                        <p ng-if="gg.status==1" class="agent-name"><span ng-bind="gg.first_name"></span> <span ng-bind="gg.last_name"></span></p>
                                        <p ng-if="gg.status==0" class="agent-name"><span>Anonymous Agent</span></p>
                                        <p class="mission-desc" ng-bind="gg.title | limitTo:31"></p>
                                        <div class="btn-container">
                                            <p class="actions" > <span ng-bind="gg.actions"></span> <i class="fa fa-map-marker"></i></p>
                                            <div class="pull-right">
                                                <a href="#" class=""><i class="fa fa-eye"></i> {[{ gg.views }]}</a>
                                                <a href="#" class=""><i class="fa fa-comment"></i> {[{ addComments(gg.countComments, gg.countRepComments)}]}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="pull-left full-width">
                        <a href="" class="seemore" ng-click="seeMore('GREATEST')">see more <i class="fa fa-play"></i></a>
                    </div>
                </div>
                <!-- greatest missions -->

                <!-- popular missions -->
                <div class="mission-group">
                    <div class="content-header">
                        <p><img src="/img/frontend/popularicon.png" alt=""> <strong>Popular</strong> Missions</p>
                    </div>
                    <div class="missions-container">
                        <div class="three-column" ng-repeat="pp in popular">
                            <a href="/action-map/{[{ pp.mapslugs }]}" class="no-hover">
                                <div class="mission" >
                                    <div ng-if="pp.coverType=='image'" class="map" style="background: url('{[{ bucketUrl }]}uploads/maps/{[{ pp.mapid }]}/{[{ pp.cover }]}')">
                                    </div>
                                    <div ng-if="pp.coverType=='video'" class="map" style="background: url('{[{ pp.cover | returnYoutubeThumb }]}')">
                                    </div>
                                    <div ng-if="pp.coverType!='image' && pp.coverType!='video'" class="map" >
                                    </div>
                                    <div class="mission-details">
                                        <img ng-if="pp.profile_pic_name && pp.hide_agent=='1'" src='{[{ bucketUrl }]}uploads/agentpic/{[{ pp.agentid }]}/{[{ pp.profile_pic_name }]}' class="agent-pic">
                                        <img ng-if="!pp.profile_pic_name || pp.hide_agent=='0'" src='/img/user.png' class="agent-pic">
                                        <p ng-if="pp.status==1" class="agent-name"><span ng-bind="pp.first_name"></span> <span ng-bind="pp.last_name"></span></p>
                                        <p ng-if="pp.status==0" class="agent-name"><span>Anonymous Agent</span></p>
                                        <p class="mission-desc" ng-bind="pp.title | limitTo:31"></p>
                                        <div class="btn-container">
                                            <p class="actions" > <span ng-bind="pp.actions"></span> <i class="fa fa-map-marker"></i></p>
                                            <div class="pull-right">
                                                <a href="#" class=""><i class="fa fa-eye"></i> {[{ pp.views }]}</a>
                                                <a href="#" class=""><i class="fa fa-comment"></i> {[{ addComments(pp.countComments, pp.countRepComments)}]}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="pull-left full-width">
                        <a href="" class="seemore" ng-click="seeMore('POPULAR')">see more <i class="fa fa-play"></i></a>
                    </div>
                </div>
                <!-- popular missions -->

                <!-- recommended missions
                <div class="mission-group">
                    <div class="content-header">
                        <p><img src="/img/frontend/recommendedicon.png" alt=""> <strong>Recommended</strong> Missions</p>
                    </div>
                    <div class="missions-container">
                        <div class="three-column">
                            <div class="mission">
                                <div class="map">
                                </div>
                                <div class="mission-details">
                                    <img src="/img/frontend/von.png" alt="" class="agent-pic">
                                    <p class="agent-name">John Jeevon Ang</p>
                                    <p class="mission-desc">SHOW AND THROW A BAG</p>
                                    <div class="btn-container">
                                        <p class="actions">12, 090 Actions</p>
                                        <div class="pull-right">
                                            <a href="#"><i class="fa fa-heart fa-lg"></i></a>
                                            <a href="#"><i class="fa fa-share fa-lg"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="three-column">
                            <div class="mission">
                                <div class="map">
                                </div>
                                <div class="mission-details">
                                    <img src="/img/frontend/von.png" alt="" class="agent-pic">
                                    <p class="agent-name">John Jeevon Ang</p>
                                    <p class="mission-desc">SHOW AND THROW A BAG</p>
                                    <div class="btn-container">
                                        <p class="actions">12, 090 Actions</p>
                                        <div class="pull-right">
                                            <a href="#"><i class="fa fa-heart fa-lg"></i></a>
                                            <a href="#"><i class="fa fa-share fa-lg"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="three-column">
                            <div class="mission">
                                <div class="map">
                                </div>
                                <div class="mission-details">
                                    <img src="/img/frontend/von.png" alt="" class="agent-pic">
                                    <p class="agent-name">John Jeevon Ang</p>
                                    <p class="mission-desc">SHOW AND THROW A BAG</p>
                                    <div class="btn-container">
                                        <p class="actions">12, 090 Actions</p>
                                        <div class="pull-right">
                                            <a href="#"><i class="fa fa-heart fa-lg"></i></a>
                                            <a href="#"><i class="fa fa-share fa-lg"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="seemore">see more <i class="fa fa-play"></i></a>
                </div>
                <!-- recommended missions -->
            </div>   
            <!-- main content -->

            <!-- sidebar -->
            <div class="sidebar">
                <div class="panel">
                    <div class="panel-header">
                        <img src="/img/frontend/von.png" alt="">
                        <p>Hello Agent!</p>
                        <p class="agent-alias">Captain Planet</p>
                    </div>
                    <div class="panel-content">
                        <div class="list">
                            <div class="date">
                                <strong>6</strong>11
                            </div>
                            <p>&#34;Well, call me Captain Combustion, a spontaneous kinda guy.&#34;</p>
                        </div>
                        <div class="list">
                            <div class="date">
                                <strong>6</strong>11
                            </div>
                            <p>&#34;Well, call me Captain Combustion, a spontaneous kinda guy.&#34;</p>
                        </div>
                        <div class="list">
                            <div class="date">
                                <strong>6</strong>11
                            </div>
                            <p>&#34;Well, call me Captain Combustion, a spontaneous kinda guy.&#34;</p>
                        </div>
                        <a href="#" class="viewmore">view more</a>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-header">
                        <p class="text-center category">Top Categoriess</p>
                    </div>
                    <div class="panel-content categories">
                        <div ng-repeat="cc in categories"><p>&#34; <a href="/category/{[{ cc.catslugs }]}"> {[{ cc.cattitle }]} </a> &#34;</p></div>
                        <a href="#" class="viewmore" ng-click="seeMoreCategories()">view more</a>
                    </div>
                </div>
                <a href="#">
                    <img src="/img/frontend/contactus.png" alt="">
                </a>
            </div>
            <!-- sidebar -->
        </div>
    </div>
    <!-- content -->